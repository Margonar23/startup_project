<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'News',
    'header_create' => 'Creazione di una nuova news',
    'header_edit' => 'Modifica news',
    'empty' => 'Non ci sono ancora news... creane una',
    'tb_title' => 'Titolo',
    'tb_subtitle' => 'Sottotitolo',
    'tb_header_image' => 'Immagine di copertina',
    'tb_user' => 'Creato da',
    'tb_updated_at' => 'ultima modifica',
    'tb_description' => 'Descrizione',
    'delete' => ':news eliminata correttamente!',
];
