<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Status',
    'header_create' => 'Creazione di un nuovo status',
    'header_edit' => 'Modifica status',
    'empty' => 'Non ci sono ancora status... creane uno',
    'tb_name' => 'Status',
    'tb_label' => 'Label',
    'tb_active' => 'Active',
    'tb_icon' => 'Icon',
    'tb_icon_color' => 'Color',
    'delete' => ':company_status eliminato correttamente!',
];
