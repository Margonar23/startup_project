<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Aziende',
    'header_create' => 'Creazione di una nuova categoria',
    'header_edit' => 'Modifica categoria',
    'empty' => 'Non ci sono ancora categorie... creane una',
    'tb_name' => 'Categoria',
    'delete' => ':company_category eliminata correttamente!',
];
