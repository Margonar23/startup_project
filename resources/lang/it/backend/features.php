<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Funzionalità',
    'header_create' => 'Creazione di una nuova funzionalità',
    'header_edit' => 'Modifica funzionalità',
    'empty' => 'Non ci sono ancora funzionalità... creane una',
    'tb_name' => 'Funzionalità',
    'tb_label' => 'Label',
    'tb_active' => 'Disponibile',
    'tb_description' => 'Descrizione',
    'delete' => ':feature eliminata correttamente!',
];
