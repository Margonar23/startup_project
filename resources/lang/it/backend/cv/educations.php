<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Formazione',
    'header_create' => 'Creazione di una nuova formazione',
    'header_edit' => 'Modifica formazione',
    'empty' => 'Non ci sono ancora formazioni... creane una',
    'tb_certificate' => 'Certificato',
    'tb_school' => 'Scuola',
    'tb_school_link' => 'Link',
    'tb_period' => 'Periodo/data',
    'delete' => ':education eliminato correttamente!',
];
