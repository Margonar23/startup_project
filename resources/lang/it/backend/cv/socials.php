<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Socials',
    'header_create' => 'Creazione di un nuovo social',
    'header_edit' => 'Modifica social',
    'empty' => 'Non ci sono ancora social... creane uno',
    'tb_link' => 'Link',
    'tb_icon_bg_color' => 'Colore sfondo',
    'tb_icon_tx_color' => 'Colore testo',
    'tb_icon' => 'Icona',
    'delete' => ':social eliminata correttamente!',
];
