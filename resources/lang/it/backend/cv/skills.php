<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Skills',
    'header_create' => 'Creazione di una nuova skill',
    'header_edit' => 'Modifica skill',
    'empty' => 'Non ci sono ancora skill... creane una',
    'tb_name' => 'Nome',
    'tb_value' => 'Valore',
    'delete' => ':skill eliminata correttamente!',
];
