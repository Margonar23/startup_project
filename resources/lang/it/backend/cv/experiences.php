<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Esperienza',
    'header_create' => 'Creazione di una nuova esperienza',
    'header_edit' => 'Modifica esperienza',
    'empty' => 'Non ci sono ancora esperienze... creane una',
    'tb_job_function' => 'Funzione',
    'tb_company' => 'Azienda',
    'tb_company_link' => 'Link',
    'tb_description' => 'Descrizione del ruolo',
    'tb_period' => 'Periodo/data',
    'delete' => ':experience eliminato correttamente!',
];
