<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Lingue',
    'header_create' => 'Creazione di una nuova lingua',
    'header_edit' => 'Modifica lingua',
    'empty' => 'Non ci sono ancora lingue... creane una',
    'tb_language' => 'Lingua',
    'tb_knowledge' => 'Livello',
    'tb_certificate' => 'Certificato',
    'delete' => ':language eliminata correttamente!',
];
