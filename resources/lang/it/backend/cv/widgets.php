<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Widget',
    'header_create' => 'Creazione di un nuovo widget',
    'header_edit' => 'Modifica widget',
    'empty' => 'Non ci sono ancora widget... creane uno',
    'tb_icon_bg_color' => 'Colore di sfondo',
    'tb_icon_tx_color' => 'Colore dell\'icona',
    'tb_icon' => 'Icona',
    'tb_title' => 'Titolo',
    'tb_text' => 'Testo',
    'delete' => ':widget eliminato correttamente!',
];
