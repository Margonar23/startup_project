<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Ruoli',
    'header_create' => 'Creazione di un nuovo ruolo',
    'header_edit' => 'Modifica ruolo',
    'empty' => 'Non ci sono ancora ruoli... creane uno',
    'tb_text' => 'Ruolo',
    'tb_tx_color' => 'Livello',
    'delete' => ':role eliminato correttamente!',
];
