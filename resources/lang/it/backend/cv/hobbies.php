<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Hobby',
    'header_create' => 'Creazione di un nuovo hobby',
    'header_edit' => 'Modifica hobby',
    'empty' => 'Non ci sono ancora hobby... creane uno',
    'tb_description' => 'Descrizione',
    'delete' => ':hobby eliminato correttamente!',
];
