<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Descrizione',
    'header_create' => 'Creazione di una nuova descrizione',
    'header_edit' => 'Modifica descrizione',
    'empty' => 'Non ci sono ancora descrizioni... creane una',
    'tb_description' => 'Descrizione',
    'delete' => ':description eliminata correttamente!',
];
