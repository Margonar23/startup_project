<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Certificati',
    'header_create' => 'Creazione di un nuovo certificato',
    'header_edit' => 'Modifica certificato',
    'empty' => 'Non ci sono ancora certificati... creane uno',
    'tb_certificate' => 'Certificato',
    'tb_company' => 'Azienda',
    'tb_company_link' => 'Link',
    'tb_period' => 'Periodo/data',
    'delete' => ':certificate eliminato correttamente!',
];
