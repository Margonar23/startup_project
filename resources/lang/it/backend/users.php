<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Utenti',
    'header_create' => 'Creazione di un nuovo utente',
    'header_edit' => 'Modifica utente',
    'empty' => 'Non ci sono ancora utenti... creane uno',
    'tb_name' => 'Nome',
    'tb_firstname' => 'Nome',
    'tb_lastname' => 'Cognome',
    'tb_role' => 'Ruolo',
    'tb_email' => 'E-Mail',
    'delete' => ':user eliminato correttamente!',
    'role' => 'Scegli un ruolo',
    'account' => 'Profilo',
    'account_edit' => 'Modifica profilo',
    'account_update' => 'Profilo modificato correttamente',
    'avatar_not_set' => 'Avatar non ancora inserito! Trascina qui sotto un immagine per caricare la tua immagine profilo.',
    'overview' => 'Overview',
    'credential' => 'Credenziali',
    'avatar' => 'Avatar',
    'password' => 'Password',
    'password_confirmation' => 'Conferma password',
    'avatar_dropzone' => 'Trascina o clicca per caricare un avatar',
    'new_user_notification' => 'Yeah, nuovo utente!',
];
