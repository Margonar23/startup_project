<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Pacchetti',
    'header_create' => 'Creazione di un nuovo pacchetto',
    'header_edit' => 'Modifica pacchetto',
    'empty' => 'Non ci sono ancora pacchetti... creane uno',
    'tb_name' => 'Nome',
    'tb_price' => 'Prezzo mensile',
    'tb_price_year' => 'Prezzo annuale',
    'tb_color' => 'Colore',
    'tb_active' => 'Disponibile',
    'tb_features' => 'Funzionalità',
    'tb_description' => 'Descrizione',
    'delete' => ':package eliminato correttamente!',
];
