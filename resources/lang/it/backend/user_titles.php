<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Titolo utenti',
    'header_create' => 'Creazione di una nuovo titolo',
    'header_edit' => 'Modifica titolo',
    'empty' => 'Non ci sono ancora titoli... creane uno',
    'tb_name' => 'Titolo',
    'tb_label' => 'Label',
    'delete' => ':user_title eliminato correttamente!',
];
