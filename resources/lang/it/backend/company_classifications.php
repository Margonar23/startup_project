<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Classificazione',
    'header_create' => 'Creazione di una nuova classificazione',
    'header_edit' => 'Modifica classificazione',
    'empty' => 'Non ci sono ancora classificazioni... creane una',
    'tb_name' => 'Classificazione',
    'tb_description' => 'Descrizione',
    'delete' => ':company_classification eliminata correttamente!',
];
