<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'create_new_user_title' => 'Sei un utente?',
    'create_new_user_desc' => 'Vivi in Ticino e vuoi entrare a far parte della community di tigit? Crea il tuo profilo gratuitamente e goditi le opportunità di Tigit',
    'create_new_user_btn' => 'Crea utente',

    'create_new_company_title' => 'Rappresenti o sei un dipendente di un\'azienda?',
    'create_new_company_desc' => 'Qui puoi creare una nuova azienda o se possiedi il codice azienda potrai aderire ad una già esistente!',
    'create_new_company_btn' => 'Inizia da qui',

    'create_account' => 'Crea il tuo account!',
    'create_account_btn' => 'Crea account',
    'already_have_login' => 'Hai già un account? Vai al login',
    'first_name' => 'Nome',
    'last_name' => 'Cognome',
    'email' => 'E-Mail',
    'password' => 'Password',
    'password_confirmation' => 'Conferma password',

    'create_company_step_1' => 'Comincia creando il tuo profilo!'

];
