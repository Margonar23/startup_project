<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login_title' => 'Effettua l\'accesso!',
    'are_you_new' => 'Sei nuovo?',
    'create_an_account' => 'Crea un account!',
];
