<?php

return [
    'profile' => 'Profilo',
    'profile_sub' => 'Modifica il tuo profilo',
    'back_to_home_page' => 'Torna alla Home',
    'cv' => 'Curriculum Vitae',
];
