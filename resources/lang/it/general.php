<?php

return [

    'new' => 'Nuovo',
    'back' => 'Indietro',
    'save' => 'Salva',
    'update' => 'Aggiorna',
    'tb_actions' => 'Azioni',

];
