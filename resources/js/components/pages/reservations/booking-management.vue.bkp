<template>
    <div class="row" v-if="company.restaurants.length">
        <div class="col-md-12" v-if="loading">
            <div class="text-center">
                <i class="fa fa-spinner fa-spin fa-4x"></i>
            </div>
        </div>
        <div class="col-md-12" v-else>
            <div class="row">
                <div class="col-md-4 d-inline">
                    <select v-model="selectedRestaurant" class="form-control form-control-sm"
                            @change="setRestaurant()">
                        <option v-for="restaurant in company.restaurants" v-bind:value="restaurant.id">
                            {{ restaurant.name }}
                        </option>
                    </select>
                </div>
                <div class="col-md-4 d-inline">
                    <date-picker type="date" v-model="bookingDate" @change="changeBookingDate" valueType="format" format="DD-MM-YYYY"></date-picker>
                </div>
                <div class="col-md-4 d-inline">adf</div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12 d-inline">
                    <vue-slider v-model="bookingHours"

                                :adsorb="true"
                                :data="hours"
                                :marks="true">

                    </vue-slider>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-4">
                    <div class="card">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li v-for="stat in status" class="nav-item" :role="stat.label"
                                @click.stop.prevent="set_status(stat.id)">
                                <a class="nav-link" :class="{'active show':stat.id === selectedStatus}" :id="stat.label"
                                   data-toggle="tab" :href="'#'+stat.label"
                                   role="tab" :aria-controls="stat.id" aria-selected="true">
                                    <i class="fa" :class="'fa-'+stat.icon+' '+stat.color"></i> {{ stat.name }}
                                    <span class="badge badge-primary-soft">XX</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade active show">
                                <div class="row card-body" v-if="bookingDataLoading">
                                    <div class="col-md-12  text-center">
                                        <i class="fa fa-4x fa-spin fa-spinner"></i>
                                    </div>
                                </div>
                                <div class="row card-body" v-else>
                                    <div class="col-md-12 border p-1 mb-1" v-for="booking in bookingData">
                                        <span class="font-weight-bold">{{ booking.firstname }} {{ booking.lastname }}
                                            <div class="float-right">
                                                <i class="fa fa-info-circle"></i>
                                            </div>
                                        </span>
                                        <table class="table table-responsive-sm mb-0">
                                            <tr>
                                                <td><i class="fa fa-users"></i> {{ booking.people_nr }}</td>
                                                <td><i class="fa fa-clock"></i> {{ booking.booking_time }}</td>
                                                <td><i class="fa fa-calendar-day"></i> {{ booking.booking_date }}</td>
                                                <td>
                                                    <i class="fa fa-chess-rook"></i>
                                                    <span v-if="booking.room">{{ booking.room.name }}</span>
                                                    <span v-else>-</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <small>
                                            {{ booking.message }}
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card mb-4">
                        <ul class="nav nav-tabs" id="room_nav" role="room_content">
                            <li v-for="room in selectedRestaurantObj.rooms" class="nav-item" :role="room.name"
                                @click.stop.prevent="set_room(room.id)">
                                <a class="nav-link" :class="{'active show':room.id === selectedRoom}" :id="room.name"
                                   data-toggle="tab" :href="'#'+room.name"
                                   role="tab" :aria-controls="room.id" aria-selected="true">
                                    {{ room.name }}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content bg-white" id="room_content">
                        <div class="tab-pane fade active show">
                            <div class="row card-body" v-if="roomLoading">
                                <div class="col-md-12  text-center">
                                    <i class="fa fa-4x fa-spin fa-spinner"></i>
                                </div>
                            </div>
                            <div class="row card-body" v-else>
                                <div class="col-md-12 ">
                                    <div class="row">
                                        <div class="col-md-2 border ml-4 p-5 mb-5" :class="'shape_'+table.shape.name"
                                             v-for="table in selectedRoomObj.tables">
                                            {{ table.name }}<br>
                                            <i class="fa fa-users"></i>
                                            {{ table.max_capacity }}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div v-else>
        <div class="alert alert-danger">Non hai ancora dei ristoranti attivi</div>
    </div>
</template>

<script>
import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';
import VueSlider from 'vue-slider-component'
import 'vue-slider-component/theme/default.css'
import moment from 'moment'
export default {
    name: "booking-management",
    prop: ['companyId'],
    components: {DatePicker, VueSlider, moment},
    data() {
        return {
            loading: true,
            bookingDataLoading: true,
            roomLoading: true,
            company: null,
            company_id: null,
            bookingDate: moment().format('DD-MM-Y'),
            bookingHours: moment().format('H:s'),
            selectedRestaurant: null,
            selectedRestaurantObj: null,
            status: null,
            hours: null,
            selectedStatus: null,
            selectedRoom: null,
            selectedRoomObj: null,
            bookingData: null,
        }
    },
    methods: {
        changeBookingDate() {
            this.get_hours(this.selectedRestaurant, this.bookingDate)
        },
        set_status(status_id) {
            this.selectedStatus = status_id
            // get booking data for status
            this.get_bookings()
        },
        set_room(room_id) {
            this.get_room(room_id)
        },
        setRestaurant() {
            axios
                .post('/api/v1/get_restaurant/' + this.selectedRestaurant)
                .then(response => {
                    this.selectedRestaurantObj = response.data;
                    this.selectedRoom = this.selectedRestaurantObj.rooms[0].id
                    this.selectedRoomObj = this.get_room(this.selectedRoom)
                    this.get_bookings()
                    this.get_hours(this.selectedRestaurant, this.bookingDate)
                })
                .catch(error => {
                    console.log(error);
                })
                .finally(() => this.bookingDataLoading = false)

        },
        get_room(room_id) {
            this.roomLoading = true
            axios
                .post('/api/v1/get_room/' + room_id)
                .then(response => {
                    this.selectedRoom = room_id
                    this.selectedRoomObj = response.data;
                })
                .catch(error => {
                    console.log(error);
                })
                .finally(() => this.roomLoading = false)
        },
        get_bookings() {
            this.bookingDataLoading = true
            axios
                .post('/api/v1/get_bookings/' + this.selectedStatus + '/' + this.selectedRestaurant)
                .then(response => {
                    this.bookingData = response.data;
                })
                .catch(error => {
                    console.log(error);
                })
                .finally(() => this.bookingDataLoading = false)
        },
        get_status() {
            axios
                .post('/api/v1/get_reservation_status')
                .then(response => {
                    this.status = response.data;
                    this.set_status(this.status[0].id)
                })
                .catch(error => {
                    console.log(error);
                })
                .finally(() => this.loading = false)
        },
        get_hours(restaurant, date) {
            axios
                .post('/api/v1/get_hours/'+restaurant+'/'+date)
                .then(response => {
                    this.hours = Object.values(response.data);
                })
                .catch(error => {
                    console.log(error);
                })
                .finally(() => this.loading = false)
        }
    },
    created() {
        this.company_id = this.$attrs.companyid;
    },
    mounted() {
        axios
            .post('/api/v1/company/' + this.company_id)
            .then(response => {
                this.company = response.data;
                this.selectedRestaurant = this.company.restaurants[0].id
                this.selectedRestaurantObj = this.setRestaurant(this.selectedRestaurant)

            })
            .catch(error => {
                console.log(error);
            })
            .finally(() => this.loading = false)
        this.get_status()
    }
}
</script>

<style scoped>
.shape_rectangle {
    text-align: center;
    border-radius: 0;
}

.shape_rounded {
    text-align: center;
    border-radius: 50%;
}
</style>
