/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import moment from 'moment';
import VModal from 'vue-js-modal/dist/index.nocss.js'
import 'vue-js-modal/dist/styles.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Tooltip from 'vue-directive-tooltip';
import 'vue-directive-tooltip/dist/vueDirectiveTooltip.css';
import VueToast from 'vue-toast-notification';
import StarRating from 'vue-star-rating';
// Import one of available themes
import 'vue-toast-notification/dist/theme-default.css';
import 'vue-toast-notification/dist/theme-sugar.css';



Vue.use(Tooltip);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueToast);
Vue.use(StarRating)
Vue.use(VModal)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
// const files = require.context('./', true, /\.vue$/i)

Vue.component('companies', require('./components/pages/companies/companies.vue').default);
Vue.component('coupon-companies', require('./components/pages/companies/coupon-companies.vue').default);
Vue.component('company-detail', require('./components/pages/companies/company-detail.vue').default);
Vue.component('events', require('./components/pages/events/events.vue').default);
Vue.component('event-detail', require('./components/pages/events/event-detail.vue').default);
Vue.component('modal', require('./components/pages/widget/modal.vue').default);
Vue.component('profession', require('./components/pages/cv/profession.vue').default);
Vue.component('employment', require('./components/pages/cv/employment.vue').default);
Vue.component('permit', require('./components/pages/cv/permit.vue').default);
Vue.component('percentage', require('./components/pages/cv/percentage.vue').default);
Vue.component('skills', require('./components/pages/cv/skills.vue').default);
Vue.component('license', require('./components/pages/cv/license.vue').default);
Vue.component('language', require('./components/pages/cv/language.vue').default);
Vue.component('certificate', require('./components/pages/cv/certificate.vue').default);
Vue.component('diploma', require('./components/pages/cv/diploma.vue').default);
Vue.component('job-experience', require('./components/pages/cv/job-experience.vue').default);
Vue.component('hobby', require('./components/pages/cv/hobby.vue').default);
Vue.component('search-work', require('./components/pages/cv/search-work.vue').default);
Vue.component('cv-pdf', require('./components/pages/cv/cv_pdf.vue').default);
Vue.component('reservations', require('./components/pages/reservations/reservations.vue').default);
Vue.component('booking-management', require('./components/pages/reservations/booking-management.vue').default);
Vue.component('presentation', require('./components/pages/presentation/presentation.vue').default);
Vue.component('inbox', require('./components/pages/inbox/inbox.vue').default);
Vue.component('inbox-backend', require('./components/pages/inbox/inbox-backend.vue').default);
Vue.component('edit-company', require('./components/companies/edit_company.vue').default);
Vue.component('edit-logo', require('./components/companies/edit_logo.vue').default);
Vue.component('add-logo', require('./components/companies/add_logo.vue').default);



Vue.component('t1', require('./components/pages/presentation/templetes/t1.vue').default);
Vue.component('t2', require('./components/pages/presentation/templetes/t2.vue').default);
Vue.component('t3', require('./components/pages/presentation/templetes/t3.vue').default);
Vue.component('t4', require('./components/pages/presentation/templetes/t4.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
