<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>

    <link href="{{ asset('vendor/bootstrap-4.4.1-dist/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/fontawesome-free-5.13.0-web/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/coming-soon.css') }}" rel="stylesheet">
</head>
<body>

<div class="overlay"></div>
<video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="{{ asset('mp4/bg.mp4') }}" type="video/mp4">
</video>

<div class="masthead">
    <div class="masthead-bg"></div>
    <div class="container h-100">
        <div class="row h-100">
            <div class="col-12 my-auto">
                <div class="masthead-content text-white py-5 py-md-0">
                    <h1 class="mb-3">Coming Soon!</h1>
                    <p class="mb-5">We're working hard to finish the development of this site.</p>
                    <p class="mb-5">
                        <a href="{{ route('users.login') }}" >Login</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="social-icons">
    <ul class="list-unstyled text-center mb-0">
        <li class="list-unstyled-item">
            <a href="#">
                <i class="fab fa-twitter"></i>
            </a>
        </li>
        <li class="list-unstyled-item">
            <a href="#">
                <i class="fab fa-facebook-f"></i>
            </a>
        </li>
        <li class="list-unstyled-item">
            <a href="#">
                <i class="fab fa-instagram"></i>
            </a>
        </li>
    </ul>
</div>

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('vendor/jquery-3.4.1/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-4.4.1-dist/js/bootstrap.bundle.js') }}"></script>

<!-- Custom scripts for this template -->
<script src="{{ asset('js/coming-soon.js' )}}"></script>


</body>
</html>
