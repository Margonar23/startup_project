@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($company_status))
                        <span>{{ trans('backend/company_statuses.header_edit') }}</span>
                    @else
                        <span>{{ trans('backend/company_statuses.header_create') }}</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($company_status))
                    <span>{{ trans('backend/company_statuses.header_edit') }}</span>
                @else
                    <span>{{ trans('backend/company_statuses.header_create') }}</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('company_statuses.index') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($company_status))
                    <form action="{{ route('company_statuses.update', ['company_status' => $company_status->id]) }}" method="post">
                        @method('PUT')
                @else
                    <form action="{{ route('company_statuses.store') }}" method="post">
                @endif

                    @csrf
                    <div class="form-group">
                        <label for="role_name">{{ trans('backend/company_statuses.tb_name') }}</label>
                        <input class="form-control @error('name') is-invalid @enderror" name="name" type="text"
                               value="{{ isset($company_status) ? $company_status->name : old('name') }}"/>
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox custom-control-solid">
                                @if(isset($company_status) && $company_status->active)
                                    <input class="custom-control-input" id="active" name="active" checked
                                           type="checkbox">
                                @else
                                    <input class="custom-control-input" id="active" name="active"
                                           type="checkbox">
                                @endif
                                <label class="custom-control-label"
                                       for="active">{{ trans('backend/company_statuses.tb_active') }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="role_name">{{ trans('backend/company_statuses.tb_icon') }}</label>
                            <input class="form-control @error('icon') is-invalid @enderror" name="icon" type="text"
                                   value="{{ isset($company_status) ? $company_status->icon : old('icon') }}"/>
                            @error('icon')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="role_name">{{ trans('backend/company_statuses.tb_icon_color') }}</label>
                            <input class="form-control @error('icon_color') is-invalid @enderror" name="icon_color" type="text"
                                   value="{{ isset($company_status) ? $company_status->icon_color : old('icon_color') }}"/>
                            @error('icon_color')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="{{ isset($company_status) ? trans('general.update') : trans('general.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
