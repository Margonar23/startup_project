@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>{{ trans('backend/company_statuses.header') }}</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                {{ trans('backend/company_statuses.header') }}
                <div class="dropdown">
                    <a href="{{ route('company_statuses.create') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                    <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>{{ trans('backend/company_statuses.tb_name') }}</th>
                            <th>{{ trans('backend/company_statuses.tb_label') }}</th>
                            <th>{{ trans('backend/company_statuses.tb_active') }}</th>
                            <th>{{ trans('backend/company_statuses.tb_icon') }}</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>{{ trans('backend/company_statuses.tb_name') }}</th>
                            <th>{{ trans('backend/company_statuses.tb_label') }}</th>
                            <th>{{ trans('backend/company_statuses.tb_active') }}</th>
                            <th>{{ trans('backend/company_statuses.tb_icon') }}</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($company_statuses as $status)
                            <tr>
                                <td>{{ $status->name }}</td>
                                <td>{{ $status->label }}</td>
                                <td><i class="fa fa-{{ $status->active ? 'check' : 'times' }}" </td>
                                <td><i class="fa fa-{{ $status->icon }} {{ $status->icon_color }}"></i></td>
                                <td>
                                    <a href="{{ route('company_statuses.edit', ['company_status' => $status->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault();
                                       document.getElementById('trash-form-{{ $status->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $status->id }}"
                                          action="{{ route('company_statuses.destroy', ['company_status' => $status->id]) }}" method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/company_statuses.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
