@extends('layouts.backend.backend')
@section('styles')
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($package))
                        <span>{{ trans('backend/packages.header_edit') }}</span>
                    @else
                        <span>{{ trans('backend/packages.header_create') }}</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($package))
                    <span>{{ trans('backend/packages.header_edit') }}</span>
                @else
                    <span>{{ trans('backend/packages.header_create') }}</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('packages.index') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($package))
                    <form action="{{ route('packages.update', ['package' => $package->id]) }}" method="post">
                        @method('PUT')
                        @else
                            <form action="{{ route('packages.store') }}" method="post">
                                @endif

                                @csrf
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/packages.tb_name') }}</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name"
                                           type="text"
                                           value="{{ isset($package) ? $package->name : old('name') }}"/>
                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/packages.tb_price') }}</label>
                                    <input class="form-control @error('price') is-invalid @enderror" name="price"
                                           type="number" step="0.01"
                                           value="{{ isset($package) ? $package->price : old('price') }}"/>
                                    @error('price')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/packages.tb_price_year') }}</label>
                                    <input class="form-control @error('price_year') is-invalid @enderror" name="price_year"
                                           type="number" step="0.01"
                                           value="{{ isset($package) ? $package->price_year : old('price_year') }}"/>
                                    @error('price_year')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('backend/packages.tb_features') }}</label><br>
                                    <select id="features" name="features[]" class="form-control" multiple>
                                        @foreach($features as $feature)
                                            <option value="{{ $feature->id }}"
                                                {{ isset($package) && $package->features->contains('id', $feature->id)
                                                ? 'selected'
                                                : '' }}>
                                                {{ $feature->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('features')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/packages.tb_description') }}</label>
                                    <textarea class="form-control @error('description') is-invalid @enderror"
                                              name="description">
                                        {{ isset($package) ? $package->description : old('description') }}
                                    </textarea>
                                    @error('description')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/packages.tb_color') }}</label>
                                    <input class="form-control @error('color') is-invalid @enderror" name="color"
                                           type="color"
                                           value="{{ isset($package) ? $package->color : old('color') }}"/>
                                    @error('color')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="custom-control custom-checkbox custom-control-solid">
                                        @if(isset($package) && $package->active)
                                            <input class="custom-control-input" id="active" name="active" checked
                                                   type="checkbox">
                                        @else
                                            <input class="custom-control-input" id="active" name="active"
                                                   type="checkbox">
                                        @endif
                                        <label class="custom-control-label"
                                               for="active">{{ trans('backend/packages.tb_active') }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($package) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
