@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>{{ trans('backend/packages.header') }}</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                {{ trans('backend/packages.header') }}
                <div class="dropdown">
                    <a href="{{ route('packages.create') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                    <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>{{ trans('backend/packages.tb_name') }}</th>
                            <th>{{ trans('backend/packages.tb_price') }}</th>
                            <th>{{ trans('backend/packages.tb_price_year') }}</th>
                            <th>{{ trans('backend/packages.tb_color') }}</th>
                            <th>{{ trans('backend/packages.tb_features') }}</th>
                            <th>{{ trans('backend/packages.tb_active') }}</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>{{ trans('backend/packages.tb_name') }}</th>
                            <th>{{ trans('backend/packages.tb_price') }}</th>
                            <th>{{ trans('backend/packages.tb_price_year') }}</th>
                            <th>{{ trans('backend/packages.tb_color') }}</th>
                            <th>{{ trans('backend/packages.tb_features') }}</th>
                            <th>{{ trans('backend/packages.tb_active') }}</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($packages as $package)
                            <tr>
                                <td>{{ $package->name }}</td>
                                <td>{{ $package->price }}</td>
                                <td>{{ $package->price_year }} {{ $package->discount_year_package() }}</td>
                                <td><i class="badge text-light" style="background-color: {{ $package->color }}"> {{ $package->color }} </i></td>
                                <td>{{ $package->features->implode('label', ', ') }}</td>
                                <td>
                                    @if($package->active)
                                        <i class="fas fa-check text-green"></i>
                                    @else
                                        <i class="fas fa-times text-red"></i>
                                    @endif



                                </td>
                                <td>
                                    <a href="{{ route('packages.edit', ['package' => $package->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault();
                                       document.getElementById('trash-form-{{ $package->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $package->id }}"
                                          action="{{ route('packages.destroy', ['package' => $package->id]) }}" method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/packages.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
