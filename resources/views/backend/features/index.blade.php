@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>{{ trans('backend/features.header') }}</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                {{ trans('backend/features.header') }}
                <div class="dropdown">
                    <a href="{{ route('features.create') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                    <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>{{ trans('backend/features.tb_name') }}</th>
                            <th>{{ trans('backend/features.tb_label') }}</th>
                            <th>{{ trans('backend/features.tb_active') }}</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>{{ trans('backend/features.tb_name') }}</th>
                            <th>{{ trans('backend/features.tb_label') }}</th>
                            <th>{{ trans('backend/features.tb_active') }}</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($features as $feature)
                            <tr>
                                <td>{{ $feature->name }}</td>
                                <td>{{ $feature->label }}</td>
                                <td>
                                    @if($feature->active)
                                        <i class="fas fa-check text-green"></i>
                                    @else
                                        <i class="fas fa-times text-red"></i>
                                    @endif



                                </td>
                                <td>
                                    <a href="{{ route('features.edit', ['feature' => $feature->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault();
                                       document.getElementById('trash-form-{{ $feature->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $feature->id }}"
                                          action="{{ route('features.destroy', ['feature' => $feature->id]) }}" method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/features.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
