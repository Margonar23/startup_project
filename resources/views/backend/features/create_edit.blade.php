@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($feature))
                        <span>{{ trans('backend/features.header_edit') }}</span>
                    @else
                        <span>{{ trans('backend/features.header_create') }}</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($feature))
                    <span>{{ trans('backend/features.header_edit') }}</span>
                @else
                    <span>{{ trans('backend/features.header_create') }}</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('features.index') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($feature))
                    <form action="{{ route('features.update', ['feature' => $feature->id]) }}" method="post">
                        @method('PUT')
                        @else
                            <form action="{{ route('features.store') }}" method="post">
                                @endif

                                @csrf
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/features.tb_name') }}</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name"
                                           type="text"
                                           value="{{ isset($feature) ? $feature->name : old('name') }}"/>
                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/features.tb_label') }}</label>
                                    <input class="form-control @error('label') is-invalid @enderror" name="label"
                                           type="text"
                                           value="{{ isset($feature) ? $feature->label : old('label') }}"/>
                                    @error('label')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/features.tb_description') }}</label>
                                    <textarea class="form-control @error('description') is-invalid @enderror"
                                              name="description">
                                        {{ isset($feature) ? $feature->description : old('description') }}
                                    </textarea>
                                    @error('description')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox custom-control-solid">
                                        @if(isset($feature) && $feature->active)
                                            <input class="custom-control-input" id="active" name="active" checked
                                                   type="checkbox">
                                        @else
                                            <input class="custom-control-input" id="active" name="active"
                                                   type="checkbox">
                                        @endif
                                        <label class="custom-control-label"
                                               for="active">{{ trans('backend/features.tb_active') }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($feature) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
