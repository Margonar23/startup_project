@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="link"></i></div>
                    <span>{{ trans('backend/news.header') }}</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                {{ trans('backend/news.header') }}
                <div class="dropdown">
                    <a href="{{ route('news.create') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>{{ trans('backend/news.tb_title') }}</th>
                            <th>{{ trans('backend/news.tb_subtitle') }}</th>
                            <th>{{ trans('backend/news.tb_user') }}</th>
                            <th>{{ trans('backend/news.tb_updated_at') }}</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>{{ trans('backend/news.tb_title') }}</th>
                            <th>{{ trans('backend/news.tb_subtitle') }}</th>
                            <th>{{ trans('backend/news.tb_user') }}</th>
                            <th>{{ trans('backend/news.tb_updated_at') }}</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($news as $n)
                            <tr>
                                <td>{{ $n->title }}</td>
                                <td>{{ $n->subtitle }}</td>
                                <td>{{ $n->user->name() }}</td>
                                <td>{{ $n->updated_at }}</td>
                                <td>
                                    <a href="{{ route('news.edit', ['news' => $n->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault(); document.getElementById('trash-form-{{ $n->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $n->id }}"
                                          action="{{ route('news.destroy', ['news' => $n->id]) }}" method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5"
                                    class="bg-gradient-primary-to-secondary text-white">{{ trans('backend/news.empty') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
