@extends('layouts.backend.backend')
@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/summernote-0.8.16-dist/summernote-bs4.min.css') }}">
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($news))
                        <span>{{ trans('backend/news.header_edit') }}</span>
                    @else
                        <span>{{ trans('backend/news.header_create') }}</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($news))
                    <span>{{ trans('backend/news.header_edit') }}</span>
                @else
                    <span>{{ trans('backend/news.header_create') }}</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('news.index') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($news))
                    <form action="{{ route('news.update', ['news' => $news->id]) }}" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @else
                            <form action="{{ route('news.store') }}" method="post" enctype="multipart/form-data">
                                @endif
                                @csrf
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/news.tb_title') }}</label>
                                    <input class="form-control @error('title') is-invalid @enderror" name="title"
                                           type="text"
                                           value="{{ isset($news) ? $news->title : old('title') }}"/>
                                    @error('title')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/news.tb_subtitle') }}</label>
                                    <input class="form-control @error('subtitle') is-invalid @enderror" name="subtitle"
                                           type="text"
                                           value="{{ isset($news) ? $news->subtitle : old('subtitle') }}"/>
                                    @error('subtitle')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/news.tb_header_image') }}</label><br>
                                    <input name="header_img" type="file"/>
                                    @error('header_img')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/news.tb_description') }}</label>
                                    <textarea class="@error('description') is-invalid @enderror" id="summernote" name="description">
                                        {{ isset($news) ? $news->description : old('description') }}
                                    </textarea>
                                    @error('description')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <br>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($news) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@parent
    <script src="{{ asset('vendor/summernote-0.8.16-dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('vendor/summernote-0.8.16-dist/lang/summernote-'.app()->getLocale().'-'.strtoupper(app()->getLocale()).'.js') }}"></script>
    <script>
        $(document).ready(function () {
            $("#summernote").summernote({
                height: 200,
                dialogsInBody: true,
                lang: '{{ app()->getLocale() }}-{{ strtoupper(app()->getLocale()) }}',
                callbacks:{
                    onInit:function(){
                        $('body > .note-popover').hide();
                    }
                },
            });
        });
    </script>
@endsection
