@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($company_classification))
                        <span>{{ trans('backend/company_classifications.header_edit') }}</span>
                    @else
                        <span>{{ trans('backend/company_classifications.header_create') }}</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($company_classification))
                    <span>{{ trans('backend/company_classifications.header_edit') }}</span>
                @else
                    <span>{{ trans('backend/company_classifications.header_create') }}</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('company_classifications.index') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($company_classification))
                    <form action="{{ route('company_classifications.update', ['company_classification' => $company_classification->id]) }}" method="post">
                        @method('PUT')
                @else
                    <form action="{{ route('company_classifications.store') }}" method="post">
                @endif

                    @csrf
                    <div class="form-group">
                        <label for="role_name">{{ trans('backend/company_classifications.tb_name') }}</label>
                        <input class="form-control @error('name') is-invalid @enderror" name="name" type="text"
                               value="{{ isset($company_classification) ? $company_classification->name : old('name') }}"/>
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="role_label">{{ trans('backend/company_classifications.tb_description') }}</label>
                        <textarea class="form-control @error('label') is-invalid @enderror" name="description">
                            {{ isset($company_classification) ? $company_classification->description : old('description') }}
                        </textarea>
                        @error('description')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="{{ isset($company_classification) ? trans('general.update') : trans('general.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
