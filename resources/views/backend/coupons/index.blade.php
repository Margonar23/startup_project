@extends('layouts.backend.backend')
@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/dropzone-5.7.0/dist/dropzone.css') }}">
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>Aziende affiliate a T-REGALO</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                T-REGALO
                <div class="dropdown">
                    <a href="{{ route('coupons.create') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                    <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Azienda</th>
                            <th>Categoria</th>
                            <th>Link</th>
                            <th>Indirizzo</th>
                            <th>Distretto</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Azienda</th>
                            <th>Categoria</th>
                            <th>Link</th>
                            <th>Indirizzo</th>
                            <th>Distretto</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($coupons as $coupon)
                            <tr>
                                <td>{{ $coupon->name }}</td>
                                <td>{{ $coupon->category->name }}</td>
                                <td>{{ $coupon->link }}</td>
                                <td>{{ $coupon->address }}</td>
                                <td>{{ $coupon->district->name }}</td>
                                <td>
                                    <a href="{{ route('coupons.edit', ['coupon' => $coupon->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault();
                                       document.getElementById('trash-form-{{ $coupon->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $coupon->id }}"
                                          action="{{ route('coupons.destroy', ['coupon' => $coupon->id]) }}" method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/company_categories.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
