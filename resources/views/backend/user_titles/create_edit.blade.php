@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($user_title))
                        <span>{{ trans('backend/user_titles.header_edit') }}</span>
                    @else
                        <span>{{ trans('backend/user_titles.header_create') }}</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($user_title))
                    <span>{{ trans('backend/user_titles.header_edit') }}</span>
                @else
                    <span>{{ trans('backend/user_titles.header_create') }}</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('user_titles.index') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($user_title))
                    <form action="{{ route('user_titles.update', ['user_title' => $user_title->id]) }}" method="post">
                        @method('PUT')
                        @else
                            <form action="{{ route('user_titles.store') }}" method="post">
                                @endif

                                @csrf
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/user_titles.tb_name') }}</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name"
                                           type="text"
                                           value="{{ isset($user_title) ? $user_title->name : old('name') }}"/>
                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">{{ trans('backend/user_titles.tb_label') }}</label>
                                    <input class="form-control @error('label') is-invalid @enderror" name="label"
                                           type="text"
                                           value="{{ isset($user_title) ? $user_title->label : old('label') }}"/>
                                    @error('label')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($user_title) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
