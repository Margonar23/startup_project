@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>{{ $company->name }}</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                {{ $company->name }}
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="row">
                    <div class="col-md-2">
                        @if(!is_null($company->logo))
                            <img src="{{ asset($company->logo) }}" width="100%" class="img-fluid img-thumbnail">
                        @endif
                    </div>
                    <div class="col-md-4">
                        <table class="table table-responsive table-striped ">
                            <tr>
                                <th>Nome</th>
                                <td>{{ $company->name }}</td>
                            </tr>
                            <tr>
                                <th>Richiesto da</th>
                                <td>{{ $company->user->name() }}</td>
                            </tr>
                            <tr>
                                <th>Label</th>
                                <td>{{ $company->label }}</td>
                            </tr>
                            <tr>
                                <th>UID</th>
                                <td>{{ $company->uid }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{ isset($company->status) ? $company->status->name : '' }}</td>
                            </tr>
                            <tr>
                                <th>Categoria</th>
                                <td>{{ isset($company->category) ? $company->category->name : '' }}</td>
                            </tr>
                            <tr>
                                <th>Classificazione</th>
                                <td>{{ isset($company->classification) ? $company->classification->name : '' }}</td>
                            </tr>
                            <tr>
                                <th>E-Mail</th>
                                <td>{{ $company->email }}</td>
                            </tr>
                            <tr>
                                <th>Fax</th>
                                <td>{{ $company->fax }}</td>
                            </tr>
                            <tr>
                                <th>Telefono</th>
                                <td>{{ $company->phone }}</td>
                            </tr>
                            <tr>
                                <th>Indirizzo</th>
                                <td>{{ $company->address }}</td>
                            </tr>
                            <tr>
                                <th>Web site</th>
                                <td>{{ $company->website }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        @if(isset($company->company_package))
                            <p class="font-weight-bold">Package</p>
                            <p>{{ $company->company_package->package->name }}</p>
                            <p class="font-weight-bold">Metodo di pagamento</p>
                            <p>{{ $company->company_package->payment_method->name }}</p>
                            <p class="font-weight-bold">Tipo di pagamento</p>
                            <p>{{ $company->company_package->payment_type->name }}</p>
                            <p class="font-weight-bold">Ultimo pagamento</p>
                            <p>{{ $company->company_package->last_payment_at }}</p>
                            <p class="font-weight-bold">Scandenza della subscription</p>
                            <p>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$company->company_package->subscription_expiry_at)->format('d M y') }}</p>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <h1>Operazioni</h1>
                        <div class="btn-group">
                            @foreach($statuses as $status)
                                <a href="{{ route('company_management.update_status', ['company' => $company->id, 'status' => $status->id])}}"
                                   class="btn btn-{{ $status->btn_color }} btn-xs" data-toggle="button"
                                   aria-pressed="{{ $company->status->id == $status->id ? 'true' : 'false' }}">{{ $status->name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
