@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>Aziende partner</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                Aziende
                <div class="dropdown">
                    <a href="{{ route('company_management.create') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Nr. Riferimento</th>
                            <th>Utente</th>
                            <th>Stato</th>
                            <th>Pacchetto</th>
                            <th>Stato pagamento</th>
                            <th>E-Mail</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Nome</th>
                            <th>Nr. Riferimento</th>
                            <th>Utente</th>
                            <th>Stato</th>
                            <th>Pacchetto</th>
                            <th>Stato pagamento</th>
                            <th>E-Mail</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($companies as $company)
                            <tr>
                                <td>{{ $company->name }}</td>
                                <td>{{ $company->uid }}</td>
                                <td>{{ $company->user->name() }}</td>
                                <td>
                                    <i class="fa fa-{{ $company->status->icon }} {{ $company->status->icon_color }}"></i>
                                    {{ $company->status->name }}
                                </td>
                                <td>
                                    @if(isset($company->company_package))
                                        {{ $company->company_package->package->name }}
                                        scade
                                        il {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$company->company_package->subscription_expiry_at)->format('d M y') }}
                                        ({{ \Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$company->company_package->subscription_expiry_at), false) }}
                                        giorni)
                                    @endif
                                </td>
                                <td>
                                    @if(isset($company->company_package))
                                        @if(\Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$company->company_package->subscription_expiry_at), false) < 10 && \Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$company->company_package->subscription_expiry_at), false) > 3)
                                            <span class=" badge badge-warning-soft">{{\Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$company->company_package->subscription_expiry_at), false)}} giorni alla scadenza</span>
                                        @elseif(\Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$company->company_package->subscription_expiry_at), false) <= 3 && \Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$company->company_package->subscription_expiry_at), false) > 0)
                                            <span class=" badge badge-warning">{{\Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$company->company_package->subscription_expiry_at), false)}} giorni alla scadenza</span>
                                        @elseif(\Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$company->company_package->subscription_expiry_at), false) < 0)
                                            <span class=" badge badge-danger">Scaduto</span>
                                        @endif
                                    @endif
                                </td>
                                <td>{{ $company->email }}</td>
                                <td class="btn-group-xs">
                                    <a href="{{ route('company_management.show', ['company' => $company->id]) }}"
                                       class="btn btn-xs btn-primary">Mostra dettagli</a>
                                    @if($company->created_by_staff)
                                        <a href="{{ route('company_management.send_mail', ['company' => $company->id]) }}" class="btn btn-xs btn-warning">Iniva Credenziali</a>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/company_categories.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
