@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>Creazione azienda partner</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                Crea
                <div class="dropdown">
                    <a href="{{ route('company_management.index') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                @if(isset($company))
                    <form action="{{ route('company_statuses.update', ['company_status' => $company_status->id]) }}"
                          method="post">
                        @method('PUT')
                        @else
                            <form action="{{ route('company_management.store') }}" method="post" enctype="multipart/form-data">
                                @endif
                                @csrf
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="role_name">Nome Azienda</label>
                                            <input class="form-control @error('name') is-invalid @enderror" name="name"
                                                   type="text"
                                                   value="{{ isset($company) ? $company->name : old('name') }}"/>
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="role_name">Registro di commercio</label>
                                            <input class="form-control @error('uid') is-invalid @enderror" name="uid"
                                                   type="text"
                                                   value="{{ isset($company) ? $company->uid : old('uid') }}"/>
                                            @error('uid')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="role_name">Logo</label><br>
                                            <input class=" @error('logo') is-invalid @enderror" name="file"
                                                   type="file"/>
                                            @error('logo')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="role_name">Classificazione</label>
                                            <select name="classification_id" class="form-control">
                                                @foreach($classifications as $classification)
                                                    <option value="{{ $classification->id }}">{{ $classification->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('category_id')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="role_name">Categoria</label>
                                            <select name="category_id" class="form-control">
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('category_id')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <h5>Indirizzo</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="role_name">Indirizzo</label>
                                            <input class="form-control @error('address') is-invalid @enderror" name="address"
                                                   type="text"/>
                                            @error('address')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="role_name">Distretto</label>
                                            <select name="district_id" class="form-control">
                                                @foreach($districts as $district)
                                                    <option value="{{ $district->id }}">{{ $district->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('district_id')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <h5>Dati di contatto</h5>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="role_name">E-mail</label>
                                            <input class="form-control @error('email') is-invalid @enderror" name="email"
                                                   type="text"/>
                                            @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="role_name">Telefono</label>
                                            <input class="form-control @error('phone') is-invalid @enderror" name="phone"
                                                   type="text"/>
                                            @error('phone')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="role_name">Fax</label>
                                            <input class="form-control @error('fax') is-invalid @enderror" name="fax"
                                                   type="text"/>
                                            @error('fax')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="role_name">Website</label>
                                            <input class="form-control @error('website') is-invalid @enderror" name="website"
                                                   type="text"/>
                                            @error('website')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <h5>Utente</h5>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="role_name">Nome</label>
                                            <input class="form-control @error('first_name') is-invalid @enderror" name="first_name"
                                                   type="text"/>
                                            @error('first_name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="role_name">Cognome</label>
                                            <input class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                                                   type="text"/>
                                            @error('last_name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="role_name">E-Mail</label>
                                            <input class="form-control @error('user_mail') is-invalid @enderror" name="user_mail"
                                                   type="email"/>
                                            @error('user_mail')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <h5>Pacchetto</h5>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="role_name">Pacchetto</label>
                                            <select name="package_id" class="form-control">
                                            @foreach($packages as $package)
                                                <option value="{{ $package->id }}">{{ $package->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('package_id')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="role_name">Tipo di pagamento</label>
                                            <select name="payment_type_id" class="form-control">
                                                @foreach($payment_types as $payment_type)
                                                    <option value="{{ $payment_type->id }}">{{ $payment_type->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('payment_type_id')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <h5>Dati di fatturazione</h5>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="role_name">Nome</label>
                                            <input class="form-control @error('invoice_name') is-invalid @enderror" name="invoice_name"
                                                   type="text"/>
                                            @error('invoice_name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="role_name">E-Mail</label>
                                            <input class="form-control @error('invoice_email') is-invalid @enderror" name="invoice_email"
                                                   type="text"/>
                                            @error('invoice_email')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="role_name">Indirizzo</label>
                                            <input class="form-control @error('invoice_address') is-invalid @enderror" name="invoice_address"
                                                   type="text"/>
                                            @error('invoice_address')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($company) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
