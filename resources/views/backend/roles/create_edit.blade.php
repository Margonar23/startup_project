@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($role))
                        <span>{{ trans('backend/roles.header_edit') }}</span>
                    @else
                        <span>{{ trans('backend/roles.header_create') }}</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($role))
                    <span>{{ trans('backend/roles.header_edit') }}</span>
                @else
                    <span>{{ trans('backend/roles.header_create') }}</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('roles.index') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($role))
                    <form action="{{ route('roles.update', ['role' => $role->id]) }}" method="post">
                        @method('PUT')
                @else
                    <form action="{{ route('roles.store') }}" method="post">
                @endif

                    @csrf
                    <div class="form-group">
                        <label for="role_name">{{ trans('backend/roles.tb_name') }}</label>
                        <input class="form-control @error('name') is-invalid @enderror" name="name" type="text"
                               value="{{ isset($role) ? $role->name : old('name') }}"/>
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="role_label">{{ trans('backend/roles.tb_label') }}</label>
                        <input class="form-control @error('label') is-invalid @enderror" name="label" type="text"
                               value="{{ isset($role) ? $role->label : old('label') }}"/>
                        @error('label')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="{{ isset($role) ? trans('general.update') : trans('general.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
