@extends('layouts.backend.backend')
@section('styles')
    @parent
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>Ordine T-REGALO {{ $tregalo->id }}</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                Ordine T-REGALO {{ $tregalo->id }}
                <div class="dropdown">
                    @if($tregalo->shipped)
                        <span class="badge badge-success">Ordine evaso</span>
                    @else
                        <span class="badge badge-warning">Ordine ricevuto</span>
                    @endif
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                Ordine effettuato da {{ $tregalo->user->name() }} - {{ $tregalo->user->email }}
                <hr>
                <h3>Dettagli ordine</h3>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <td>Nome</td>
                                <th>{{ $tregalo->name }}</th>
                            </tr>
                            <tr>
                                <td>E-Mail</td>
                                <th>{{ $tregalo->email }}</th>
                            </tr>
                            <tr>
                                <td>Telefono</td>
                                <th>{{ $tregalo->phone }}</th>
                            </tr>
                            <tr>
                                <td>Indirizzo</td>
                                <th>{{ $tregalo->address }}</th>
                            </tr>
                            <tr>
                                <td>Totale</td>
                                <th>{{ $tregalo->total() }} CHF</th>
                            </tr>
                        </table>
                    </div>

                </div>
                <h3 class="mt-3">Prodotti</h3>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Prodotto</th>
                                <th>Quantità</th>
                                <th>Prezzo</th>
                            </tr>
                            @foreach($tregalo->order_row as $item)
                                <tr>
                                    <td>{{ $item->tregalo->name }}</td>
                                    <td>{{ $item->qty }}</td>
                                    <td>{{ ($item->tregalo->price / 100) }} CHF</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="2">Totale</th>
                                <th>{{ $tregalo->total() }} CHF</th>
                            </tr>
                        </table>
                    </div>
                </div>
                @if(!$tregalo->shipped)
                    <a href="{{ route('sell_coupons.edit', ['sell_coupon' => $tregalo->id]) }}"
                       class="btn btn-lg btn-success mt-3">Segna come spedito</a>
                @endif
            </div>
        </div>
    </div>
@endsection
