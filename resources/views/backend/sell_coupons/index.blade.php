@extends('layouts.backend.backend')
@section('styles')
    @parent
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>Vendite</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                T-REGALO
                <div class="dropdown">

                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>E-Mail</th>
                            <th>Telefono</th>
                            <th>Indirizzo</th>
                            <th>Totale</th>
                            <th>Status</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Nome</th>
                            <th>E-Mail</th>
                            <th>Telefono</th>
                            <th>Indirizzo</th>
                            <th>Totale</th>
                            <th>Status</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($sell_coupons as $coupon)
                            <tr>
                                <td>{{ $coupon->name }}</td>
                                <td>{{ $coupon->email }}</td>
                                <td>{{ $coupon->phone }}</td>
                                <td>{{ $coupon->address }}</td>
                                <td>
                                    {{$coupon->total()}} CHF
                                </td>
                                <td>{{ $coupon->shipped ? 'spedito' : 'ordine ricevuto' }}</td>
                                <td>
                                    <a href="{{ route('sell_coupons.show', ['sell_coupon' => $coupon->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/company_categories.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
