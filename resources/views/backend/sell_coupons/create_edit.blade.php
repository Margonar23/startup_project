@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($coupon))
                        <span>Modifica</span>
                    @else
                        <span>Aggiungi una nuova azienda affiliata a T-REGALO</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($coupon))
                    <span>Modifica</span>
                @else
                    <span>Crea</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('coupons.index') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($coupon))
                    <form action="{{ route('coupons.update', ['coupon' => $coupon->id]) }}" method="post"
                          enctype="multipart/form-data">
                        @method('PUT')
                        @else
                            <form action="{{ route('coupons.store') }}" method="post" enctype="multipart/form-data">
                                @endif

                                @csrf
                                <div class="form-group">
                                    <label for="role_name">Azienda</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name"
                                           type="text"
                                           value="{{ isset($coupon) ? $coupon->name : old('name') }}"/>
                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        @if(isset($coupon) && !is_null($coupon->link))
                                            <div class="col-md-3">
                                                <img src="{{ asset($coupon->logo) }}" class="img-thumbnail" width="100%">
                                            </div>
                                        @endif
                                        <div class="col-md-6">
                                            <label for="role_name">Logo</label><br>
                                            <input name="file"
                                                   type="file"/>
                                            @error('logo')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="role_name">Categoria</label>
                                            <select name="category_id" class="form-control">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}" @if(isset($coupon) && $coupon->category_id == $category->id) selected @endif>
                                                        {{$category->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="role_name">Link</label>
                                            <input class="form-control @error('link') is-invalid @enderror" name="link"
                                                   type="text"
                                                   value="{{ isset($coupon) ? $coupon->link : old('link') }}"/>
                                            @error('link')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="role_name">Indirizzo</label>
                                            <input class="form-control @error('address') is-invalid @enderror"
                                                   name="address"
                                                   type="text"
                                                   value="{{ isset($coupon) ? $coupon->address : old('address') }}"/>
                                            @error('address')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="role_name">Distretto</label>
                                            <select name="district_id" class="form-control">
                                                @foreach($districts as $district)
                                                    <option value="{{$district->id}}" @if(isset($coupon) && $coupon->district_id == $district->id) selected @endif>{{$district->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($coupon) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent

@endsection
