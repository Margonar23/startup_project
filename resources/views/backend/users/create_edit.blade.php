@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($user))
                        <span>{{ trans('backend/users.header_edit') }}</span>
                    @else
                        <span>{{ trans('backend/users.header_create') }}</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($user))
                    <span>{{ trans('backend/users.header_edit') }}</span>
                @else
                    <span>{{ trans('backend/users.header_create') }}</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('users.index') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($user))
                    <form action="{{ route('users.update', ['user' => $user->id]) }}" method="post">
                        @method('PUT')
                        @else
                            <form action="{{ route('users.store') }}" method="post">
                                @endif

                                @csrf
                                <div class="form-group">
                                    <label for="user_name">{{ trans('backend/users.tb_firstname') }}</label>
                                    <input class="form-control @error('first_name') is-invalid @enderror" name="first_name"
                                           type="text"
                                           value="{{ isset($user) ? $user->first_name : old('first_name') }}"/>
                                    @error('first_name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="user_name">{{ trans('backend/users.tb_lastname') }}</label>
                                    <input class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                                           type="text"
                                           value="{{ isset($user) ? $user->last_name : old('last_name') }} "/>
                                    @error('last_name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_label">{{ trans('backend/users.tb_email') }}</label>
                                    <input class="form-control @error('email') is-invalid @enderror" name="email"
                                           type="text"
                                           value="{{ isset($user) ? $user->email : old('email') }}"/>
                                    @error('email')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('backend/users.role') }}</label>
                                    <select name="role_id" class="form-control">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" {{ old('role_id') == $role->id || isset($user) && $user->role->id == $role->id ? 'selected' : '' }}>{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($user) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
