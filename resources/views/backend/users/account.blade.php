@extends('layouts.backend.backend')

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/dropzone-5.7.0/dist/dropzone.css') }}">
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="user"></i></div>
                    <span>{{ trans('backend/users.account') }}</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card">
            <div class="card-header">{{ trans('backend/users.account_edit') }}</div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="row">
                    <div class="col-md-3">
                        <ul class="nav nav-pills flex-column" id="cardPillVertical" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link {{ $subnav == 'overview' || is_null($subnav) ? 'active' : '' }}"
                                   href="{{ route('backend.account', ['subnav' => 'overview']) }}">
                                    {{ trans('backend/users.overview') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ $subnav == 'credential' ? 'active' : '' }}"
                                   href="{{ route('backend.account', ['subnav' => 'credential']) }}">
                                    {{ trans('backend/users.credential') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ $subnav == 'avatar' ? 'active' : '' }}"
                                   href="{{ route('backend.account', ['subnav' => 'avatar']) }}">
                                    {{ trans('backend/users.avatar') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content" id="cardPillContentVertical">
                            <div class="tab-pane fade {{ $subnav == 'overview' || is_null($subnav) ? ' show active' : '' }}">
                                <h5 class="card-title">{{ trans('backend/users.overview') }}</h5>
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            @if(is_null(Auth::user()->avatar))
                                                <img class="img-fluid" src="{{ asset('images/default_avatar.jpg') }}"/>
                                            @else
                                                <img class="img-fluid" src="{{ asset(Auth::user()->avatar) }}" />
                                            @endif
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title">{{ Auth::user()->name() }}</h5>
                                                <p class="card-text">{{ Auth::user()->email }}</p>
                                                <p class="card-text">{{ Auth::user()->role->name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade {{ $subnav == 'credential' ? 'show active' : '' }}">
                                <h5 class="card-title">{{ trans('backend/users.credential') }}</h5>
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <form action="{{ route('backend.update_account') }}" method="post">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label
                                                            class="text-gray-600 small">{{ trans('backend/users.password') }}</label>
                                                        <input class="form-control form-control-solid py-4"
                                                               name="password" type="password"
                                                               placeholder="" aria-label="password"
                                                               aria-describedby="password"/>
                                                        @error('password')
                                                        <span class="text-danger">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label
                                                            class="text-gray-600 small">{{ trans('backend/users.password_confirmation') }}</label>
                                                        <input class="form-control form-control-solid py-4"
                                                               name="password_confirmation" type="password"
                                                               placeholder="" aria-label="password_confirmation"
                                                               aria-describedby="password_confirmation"/>
                                                    </div>
                                                    <div
                                                        class="form-group d-flex align-items-center justify-content-between mb-0">
                                                        <input type="submit" value="{{ trans('general.save') }}"
                                                               class="btn btn-primary"/>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade {{ $subnav == 'avatar' ? 'show active' : '' }}">
                                <h5 class="card-title">{{ trans('backend/users.avatar') }}</h5>
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            @if(is_null(Auth::user()->avatar) || Auth::user()->avatar == '')
                                                <img class="img-fluid" src="{{ asset('images/default_avatar.jpg') }}"/>
                                            @else
                                                <img class="img-fluid" src="{{ asset(Auth::user()->avatar) }}" />
                                            @endif
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title">
                                                    @if(is_null(Auth::user()->avatar) || Auth::user()->avatar == '')
                                                        <span
                                                            class="text-danger">{{ trans('backend/users.avatar_not_set') }}</span>
                                                    @endif
                                                </h5>
                                                <form action="{{ route('backend.update_account_avatar') }}" class="dropzone bg-blue-soft" id="dropzone-form" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="dz-message">
                                                        <div class="col-xs-8">
                                                            <div class="message">
                                                                <p>{{ trans('backend/users.avatar_dropzone') }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fallback">
                                                        <input type="file" name="avatar">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{ asset('vendor/dropzone-5.7.0/dist/dropzone.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;
        $("#dropzone-form").dropzone({
            addRemoveLinks: true,
            maxFiles: 1,
            init: function () {
                this.on('complete', function () {
                    window.location.href = '{{ route('backend.account') }}';
                });
            }
        });
    </script>
@endsection
