@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="users"></i></div>
                    <span>{{ trans('backend/users.header') }}</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                {{ trans('backend/users.header') }}
                <div class="dropdown">
                    <a href="{{ route('users.create') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{ trans('backend/users.tb_name') }}</th>
                            <th>{{ trans('backend/users.tb_email') }}</th>
                            <th>{{ trans('backend/users.tb_role') }}</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th>{{ trans('backend/users.tb_name') }}</th>
                            <th>{{ trans('backend/users.tb_email') }}</th>
                            <th>{{ trans('backend/users.tb_role') }}</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($users as $user)
                            <tr>
                                <td>
                                    <a class="btn btn-icon btn-sm btn-transparent-dark">
                                        @if(is_null($user->avatar))
                                            <img class="img-fluid" src="{{ asset('images/default_avatar.jpg') }}"/>
                                        @else
                                            <img class="img-fluid" src="{{ asset($user->avatar) }}"/>
                                        @endif

                                    </a>
                                </td>
                                <td>{{ $user->name() }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role->name }}</td>
                                <td>
                                    <a href="{{ route('users.edit', ['user' => $user->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault(); document.getElementById('trash-form-{{ $user->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $user->id }}"
                                          action="{{ route('users.destroy', ['user' => $user->id]) }}" method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5"
                                    class="bg-gradient-primary-to-secondary text-white">{{ trans('backend/users.empty') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
