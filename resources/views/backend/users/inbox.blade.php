@extends('layouts.backend.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="inbox"></i>
                    </div>
                    <span>Inbox</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <inbox-backend :notifications="{{Auth::user()->notifications}}"></inbox-backend>
    </div>
@endsection
