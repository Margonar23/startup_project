<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178696265-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-178696265-1');
    </script>
    <link rel="shortcut icon" href="{{ asset('images/logo-tigit-no-bg.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('images/logo-tigit-no-bg.png') }}" type="image/x-icon">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('vendor/bootstrap-4.4.1-dist/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/fontawesome-free-5.13.0-web/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/fontawesome-free-5.13.0-web/css/font-awesome-animation.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body class="nav-fixed">
@include('layouts.backend.nav')
<div id="layoutSidenav">
    @include('layouts.backend.sidebar')
    <div id="layoutSidenav_content">
        <main>
            @yield('content')
        </main>
        @include('layouts.backend.footer')
    </div>
</div>
<!-- Bootstrap core JavaScript -->
<script src="{{ asset('vendor/jquery-3.4.1/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-4.4.1-dist/js/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('vendor/feather/feather.min.js') }}"></script>
<!-- Scripts -->
<script src="{{ asset('js/scripts.js') }}" defer></script>

<!-- ChartJs -->
<script src="{{ asset('vendor/chartjs/Chart.min.js') }}" ></script>


<!-- DataTables -->
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}" ></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}" ></script>

@yield('scripts')

</body>
</html>
