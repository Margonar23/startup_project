<nav class="topnav navbar navbar-expand shadow navbar-light bg-white" id="sidenavAccordion">
    <a class="navbar-brand d-none d-sm-block" href="{{ route('home') }}">
        {{ config('app.name') }}
    </a>
    <button class="btn btn-icon btn-transparent-dark order-1 order-lg-0 mr-lg-2" id="sidebarToggle" href="#"><i
            data-feather="menu"></i></button>
    <ul class="navbar-nav align-items-center ml-auto">
        <li class="nav-item dropdown no-caret mr-3 dropdown-notifications">
            <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownAlerts"
               href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true"
               aria-expanded="false">
                @if(count(Auth::user()->unreadNotifications))
                    <i class="fa fa-bell faa-ring animated text-red"></i>
                @else
                    <i class="fa fa-bell"></i>
                @endif
            </a>
            <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up"
                 aria-labelledby="navbarDropdownAlerts">
                <h6 class="dropdown-header dropdown-notifications-header"><i class="mr-2" data-feather="bell"></i>Alerts
                    Center</h6>
                @foreach(Auth::user()->notifications->forPage(1,5) as $notification)
                    <a class="dropdown-item dropdown-notifications-item {{ is_null($notification->read_at) ? 'bg-light font-weight-bold' : '' }} "
                       href="#!">
                        <div class="dropdown-notifications-item-icon bg-success">
                            <i class="{{ $notification->data['label'] }}"></i>
                        </div>
                        <div class="dropdown-notifications-item-content">
                            <div
                                class="dropdown-notifications-item-content-details">{{ $notification->created_at }}</div>
                            <div class="dropdown-notifications-item-content-text">
                                {{ $notification->data['message'] }}
                            </div>
                        </div>
                    </a>
                @endforeach
                <a class="dropdown-item dropdown-notifications-footer" href="{{ route('backend.inbox') }}">Vedi tutto</a>
            </div>
        </li>


        <li class="nav-item dropdown no-caret mr-3 dropdown-user">
            <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownUserImage"
               href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true"
               aria-expanded="false">
                @if(is_null(Auth::user()->avatar))
                    <img class="img-fluid" src="{{ asset('images/default_avatar.jpg') }}"/>
                @else
                    <img class="img-fluid" src="{{ asset(Auth::user()->avatar) }}"/>
                @endif

            </a>
            <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up"
                 aria-labelledby="navbarDropdownUserImage">
                <h6 class="dropdown-header d-flex align-items-center">
                    @if(is_null(Auth::user()->avatar))
                        <img class="dropdown-user-img" src="{{ asset('images/default_avatar.jpg') }}"/>
                    @else
                        <img class="dropdown-user-img" src="{{ asset(Auth::user()->avatar) }}"/>
                    @endif

                    <div class="dropdown-user-details">
                        <div class="dropdown-user-details-name">{{ Auth::user()->name() }}</div>
                        <div class="dropdown-user-details-email">{{ Auth::user()->email }}</div>
                    </div>
                </h6>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('backend.account') }}">
                    <div class="dropdown-item-icon">
                        <i data-feather="settings"></i>
                    </div>
                    {{ trans('backend/users.account') }}
                </a>

                <a class="dropdown-item" href="{{ url('/logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <div class="dropdown-item-icon">
                        <i data-feather="log-out"></i>
                    </div>
                    Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </li>
    </ul>
</nav>
