<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178696265-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-178696265-1');
    </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>
    <link rel="shortcut icon" href="{{ asset('images/logo-tigit-no-bg.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('images/logo-tigit-no-bg.png') }}" type="image/x-icon">

    <link href="{{ asset('vendor/bootstrap-4.4.1-dist/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/fontawesome-free-5.13.0-web/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/fontawesome-free-5.13.0-web/css/fontawesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles_kit.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <script src="{{ asset('js/app.js') }}" defer></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @yield('styles')
</head>
<body>

<div id="app">
    <div id="layoutDefault">
        <div id="layoutDefault_content">
            <main>
                <nav class="navbar navbar-marketing navbar-expand-lg bg-transparent  navbar-dark fixed-top">
                    <div class="container">
                        <a class="navbar-brand text-white" href="{{ route('home') }}">
                            <img src="{{ asset('images/logo-tigit-no-bg.png') }}">
                            {{ strtoupper(config('app.name')) }}
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                                data-feather="menu"></i></button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto mr-lg-5">
                                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home </a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('coupon') }}">T-Regalo </a></li>
{{--                                <li class="nav-item">--}}
{{--                                    <p class="nav-link"> Bar & Ristorazione </p>--}}
{{--                                    <img src="{{ asset('images/cs.png') }}" class="mt-n4 ml-5 position-absolute" width="85px" />--}}
{{--                                </li>--}}
{{--                                <li class="nav-item">--}}
{{--                                    <p class="nav-link"> Per le aziende </p>--}}
{{--                                    <img src="{{ asset('images/cs.png') }}" class="mt-n4 ml-3 position-absolute" width="85px" />--}}
{{--                                </li>--}}
{{--                                <li class="nav-item">--}}
{{--                                    <p class="nav-link"> Offerte di lavoro </p>--}}
{{--                                    <img src="{{ asset('images/cs.png') }}" class="mt-n4 ml-3 position-absolute" width="85px" />--}}
{{--                                </li>--}}
{{--                                <li class="nav-item">--}}
{{--                                    <p class="nav-link"> Eventi </p>--}}
{{--                                    <img src="{{ asset('images/cs.png') }}" class="mt-n4 position-absolute" width="85px" />--}}
{{--                                </li>--}}
                                <li class="nav-item">
                                <li class="nav-item"><a class="nav-link" href="{{ route('about') }}">Chi siamo </a></li>
                                @auth()
                                    <li class="nav-item dropdown no-caret">
                                        <a class="nav-link dropdown-toggle" id="navbarDropdownDocs" href="#" role="button"
                                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ Auth::user()->name() }}
                                            <i class="fas fa-chevron-right dropdown-arrow"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right animated--fade-in-up"
                                             aria-labelledby="navbarDropdownDocs">
                                            @if(Auth::user()->role->internal)
                                                <a class="dropdown-item py-3" href="{{ route('backend.dashboard') }}">
                                                    <div class="icon-stack bg-primary-soft text-primary mr-4">
                                                        <i class="fas fa-user"></i>
                                                    </div>
                                                    <div>
                                                        <div class="small text-gray-500">
                                                            {{ trans('backend/sidebar.dashboard') }}
                                                        </div>
                                                        {{ trans('backend/sidebar.dashboard') }}
                                                    </div>
                                                </a>
                                            @elseif(Auth::user()->role->label == 'company_user')
                                                <a class="dropdown-item py-3" href="{{ route('company.dashboard') }}">
                                                    <div class="icon-stack bg-primary-soft text-primary mr-4">
                                                        <i class="fas fa-user"></i>
                                                    </div>
                                                    <div>
                                                        <div class="small text-gray-500">
                                                            Gestione azienda
                                                        </div>
                                                        Gestione azienda
                                                    </div>
                                                </a>
                                            @endif


                                            <div class="dropdown-divider m-0"></div>
                                            <a class="dropdown-item py-3" href="{{ route('users.profile') }}">
                                                <div class="icon-stack bg-primary-soft text-primary mr-4">
                                                    <i class="fas fa-user"></i>
                                                </div>
                                                <div>
                                                    <div class="small text-gray-500">
                                                        {{ trans('frontend/users/profile.profile') }}
                                                    </div>
                                                    {{ trans('frontend/users/profile.profile_sub') }}
                                                </div>
                                            </a>
                                            <a class="dropdown-item py-3"
                                               href="#" target="_blank">
                                                <div class="icon-stack bg-primary-soft text-primary mr-4">
                                                    <i class="fas fa-cogs"></i>
                                                </div>
                                                <div>
                                                    <div class="small text-gray-500">Settings</div>
                                                    Configure your settings
                                                </div>
                                            </a>
                                            <a class="dropdown-item py-3" href="{{ url('/logout') }}"
                                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                <div class="icon-stack bg-primary-soft text-primary mr-4">
                                                    <i class="fas fa-sign-out-alt"></i>
                                                </div>
                                                <div>
                                                    Logout
                                                </div>
                                                <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                                      style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </a>
                                        </div>
                                    </li>
                                @endauth
                            </ul>
                            @guest()
                                <a class="btn-teal btn rounded-pill px-4 ml-lg-4" href="{{ route('users.login') }}">
                                    {{ trans('frontend/nav.login') }}
                                    <i class="fas fa-sign-in-alt ml-1"></i>
                                </a>
                            @endguest
                        </div>
                    </div>
                </nav>

                @yield('content')
            </main>
        </div>
        <div id="layoutDefault_footer">
            <footer class="footer pt-10 pb-5 mt-auto bg-dark footer-dark">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="footer-brand">{{ env('COMPANY_NAME') }}</div>
                            <div class="mb-3">{{ env('COMPANY_SLOGAN') }}</div>
                            <div class="icon-list-social mb-5">
                                <a class="icon-list-social-link" href="{{ env('COMPANY_INSTAGRAM') }}" target="_blank"><i class="fab fa-instagram"></i></a>
                                <a class="icon-list-social-link" href="{{ env('COMPANY_LINKEDIN') }}" target="_blank"><i class="fab fa-linkedin"></i></a>
                                <a class="icon-list-social-link" href="{{ env('COMPANY_YOUTUBE') }}" target="_blank"><i class="fab fa-youtube"></i></a>
                                <a class="icon-list-social-link" href="{{ env('COMPANY_FACEBOOK') }}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            </div>
                        </div>
                    </div>
                    <hr class="my-5" />
                    <div class="row align-items-center">
                        <div class="col-md-6 small">Copyright &copy; Tigit Sagl {{ date('Y', time()) }}</div>
                        <div class="col-md-6 text-md-right small">
{{--                            <a href="{{ route('privacy_policy')}}">Privacy Policy</a>--}}
{{--                            &middot;--}}
{{--                            <a href="{{ route('terms_and_conditions') }}">Terms &amp; Conditions</a>--}}
                        </div>
                    </div>
                </div>
            </footer>
        </div>

    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('vendor/jquery-3.4.1/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-4.4.1-dist/js/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('vendor/feather/feather.min.js') }}"></script>
<!-- Custom scripts for this template -->
<script src="{{ asset('js/scripts-kit.js' )}}"></script>
<script src="{{ asset('vendor/sweet-alert2/sweetalert2@9.js' )}}"></script>
@yield('scripts')
<script>
    $(document).ready(function() {
        $(".dropdown-toggle").dropdown();
    });
</script>
</body>
</html>
