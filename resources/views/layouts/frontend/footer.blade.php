<div id="layoutDefault_footer">
    <footer class="footer pt-10 pb-5 mt-auto bg-dark footer-dark">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer-brand">{{ env('COMPANY_NAME') }}</div>
                    <div class="mb-3">{{ env('COMPANY_SLOGAN') }}</div>
                    <div class="icon-list-social mb-5">
                        <a class="icon-list-social-link" href="{{ env('COMPANY_INSTAGRAM') }}" target="_blank"><i class="fab fa-instagram"></i></a>
                        <a class="icon-list-social-link" href="{{ env('COMPANY_LINKEDIN') }}" target="_blank"><i class="fab fa-linkedin"></i></a>
                        <a class="icon-list-social-link" href="{{ env('COMPANY_YOUTUBE') }}" target="_blank"><i class="fab fa-youtube"></i></a>
                        <a class="icon-list-social-link" href="{{ env('COMPANY_FACEBOOK') }}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                            <div class="text-uppercase-expanded text-xs mb-4">Aziende</div>
                            <ul class="list-unstyled mb-0">
                                <li class="mb-2"><a href="{{ route('packages') }}">La nostra offerta</a></li>
                                <li class="mb-2"><a href="{{ route('companies') }}">Partner</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                            <div class="text-uppercase-expanded text-xs mb-4">Utenti</div>
                            <ul class="list-unstyled mb-0">
                                <li class="mb-2"><a href="{{ route('reservations') }}">Riserva il tuo tavolo</a></li>
                                <li class="mb-2"><a href="{{ route('events') }}">Eventi</a></li>
                                <li class="mb-2"><a href="{{ route('job_offers') }}">Trova lavoro</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
                            <div class="text-uppercase-expanded text-xs mb-4">Tigit</div>
                            <ul class="list-unstyled mb-0">
                                <li class="mb-2"><a href="{{ route('news') }}">News</a></li>
                                <li class="mb-2"><a href="{{ route('contacts') }}">Contattaci</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="text-uppercase-expanded text-xs mb-4">Legale</div>
                            <ul class="list-unstyled mb-0">
                                <li class="mb-2"><a href="{{ route('privacy_policy')}}">Privacy Policy</a></li>
                                <li class="mb-2"><a href="{{ route('terms_and_conditions') }}">Terms and Conditions</a></li>
                                <li class="mb-2"><a href="{{ route('impressum') }}">Impressum</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="my-5" />
            <div class="row align-items-center">
                <div class="col-md-6 small">Copyright &copy; Tigit Sagl {{ date('Y', time()) }}</div>
                <div class="col-md-6 text-md-right small">
                    <a href="{{ route('privacy_policy')}}">Privacy Policy</a>
                    &middot;
                    <a href="{{ route('terms_and_conditions') }}">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
