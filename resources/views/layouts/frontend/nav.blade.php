<nav class="navbar navbar-marketing navbar-expand-lg bg-transparent  navbar-dark fixed-top">
    <div class="container">
        <a class="navbar-brand text-white" href="{{ route('home') }}">
            <img src="{{ asset('images/logo-tigit-no-bg.png') }}">
            {{ strtoupper(config('app.name')) }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu"
                aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation"><i
                data-feather="menu"></i></button>
        <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="{{ route('job_offers')  }}">T-ASSUMO</a></li>
                <li class="nav-item"><a class="nav-link" href="{{  route('companies') }}">T-VEDO</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('reservations') }}">T-RISERVO</a></li>
                <li class="nav-item"><a class="nav-link" href="{{  route('events') }}">T-FESTEGGIO</a></li>
                <li class="nav-item"><a class="nav-link" href="{{  route('tRegalo') }}">T-REGALO</a></li>
                <li class="nav-item"><a class="nav-link" href="{{  route('packages') }}">OFFERTA</a></li>
                <li class="nav-item"><a class="nav-link" href="{{  route('contacts') }}">Chi siamo</a></li>

                @auth()
                    <li class="nav-item dropdown no-caret">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownDocs" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->name() }}
                            <i class="fas fa-chevron-right dropdown-arrow"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right animated--fade-in-up"
                             aria-labelledby="navbarDropdownDocs">
                            @if(Auth::user()->role->internal)
                                <a class="dropdown-item py-3" href="{{ route('backend.dashboard') }}">
                                    <div class="icon-stack bg-primary-soft text-primary mr-4">
                                        <i class="fas fa-user"></i>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">
                                            {{ trans('backend/sidebar.dashboard') }}
                                        </div>
                                        {{ trans('backend/sidebar.dashboard') }}
                                    </div>
                                </a>
                            @elseif(Auth::user()->role->label == 'company_user')
                                <a class="dropdown-item py-3" href="{{ route('company.dashboard') }}">
                                    <div class="icon-stack bg-primary-soft text-primary mr-4">
                                        <i class="fas fa-user"></i>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">
                                            Gestione azienda
                                        </div>
                                        Gestione azienda
                                    </div>
                                </a>
                            @endif


                            <div class="dropdown-divider m-0"></div>
                            <a class="dropdown-item py-3" href="{{ route('users.profile') }}">
                                <div class="icon-stack bg-primary-soft text-primary mr-4">
                                    <i class="fas fa-user"></i>
                                </div>
                                <div>
                                    <div class="small text-gray-500">
                                        {{ trans('frontend/users/profile.profile') }}
                                    </div>
                                    {{ trans('frontend/users/profile.profile_sub') }}
                                </div>
                            </a>
                            {{--                            <a class="dropdown-item py-3"--}}
                            {{--                               href="#" target="_blank">--}}
                            {{--                                <div class="icon-stack bg-primary-soft text-primary mr-4">--}}
                            {{--                                    <i class="fas fa-cogs"></i>--}}
                            {{--                                </div>--}}
                            {{--                                <div>--}}
                            {{--                                    <div class="small text-gray-500">Settings</div>--}}
                            {{--                                    Configure your settings--}}
                            {{--                                </div>--}}
                            {{--                            </a>--}}
                            <a class="dropdown-item py-3" href="{{ url('/logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <div class="icon-stack bg-primary-soft text-primary mr-4">
                                    <i class="fas fa-sign-out-alt"></i>
                                </div>
                                <div>
                                    Logout
                                </div>
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </a>
                        </div>
                    </li>
                @endauth
            </ul>
            @guest()
                <a class="btn-teal btn rounded-pill px-4 ml-lg-4" href="{{ route('users.login') }}">
                    {{ trans('frontend/nav.login') }}
                    <i class="fas fa-sign-in-alt ml-1"></i>
                </a>
            @endguest
        </div>
    </div>
</nav>

