<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178696265-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-178696265-1');
    </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>
    <link rel="shortcut icon" href="{{ asset('images/logo-tigit-no-bg.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('images/logo-tigit-no-bg.png') }}" type="image/x-icon">

    <link href="{{ asset('vendor/bootstrap-4.4.1-dist/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/fontawesome-free-5.13.0-web/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/fontawesome-free-5.13.0-web/css/fontawesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles_kit.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <script src="{{ asset('js/app.js') }}" defer></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @yield('styles')
</head>
<body>

<div id="app">
    <div id="layoutDefault">
        <div id="layoutDefault_content">
            <main>
                @include('layouts.frontend.nav')
                @yield('content')
            </main>
        </div>
        @include('layouts.frontend.footer')
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('vendor/jquery-3.4.1/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-4.4.1-dist/js/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('vendor/feather/feather.min.js') }}"></script>
<!-- Custom scripts for this template -->
<script src="{{ asset('js/scripts-kit.js' )}}"></script>
<script src="{{ asset('vendor/sweet-alert2/sweetalert2@9.js' )}}"></script>
@yield('scripts')
<script>
    $(document).ready(function () {
        $(".dropdown-toggle").dropdown();
    });

</script>
</body>
</html>
