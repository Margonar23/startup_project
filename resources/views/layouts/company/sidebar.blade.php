<div id="layoutSidenav_nav">
    <nav class="sidenav shadow-right sidenav-light">
        <div class="sidenav-menu">
            <div class="nav accordion" id="accordionSidenav">
                <div class="sidenav-menu-heading">{{ trans('backend/sidebar.dashboard') }}</div>
                <a class="nav-link" href=" {{ route('company.dashboard') }}">
                    <div class="nav-link-icon">
                        <i data-feather="activity"></i>
                    </div>
                    {{ trans('backend/sidebar.dashboard') }}
                </a>

                <div class="sidenav-menu-heading">La tua azienda</div>
                @forelse(Auth::user()->hasCompanies as $company)
                    <a class="nav-link" href="{{ route('company.index', ['company' => $company->id]) }}">
                        <div class="nav-link-icon">
                            <i class="fa fa-building"></i>
                        </div>
                        {{ $company->name }}
                    </a>
                    @if(isset($company->company_package))
                        @if($company->company_package->package->features->pluck('label')->contains('hr'))
                            <div class="sidenav-menu-heading">HR</div>
                            <a class="nav-link" href="{{ route('company.hr.index', ['company' => $company->id]) }}">
                                <div class="nav-link-icon">
                                    <i data-feather="user"></i>
                                </div>
                                Ricerca di personale
                            </a>
                        @endif
                        @if($company->company_package->package->features->pluck('label')->contains('presentation'))
                            <div class="sidenav-menu-heading">Visibilità</div>
                            <a class="nav-link" href="{{ route('company.presentation', ['company' => $company->id]) }}">
                                <div class="nav-link-icon">
                                    <i data-feather="book-open"></i>
                                </div>
                                Presentazione publica
                            </a>
                        @endif
                        @if($company->company_package->package->features->pluck('label')->contains('reservation'))
                            <div class="sidenav-menu-heading">Eventi & Ristorazione</div>
                            <a class="nav-link"
                               href="{{ route('company.reservations.index', ['company' => $company->id]) }}">
                                <div class="nav-link-icon">
                                    <i class="fa fa-cookie"></i>
                                </div>
                                Riservazioni
                                <div class="mt-n3 text-white badge badge-red text-center pull-right font-weight-bolder ml-1 p-1" style="border-radius: 50%">
                                    {{ $company->reservationRequest()->count() }}
                                </div>
                            </a>
                            <a class="nav-link" href="{{ route('company.events.index', ['company' => $company->id]) }}">
                                <div class="nav-link-icon">
                                    <i class="fa fa-calendar-week"></i>
                                </div>
                                Eventi
                            </a>
                            <a class="nav-link"
                               href="{{ route('company.restaurants.index', ['company' => $company->id]) }}">
                                <div class="nav-link-icon">
                                    <i class="fa fa-glass-martini-alt"></i>
                                </div>
                                Bar/Ristorazione
                            </a>
                            <a class="nav-link"
                               href="{{ route('company.locations.index', ['company' => $company->id]) }}">
                                <div class="nav-link-icon">
                                    <i class="fa fa-search-location"></i>
                                </div>
                                Location
                            </a>
                        @endif
                    @endif
                    @if($company->status->label == 'sub')
                        <div class="sidenav-menu-heading">Accessi</div>
{{--                        <a class="nav-link" href="{{ route('company.roles.index', ['company' => $company->id]) }}">--}}
{{--                            <div class="nav-link-icon">--}}
{{--                                <i data-feather="users"></i>--}}
{{--                            </div>--}}
{{--                            Ruoli--}}
{{--                        </a>--}}
                        <a class="nav-link" href="{{ route('company.users.index', ['company' => $company->id]) }}">
                            <div class="nav-link-icon">
                                <i data-feather="users"></i>
                            </div>
                            Utenti
                        </a>
                    @endif
                @empty
                    <a class="nav-link" href="{{ route('company.create') }}">
                        <div class="nav-link-icon">
                            <i data-feather="bar-chart"></i>
                        </div>
                        Crea la tua azienda
                    </a>
                @endforelse

            </div>
        </div>
        <div class="sidenav-footer">
            <div class="sidenav-footer-content">
                <div class="sidenav-footer-subtitle">Logged in as:</div>
                <div class="sidenav-footer-title">{{ Auth::user()->name() }}</div>
            </div>
        </div>
    </nav>
</div>
