<footer class="footer mt-auto footer-light">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 small">Copyright &copy; Tigit Sagl {{ date('Y', time()) }}</div>
            <div class="col-md-6 text-md-right small">
                <a href="{{ route('privacy_policy') }}">Privacy Policy</a>
                &middot;
                <a href="{{ route('terms_and_conditions') }}">Terms &amp; Conditions</a>
            </div>
        </div>
    </div>
</footer>
