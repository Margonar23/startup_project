<nav class="topnav navbar navbar-expand shadow navbar-light bg-white" id="sidenavAccordion">
    <a class="navbar-brand d-none d-sm-block" href="{{ route('home') }}">
        {{ config('app.name') }}
    </a>
    <button class="btn btn-icon btn-transparent-dark order-1 order-lg-0 mr-lg-2" id="sidebarToggle" href="#"><i
            data-feather="menu"></i></button>

    <div class="navbar-nav align-items-center ml-auto">
        <a class="btn btn-transparent-dark order-0" href="{{ route('company.inbox') }}">
            <div class="icon-badge-container">
                <i class="far fa-envelope-open icon-badge-icon"></i>
                @if(Auth::user()->unreadNotifications->count())
                    <div class="icon-badge">{{ Auth::user()->unreadNotifications->count() }}</div>
                @endif
            </div>
        </a>

        <b-nav-item-dropdown right>

            <!-- Using 'button-content' slot -->
            <template v-slot:button-content>
                <b-avatar variant="default"
                          src="{{ asset(\Illuminate\Support\Facades\Auth::user()->avatar) }}"></b-avatar>
            </template>
            <b-dropdown-item href="{{ route('users.profile') }}">Profile</b-dropdown-item>
            <b-dropdown-item href="{{ url('/logout') }}"
                             onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout
            </b-dropdown-item>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </b-nav-item-dropdown>
    </div>
</nav>
