@extends('layouts.company.backend')
@section('styles')
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($event))
                        <span>Modifica evento</span>
                    @else
                        <span>Crea un nuovo evento</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($event))
                    <span>Modifica</span>
                @else
                    <span>Crea</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('company.events.index', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($event))
                    <form
                        action="{{ route('company.events.update', ['company' => $company->id, 'event' => $event->id]) }}"
                        method="post" enctype="multipart/form-data">
                        @else
                            <form action="{{ route('company.events.store', ['company' => $company->id]) }}"
                                  method="post" enctype="multipart/form-data">
                                @endif
                                @csrf
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img id="cover_box" src="{{ isset($event) && !is_null($event->cover) ? asset($event->cover) : '#' }}" class="img-thumbnail {{ isset($event) && !is_null($event->cover) ? '' : 'd-none'}}" width="100%"/>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="role_name">Locandina evento</label><br>
                                            <input name="file" id="cover" type="file"/>
                                            @error('cover')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="role_name">Nome evento</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name"
                                           type="text"
                                           value="{{ isset($event) ? $event->name : old('name') }}"/>
                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="role_name">Nr. massimo di partecipanti</label>
                                            <input class="form-control @error('max_participants') is-invalid @enderror"
                                                   name="max_participants"
                                                   type="number"
                                                   value="{{ isset($event) ? $event->max_participants : old('max_participants') }}"/>
                                            @error('max_participants')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-3">
                                            <label for="role_name">Se è richiesta un età minima, inserire l'età</label>
                                            <input class="form-control @error('minimum_age') is-invalid @enderror"
                                                   name="minimum_age"
                                                   type="number"
                                                   value="{{ isset($event) ? $event->minimum_age : old('minimum_age') }}"/>
                                            @error('minimum_age')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input class="@error('with_reservation') is-invalid @enderror"
                                           name="with_reservation"
                                           type="checkbox"
                                           id="with_reservation_checkbox"
                                        {{ isset($event) && $event->with_reservation ? "checked" : "" }}/>
                                    <label for="role_name">Evento con riservazione</label>
                                </div>
                                <div class="form-group d-none" id="reservation_closing_date">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="role_name">Data/ora di chiusura delle riservazioni</label>
                                            <input
                                                class="form-control @error('reservation_end_at') is-invalid @enderror"
                                                name="reservation_end_at"
                                                type="datetime-local"
                                                value="{{ isset($event) ? $event->reservation_end_at : old('reservation_end_at') }}"/>
                                            @error('reservation_end_at')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="role_name">Data di inizio dell'evento</label>
                                            <input class="form-control @error('start_time') is-invalid @enderror"
                                                   name="start_time"
                                                   type="datetime-local"
                                                   value="{{ isset($event) ? $event->start_time : old('start_time') }}"/>
                                            @error('start_time')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="role_name">Data di fine dell'evento</label>
                                            <input class="form-control @error('end_time') is-invalid @enderror"
                                                   name="end_time"
                                                   id="end-time"
                                                   type="datetime-local"
                                                   value="{{ isset($event) ? $event->end_time : old('end_time') }}"/>
                                            @error('end_time')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        @if(count($company->locations) == 0)
                                            <i class="text-danger">Non hai ancora location... creane una</i>
                                        @endif
                                        <small class="text-danger">Puoi creare eventi senza location lasciando vuoto il
                                            campo location</small>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="role_name">Location</label>
                                                    <select class="form-control" name="location_id">
                                                        <option value="0">-- Evento senza location</option>
                                                        @foreach($company->locations as $location)
                                                            <option
                                                                value="{{ $location->id }}" {{ isset($event->location) && $event->location->id == $location->id ? "selected" : "" }}>{{ $location->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($event) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        function readImg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#cover_box').attr('src', e.target.result);
                    $("#cover_box").removeClass("d-none");
                }
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $(function () {
            $("#cover").change(function () {
               readImg(this);
            });

            $("#with_reservation_checkbox").on('change', function () {
                if ($(this).is(':checked')) {
                    $(this).attr('value', 'true');
                    $("#reservation_closing_date").removeClass('d-none');
                } else {
                    $(this).attr('value', 'false');
                    $("#reservation_closing_date").addClass('d-none');
                }
            });
            if ($("#with_reservation_checkbox").is(':checked')) {
                $("#with_reservation_checkbox").attr('value', 'true');
                $("#reservation_closing_date").removeClass('d-none');
            } else {
                $("#with_reservation_checkbox").attr('value', 'false');
                $("#reservation_closing_date").addClass('d-none');
            }

            $("#recurrent_checkbox").on('change', function () {
                if ($(this).is(':checked')) {
                    $(this).attr('value', 'true');
                    $("#recurrent_div").removeClass('d-none');
                } else {
                    $(this).attr('value', 'false');
                    $("#recurrent_div").addClass('d-none');
                }
            });
            if ($("#recurrent_checkbox").is(':checked')) {
                $("#recurrent_checkbox").attr('value', 'true');
                $("#recurrent_div").removeClass('d-none');
            } else {
                $("#recurrent_checkbox").attr('value', 'false');
                $("#recurrent_div").addClass('d-none');
            }
        });
    </script>
@endsection
