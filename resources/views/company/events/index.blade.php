@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>I tuoi eventi</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                Eventi
                <div class="dropdown">
                    <a href="{{ route('company.events.create', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Evento</th>
                            <th>Nr. max partecipanti</th>
                            <th>Nr. partecipanti</th>
                            <th>Età minima</th>
                            <th>con riservazione</th>
                            <th>Data fine riservazione</th>
                            <th>Data d'inizio</th>
                            <th>Data di fine</th>
                            <th>Location</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Evento</th>
                            <th>Nr. max partecipanti</th>
                            <th>Nr. partecipanti</th>
                            <th>Età minima</th>
                            <th>con riservazione</th>
                            <th>Data fine riservazione</th>
                            <th>Data d'inizio</th>
                            <th>Data di fine</th>
                            <th>Location</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($company->events as $event)
                            <tr>
                                <td>{{ $event->name }}</td>
                                <td>{{ $event->max_participants }}</td>
                                <td>{{ count($event->participants) }}</td>
                                <td>{{ $event->minimum_age }}</td>
                                <td><i class="fa fa-{{ $event->with_reservation ? 'check' : 'times' }}"></i></td>
                                <td>{{ $event->with_reservation && !is_null($event->reservation_end_at)? $event->reservation_end_at : '' }}</td>
                                <td>{{ $event->start_time }}</td>
                                <td>{{ $event->end_time }}</td>
                                <td>{{ isset($event->location) ? $event->location->name : '' }}</td>
                                <td>
                                    <a href="{{ route('company.events.participants', ['company' => $company->id, 'event' => $event->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="users"></i>
                                    </a>
                                    <a href="{{ route('company.events.edit', ['company' => $company->id, 'event' => $event->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault();
                                           document.getElementById('trash-form-{{ $event->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $event->id }}"
                                          action="{{ route('company.events.delete', ['company' => $company->id, 'event' => $event->id]) }}"
                                          method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="10" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/features.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
