@extends('layouts.company.backend')
@section('styles')
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($location))
                        <span>Modifica location</span>
                    @else
                        <span>Crea una nuova location</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($location))
                    <span>Modifica</span>
                @else
                    <span>Crea</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('company.locations.index', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($location))
                    <form
                        action="{{ route('company.locations.update', ['company' => $company->id, 'location' => $location->id]) }}"
                        method="post">
                        @else
                            <form action="{{ route('company.locations.store', ['company' => $company->id]) }}"
                                  method="post">
                                @endif
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role_name">Nome location</label>
                                            <input class="form-control @error('name') is-invalid @enderror" name="name"
                                                   type="text"
                                                   value="{{ isset($location) ? $location->name : old('name') }}"/>
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role_name">Indirizzo</label>
                                            <input class="form-control @error('address') is-invalid @enderror"
                                                   name="address"
                                                   type="text"
                                                   value="{{ isset($location) ? $location->address : old('address') }}"/>
                                            @error('address')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role_name">Cantone</label>
                                            <input class="form-control @error('state') is-invalid @enderror"
                                                   name="state"
                                                   type="text" readonly
                                                   value="{{ isset($state) ? $state->name : '' }}"/>
                                            @error('state')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role_name">Distretto</label>
                                            <select class="form-control" name="district_id">
                                                @foreach($districts as $district)
                                                    <option value="{{ $district->id }}" {{ isset($location) && $district->id == $location->district->id ? 'selected' : '' }}>{{ $district->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('district_id')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role_name">Capacità massima della location</label>
                                            <input class="form-control @error('max_capacity') is-invalid @enderror"
                                                   name="max_capacity"
                                                   type="number"
                                                   value="{{ isset($location) ? $location->max_capacity : old('max_capacity') }}"/>
                                            @error('max_capacity')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input name="has_tables" type="checkbox"
                                        {{ isset($location) && $location->has_tables ? "checked" : "" }}
                                    />
                                    <label for="role_name">Possibilità di riservare tavoli?</label>
                                </div>
                                <div class="form-group">
                                    <input name="has_restaurant" type="checkbox"
                                        {{ isset($location) && $location->has_restaurant ? "checked" : "" }}
                                    />
                                    <label for="role_name">è un bar/ristorante?</label>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($location) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
            $("#with_reservation_checkbox").on('change', function () {
                if ($(this).is(':checked')) {
                    $(this).attr('value', 'true');
                    $("#reservation_closing_date").removeClass('d-none');
                } else {
                    $(this).attr('value', 'false');
                    $("#reservation_closing_date").addClass('d-none');
                }
            });
        });
    </script>
@endsection
