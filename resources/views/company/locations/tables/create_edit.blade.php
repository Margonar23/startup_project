@extends('layouts.company.backend')
@section('styles')
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($table))
                        <span>Modifica tavolo</span>
                    @else
                        <span>Crea un nuovo tavolo</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($table))
                    <span>Modifica</span>
                @else
                    <span>Crea</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('company.locations.tables.index', ['company' => $company->id, 'location' => $location->id, 'room' => $room->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($table))
                    <form
                        action="{{ route('company.locations.tables.update', ['company' => $company->id, 'location' => $location->id, 'room' => $room->id, 'table' => $table->id]) }}"
                        method="post">
                        @else
                            <form
                                action="{{ route('company.locations.tables.store', ['company' => $company->id, 'location' => $location->id, 'room' => $room->id]) }}"
                                method="post">
                                @endif
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role_name">Nome tavolo</label>
                                            <input class="form-control @error('name') is-invalid @enderror" name="name"
                                                   type="text"
                                                   value="{{ isset($table) ? $table->name : old('name') }}"/>
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role_name">Capacità massima del tavolo</label>
                                            <input class="form-control @error('max_capacity') is-invalid @enderror"
                                                   name="max_capacity"
                                                   type="number"
                                                   value="{{ isset($table) ? $table->max_capacity : old('max_capacity') }}"/>
                                            @error('max_capacity')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select name="table_shape_id" class="form-control">
                                                @foreach($shapes as $shape)
                                                    <option
                                                        value="{{ $shape->id }}" {{ isset($table) && $table->table_shape_id == $shape->id ? "selected" : "" }}>{{ $shape->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($table) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
            $("#with_reservation_checkbox").on('change', function () {
                if ($(this).is(':checked')) {
                    $(this).attr('value', 'true');
                    $("#reservation_closing_date").removeClass('d-none');
                } else {
                    $(this).attr('value', 'false');
                    $("#reservation_closing_date").addClass('d-none');
                }
            });
        });
    </script>
@endsection
