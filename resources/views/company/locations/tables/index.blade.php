@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>Organizza i tavoli di {{ $location->name }} in {{ $room->name }}</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                Eventi
                    <div class="dropdown">
                        <a href="{{ route('company.locations.rooms.index', ['company' => $company->id, 'location' => $location->id]) }}"
                           class="btn btn-primary btn-icon btn-sm">
                            <i data-feather="skip-back"></i>
                        </a>
                    <a href="{{ route('company.locations.tables.create', ['company' => $company->id, 'location' => $location->id, 'room' => $room->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Capacità</th>
                            <th>Forma</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Nome</th>
                            <th>Capacità</th>
                            <th>Forma</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($room->tables as $table)
                            <tr>
                                <td>{{ $table->name }}</td>
                                <td>{{ $table->max_capacity }}</td>
                                <td>{{ $table->shape->name }}</td>
                                <td>
                                    <a href="{{ route('company.locations.tables.edit', ['company' => $company->id, 'location' => $location->id, 'room' => $room->id, 'table' => $table->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault();
                                           document.getElementById('trash-form-{{ $table->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $table->id }}"
                                          action="{{ route('company.locations.tables.delete', ['company' => $company->id, 'location' => $location->id, 'room' => $room->id, 'table' => $table->id]) }}"
                                          method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/tables.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
