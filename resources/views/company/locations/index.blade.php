@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>Le tue locations</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                Eventi
                <div class="dropdown">
                    <a href="{{ route('company.locations.create', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Indirizzo</th>
                            <th>Capacità</th>
                            <th>Con tavoli</th>
                            <th>Con ristorazione</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Nome</th>
                            <th>Indirizzo</th>
                            <th>Capacità</th>
                            <th>Con tavoli</th>
                            <th>Con ristorazione</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($company->locations as $location)
                            <tr>
                                <td>{{ $location->name }}</td>
                                <td>{{ $location->address }} ({{ $location->district->name }} - {{ $location->district->state->name }})</td>
                                <td>{{ $location->max_capacity }}</td>
                                <td><i class="fa fa-{{ $location->has_tables ? 'check' : 'times' }}"></i></td>
                                <td><i class="fa fa-{{ $location->has_restaurant ? 'cocktail' : '' }}"></i></td>
                                <td>

                                        <a href="{{ route('company.locations.rooms.index', ['company' => $company->id, 'location' => $location->id]) }}"
                                           class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                            <i data-feather="target"></i>
                                        </a>
                                    <a href="{{ route('company.locations.edit', ['company' => $company->id, 'location' => $location->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault();
                                           document.getElementById('trash-form-{{ $location->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $location->id }}"
                                          action="{{ route('company.locations.delete', ['company' => $company->id, 'location' => $location->id]) }}"
                                          method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/locations.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
