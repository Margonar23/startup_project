@extends('layouts.company.backend')
@section('styles')
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($room))
                        <span>Modifica zona</span>
                    @else
                        <span>Crea una nuova zona</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($room))
                    <span>Modifica</span>
                @else
                    <span>Crea</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('company.locations.rooms.index', ['company' => $company->id, 'location' => $location->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($room))
                    <form
                        action="{{ route('company.locations.rooms.update', ['company' => $company->id, 'location' => $location->id, 'room' => $room->id]) }}"
                        method="post">
                        @else
                            <form
                                action="{{ route('company.locations.rooms.store', ['company' => $company->id, 'location' => $location->id]) }}"
                                method="post">
                                @endif
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role_name">Nome zona</label>
                                            <input class="form-control @error('name') is-invalid @enderror" name="name"
                                                   type="text"
                                                   value="{{ isset($room) ? $room->name : old('name') }}"/>
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="role_name">Capacità massima della zona</label>
                                                <input class="form-control @error('max_capacity') is-invalid @enderror"
                                                       name="max_capacity"
                                                       type="number"
                                                       value="{{ isset($room) ? $room->max_capacity : old('max_capacity') }}"/>
                                                @error('max_capacity')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($room) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>

    </script>
@endsection
