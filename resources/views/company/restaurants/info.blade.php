@extends('layouts.company.backend')
@section('styles')
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>Aggiorna informazioni</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Informazioni</span>
                <div class="dropdown">
                    <a href="{{ route('company.restaurants.index', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('company.restaurants.info.store', ['company' => $company->id, 'location' => $location->id]) }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="role_name">Scegli il tipo di ristorazione</label>
                                <select class="form-control" name="restaurant_type_id">
                                    @foreach($types as $type)
                                        <option value="{{ $type->id }}"
                                            {{ !is_null($location->restaurant_info) && $location->restaurant_info->restaurant_type_id == $type->id
                                                ? 'selected'
                                                : '' }}
                                        >
                                            {{ $type->name }}</option>
                                    @endforeach
                                </select>
                                @error('restaurant_type_id')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="role_name">Scegli il tipo di ristorazione</label>
                                <select class="form-control" name="kitchen_type_id">
                                    @foreach($kitchen_types as $kitchen_type)
                                        <option value="{{ $kitchen_type->id }}"
                                            {{ !is_null($location->restaurant_info) && $location->restaurant_info->kitchen_type_id == $kitchen_type->id
                                                ? 'selected'
                                                : '' }}
                                        >
                                            {{ $kitchen_type->name }}</option>
                                    @endforeach
                                </select>
                                @error('kitchen_type_id')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="role_name">Scegli l'offerta alimentare</label>
                                <select class="form-control" name="alimentary_type[]" multiple>
                                    @foreach($alimentary_types as $alimentary_type)
                                        <option value="{{ $alimentary_type->id }}"
                                            {{ !is_null($location->restaurant_info) && collect(json_decode($location->restaurant_info->alimentary_type,true))->contains($alimentary_type->id)
                                                ? 'selected'
                                                : '' }}
                                        >
                                            {{ $alimentary_type->name }}</option>
                                    @endforeach
                                </select>
                                @error('kitchen_type_id')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="role_name">Aggiungi una descrizione alla tua attività</label>
                                <textarea name="description" class="form-control">
                                    {{ !is_null($location->restaurant_info)
                                                ? $location->restaurant_info->description
                                                : '' }}
                                </textarea>
                                @error('description')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" value="Salva" class="btn btn-success">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {

        });
    </script>
@endsection
