@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>I tuoi bar/ristoranti</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                Bar/Ristoranti
                <div class="dropdown">
                    {{--                    <a href="{{ route('company.events.create', ['company' => $company->id]) }}"--}}
                    {{--                       class="btn btn-primary btn-icon btn-sm">--}}
                    {{--                        <i data-feather="plus"></i>--}}
                    {{--                    </a>--}}
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Indirizzo</th>
                            <th>Capacità</th>
                            <th>Tipo</th>
                            <th>Cucina</th>
                            <th>Abitudini alimentari</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Nome</th>
                            <th>Indirizzo</th>
                            <th>Capacità</th>
                            <th>Tipo</th>
                            <th>Cucina</th>
                            <th>Abitudini alimentari</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($company->locations as $location)
                            @if($location->has_restaurant)
                                <tr>
                                    <td>{{ $location->name }}</td>
                                    <td>{{ $location->address }}</td>
                                    <td>{{ $location->max_capacity }}</td>
                                    <td>{{ !is_null($location->restaurant_info) ? $location->restaurant_info->restaurant_type->name : '' }}</td>
                                    <td>{{ !is_null($location->restaurant_info) ? $location->restaurant_info->kitchen_type->name : '' }}</td>
                                    <td>
                                        @if(!is_null($location->restaurant_info))
                                            @foreach(json_decode($location->restaurant_info->alimentary_type,true) as $ali_type)
                                                <img src="{{ asset(App\AlimentaryType::find($ali_type)->icon) }}" width="20px" title="{{ App\AlimentaryType::find($ali_type)->name }}">
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('company.restaurants.info', ['company' => $company->id, 'location' => $location->id]) }}"
                                           class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                            <i class="fa fa-info"></i>
                                        </a>
                                        <a href="{{ route('company.restaurants.schedule', ['company' => $company->id, 'location' => $location->id]) }}"
                                           class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                            <i class="fa fa-calendar-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endif
                        @empty
                            <tr>
                                <td colspan="5" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/locations.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
