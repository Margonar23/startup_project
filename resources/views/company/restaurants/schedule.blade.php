@extends('layouts.company.backend')
@section('styles')
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>Orari</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Gestisci orari</span>
                <div class="dropdown">
                    <a href="{{ route('company.restaurants.index', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <form
                    action="{{ route('company.restaurants.schedule.store', ['company' => $company->id, 'location' => $location->id]) }}"
                    method="post">
                    @csrf
                    <div class="form-group">
                        <table class="table table-striped">
                            <thead>
                            <th></th>
                            <th>Orario di apertura</th>
                            <th>Orario di chiusura</th>
                            </thead>
                            <tbody>
                            @foreach($days as $day)
                                <tr>
                                    <td>
                                        <label for="role_name">{{ $day->name }}</label>
                                    </td>
                                    <td>
                                        <input class="form-control" type="time" name="{{ $day->id }}_opening_at" value="{{ count($location->restaurant->where('day_id', $day->id)) ? $location->restaurant->where('day_id', $day->id)->first()->opening_at : null}}">
                                    </td>
                                    <td>
                                        <input class="form-control" type="time" name="{{ $day->id }}_closing_at" value="{{ count($location->restaurant->where('day_id', $day->id)) ? $location->restaurant->where('day_id', $day->id)->first()->closing_at  : null}}">
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary"
                               value="{{ isset($schedule) ? trans('general.update') : trans('general.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
            $(".day_checkbox").on('change', function () {
                if ($(this).is(':checked')) {
                    $("#" + $(this).val() + "_opening_at").removeClass('d-none');
                    $("#" + $(this).val() + "_closing_at").removeClass('d-none');
                } else {
                    $("#" + $(this).val() + "_opening_at").addClass('d-none');
                    $("#" + $(this).val() + "_closing_at").addClass('d-none');
                }
            });
            if ($("#with_reservation_checkbox").is(':checked')) {
                $("#with_reservation_checkbox").attr('value', 'true');
                $("#reservation_closing_date").removeClass('d-none');
            } else {
                $("#with_reservation_checkbox").attr('value', 'false');
                $("#reservation_closing_date").addClass('d-none');
            }

        });
    </script>
@endsection
