@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>Modifica la tua azienda</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Modifica i tuoi dati</span>
                <div class="dropdown">
                    <a href="{{ route('company.dashboard') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                    </div>
                    <form action="{{ route('company.update', ['company' => $company->id]) }}" method="post">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="img-box">
                                    <img src="{{ asset($company->logo) }}" class="img-thumbnail">
                                    <div class="overlay img-thumbnail">
                                        <a href="{{ route('company.edit_logo', ['company' => $company->id]) }}"
                                           class="icon" title="Modifica logo">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @csrf
                            <div class="col-md-9">
                                <label for="role_name">Nome azienda</label><br>
                                <input type="text" class="form-control" name="name"
                                       value="{{ isset($company->name) ? $company->name : '' }}">
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-12 mt-2">
                                <label for="role_name">Indirizzo</label><br>
                                <input type="text" class="form-control" name="address"
                                       value="{{ isset($company->address) ? $company->address : '' }}">
                                @error('address')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6 mt-2">
                                <label for="role_name">E-Mail di contatto</label><br>
                                <input type="text" class="form-control" name="email"
                                       value="{{ isset($company->email) ? $company->email : '' }}">
                                @error('email')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6 mt-2">
                                <label for="role_name">Sito web</label><br>
                                <input type="text" class="form-control" name="website"
                                       value="{{ isset($company->website) ? $company->website : '' }}">
                                @error('website')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6 mt-2">
                                <label for="role_name">Telefono</label><br>
                                <input type="text" class="form-control" name="phone"
                                       value="{{ isset($company->phone) ? $company->phone : '' }}">
                                @error('phone')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6 mt-2">
                                <label for="role_name">Fax</label><br>
                                <input type="text" class="form-control" name="fax"
                                       value="{{ isset($company->fax) ? $company->fax : '' }}">
                                @error('fax')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-4 mt-2">
                                <label for="role_name">Categoria</label><br>
                                <select name="category_id" class="form-control">
                                    @foreach($categories as $category)
                                        <option
                                            value="{{ $category->id }}" {{ $company->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4 mt-2">
                                <label for="role_name">Classificazione</label><br>
                                <select name="classification_id" class="form-control">
                                    @foreach($classifications as $classification)
                                        <option
                                            value="{{ $classification->id }}" {{ $company->classification_id == $classification->id ? 'selected' : '' }}>{{ $classification->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4 mt-2">
                                <label for="role_name">Distretto</label><br>
                                <select name="district_id" class="form-control">
                                    @foreach($districts as $district)
                                        <option
                                            value="{{ $district->id }}" {{ $company->district_id == $district->id ? 'selected' : '' }}>{{ $district->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 mt-2">
                                <input type="submit" class="btn btn-primary" value="Salva">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
