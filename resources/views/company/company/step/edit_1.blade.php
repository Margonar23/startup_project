@extends('layouts.company.backend')
@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/dropzone-5.7.0/dist/dropzone.css') }}">
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>Inserisci il logo della tua azienda</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Step 1</span>

                <div class="dropdown">
                    <a href="{{ route('company.index', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                    <a href="{{ route('company.edit', ['company' => $company->id, 'step' => 2]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-forward"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        @if(!is_null($company->logo))
                            <img src="{{ asset($company->logo) }}" width="100%" class="img-fluid">
                        @endif
                    </div>
                    <div class="col-md-10">
                        <h3>{{ $company->name }}</h3>
                        <small>{{ $company->label }}</small><br>
                        <small>{{ $company->uid }}</small>
                    </div>
                </div>
                <hr>
                <add-logo :company="{{ $company->id }}"></add-logo>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent

@endsection
