@extends('layouts.company.backend')
@section('styles')

@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>Inserisci il logo della tua azienda</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Step 2</span>

                <div class="dropdown">
                    <a href="{{ route('company.edit', ['company' => $company->id, 'step' => 1]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>

                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{ asset($company->logo) }}" width="100%" class="img-fluid">
                    </div>
                    <div class="col-md-10">
                        <h3>{{ $company->name }}</h3>
                        <small>{{ $company->label }}</small><br>
                        <small>{{ $company->uid }}</small>
                    </div>
                </div>
                <hr>
                <form action="{{ route('company.update', ['company' => $company->id]) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="role_name">Nome completo</label>
                                <input class="form-control @error('name') is-invalid @enderror" name="name"
                                       type="text"
                                       value="{{ isset($company->name) ? $company->name : old('name') }}" readonly/>
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="role_name">Numero registro di commercio</label>
                                <small for="role_name" class="text-success">Questo numero ci serve per verificare la tua
                                    azienda</small>
                                <input class="form-control @error('uid') is-invalid @enderror" name="uid"
                                       type="text"
                                       value="{{ isset($company->uid) ? $company->uid : old('uid') }}" readonly/>
                                @error('uid')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="role_name">Categoria</label>
                                <select name="category_id" class="form-control">
                                    @foreach($categories as $category)
                                        <option
                                            value="{{ $category->id }}" {{ $company->category_id == $category->id ? 'selected' : '' }}>
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="role_name">Classificazione</label>
                                <select name="classification_id" class="form-control">
                                    @foreach($classifications as $classification)
                                        <option
                                            value="{{ $classification->id }}" {{ $company->classification_id == $classification->id ? 'selected' : '' }}>
                                            {{ $classification->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="role_name">E-Mail di contatto</label>
                                <input class="form-control @error('email') is-invalid @enderror" name="email"
                                       type="text"
                                       value="{{ isset($company->email) ? $company->email : old('email') }}"/>
                                @error('email')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <label for="role_name">Numero di telefono</label>
                                <input class="form-control @error('phone') is-invalid @enderror" name="phone"
                                       type="text"
                                       value="{{ isset($company->phone) ? $company->phone : old('phone') }}"/>
                                @error('phone')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <label for="role_name">Fax</label>
                                <input class="form-control @error('fax') is-invalid @enderror" name="fax"
                                       type="text"
                                       value="{{ isset($company->fax) ? $company->fax : old('fax') }}"/>
                                @error('fax')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="role_name">Website</label>
                                <input class="form-control @error('website') is-invalid @enderror" name="website"
                                       type="text"
                                       value="{{ isset($company->website) ? $company->website : old('website') }}"/>
                                @error('website')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="role_name">Indirizzo di corrispondenza completo</label>
                                <input class="form-control @error('address') is-invalid @enderror" name="address"
                                       type="text"
                                       value="{{ isset($company->address) ? $company->address : old('address') }}"/>
                                @error('address')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="role_name">Distretto</label>
                                <select name="district_id" class="form-control">
                                    @foreach($districts as $district)
                                        <option
                                            value="{{ $district->id }}" {{ $company->district_id == $district->id ? 'selected' : '' }}>
                                            {{ $district->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="{{ trans('general.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
