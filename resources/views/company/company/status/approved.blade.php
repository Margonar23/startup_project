@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-success">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>La tua richiesta è stata accettata!</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Benvenuto/a {{ $company->name }}! Iniziamo...</span>

                <div class="dropdown">
                    <a href="{{ route('company.dashboard') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        <h1>{{ $company->name }}</h1>
                        @if(!is_null($company->logo))
                            <img src="{{ asset($company->logo) }}" width="100%" class="img-thumbnail img-fluid">
                        @endif
                    </div>
                    <div class="col-md-10">
                        <p>Scegli il pacchetto che fa per te...</p>
                        <div class="row">
                            @foreach($packages as $package)
                                <div class="col-md-4">
                                    <div class="card text-center">
                                        <div class="card-header bg-{{ $package->color }}">
                                            <span class="text-black-50">{{ $package->name }}</span>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-text">{{ $package->description }}</h5>
                                            <ul class="list-group text-left">
                                                @foreach($package->features as $feature)
                                                    <li class="list-group-item">
                                                        <i class="fa fa-check-circle text-success"></i>
                                                        {{ $feature->name }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <br>
                                            <div class="btn-group">
                                                <a href="{{ route('packages') }}" class="btn btn-outline-primary btn-xs">Scopri di piu</a>
                                                <a href="{{ route('company.subscription', ['company' => $company->id, 'package' => $package->id]) }}" class="btn btn-{{ $package->color }} btn-xs">Seleziona</a>
                                            </div>
                                        </div>
                                        <div class="card-footer text-muted bg-{{ $package->color }} text-black-50">
                                            <div class="pricing-price">
                                                <sup>CHF</sup>
                                                {{ $package->package_prices->filter(function ($price, $index){
                                                        return $price->payment_type_id == 1;
                                                })->first()->price }}
                                                <span class="pricing-price-period">/mese</span>
                                            </div>
                                            <div class="text-xs pricing-price">
                                                <sup>CHF</sup>
                                                {{ $package->package_prices->filter(function ($price, $index){
                                                        return $price->payment_type_id == 2;
                                                })->first()->price }}
                                                <span class="pricing-price-period">/anno</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
