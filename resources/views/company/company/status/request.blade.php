@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>La tua richiesta è stata inviata</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Verifica che i dati siano corretti</span>

                <div class="dropdown">
                    <a href="{{ route('company.dashboard') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="jumbotron">
                    <h1 class="display-4">Richiesta effettuata con successo!</h1>
                    <p class="lead">Il nostro staff sta verificando la tua richiesta, riceverà una e-mail ad ogni cambio di stato.</p>
                    <hr class="my-4">
                    <p>Hai domande? </p>
                    <a class="btn btn-primary btn-lg" href="#" role="button">Contattaci</a>
                </div>

                <table class="table table-striped table-responsive">
                    <tr>
                        <th>Nome</th>
                        <td>{{ $company->name }}</td>
                    </tr>
                    <tr>
                        <th>Label</th>
                        <td>{{ $company->label }}</td>
                    </tr>
                    <tr>
                        <th>Numero registro di commercio</th>
                        <td>{{ $company->uid }}</td>
                    </tr>
                    <tr>
                        <th>Richiedente</th>
                        <td>{{ $company->user->name() }}</td>
                    </tr>
                </table>
                <a href="{{ route('company.edit', ['company' => $company->id, 'step' => 1]) }}" class="btn btn-sm btn-outline-primary">Continua ad inserire i dati della tua azienda mentre noi facciamo la verifica.</a>
            </div>
        </div>
    </div>
@endsection
