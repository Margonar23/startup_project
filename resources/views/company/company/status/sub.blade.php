@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-success">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>
                        @if(!is_null($company->company_package->package))
                            Pacchetto {{ $company->company_package->package->name }}
                        @endif
                    </span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>{{ $company->name }}</span>
                <div class="dropdown">
                    <a href="{{ route('company.dashboard') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                    <a href="{{ route('company.edit_data', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="edit"></i>
                    </a>
                    <a href="{{ route('company.delete_page', ['company' => $company->id]) }}"
                       class="btn btn-danger btn-icon btn-sm">
                        <i data-feather="trash"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        <h1>{{ $company->name }}</h1>
                        @if(!is_null($company->logo))
                            <img src="{{ asset($company->logo) }}" width="100%" class="img-thumbnail img-fluid">
                        @endif
                    </div>
                    <div class="col-md-10">
                        <p>Dettagli</p>
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-responsive table-striped">
                                    <tr>
                                        <th>Pacchetto</th>
                                        <td>{{ $company->company_package->package->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Scadenza</th>
                                        <td>{{ $company->subscription->subscription_expiry_at }}</td>
                                    </tr>
                                    <tr>
                                        <th>Metodo di pagamento</th>
                                        <td>{{ $company->subscription->payment_method->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tipo di pagamento</th>
                                        <td>{{ $company->subscription->payment_type->name }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-responsive table-striped">
                                    @foreach($company->company_package->package->features as $feature)
                                        <tr>
                                            <th>{{ $feature->name }}</th>
                                            <td>{{ $feature->description }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Fatture</h1>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Totale</th>
                                <th></th>
                                <th></th>
                                <th>Numero fattura</th>
                                <th>Cliente</th>
                                <th>Periodo</th>
                                <th>Data emissione</th>
                                <th></th>
                            </tr>
                            </thead>
                            @foreach($invoices as $invoice)
                                <tr>
                                    <td>{{ $invoice->total() }}</td>
                                    <td>{{ $invoice->currency }}</td>
                                    <td>
                                        @if($invoice->amount_remaining == 0)
                                            <span class="badge badge-success">{{ $invoice->status }}</span>
                                        @else
                                            <span class="badge badge-warning">{{ $invoice->status }}</span>
                                        @endif
                                    </td>
                                    <td>{{ $invoice->receipt_number }}</td>
                                    <td>{{ $invoice->customer_email }}</td>
                                    <td>{{ $invoice->next_payment_attempt }}</td>
                                    <td>{{ $invoice->date()->toFormattedDateString() }}</td>
                                    <td><a href="{{ route('company.invoice.download', ['invoice' => $invoice->id]) }}">Download</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
