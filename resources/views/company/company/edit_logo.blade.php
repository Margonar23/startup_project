@extends('layouts.company.backend')
@section('styles')
    <link href="{{ asset('vendor/dropzone-5.7.0/dist/min/dropzone.min.css') }}">
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>Modifica logo</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Modifica il tuo logo</span>
                <div class="dropdown">
                    <a href="{{ route('company.edit_data', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{ asset($company->logo) }}" width="100%">
                    </div>
                    <div class="col-md-8" >
                        <form action="{{ route('company.insert_company_logo', ['company' => $company->id]) }}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="col-md-12">
                                <input type="file" name="file" />
                            </div>
                            <div class="col-md-12 mt-3">
                                <input type="submit" value="Aggiorna" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
