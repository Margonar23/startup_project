@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-danger">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="trash"></i>
                    </div>
                    <span>Vuoi davvero eliminare la tua azienda?</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Puoi cambiare idea quando vuoi.. i tuoi dati non saranno cancellati e sarà possibile ricuperarli in seguito</span>

                <div class="dropdown">
                    <a href="{{ route('company.index', ['company' => $company->id]) }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <a href="{{ route('company.delete', ['company' => $company->id]) }}" class="btn btn-danger">Disattiva la tua azienda</a>

            </div>
        </div>
    </div>
@endsection
