@extends('layouts.company.backend')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/payment.css') }}">
@endsection
@section('content')
    <div class="page-header page-header-dark bg-{{ $package->color }}">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span class="text-black-50">Bene, hai scelto il pacchetto {{ $package->name }}!</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        @error('alert-danger')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Inserisci i dettagli di pagamento</span>

                <div class="dropdown">
                    <a href="{{ route('company.index', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        <h1>{{ $company->name }}</h1>
                        @if(!is_null($company->logo))
                            <img src="{{ asset($company->logo) }}" width="100%" class="img-thumbnail img-fluid">
                        @endif
                    </div>

                    <div class="col-md-4">
                        <div class="card credit-card-box">
                            <div class="card-header display-table">
                                <div class="row display-tr">
                                    <h3 class="card-title display-td">Informazioni di pagamento</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="font-weight-bold"> Scegli il tipo di pagamento:</p>
                                @foreach($package->package_prices as $price)
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="payment_type_id"
                                               id="payment_type_id" value="{{ $price->payment_type->id }}">
                                        <label class="form-check-label" for="payment_type_id">
                                            {{ $price->price }} CHF/{{ $price->payment_type->name }}
                                        </label>
                                    </div>
                                @endforeach
                                <span class="text-danger d-none" id="payment_type_error_msg">Scegli il tipo di pagamento.</span>
                                <br>
                                <br>
                                <p class="font-weight-bold">Scegli il metodo di pagamento</p>
                                @foreach($payment_methods as $payment_method)
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="payment_method_id"
                                               id="payment_method_id" value="{{ $payment_method->id }}"
                                               data-type="{{ $payment_method->label }}">
                                        <label class="form-check-label" for="payment_type_id">
                                            <i class="fa fa-{{ $payment_method->icon }}"></i> {{ $payment_method->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <!-- CREDIT CARD FORM STARTS HERE -->
                        <div class="card credit-card-box d-none payment-type-box" id="credit_card">
                            <div class="card-header display-table">
                                <div class="row display-tr">
                                    <h3 class="card-title display-td">Payment Details</h3>
                                    <div class="display-td">
                                        <img class="img-responsive pull-right"
                                             src="{{ asset('images/payment_method/credit-card.png') }}"
                                             width="35px">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="post"
                                    action="{{ route('company.subscription.store.card', ['company' => $company->id, 'package' => $package->id]) }}"
                                    id="payment-form">
                                    @csrf
                                    <input type="hidden" class="payment_type_id" name="payment_type_id"/>
                                    <input type="hidden" class="payment_method_id" name="payment_method_id"/>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i
                                                                class="fa fa-user"></i></div>
                                                    </div>
                                                    <input type="text" class="form-control"
                                                           name="username"
                                                           placeholder="Nome completo">
                                                </div>
                                                @error('username')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i
                                                                class="fa fa-envelope"></i></div>
                                                    </div>
                                                    <input type="text" class="form-control"
                                                           name="email"
                                                           placeholder="Indirizzo e-mail a cui inviare la fattura">
                                                </div>
                                                @error('email')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="cardExpiry">
                                                <span class="hidden-xs">Indirizzo completo</span>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fa fa-map-marker"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control"
                                                           name="route"
                                                           placeholder="Indirizzo">
                                                </div>
                                                @error('route')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control"
                                                           name="cap"
                                                           placeholder="CAP">
                                                </div>
                                                @error('cap')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control"
                                                           name="city"
                                                           placeholder="Città">
                                                </div>
                                                @error('city')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i
                                                                class="fa fa-flag"></i></div>
                                                    </div>
                                                    <input type="text" class="form-control"
                                                           name="country"
                                                           value="Svizzera" readonly>
                                                </div>
                                                @error('country')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label for="couponCode">COUPON CODE</label>
                                                <input type="text" class="form-control" name="couponCode"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div id="card-element"></div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span class="payment-errors" id="card-errors"
                                                  style="color: red;margin-top:10px;"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <input type="submit" class="subscribe btn btn-success btn-lg btn-block" id="card-button" value="Start Subscription">
                                        </div>
                                    </div>
                                    <div class="row" style="display:none;">
                                        <div class="col-xs-12">
                                            <p class="payment-errors"></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card credit-card-box payment-type-box d-none" id="paypal">
                            <div class="card-header display-table">
                                <div class="row display-tr">
                                    <h3 class="card-title display-td">Pay Pal</h3>
                                    <div class="display-td">
                                        <img class="img-responsive pull-right"
                                             src="{{ asset('images/payment_method/paypal.svg') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                paypal button
                            </div>
                        </div>
                        <div class="card credit-card-box payment-type-box d-none" id="invoice">
                            <div class="card-header display-table">
                                <div class="row display-tr">
                                    <h3 class="card-title display-td">Informazioni per la fattura</h3>
                                    <div class="display-td">

                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="post"
                                      action="{{ route('company.subscription.store.invoice', ['company' => $company->id, 'package' => $package->id]) }}"
                                      class="form-billing-info">
                                    @csrf
                                    <input type="hidden" class="payment_type_id" name="payment_type_id"/>
                                    <input type="hidden" class="payment_method_id" name="payment_method_id"/>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i
                                                                class="fa fa-user"></i></div>
                                                    </div>
                                                    <input type="text" class="form-control"
                                                           name="username"
                                                           placeholder="Nome completo">
                                                </div>
                                                @error('username')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i
                                                                class="fa fa-envelope"></i></div>
                                                    </div>
                                                    <input type="text" class="form-control"
                                                           name="email"
                                                           placeholder="Indirizzo e-mail a cui inviare la fattura">
                                                </div>
                                                @error('email')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="cardExpiry">
                                                <span class="hidden-xs">Indirizzo completo</span>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fa fa-map-marker"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control"
                                                           name="route"
                                                           placeholder="Indirizzo">
                                                </div>
                                                @error('route')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control"
                                                           name="cap"
                                                           placeholder="CAP">
                                                </div>
                                                @error('cap')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control"
                                                           name="city"
                                                           placeholder="Città">
                                                </div>
                                                @error('city')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i
                                                                class="fa fa-flag"></i></div>
                                                    </div>
                                                    <input type="text" class="form-control"
                                                           name="country"
                                                           value="Svizzera" readonly>
                                                </div>
                                                @error('country')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label for="couponCode">COUPON CODE</label>
                                                <input type="text" class="form-control" name="couponCode"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input class="subscribe btn btn-success btn-lg btn-block"
                                                   type="submit" value="Start Subscription">
                                        </div>
                                    </div>
                                </form>
                                <div class="row" style="display:none;">
                                    <div class="col-xs-12">
                                        <p class="payment-errors"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- CREDIT CARD FORM ENDS HERE -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        $(document).ready(function () {
            $('input[type=radio][name=payment_method_id]').change(function () {
                $(".payment-type-box").each(function () {
                    $(this).addClass('d-none');
                });
                var element = $(this).data('type');
                $("#" + element).removeClass('d-none');
            });
            $(".form-billing-info").on('submit', function (e) {
                e.preventDefault();
                e.stopPropagation();

                let payment_method_id = $("input[type=radio][name=payment_method_id]:checked").val();
                let payment_type_id = $("input[type=radio][name=payment_type_id]:checked").val();

                if (typeof payment_type_id === 'undefined') {
                    $("#payment_type_error_msg").removeClass('d-none');
                } else {
                    $(".payment_method_id").val(payment_method_id);
                    $(".payment_type_id").val(payment_type_id);
                    this.submit();
                }


            })

            var style = {
                base: {
                    color: '#32325d',
                    lineHeight: '18px',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            const stripe = Stripe('{{ env("STRIPE_KEY") }}', {locale: 'ch'}); // Create a Stripe client.
            const elements = stripe.elements(); // Create an instance of Elements.
            const card = elements.create('card', {hidePostalCode: true, style: style}); // Create an instance of the card Element.

            card.mount('#card-element'); // Add an instance of the card Element into the `card-element` <div>.

            card.on('change', function (event) {
                var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });

            // Handle form submission.
            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function (event) {
                event.preventDefault();

                stripe.createToken(card).then(function (result) {
                    if (result.error) {
                        // Inform the user if there was an error.
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        // Send the token to your server.
                        stripeTokenHandler(result.token);
                    }
                });
            });

            // Submit the form with the token ID.
            function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);
                let payment_method_id = $("input[type=radio][name=payment_method_id]:checked").val();
                let payment_type_id = $("input[type=radio][name=payment_type_id]:checked").val();

                if (typeof payment_type_id === 'undefined') {
                    $("#payment_type_error_msg").removeClass('d-none');
                } else {
                    $(".payment_method_id").val(payment_method_id);
                    $(".payment_type_id").val(payment_type_id);
                    form.submit();
                }
            }

        });
    </script>
@endsection
