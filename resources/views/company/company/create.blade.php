@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>Crea la tua azienda</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>Inserisci tutti i dati della tua azienda</span>

                <div class="dropdown">
                    <a href="{{ route('company.dashboard') }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">

                <form action="{{ route('company.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="role_name">Nome completo</label>
                        <input class="form-control @error('name') is-invalid @enderror" name="name"
                               type="text"
                               value="{{ old('name') }}"/>
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="role_name">Numero registro di commercio</label>
                        <small for="role_name" class="text-success">Questo numero ci serve per verificare la tua azienda</small>
                        <input class="form-control @error('uid') is-invalid @enderror" name="uid"
                               type="text"
                               value="{{ old('uid') }}"/>
                        @error('uid')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary"
                               value="{{ trans('general.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
