@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($company_sub_role))
                        <span>Modifica ruolo</span>
                    @else
                        <span>Crea un nuovo ruolo</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($company_sub_role))
                    <span>Modifica</span>
                @else
                    <span>Crea</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('company.roles.index', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($company_sub_role))
                    <form
                        action="{{ route('company.roles.update', ['company' => $company->id, 'company_sub_role' => $company_sub_role->id]) }}"
                        method="post">
                        @else
                            <form action="{{ route('company.roles.store', ['company' => $company->id]) }}"
                                  method="post">
                                @endif
                                @csrf
                                <div class="form-group">
                                    <label for="role_name">Nome ruolo</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name"
                                           type="text"
                                           value="{{ isset($company_sub_role) ? $company_sub_role->name : old('name') }}"/>
                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <label>Scegli i permessi disponibili nel tuo pacchetto</label><br>
                                @foreach($company->packages->first()->features as $feature)
                                    <b>{{ $feature->name }}</b>:<br>

                                    @foreach($feature->permissions as $permission)
                                        <div class="custom-control custom-checkbox custom-control-solid">
                                            <input class="custom-control-input"
                                                   id="{{ $permission->permission->label }}" name="permissions[]"
                                                   value="{{$permission->permission->id}}"
                                                   {{ isset($company_sub_role) ? collect($company_sub_role->permissions)->pluck('permission_id')->contains($permission->permission->id) ? 'checked' : '' : ''}}
                                                   type="checkbox">
                                            <label class="custom-control-label"
                                                   for="{{ $permission->permission->label }}">
                                                {{ $permission->permission->name }}
                                            </label>
                                        </div>
                                    @endforeach
                                    <br>
                                @endforeach

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($feature) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
