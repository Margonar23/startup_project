<div class="template_container">
    @if(!isset($page->page_content) && is_null($page->page_content))
    <form action="{{ route('company.presentation.save_content', ['company' => $company->id, 'page' => $page->id]) }}"
          method="post" enctype="multipart/form-data">
        @else
            <form action="{{ route('company.presentation.update_content', ['company' => $company->id, 'page' => $page->id]) }}"
                  method="post" enctype="multipart/form-data">
            @endif
        @csrf
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label for="role_name">Titolo</label><br>
                    <input type="text" class="form-control" name="title"
                           value="{{ isset($page->page_content) ? $page->page_content->title : '' }}">
                    @error('title')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-md-12">
                    <label for="role_name">Sottotitolo</label><br>
                    <input type="text" class="form-control" name="subtitle"
                           value="{{ isset($page->page_content) ? $page->page_content->subtitle : '' }}">
                    @error('subtitle')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="row">
                @if(!is_null($page->page_content) && !is_null($page->page_content->img))
                    <div class="col-md-4">
                        <img src="{{ asset($page->page_content->img) }}" width="100%" class="img-responsive">
                    </div>
                @endif
                <div class="col-md-12">
                    <label for="role_name">Immagine</label><br>
                    <input type="file" class="form-control" name="image">
                    @error('image')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="role_name">Contenuto</label><br>
                    <textarea class="form-control summernote"
                              name="pc">{!! isset($page->page_content) ? $page->page_content->content : '' !!}</textarea>
                    @error('pc')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <input type="submit" class="btn btn-primary" value="Salva">
                </div>
            </div>
        </div>
    </form>
</div>
