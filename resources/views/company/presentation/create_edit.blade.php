@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($page))
                        <span>Modifica pagina</span>
                    @else
                        <span>Crea una nuova pagina </span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($page))
                    <span>Modifica</span>
                @else
                    <span>Crea</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('company.presentation', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($page))
                    <form
                        action="{{ route('company.presentation.update', ['company' => $company->id, 'page' => $page->id]) }}"
                        method="post">
                        @else
                            <form action="{{ route('company.presentation.store', ['company' => $company->id]) }}"
                                  method="post">
                                @endif
                                @csrf
                                <div class="form-group">
                                    <label for="role_name">Nome pagina</label>
                                    <input class="form-control @error('page_name') is-invalid @enderror"
                                           name="page_name"
                                           type="text"
                                           value="{{ isset($page) ? $page->page_name : old('page_name') }}"/>
                                    @error('page_name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <label for="role_name">Scegli il template</label>
                                <div class="form-group form-inline">
                                    @foreach($templates as $template)
                                        <div class="form-check ml-3 mt-3">
                                            <div class="card" style="width: 18rem; height: 18rem; padding: 5px">
                                                <img
                                                    src="{{ asset($template->img) }}"
                                                    class="card-img-top" alt="...">
                                                <div class="card-body">
                                                    <p class="card-text text-center">
                                                        <label class="form-check-label" for="template_id">
                                                            {{ $template->name }}
                                                        </label>
                                                        <input class="form-check-input" type="radio" name="template_id"
                                                               value="{{ $template->id }}" {{ isset($page) && $page->template_id == $template->id ? 'checked' : '' }}>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                @error('template_id')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                                <div class="form-group mt-3">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($page) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
