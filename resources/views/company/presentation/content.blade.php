@extends('layouts.company.backend')
@section('styles')
    <link rel="stylesheet" href="{{ asset('vendor/summernote-0.8.16-dist/summernote-bs4.min.css') }}">
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    <span>Crea o modifica la pagina {{ $page->page_name }} </span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                <span>{{ $page->page_name }}</span>
                <div class="dropdown">
                    <a href="{{ route('company.presentation', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @include('company.presentation.template.'.$page->template->template, ['page' => $page])
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
        <script src="{{ asset('vendor/summernote-0.8.16-dist/summernote-bs4.min.js') }}"></script>
        <script
            src="{{ asset('vendor/summernote-0.8.16-dist/lang/summernote-'.app()->getLocale().'-'.strtoupper(app()->getLocale()).'.js') }}"></script>
        <script>
            $(document).ready(function () {
                $(".summernote").summernote({
                    height: 200,
                    dialogsInBody: true,
                    lang: '{{ app()->getLocale() }}-{{ strtoupper(app()->getLocale()) }}',
                    callbacks: {
                        onInit: function () {
                            $('body > .note-popover').hide();
                        }
                    },
                });
            });
        </script>
@endsection
