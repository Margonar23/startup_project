@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>Crazione della tua pagina publica</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                Creazione delle pagine
                <div class="dropdown">
                    <a href="{{ route('company.presentation.create', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nome pagina</th>
                            <th>Label</th>
                            <th>Template</th>
                            <th>Attivato</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Nome pagina</th>
                            <th>Label</th>
                            <th>Template</th>
                            <th>Attivato</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($company->presentation_pages as $page)
                            <tr>
                                <td>{{ $page->page_name }}</td>
                                <td>{{ $page->label }}</td>
                                <td>{{ isset($page->template) ? $page->template->name : '' }}</td>
                                <td><i class="fa fa-{{ $page->active ? 'check' : 'times' }}"></i> </td>
                                <td>
                                    <a href="{{ route('company.presentation.content', ['company' => $company->id, 'page' => $page->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i class="fa fa-file"></i>
                                    </a>
                                    <a href="{{ route('company.presentation.change_status', ['company' => $company->id, 'page' => $page->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i class="fa fa-{{ $page->active ? 'eye-slash' : 'eye' }}"></i>
                                    </a>
                                    <a href="{{ route('company.presentation.edit', ['company' => $company->id, 'page' => $page->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault();
                                           document.getElementById('trash-form-{{ $page->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $page->id }}"
                                          action="{{ route('company.presentation.delete', ['company' => $company->id, 'page' => $page->id]) }}"
                                          method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="bg-gradient-primary-to-secondary text-white">
                                    {{ trans('backend/features.empty') }}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
