@extends('layouts.company.backend')
@section('styles')
    <link rel="stylesheet" href="{{ asset('vendor/summernote-0.8.16-dist/summernote-bs4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
@endsection
@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($job_offer))
                        <span>Modifica offerta</span>
                    @else
                        <span>Crea una nuova offerta</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($job_offer))
                    <span>Modifica</span>
                @else
                    <span>Crea</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('company.hr.index', ['company' => $company->id]) }}" class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($job_offer))
                    <form action="{{ route('company.hr.update', ['company' => $company->id, 'job_offer' => $job_offer->id]) }}" method="post" enctype="multipart/form-data">
                        @else
                            <form action="{{ route('company.hr.store', ['company' => $company->id]) }}" method="post" enctype="multipart/form-data">
                                @endif
                                @csrf
                                <div class="form-group">
                                    <label for="role_name">Titolo</label>
                                    <input class="form-control @error('title') is-invalid @enderror"
                                           name="title"
                                           type="text"
                                           value="{{ isset($job_offer) ? $job_offer->title : old('title') }}"/>
                                    @error('title')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">Descrizione</label>
                                    <textarea class="form-control summernotes @error('description') is-invalid @enderror"
                                              name="description">
                                                {{ isset($job_offer) ? $job_offer->description : old('description') }}
                                            </textarea>
                                    @error('description')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">Scadenza</label>
                                    <input class="form-control @error('offer_expiry_at') is-invalid @enderror"
                                           name="offer_expiry_at"
                                           type="date"
                                           value="{{ isset($job_offer) ? \Carbon\Carbon::parse($job_offer->offer_expiry_at)->format('Y-m-d') : \Carbon\Carbon::parse(old('offer_expiry_at'))->format('Y-m-d') }}"/>
                                    @error('offer_expiry_at')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_name">Visibilità</label><br>
                                    <input name="is_public" type="checkbox"
                                        {{ isset($job_offer) && $job_offer->is_public ? "checked" : "" }}
                                    />
                                    <small for="role_name">Cliccare per mostrare l’offerta di lavoro</small>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="role_name">Tipo di impiego</label>
                                            <select name="employment_id" class="form-control" data-live-search="true" id="state_list">
                                                @foreach($employments as $employment)
                                                    <option value="{{ $employment->id }}"
                                                        {{ isset($job_offer) ?
                                                                $job_offer->employment->id == $employment->id ? 'selected' : ''
                                                                : '' }}>
                                                        {{ $employment->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('employment_id')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="role_name">Professione ricercata</label>
                                            <select name="profession_id" class="form-control" data-live-search="true" id="staste_list">
                                                @foreach($professions as $profession)
                                                    <option value="{{ $profession->id }}"
                                                        {{ isset($job_offer) ?
                                                                $job_offer->profession_id == $profession->id ? 'selected' : ''
                                                                : '' }}>
                                                        {{ $profession->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('profession_id')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="role_name">Esperienza minima richiesta</label>
                                            <input class="form-control @error('min_experience') is-invalid @enderror"
                                                   name="min_experience"
                                                   type="number"
                                                   value="{{ isset($job_offer) ? $job_offer->min_experience : old('min_experience') }}"/>
                                            @error('min_experience')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="role_name">Requisiti / Skills</label>
                                            <select name="skills[]" class="form-control skills" multiple>
                                                @foreach($skills as $skill)
                                                    <option value="{{ $skill->id }}" id="{{ $skill->id }}"
                                                        {{ isset($job_offer) && collect(json_decode($job_offer->skills,true))->contains($skill->id) ? "selected" : "" }}
                                                    >
                                                        {{ $skill->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('skills')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($news) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{asset('vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('vendor/summernote-0.8.16-dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('vendor/summernote-0.8.16-dist/lang/summernote-'.app()->getLocale().'-'.strtoupper(app()->getLocale()).'.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".skills").select2();
            $(".summernotes").summernote({
                height: 200,
                dialogsInBody: true,
                lang: '{{ app()->getLocale() }}-{{ strtoupper(app()->getLocale()) }}',
                callbacks:{
                    onInit:function(){
                        $('body > .note-popover').hide();
                    }
                },
            });
        });
    </script>
@endsection
