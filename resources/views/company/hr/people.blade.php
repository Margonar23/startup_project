@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>Candidati</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="jumbotron pt-3 pb-2">
            <h2>{{ $job_offer->title }}</h2>
            <p>{{ $job_offer->description }}</p>

            <ul class="list-group list-group-horizontal-sm mb-2">
                <li class="list-group-item">{{ $job_offer->employment->name }}</li>
                <li class="list-group-item">{{ $job_offer->profession->name }}</li>
                <li class="list-group-item">{{ $job_offer->min_experience }} anni di esperienza</li>
                <li class="list-group-item">Scade
                    il {{ \Carbon\Carbon::parse($job_offer->offer_expiry_at)->format('d M Y') }}</li>
                <li class="list-group-item">{{ $job_offer->is_public ? 'Offerta publica' : 'Offerta privata' }}</li>
            </ul>
            <div class="form-inline mb-4">
                @foreach($job_offer->skills_list() as $skill)
                    <span class="badge badge-warning mr-1">{{ $skill->name }}</span>
                @endforeach
            </div>
        </div>
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                Candidati idonei, trovati da Tigit
                <div class="dropdown">
                    <a href="{{ route('company.hr.index', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror

                <div class="datatable table-responsive">
                    <pre class="text-sm-right float-right">
                        <span class="text-primary">Vengono mostrati i candidati con un idoneità maggiore del 60%</span>
                        professione diversa (-10%)
                        impiego diverso (-10%)
                        esperienza minore (-10%)
                        skill mancante (-5%)
                    </pre>
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Professione</th>
                            <th>Esperienza</th>
                            <th>Employment</th>
                            <th>Skills</th>
                            <th>Idoneità</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Nome</th>
                            <th>Professione</th>
                            <th>Esperienza</th>
                            <th>Employment</th>
                            <th>Skills</th>
                            <th>Idoneità</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($users as $key => $value)
                            <tr>
                                <td>
                                    @if(!is_null($value['user_name']))
                                        <span class="text-{{ $value['user_name']['props'] }}">
                                            {{ $value['user_name']['value'] }}
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    @if(!is_null($value['profession']))
                                        <span class="text-{{ $value['profession']['props'] }}">
                                            {{ $value['profession']['value'] }}
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    @if(!is_null($value['experience']))
                                        <span class="text-{{ $value['experience']['props'] }}">
                                            {{ $value['experience']['value'] }}
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    @if(!is_null($value['employment']))
                                        <span class="text-{{ $value['employment']['props'] }}">
                                            {{ $value['employment']['value'] }}
                                        </span>
                                    @endif
                                </td>
                                <td><span
                                        class="text-{{ $value['skills']['props'] }}"> {{ collect($value['skills']['value'])->join(', ') }}</span>
                                </td>
                                <td><span
                                        class="text-{{ $value['matching']['props'] }}"> {{ $value['matching']['value'] }}</span>
                                </td>
                                <td>
                                    <a href="{{ route('company.hr.people_detail', ['company' => $company->id, 'job_offer' => $job_offer->id, 'user' => $value['user_id']['value']]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i class="fa fa-user-tag"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="bg-gradient-primary-to-secondary text-white">
                                    Non ci sono persone che corrispondono alla tua ricerca
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @if($job_offer->is_public)
            <div class="card mb-4 card-header-actions">
                <div class="card-header">
                    Ecco qui le persone che si sono candidate su Tigit per questa posizione
                </div>
                <div class="card-body">
                    @error('alert-success')
                    <div class="alert alert-primary" role="alert">{{ $message }}</div>
                    @enderror

                    <div class="datatable table-responsive">
                        <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Professione</th>
                                <th>Esperienza</th>
                                <th>Employment</th>
                                <th>Skills</th>
                                <th>Idoneità</th>
                                <th>{{ trans('general.tb_actions') }}</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Professione</th>
                                <th>Esperienza</th>
                                <th>Employment</th>
                                <th>Skills</th>
                                <th>Idoneità</th>
                                <th>{{ trans('general.tb_actions') }}</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @forelse($applier as $key => $value)
                                <tr>
                                    <td>
                                        @if(!is_null($value['user_name']))
                                            <span
                                                class="text-{{ $value['user_name']['props'] }}"> {{ $value['user_name']['value'] }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if(!is_null($value['profession']))
                                            <span
                                            class="text-{{ $value['profession']['props'] }}"> {{ $value['profession']['value'] }}</span>
                                            @endif
                                    </td>
                                    <td>
                                        @if(!is_null($value['experience']))
                                            <span
                                            class="text-{{ $value['experience']['props'] }}"> {{ $value['experience']['value'] }}</span>@endif
                                    </td>
                                    <td>
                                        @if(!is_null($value['employment']))
                                            <span
                                            class="text-{{ $value['employment']['props'] }}"> {{ $value['employment']['value'] }}</span>@endif
                                    </td>
                                    <td>
                                        @if(!is_null($value['skills']))<span
                                            class="text-{{ $value['skills']['props'] }}"> {{ collect($value['skills']['value'])->join(', ') }}</span>@endif
                                    </td>
                                    <td>
                                        @if(!is_null($value['matching']))
                                            <span
                                            class="text-{{ $value['matching']['props'] }}"> {{ $value['matching']['value'] }}</span>@endif
                                    </td>
                                    <td>
                                        <a href="{{ route('company.hr.people_detail', ['company' => $company->id, 'job_offer' => $job_offer->id, 'user' => $value['user_id']['value']]) }}"
                                           class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                            <i class="fa fa-user-tag"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="bg-gradient-primary-to-secondary text-white">
                                        Non ci sono persone che corrispondono alla tua ricerca
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
