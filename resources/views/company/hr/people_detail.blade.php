@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>{{ $user->name() }}</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-md-12 mt-3 p-5 bg-white">
                <h1>{{ $user->name() }}</h1>

                <h3 class="mt-3">{{ $user->email }}</h3>
                <h3>{{ $user->phone }}</h3>
                <h3>
                    @foreach($user->address as $address)
                        {{ $address->route }} {{ $address->nr }}, {{ $address->cap }} {{ $address->town }}
                        - {{ $address->country }}
                    @endforeach
                </h3>
                <h3>Ricerca lavoro come: {{ $user->profession->profession->name }}</h3>
                <h3>Ha {{ $user->profession->experience }} anni di esperienza in questa posizione</h3>
                <h3>Grado di occupazione: {{ $user->employment->employment->name }}</h3><br>
                <h3>Formazione:</h3>
                <table class="table table-bordered table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Diploma</th>
                        <th>Scuola</th>
                        <th>Periodo</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->diplomas as $diploma)
                        <tr>
                            <td>{{ $diploma->title }}</td>
                            <td>
                                @if(!is_null($diploma->link))
                                    <a href="{{ $diploma->link }}">{{ $diploma->school }}</a>
                                @else
                                    {{ $diploma->school }}
                                @endif
                            </td>
                            <td>{{ $diploma->begin_at }} - {{ $diploma->end_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                <h3>Esperienza lavorativa:</h3>
                <table class="table table-bordered table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Titolo</th>
                        <th>Azienda</th>
                        <th>Periodo</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->job_experiences as $job_experience)
                        <tr>
                            <td>{{ $job_experience->job }}</td>
                            <td>
                                @if(!is_null($job_experience->link))
                                    <a href="{{ $job_experience->link }}">{{ $job_experience->company }}</a>
                                @else
                                    {{ $job_experience->company }}
                                @endif
                            </td>
                            <td>{{ $job_experience->begin_at }} - {{ $job_experience->end_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                <h3>Certificati raggiunti:</h3>
                <table class="table table-bordered table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Certificato</th>
                        <th>Rilasciato da</th>
                        <th>Acquisito il</th>
                        <th>Scade il</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->certificates as $certificate)
                        <tr>
                            <td>{{ $certificate->certificate }}</td>
                            <td>
                                @if(!is_null($certificate->link))
                                    <a href="{{ $certificate->link }}">{{ $certificate->released_by }}</a>
                                @else
                                    {{ $certificate->released_by }}
                                @endif
                            </td>
                            <td>{{ $certificate->acquired_at }}</td>
                            <td>{{ $certificate->expiry_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                <h3 class="mt-1">Lingue parlate:</h3>
                @foreach($user->languages as $language)
                    <span
                        class="badge badge-primary"> {{ $language->language->name }} ({{ $language->level->name }})</span>
                @endforeach<br>
                <h3 class="mt-1">Skills:</h3>
                @foreach($user->skills as $skill)
                    <span class="badge badge-warning"> {{ $skill->skill->name }}</span>
                @endforeach<br>
                <h3 class="mt-1">Hobby:</h3>
                @foreach($user->hobbies as $hobby)
                    <span class="badge badge-cyan"> {{ $hobby->hobby }}</span>
                @endforeach
                <p class="mt-4">{{ !is_null(\App\JobOfferApply::where('user_id', $user->id)->first()) ? \App\JobOfferApply::where('user_id', $user->id)->first()->message : '' }}</p>
            </div>
        </div>
    </div>
@endsection
