@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="bar-chart-2"></i></div>
                    <span>Le tue offerte di impiego</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                Offerte create
                <div class="dropdown">
                    <a href="{{ route('company.hr.create', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @error('alert-success')
                <div class="alert alert-primary" role="alert">{{ $message }}</div>
                @enderror
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Titolo</th>
                            <th>Publica/Privata</th>
                            <th>Scadenza</th>
                            <th>Professione</th>
                            <th>Impiego</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Titolo</th>
                            <th>Publica/Privata</th>
                            <th>Scadenza</th>
                            <th>Professione</th>
                            <th>Impiego</th>
                            <th>{{ trans('general.tb_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($company->job_offers as $offer)
                            <tr>
                                <td>{{ $offer->title }}</td>
                                <td><i class="fa fa-{{ $offer->is_public ? 'lock-open' : 'lock' }}"></i> </td>
                                <td>{{ \Carbon\Carbon::parse($offer->offer_expiry_at)->format('d M Y') }}</td>
                                <td>{{ $offer->profession->name }}</td>
                                <td>{{ $offer->employment->name }}</td>
                                <td>

                                    <a href="{{ route('company.hr.people', ['company' => $company->id, 'job_offer' => $offer->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i class="fa fa-user-tie"></i>
                                    </a>
                                    <a href="{{ route('company.hr.edit', ['company' => $company->id, 'job_offer' => $offer->id]) }}"
                                       class="btn btn-datatable btn-icon btn-transparent-dark mr-2">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-datatable btn-icon btn-transparent-dark"
                                       onclick="event.preventDefault();
                                           document.getElementById('trash-form-{{ $offer->id }}').submit();">
                                        <i data-feather="trash-2"></i>
                                    </a>
                                    <form id="trash-form-{{ $offer->id }}"
                                          action="{{ route('company.hr.delete', ['company' => $company->id, 'job_offer' => $offer->id]) }}"
                                          method="post"
                                          style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="bg-gradient-primary-to-secondary text-white">
                                    non hai ancora creato offerte... creane una
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
