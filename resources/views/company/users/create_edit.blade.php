@extends('layouts.company.backend')

@section('content')
    <div class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content py-5">
                <h1 class="page-header-title">
                    <div class="page-header-icon">
                        <i data-feather="link"></i>
                    </div>
                    @if(isset($user))
                        <span>Modifica utente</span>
                    @else
                        <span>Crea un nuovo utente</span>
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="card mb-4 card-header-actions">
            <div class="card-header">
                @if(isset($user))
                    <span>Modifica</span>
                @else
                    <span>Crea</span>
                @endif
                <div class="dropdown">
                    <a href="{{ route('company.users.index', ['company' => $company->id]) }}"
                       class="btn btn-primary btn-icon btn-sm">
                        <i data-feather="skip-back"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if(isset($user))
                    <form
                        action="{{ route('company.users.update', ['company' => $company->id, 'user' => $user->id]) }}"
                        method="post">
                        @else
                            <form action="{{ route('company.users.store', ['company' => $company->id]) }}"
                                  method="post">
                                @endif
                                @csrf
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="role_name">Nome</label>
                                            <input class="form-control @error('first_name') is-invalid @enderror" name="first_name"
                                                   type="text"
                                                   value="{{ isset($user) ? $user->first_name : old('first_name') }}"/>
                                            @error('first_name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="role_name">Cognome</label>
                                            <input class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                                                   type="text"
                                                   value="{{ isset($user) ? $user->last_name : old('last_name') }}"/>
                                            @error('last_name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="role_name">E-Mail</label>
                                            <input class="form-control @error('email') is-invalid @enderror" name="email"
                                                   type="text"
                                                   value="{{ isset($user) ? $user->email : old('email') }}"/>
                                            @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="role_name">Ruolo</label>
                                            <select name="role" class="form-control">
                                                @foreach($company->roles as $role)
                                                    <option value="{{ $role->id }}"
                                                    {{ isset($user) ?
                                                            $user->subrole->subrole->id == $role->id ? 'selected' : ''
                                                            : '' }}>
                                                        {{ $role->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('role')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary"
                                           value="{{ isset($user) ? trans('general.update') : trans('general.save') }}">
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
