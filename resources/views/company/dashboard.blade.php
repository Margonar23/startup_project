@extends('layouts.company.backend')

@section('content')
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Dashboard</span>
                </h1>
            </div>
        </div>
    </div>

    @if(count(Auth::user()->hasCompanies))
        <div class="container-fluid mt-n10">
            @if(!is_null(Auth::user()->sub()) &&  strtotime(Auth::user()->sub()->trial_ends_at) > time())
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning">il tuo Free trial termina {{ Auth::user()->sub()->trial_ends_at }}</div>
                    </div>
                </div>
            @endif
            {{--                <div class="col-md-6">--}}
            {{--                    <div class="card mb-4">--}}
            {{--                        <div class="card-header">Bar Chart Example</div>--}}
            {{--                        <div class="card-body">--}}
            {{--                            <div class="chart-bar">--}}
            {{--                                <canvas id="myBarChart" width="100%" height="30"></canvas>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="row">--}}
            {{--                <div class="col-xl-3 col-md-6">--}}
            {{--                    <div class="card bg-primary text-white mb-4">--}}
            {{--                        <div class="card-body">Primary Card</div>--}}
            {{--                        <div class="card-footer d-flex align-items-center justify-content-between">--}}
            {{--                            <a class="small text-white stretched-link" href="#">View Details</a>--}}
            {{--                            <div class="small text-white"><i class="fas fa-angle-right"></i></div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="col-xl-3 col-md-6">--}}
            {{--                    <div class="card bg-warning text-white mb-4">--}}
            {{--                        <div class="card-body">Warning Card</div>--}}
            {{--                        <div class="card-footer d-flex align-items-center justify-content-between">--}}
            {{--                            <a class="small text-white stretched-link" href="#">View Details</a>--}}
            {{--                            <div class="small text-white"><i class="fas fa-angle-right"></i></div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="col-xl-3 col-md-6">--}}
            {{--                    <div class="card bg-success text-white mb-4">--}}
            {{--                        <div class="card-body">Success Card</div>--}}
            {{--                        <div class="card-footer d-flex align-items-center justify-content-between">--}}
            {{--                            <a class="small text-white stretched-link" href="#">View Details</a>--}}
            {{--                            <div class="small text-white"><i class="fas fa-angle-right"></i></div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="col-xl-3 col-md-6">--}}
            {{--                    <div class="card bg-danger text-white mb-4">--}}
            {{--                        <div class="card-body">Danger Card</div>--}}
            {{--                        <div class="card-footer d-flex align-items-center justify-content-between">--}}
            {{--                            <a class="small text-white stretched-link" href="#">View Details</a>--}}
            {{--                            <div class="small text-white"><i class="fas fa-angle-right"></i></div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-header">Notifiche</div>
                        <div class="card-body">
                            <div class="datatable table-responsive">
                                <table class="table table-bordered table-hover" id="dataTable" width="100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Azione</th>
                                        <th>Messaggio</th>
                                        <th>Orario</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Azione</th>
                                        <th>Messaggio</th>
                                        <th>Orario</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach (Auth::user()->notifications as $notification)
                                        <tr>
                                            <td>{{ $notification->data['action'] }}</td>
                                            <td>{{ $notification->data['message'] }}</td>
                                            <td>{{ $notification->created_at }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">Continua con la creazione della tua azienda!</div>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('scripts')
{{--    <script src="{{ asset('vendor/datatables/demo/datatables-demo.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/chartjs/demo/chart-area-demo.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/chartjs/demo/chart-bar-demo.js') }}"></script>--}}
@endsection
