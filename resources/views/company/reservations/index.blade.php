@extends('layouts.company.backend')
@section('styles')
@endsection

@section('content')
    <div class="container-fluid mt-4">
        <h1>I tuoi ristoranti</h1>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Nuove riservazioni</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($company->restaurants as $restaurant)
                                <tr>
                                    <td>{{ $restaurant->name }}</td>
                                    <td><span class="badge badge-danger">{{ $restaurant->bookings->where('reservation_status_id', 1)->count() }}</span> </td>
                                    <td class="float-right"><a href="{{ route('company.reservations.bookings', ['company' => $company->id, 'location' => $restaurant->id]) }}" class="btn btn-xs btn-primary">Riservazioni</a> </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>
@endsection
@section('scripts')

@endsection
