@extends('layouts.company.backend')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection

@section('content')
    <div class="container-fluid mt-4">
        <h1>{{ $location->name }}</h1>
        <div class="card">
            <div class="card-body">
                    <h1 class="text-primary">
                        Riservazione #{{ $booking->id }}
                        <span class="badge badge-default {{ $booking->reservation_status->color }} float-right"><i
                                class="{{ $booking->reservation_status->icon }}"></i> {{ $booking->reservation_status->name }}</span>
                    </h1>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 border-right">
                            <h4>{{ $booking->firstname }} {{ $booking->lastname }}</h4>
                            <i>
                                <i class="fa fa-calendar"></i> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->booking_datetime)->format('d.m.Y') }}
                                <i class="fa fa-clock"></i> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->booking_datetime)->format('H:i') }}
                            </i>
                            <h4 class="mt-3"><i class="fa fa-users"></i> {{ $booking->people_nr }}</h4>
                            <hr>
                            <h3>Preferenze:</h3>
                            <p>{{ $booking->room_id !== 0 ? $booking->room->name : '-' }}</p>
                            <hr>
                            <h3>Contatti:</h3>
                            <p>{{ $booking->phone }}</p>
                            <p>{{ $booking->email }}</p>
                            <hr>
                            <h3>Indirizzo:</h3>
                            <p>{{ $booking->address }}</p>
                            <p>{{ $booking->cap }} {{ $booking->city }}</p>
                        </div>
                        <div class="col-md-6">
                            <h3>Messaggio:</h3>
                            {{ $booking->message }}
                        </div>
                    </div>

            </div>
            <div class="card-footer text-right">
                <a href="{{ route('company.reservations.reject_booking', ['company' => $company->id, 'location' => $location->id, 'booking' => $booking->id]) }}"
                   class="btn btn-sm btn-danger">Rifiturare</a>
            </div>
        </div>
        <div class="card mt-5">
            <div class="card-body">
                <h1 class="text-primary">
                    Tavoli
                </h1>
                <hr>
                <div class="row">
                    @foreach($location->rooms as $room)
                        <div class="col-md-12 border-bottom mb-4">
                            <h3>{{ $room->name }}</h3>
                            <table class="table table-striped table-sm">
                                @foreach($room->tables as $table)
                                    <tr>
                                        <td>{{ $table->name }}</td>
                                        <td><i class="fa fa-users"></i> max {{ $table->max_capacity }}</td>
                                        <td>{{ $table->shape->name }}</td>
                                        <td>
                                            @if(is_null($table->is_reserved($booking->booking_datetime)))
                                                <i class="fa fa-circle text-success"></i>
                                            @elseif(!is_null($table->is_reserved($booking->booking_datetime)) && $table->is_reserved($booking->booking_datetime)->id == $booking->id)
                                                <i class="fa fa-circle text-primary"></i>
                                            @else
                                                <i class="fa fa-circle text-danger"></i>
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if(is_null($table->is_reserved($booking->booking_datetime)))
                                                <a href="{{ route('company.reservations.assign_table', ['company' => $company->id, 'location' => $location->id, 'booking' => $booking->id, 'table' => $table->id]) }}"
                                                   class="btn btn-xs btn-primary">assegna</a>
                                            @elseif(!is_null($table->is_reserved($booking->booking_datetime)) && $table->is_reserved($booking->booking_datetime)->id == $booking->id)

                                            @else
                                                <a href="#" class="btn btn-xs btn-danger disabled">occupato</a>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        $('input[name="dates"]').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD.MM.YYYY'
            },
            maxYear: parseInt(moment().format('YYYY'), 10)
        }, function (start, end, label) {
            console.log('ciao');
            var years = moment().diff(start, 'years');
        });

        $(document).ready(function () {

        });
    </script>
@endsection
