@extends('layouts.company.backend')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}"/>
@endsection

@section('content')
    <div class="container-fluid mt-4">
        <h1>{{ $location->name }}</h1>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-primary">Nuove riservazioni</h4>
                        <table class="table table-striped table-sm">
                            <tbody>
                            @forelse($location->bookings->where('reservation_status_id', 1) as $booking)
                                <tr>
                                    <td>{{ $booking->firstname }} {{ $booking->lastname }}</td>
                                    <td><i class="fa fa-users"></i> {{ $booking->people_nr }}</td>
                                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->booking_datetime)->format('d.m.Y') }}</td>
                                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->booking_datetime)->format('H:i') }}</td>
                                    <td>{!! $booking->room_id != 0 ? '<i class="fa fa-star text-warning"></i> '. $booking->room->name : '-' !!}</td>
                                    <td class="text-right"><a
                                            href="{{ route('company.reservations.booking', ['company' => $company->id, 'location' => $location->id, 'booking' => $booking->id]) }}">
                                            <i class="fa fa-plus-circle text-primary"></i></a></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" class="bg-light">Non ci sono nuove riservazioni</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr>
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <input type="text" name="dates" class="form-control">
                            </div>
                        </div>
                        <h4 class="text-success">Riservazioni <span id="reservation_date">di oggi</span></h4>
                        <table class="table table-striped table-sm">
                            <tbody id="reservations">
                            @foreach($today_bookings as $booking)
                                <tr>
                                    <td>{{ $booking->firstname }} {{ $booking->lastname }}</td>
                                    <td><i class="fa fa-users"></i> {{ $booking->people_nr }}</td>
                                    <td>Tavolo {{ $booking->reserved->table->name }}</td>
                                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$booking->reserved->reserved_start)->format('d.m.Y H:i') }}
                                        - {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->reserved->reserved_stop)->format('H:i') }}</td>
                                    <td class="text-right"><a
                                            href="{{ route('company.reservations.booking', ['company' => $company->id, 'location' => $location->id, 'booking' => $booking->id]) }}">
                                            <i class="fa fa-info-circle text-primary"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('vendor/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/daterangepicker/daterangepicker.js') }}"></script>
    <script>
        function get_bookings(start) {
            var select_date = start.format('YYYY-MM-DD');
            $.ajax({
                url: '{{ route('company.reservations.date_bookings', ['company' => $company->id, 'location' => $location->id]) }}',
                type: 'post',
                data: {'_token': "{{ csrf_token() }}", 'date': select_date},
                success: function (response) {
                    $("#reservation_date").text('del ' + start.format('DD.MM.YYYY'));
                    $("#reservations").empty();
                    let table = '';
                    response.forEach(function (obj) {
                        table += '<tr>';
                        table += '<td>' + obj.firstname + ' ' + obj.lastname + '</td>';
                        table += '<td><i class="fa fa-users"></i> ' + obj.people_nr + '</td>';
                        table += '<td>Tavolo ' + obj.reservation.table.name + '</td>';
                        table += '<td>' + obj.reservation.reserved_string + '</td>';
                        table += '<td class="text-right">';
                        table += '<a href="/reservations/{{ $company->id }}/{{ $location->id }}/'+obj.id+'">';
                        table += '<i class="fa fa-info-circle text-primary"></i></a></td>';
                        table += '</tr>';
                    });
                    $("#reservations").append(table);
                }
            });
        }


        $(document).ready(function () {
            $('input[name="dates"]').daterangepicker({
                singleDatePicker: true,
                locale: {
                    format: 'DD.MM.YYYY'
                },
                maxYear: parseInt(moment().format('YYYY'), 10)
            }, function (start, end, label) {
                get_bookings(start);
            });
        });
    </script>
@endsection
