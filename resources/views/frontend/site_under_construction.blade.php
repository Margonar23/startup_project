<html>
<head>
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-4.4.1-dist/css/bootstrap.css') }}">
    <style>

        body {
            font-family: 'Roboto', sans-serif;
            font-weight: 300;
            color: #fff;
            text-align: center;
            background: #fff url("{{ asset('images/lugano-3272123_1280.jpg') }}") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        strong { font-weight: 700; }

        a, a:hover, a:focus {
            color: #42bfc2;
            text-decoration: none;
            -o-transition: all .3s;
            -moz-transition: all .3s;
            -webkit-transition: all .3s;
            -ms-transition: all .3s;
            transition: all .3s;
        }

        h2 {
            margin-top: 10px;
            font-size: 42px;
            font-weight: 100;
            line-height: 54px;
        }

        img { max-width: 100%; }

        ::-moz-selection { background: #42bfc2; color: #fff; text-shadow: none; }
        ::selection { background: #42bfc2; color: #fff; text-shadow: none; }


        /***** Coming Soon *****/

        .coming-soon {
            margin: 0 auto;
        }

        .inner-bg {
            padding: 75px 0 40px 0;
        }

        .coming-soon .logo {
            padding-bottom: 20px;
        }

        .coming-soon .logo h1 {
            margin: 0;
        }

        .coming-soon .logo a {
            display: inline-block;
            width: 100%;
            text-indent: -99999px;
        }
        .coming-soon .logo a:focus { outline: 0; }


        .coming-soon p {
            margin: 60px 0 10px 0;
            padding: 0 120px;
            font-size: 22px;
            line-height: 36px;
        }


        .subscribe form input {
            width: 362px;
            height: 52px;
        }


        /***** Footer *****/

        .footer{
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background: #121212;
            background: rgba(0, 0, 0, 0.3);
            color: white;
            text-align: center;
        }



    </style>
</head>
<body>
<div class="coming-soon">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="logo wow fadeInDown">
                        <img src="{{ asset('images/logo_tigit_no_bg.png') }}" width="30%"/>
                    </div>
                    <h2 class="wow fadeInLeftBig">Stiamo lavorando per voi!</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<div class="footer">

                <p>© Tigit Sagl {{ date('Y') }}<br>Tutti i diritti riservati.</p>

</div>

</body>
</html>
