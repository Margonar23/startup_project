@extends('layouts.frontend.frontend')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/avatar.css') }}"/>
@endsection
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Profilo personale</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-white">
        <div class="container mt-10">
            <div class="row">
                <div class="col-lg-3 py-5">
                    <ul class="list-group list-group-flush list-group-careers">
                        <li class="list-group-item pt-0">
                            <a class="small" href="{{ route('home') }}">
                                <i class="fas fa-arrow-left mr-1"></i> {{ trans('frontend/users/profile.back_to_home_page') }}
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('users.profile') }}"
                               class="font-weight-bold"> {{ trans('frontend/users/profile.profile') }}</a>
                            <i class="fas fa-user-edit fa-fw text-primary"></i>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('users.tregalo') }}"
                               class=""> T-REGALO</a>
                            <i class="fas fa-shopping-bag fa-fw text-primary"></i>
                        </li>
                        @if(env('APP_ENV') !== 'middle_dev')
                            <li class="list-group-item">
                                <a href="{{ route('users.cv') }}"> {{ trans('frontend/users/profile.cv') }}</a>
                                <i class="fas fa-user-graduate fa-fw text-gray-400"></i>
                            </li>
                        @endif
                    </ul>
                </div>
                <div class="col-lg-9">
                    <section class="py-5">
                        <div class="container">
                            <div class="d-flex align-items-center mb-4">
                                <div class="icon-stack icon-stack-lg bg-primary text-white" id="img-profile">
                                    <i class="fas fa-user"></i>
                                </div>
                                <div class="ml-3">
                                    <h2 class="mb-0">{{ trans('frontend/users/profile.profile') }}</h2>
                                    <p class="lead mb-0">{{ Auth::user()->name() }}</p>
                                </div>
                            </div>
                            <div class="card card-header-actions">
                                <div class="card-header">
                                    {{ trans('frontend/users/profile.profile') }}

                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <form enctype="multipart/form-data"
                                                  action="{{ route('users.edit_avatar') }}"
                                                  method="post" name="image_upload_form" id="image_upload_form">
                                                @csrf
                                                <div id="imgArea">
                                                    <img
                                                        src="{{ asset(isset(Auth::user()->avatar) ? Auth::user()->avatar : 'images/default_avatar.jpg') }}"
                                                        width="100%">
                                                    <div id="imgChange" class="bg-primary-soft">
                                                        <span>Change Photo</span>
                                                        <input type="file" accept="image/*" name="file"
                                                               id="image_upload_file">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-9">
                                            @if(!is_null($edit) && $edit == 'edit_profile')
                                                <form action="{{ route('users.update_profile') }}" method="post">
                                                    @csrf
                                                    <input type="submit" class="btn btn-xs btn-primary float-right"
                                                           value="{{ trans('general.update') }}"/>
                                                    @else
                                                        <a href="{{ route('users.profile', ['edit' => 'edit_profile']) }}">
                                                            <i class="fa fa-edit text-gray-400  float-right"></i>
                                                        </a>
                                                    @endif
                                                    <table class="table table-light table-borderless table-sm">
                                                        <tr>
                                                            <th>Titolo</th>
                                                            <th></th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                @if(!is_null($edit) && $edit == 'edit_profile')
                                                                    <select name="title_id"
                                                                            class="form-control form-control-sm">
                                                                        @foreach(\App\UserTitle::all() as $title)
                                                                            <option
                                                                                value="{{ $title->id }}" {{ Auth::user()->title_id == $title->id ? 'selected' : '' }}>{{ $title->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @error('title_id')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                    @enderror
                                                                @else
                                                                    {{ Auth::user()->title_id == 0 ? '-' : Auth::user()->title->label }}
                                                                @endif
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Nome</th>
                                                            <th>Cognome</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                @if(!is_null($edit) && $edit == 'edit_profile')
                                                                    <input type="text" name="first_name"
                                                                           class="form-control form-control-sm"
                                                                           value="{{ Auth::user()->first_name }}"/>
                                                                    @error('first_name')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                    @enderror
                                                                @else
                                                                    {{ Auth::user()->first_name }}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(!is_null($edit) && $edit == 'edit_profile')
                                                                    <input type="text" name="last_name"
                                                                           class="form-control form-control-sm"
                                                                           value="{{ Auth::user()->last_name }}"/>
                                                                    @error('last_name')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                    @enderror
                                                                @else
                                                                    {{ Auth::user()->last_name }}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>E-Mail</th>
                                                            <th>Numero di telefono</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                @if(!is_null($edit) && $edit == 'edit_profile')
                                                                    <input type="text"
                                                                           class="form-control form-control-sm"
                                                                           value="{{ Auth::user()->email }}"
                                                                           name="email"/>
                                                                    @error('email')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                    @enderror
                                                                @else
                                                                    {{ Auth::user()->email }} <i
                                                                        class="fa fa-asterisk fa-xs text-red"></i>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(!is_null($edit) && $edit == 'edit_profile')
                                                                    <input type="text"
                                                                           class="form-control form-control-sm"
                                                                           value="{{ Auth::user()->phone }}"
                                                                           name="phone"/>
                                                                    @error('phone')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                    @enderror
                                                                @else
                                                                    {{ isset(Auth::user()->phone) ? Auth::user()->phone : '-' }}
                                                                    <i class="fa fa-asterisk fa-xs text-red"></i>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>Nazionalità</th>
                                                            <th></th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                @if(!is_null($edit) && $edit == 'edit_profile')
                                                                    <select name="country_id"
                                                                            class="form-control form-control-sm">
                                                                        @foreach(\App\Country::all() as $country)
                                                                            <option
                                                                                value="{{ $country->id }}" {{ Auth::user()->country_id == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @error('country_id')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                    @enderror
                                                                @else
                                                                    @if(is_null(Auth::user()->country_id))
                                                                        -
                                                                    @else
                                                                        <img src="{{ asset('/images/countries/'.strtolower(Auth::user()->country->iso_2).'.png') }}">
                                                                {{Auth::user()->country->name}}
                                                                    @endif

                                                                @endif
                                                            </td>
                                                            <td>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                    @if(!is_null($edit) && $edit == 'edit_profile')
                                                </form>
                                            @endif
                                            <hr class="my-1"/>
                                            <small class="float-right font-italic font-weight-100 text-red">
                                                <i class="fa fa-asterisk fa-xs text-red"></i> I dati saranno visibili
                                                solamente dopo accettazione
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                @if(!is_null($edit) && $edit == 'edit_profile')
                            </div>
                            @endif
                        </div>
                        <hr class="my-5"/>
                        <div class="accordion accordion-faq mb-5" id="authAccordion">
                            <div class="card border-bottom">
                                <div class="card-body">
                                    <div class="accordion-faq-title">
                                        <div class="mr-2">
                                            <h4 class="mb-0">Altre informazioni</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card accordion-faq-item">
                                <a class="card-header collapsed" id="authHeadingTwo" data-toggle="collapse"
                                   data-target="#authCollapseTwo" aria-expanded="true"
                                   aria-controls="authCollapseTwo" href="javascript:void(0);">
                                    <div class="accordion-faq-item-heading">
                                        <i class="fas fa-route"></i> Indirizzi
                                        <i class="accordion-faq-item-heading-arrow" data-feather="chevron-down"></i>
                                    </div>
                                </a>
                                <div class="collapse" id="authCollapseTwo" aria-labelledby="authHeadingTwo"
                                     data-parent="#authAccordion">
                                    <div class="card-body border-bottom">
                                        @if(Auth::user()->address->count() == 0)
                                            <div class="alert alert-danger-soft">
                                                <strong>Aggiungi un indirizzo</strong>
                                            </div>
                                        @endif
                                        <button class="btn btn-xs btn-primary" id="add_new_address_button">
                                            <i class="fa fa-plus"></i>
                                            Aggiungi un nuovo indirizzo
                                        </button>
                                        <div class="py-2">
                                            <ul class="list-group list-group-sm" id="address_list">
                                                @if(Auth::user()->address->count() > 0)
                                                    @forelse(Auth::user()->address as $address)
                                                        <li class="list-group-item">
                                                            {{ $address->user->first_name }} {{ $address->user->last_name }}
                                                            ,
                                                            {{ $address->route }} {{ $address->nr }}
                                                            , {{ $address->country }}
                                                            -{{ $address->cap }}
                                                            {{ $address->town }}
                                                            <div class="float-right">
                                                                <a><i class="fa fa-edit"></i></a>
                                                                <a><i class="fa fa-trash"></i></a>
                                                            </div>
                                                        </li>
                                                    @empty
                                                    @endforelse
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card accordion-faq-item">
                                <a class="card-header collapsed" id="authHeadingThree" data-toggle="collapse"
                                   data-target="#authCollapseFour" aria-expanded="true"
                                   aria-controls="authCollapseFour" href="javascript:void(0);">
                                    <div class="accordion-faq-item-heading">
                                        <i class="fas fa-user-lock"></i> Modifica la password
                                        <i class="accordion-faq-item-heading-arrow" data-feather="chevron-down"></i>
                                    </div>
                                </a>
                                <div class="collapse" id="authCollapseFour" aria-labelledby="authHeadingThree"
                                     data-parent="#authAccordion">
                                    <div class="card-body border-bottom">
                                        <form action="{{ route('users.update_password') }}" method="post">
                                            @csrf
                                            <input type="password" class="form-control" name="password"
                                                   placeholder="Password">
                                            @error('password')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                            <br>
                                            <input type="password" class="form-control" name="password_confirmation"
                                                   placeholder="Conferma Password">
                                            <br>
                                            <input type="submit" value="{{ trans('general.save') }}"
                                                   class="btn btn-success">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card accordion-faq-item">
                                <a class="card-header collapsed" id="authHeadingDelete" data-toggle="collapse"
                                   data-target="#authHeadingDelete" aria-expanded="true"
                                   aria-controls="authHeadingDelete" href="javascript:void(0);">
                                    <div class="accordion-faq-item-heading">
                                        <i class="fas fa-trash"></i> Cancella il tuo account
                                        <i class="accordion-faq-item-heading-arrow" data-feather="chevron-down"></i>
                                    </div>
                                </a>
                                <div class="collapse" id="authHeadingDelete" aria-labelledby="authHeadingDelete"
                                     data-parent="#authAccordion">
                                    <div class="card-body border-bottom">
                                        <h4 class="text-danger">Attenzione! Questa operazione è irreversibile.</h4>
                                        <button class="btn btn-danger" id="delete_account">Cancella il mio account</button>
{{--                                        <form action="{{ route('users.delete', ['user' => Auth::id()]) }}" method="post">--}}
{{--                                            @csrf--}}
{{--                                            <input type="password" class="form-control" name="password"--}}
{{--                                                   placeholder="Password">--}}
{{--                                            @error('password')--}}
{{--                                            <span class="text-danger">{{ $message }}</span>--}}
{{--                                            @enderror--}}
{{--                                            <br>--}}
{{--                                            <input type="password" class="form-control" name="password_confirmation"--}}
{{--                                                   placeholder="Conferma Password">--}}
{{--                                            <br>--}}
{{--                                            <input type="submit" value="{{ trans('general.save') }}"--}}
{{--                                                   class="btn btn-success">--}}
{{--                                        </form>--}}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $("#image_upload_file").on('change', function () {
                $("#image_upload_form").submit();
            });

            $("#add_new_address_button").on('click', function () {
                Swal.fire({
                    title: 'Modifica il widgets',
                    html: '<label class="label">Via</label> ' +
                        '<input class="form-control" id="route" value="">' +
                        '<label class="label">Numero civico</label> ' +
                        '<input class="form-control" id="nr" value="">' +
                        '<label class="label">CAP</label> ' +
                        '<input class="form-control" id="cap" value="">' +
                        '<label class="label">Citta</label> ' +
                        '<input class="form-control" id="town" value="">' +
                        '<label class="label">Paese</label>' +
                        '<input class="form-control" id="country" value="Svizzera" readonly>',
                    focusConfirm: false,
                    preConfirm: () => {
                        return {
                            route: $("#route").val(),
                            nr: $("#nr").val(),
                            cap: $("#cap").val(),
                            town: $("#town").val(),
                            country: $("#country").val()
                        }
                    },
                    onOpen: (el) => {

                    }
                }).then((result) => {
                    if (result.value) {
                        axios
                            .post('{{ route('users.update_address') }}', {
                                route: result.value.route,
                                nr: result.value.nr,
                                cap: result.value.cap,
                                town: result.value.town,
                                country: result.value.country,
                            })
                            .then((response) => {
                                Swal.fire({
                                    title: 'Aggiunto correttamente',
                                    icon: 'success',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                location.reload();
                            }, (error) => {
                                Swal.fire({
                                    title: 'Opsss!',
                                    icon: 'danger',
                                    text: "Qualcosa è andato storto!",
                                    showConfirmButton: false,
                                    timer: 2500
                                });
                            });

                    }
                });
            });

            $("#delete_account").on('click', function () {
                Swal.fire({
                    title: 'Ci dispiace vederti andare via...',
                    showConfirmButton: true,
                    confirmButtonText: 'Elimina account',
                    focusConfirm: false
                }).then((result) => {
                    if (result.value) {
                        axios
                            .post('{{ route('users.delete', ['user' => Auth::id()]) }}')
                            .then((response) => {
                                Swal.fire({
                                    title: 'Account eliminato correttamente',
                                    icon: 'success',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                window.location = '/';
                            }, (error) => {
                                Swal.fire({
                                    title: 'Opsss!',
                                    icon: 'danger',
                                    text: "Qualcosa è andato storto!",
                                    showConfirmButton: false,
                                    timer: 2500
                                });
                            });

                    }
                });
            });

        });
    </script>
@endsection
