@extends('layouts.frontend.frontend')
@section('styles')
    <link rel="stylesheet"
          href="{{ asset('vendor/fontawesome-iconpicker-master/dist/css/fontawesome-iconpicker.min.css') }}">
@endsection
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Curriculum Vitae</h1>
                        <p class="page-header-text">Mostra alle aziende presenti nel territorio di che pasta sei
                            fatto</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-white">
        <div class="container mt-10">
            <div class="row">
                <div class="col-lg-3 py-5">
                    <ul class="list-group list-group-flush list-group-careers">
                        <li class="list-group-item pt-0">
                            <a class="small" href="{{ route('home') }}">
                                <i class="fas fa-arrow-left mr-1"></i> {{ trans('frontend/users/profile.back_to_home_page') }}
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('users.profile') }}"> {{ trans('frontend/users/profile.profile') }}</a>
                            <i class="fas fa-user-edit fa-fw text-gray-400"></i>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('users.cv') }}"
                               class="font-weight-bold"> {{ trans('frontend/users/profile.cv') }}</a>
                            <i class="fas fa-user-graduate fa-fw text-primary"></i>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-9 py-5">
                    @if(Auth::user()->cv->status->label == 'copied')
                        <div class="alert alert-warning">
                            Il tuo curriculum vitea è stato creato... ora tocca a te!
                        </div>
                    @elseif(Auth::user()->cv->status->label == 'draft')
                        <div class="alert alert-info">
                            <a href="{{ route('cv.change_status', ['status'=> 'saved']) }}"> Ottimo! Quando hai finito
                                clicca qui per salvare il tuo cv e renderlo visibile alle
                                aziende!</a>
                        </div>
                    @elseif(Auth::user()->cv->status->label == 'saved')
                        <div class="alert alert-success">
                            <a href="{{ route('cv.change_status', ['status'=> 'draft']) }}">
                                Benissimo ora le aziende sanno chi sei! Ma non c'è problema.. se vuoi modificarlo clicca
                                qui
                            </a>
                        </div>

                    @endif
                    <div class="row">
                        <div class="col-md-5 pl-5">
                            @if($cv_editing)
                                <h1>Widgets <a href="#" id="cv_widget_add_new"><i
                                            class="fa fa-plus-circle text-primary float-right"></i></a></h1>
                            @endif
                            <div id="widget_list">
                                @forelse(Auth::user()->cv->widgets as $widget)
                                    <icon-widget :widget="{{ $widget }}" :editing="{{ $cv_editing }}"></icon-widget>
                                @empty
                                @endforelse
                            </div>
                        </div>
                        <div class="col-md-7">
                            <h1 class="">{{ strtoupper(Auth::user()->name()) }}</h1>
                            <div class="">
                                @forelse(Auth::user()->cv->roles as $role)
                                    <role-description :role="{{ $role }}"
                                                      :editing="{{ $cv_editing }}"></role-description>
                                @empty
                                @endforelse
                            </div>
                            <div class="">
                                @if($cv_editing)
                                    <h1>Social Widgets <a href="#" id="cv_social_widget_add_new"><i
                                                class="fa fa-plus-circle text-primary float-right"></i></a></h1>
                                @endif
                                <h6 class="mb-4 social-icon">
                                    @forelse(Auth::user()->cv->socials as $social)
                                        <social-widget :social="{{ $social }}"
                                                       :editing="{{ $cv_editing }}"></social-widget>
                                    @empty
                                    @endforelse
                                </h6>
                            </div>
                        </div>
                    </div>

                    <hr class="dropdown-divider"/>
                    <div class="row">
                        @forelse(Auth::user()->cv->photos as $photo)
                            <profile-avatar :photo="{{ $photo }}"></profile-avatar>
                        @empty
                        @endforelse
                        @forelse(Auth::user()->cv->descriptions as $description)
                            <profile-description :description="{{ $description }}"
                                                 :editing="{{ $cv_editing }}"></profile-description>
                        @empty
                        @endforelse
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="mb-2 mt-2">ESPERIENZA LAVORATIVA
                                @if($cv_editing)
                                    <a href="#" id="cv_experience_widget_add_new"><i
                                            class="fa fa-plus-circle text-primary float-right"></i></a>
                            @endif
                            </h3>
                            <ul class="fa-ul mb-3">
                                @forelse(Auth::user()->cv->experiences as $experience)
                                    <experience :experience="{{ $experience }}"
                                                :editing="{{ $cv_editing }}"></experience>
                                @empty
                                @endforelse
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h3 class="mb-2 mt-2">FORMAZIONE
                                @if($cv_editing)
                                    <a href="#" id="cv_education_widget_add_new"><i
                                            class="fa fa-plus-circle text-primary float-right"></i></a>
                            @endif
                            </h3>
                            <ul class="fa-ul mb-3">
                                @forelse(Auth::user()->cv->educations as $education)
                                    <education :education="{{ $education }}" :editing="{{ $cv_editing }}"></education>
                                @empty
                                @endforelse
                            </ul>
                        </div>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="mb-2 mt-2">CERTIFICATI
                                @if($cv_editing)
                                    <a href="#" id="cv_certificate_widget_add_new"><i
                                            class="fa fa-plus-circle text-primary float-right"></i></a>
                            @endif
                            </h3>
                            <ul class="fa-ul mb-3">
                                @forelse(Auth::user()->cv->certificates as $certificate)
                                    <certificate :certificate="{{ $certificate }}"
                                                 :editing="{{ $cv_editing }}"></certificate>
                                @empty
                                @endforelse
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h3 class="mb-2 mt-2">CONOSCENZE LINGUISTICHE
                                @if($cv_editing)
                                    <a href="#" id="cv_language_widget_add_new"><i
                                            class="fa fa-plus-circle text-primary float-right"></i></a>
                            @endif
                            </h3>
                            <ul class="fa-ul mb-3">
                                @forelse(Auth::user()->cv->languages as $language)
                                    <language :language="{{ $language }}" :editing="{{ $cv_editing }}"></language>
                                @empty
                                @endforelse
                            </ul>
                        </div>
                    </div>
                    <hr class="dropdown-divider"/>
                    <h5 class="mb-4">Skills
                        @if($cv_editing)
                            <a href="#" id="cv_skill_widget_add_new"><i
                                    class="fa fa-plus-circle text-primary float-right"></i></a>
                        @endif
                    </h5>
                    <div class="row mb-5">
                        @forelse(Auth::user()->cv->skills as $skill)
                            <skill :skill="{{ $skill }}" :editing="{{ $cv_editing }}"></skill>
                        @empty
                        @endforelse
                    </div>

                    <hr class="dropdown-divider"/>
                    <h5 class="mb-4">Hobbies
                        @if($cv_editing)
                            <a href="#" id="cv_hobby_widget_add_new"><i
                                    class="fa fa-plus-circle text-primary float-right"></i></a>
                        @endif
                    </h5>
                    <ul class="fa-ul mb-5">
                        @forelse(Auth::user()->cv->hobbies as $hobby)
                            <hobby :hobby="{{$hobby}}" :editing="{{ $cv_editing }}"></hobby>
                        @empty
                        @endforelse
                    </ul>
                    <div class="card bg-light shadow-none">
                        <div class="card-body d-flex align-items-center justify-content-between p-4">
                            <p class="lead mb-0">We look forward to hearing from you</p>
                            <a class="btn btn-primary btn-marketing rounded-pill ml-2" href="#!">Apply Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('vendor/fontawesome-iconpicker-master/dist/js/fontawesome-iconpicker.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $("#cv_widget_add_new").on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: '{{ route('cv.add_widget') }}',
                    type: 'get',
                    success: function () {
                        location.reload();
                    }
                });
            });
            $("#cv_social_widget_add_new").on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: '{{ route('cv.add_social_widget') }}',
                    type: 'get',
                    success: function () {
                        location.reload();
                    }
                });
            });
            $("#cv_experience_widget_add_new").on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: '{{ route('cv.add_experience_widget') }}',
                    type: 'get',
                    success: function () {
                        location.reload();
                    }
                });
            });
            $("#cv_education_widget_add_new").on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: '{{ route('cv.add_education_widget') }}',
                    type: 'get',
                    success: function () {
                        location.reload();
                    }
                });
            });
            $("#cv_certificate_widget_add_new").on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: '{{ route('cv.add_certificate_widget') }}',
                    type: 'get',
                    success: function () {
                        location.reload();
                    }
                });
            });
            $("#cv_language_widget_add_new").on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: '{{ route('cv.add_language_widget') }}',
                    type: 'get',
                    success: function () {
                        location.reload();
                    }
                });
            });
            $("#cv_skill_widget_add_new").on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: '{{ route('cv.add_skill_widget') }}',
                    type: 'get',
                    success: function () {
                        location.reload();
                    }
                });
            });
            $("#cv_hobby_widget_add_new").on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: '{{ route('cv.add_hobby_widget') }}',
                    type: 'get',
                    success: function () {
                        location.reload();
                    }
                });
            });
        });
    </script>
@endsection
