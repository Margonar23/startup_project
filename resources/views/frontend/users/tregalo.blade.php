@extends('layouts.frontend.frontend')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/avatar.css') }}"/>
@endsection
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Profilo personale</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-white">
        <div class="container mt-10">
            @error('message')
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="alert alert-success">Grazie per il tuo ordine! riceverai una mail di conferma!</div>
                </div>
            </div>
            @enderror
            <div class="row">
                <div class="col-lg-3 py-5">
                    <ul class="list-group list-group-flush list-group-careers">
                        <li class="list-group-item pt-0">
                            <a class="small" href="{{ route('home') }}">
                                <i class="fas fa-arrow-left mr-1"></i> {{ trans('frontend/users/profile.back_to_home_page') }}
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('users.profile') }}"
                               class=""> {{ trans('frontend/users/profile.profile') }}</a>
                            <i class="fas fa-user-edit fa-fw text-primary"></i>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('users.tregalo') }}"
                               class="font-weight-bold"> T-REGALO</a>
                            <i class="fas fa-shopping-bag fa-fw text-primary"></i>
                        </li>
                        @if(env('APP_ENV') !== 'middle_dev')
                            <li class="list-group-item">
                                <a href="{{ route('users.cv') }}"> {{ trans('frontend/users/profile.cv') }}</a>
                                <i class="fas fa-user-graduate fa-fw text-gray-400"></i>
                            </li>
                        @endif
                    </ul>
                </div>
                <div class="col-lg-9">
                    <section class="py-5">
                        <div class="accordion accordion-faq mb-5" id="authAccordion">
                            <div class="card border-bottom">
                                <div class="card-body">
                                    <div class="accordion-faq-title">
                                        <div class="mr-2">
                                            <h4 class="mb-0">Ordini T-REGALO</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @foreach(Auth::user()->order_tregalo->sortByDesc('created_at') as $order)
                                <div class="card accordion-faq-item">
                                    <a class="card-header collapsed" id="authHeading{{$order->id}}"
                                       data-toggle="collapse"
                                       data-target="#authCollapse{{$order->id}}" aria-expanded="true"
                                       aria-controls="authCollapse{{$order->id}}" href="javascript:void(0);">
                                        <div class="accordion-faq-item-heading">

                                            Ordine #{{ $order->id }}
                                            del {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $order->created_at)->format('d M Y H:i') }}
                                            @if($order->shipped)
                                                <span class="badge badge-success">Spedito</span>
                                            @else
                                                <span class="badge badge-warning">In attesa di essere spedito</span>
                                            @endif
                                            <i class="accordion-faq-item-heading-arrow" data-feather="chevron-down"></i>
                                        </div>
                                    </a>
                                    <div class="collapse" id="authCollapse{{$order->id}}"
                                         aria-labelledby="authHeading{{$order->id}}"
                                         data-parent="#authAccordion">
                                        <div class="card-body border-bottom">
                                            <div class="py-2">
                                                <table class="table table-striped table-borderless">
                                                    @foreach($order->order_row as $product)
                                                        <tr>
                                                            <th>{{ $product->tregalo->name }}</th>
                                                            <td>{{ $product->qty }} x {{ ($product->tregalo->price / 100) }} CHF</td>
                                                            <td>{{ ($product->tregalo->price / 100) * $product->qty }}
                                                                CHF
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <th colspan="2">Totale</th>
                                                        <th>{{ $order->total() }} CHF</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
    </script>
@endsection
