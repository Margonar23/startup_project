@extends('layouts.frontend.frontend')
@section('styles')
    <link rel="stylesheet"
          href="{{ asset('vendor/fontawesome-iconpicker-master/dist/css/fontawesome-iconpicker.min.css') }}">
@endsection
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Curriculum Vitae</h1>
                        <p class="page-header-text">Mostra alle aziende presenti nel territorio di che pasta sei
                            fatto</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-white">
        <div class="container mt-10">
            <div class="row">
                <div class="col-lg-3 py-5">
                    <ul class="list-group list-group-flush list-group-careers">
                        <li class="list-group-item pt-0">
                            <a class="small" href="{{ route('home') }}">
                                <i class="fas fa-arrow-left mr-1"></i> {{ trans('frontend/users/profile.back_to_home_page') }}
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('users.profile') }}"> {{ trans('frontend/users/profile.profile') }}</a>
                            <i class="fas fa-user-edit fa-fw text-gray-400"></i>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('users.tregalo') }}"
                               class=""> T-REGALO</a>
                            <i class="fas fa-shopping-bag fa-fw text-primary"></i>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('users.cv') }}"
                               class="font-weight-bold"> {{ trans('frontend/users/profile.cv') }}</a>
                            <i class="fas fa-user-graduate fa-fw text-primary"></i>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-9 py-5">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>{{ Auth::user()->name() }}
                            @if(!is_null(Auth::user()->country_id))
                                    <img src="{{ asset('/images/countries/'.strtolower(Auth::user()->country->iso_2).'.png') }}">
                                    {{Auth::user()->country->name}}
                                @endif
                            </h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <small class="text-primary">
                                Questi valori ti permettono di risultare nelle ricerche di personale.<br>
                                Questi campi sono per il lavoro che stai cercando, quindi se sei studente,<br>
                                inserisci la professione che vorresti trovare.
                            </small>
                        </div>
                    </div>

                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <permit v-bind:authUser="{{ Auth::id() }}"></permit>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <profession v-bind:authUser="{{ Auth::id() }}"></profession>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <employment v-bind:authUser="{{ Auth::id() }}"></employment>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <percentage v-bind:authUser="{{ Auth::id() }}"></percentage>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <skills v-bind:authUser="{{ Auth::id() }}"></skills>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <license v-bind:authUser="{{ Auth::id() }}"></license>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <certificate v-bind:authUser="{{ Auth::id() }}"></certificate>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <diploma v-bind:authUser="{{ Auth::id() }}"></diploma>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <job-experience v-bind:authUser="{{ Auth::id() }}"></job-experience>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <language v-bind:authUser="{{ Auth::id() }}"></language>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <hobby v-bind:authUser="{{ Auth::id() }}"></hobby>
                    </div>
                    <hr class="dropdown-divider"/>
                    <div class="row">
                        <search-work v-bind:authUser="{{ Auth::id() }}"></search-work>
                    </div>
{{--                    <hr class="dropdown-divider"/>--}}
{{--                    <div class="row">--}}
{{--                        <cv-pdf v-bind:authUser="{{ Auth::id() }}" v-bind:editMode="1"></cv-pdf>--}}
{{--                    </div>--}}
                    <hr class="dropdown-divider"/>
                    <div class="card bg-light shadow-none">
                        <div class="card-body d-flex align-items-center justify-content-between p-4">
                            <p class="lead mb-0">Ci sono aziende che cercano proprio te</p>
                            <a class="btn btn-primary btn-marketing rounded-pill ml-2" href="{{ route('job_offers') }}">Guarda le offerte</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
<script>
    import Profession from "../../../js/components/pages/cv/profession";
    export default {
        components: {Profession}
    }
</script>
    <script>
    </script>
@endsection
