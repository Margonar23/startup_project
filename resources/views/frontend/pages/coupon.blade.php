@extends('layouts.frontend.frontend')

@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content pt-10">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6" data-aos="fade-up">
                        <h1 class="page-header-title text-xl-center font-weight-bolder">{{ env('COMPANY_SLOGAN') }} <br>
                        </h1>
                    </div>
                    <div class="col-lg-6 col-xs-12 d-lg-block" data-aos="fade-up" data-aos-delay="50">
                        <img class="img-fluid" src="{{ asset('images/logo-tigit-no-bg.png') }}"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-light py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center">
                        <div class="text-xs text-uppercase-expanded text-primary mb-2">T-REGALO</div>
                        <h2 class="">Buoni di promozione del Ticino</h2>
                        <a href="{{ route('buy_t_regalo') }}" class="btn btn-sm btn-primary mb-5">Compra i nostri buoni</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-5 aos-init aos-animate" data-aos="fade-up">
                    <a class="card text-center text-decoration-none h-100 lift font-weight-200  "
                       href="{{ route('buy_t_regalo') }}">
                        <div class="card-body p-0">
                            <img src="{{ asset('images/coupon/20fr.jpg') }}" width="100%">
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 mb-5 aos-init aos-animate" data-aos="fade-up">
                    <a class="card text-center text-decoration-none h-100 lift font-weight-200  bg-light" href="{{ route('buy_t_regalo') }}">
                        <div class="card-body p-0">
                            <img src="{{ asset('images/coupon/50fr.jpg') }}" width="100%">
                        </div>
                    </a>
                </div>

                <div class="col-lg-4 col-md-6 mb-5 aos-init aos-animate" data-aos="fade-up">
                    <a class="card text-center text-decoration-none h-100 lift font-weight-200  bg-light"
                       href="{{ route('buy_t_regalo') }}">
                        <div class="card-body p-0">
                            <img src="{{ asset('images/coupon/100fr.jpg') }}" width="100%">
                        </div>
                    </a>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="text-center">
                        <a class="btn btn-primary btn-lg text-white" href="{{ route('tRegalo') }}">
                            <i class="fa fa-info-circle"></i>
                            Scopri di piu sull'iniziativa
                        </a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="text-center">
                        <h2 class="m-5">Ecco dove è possibile utilizzare il buono T-Regalo</h2>
                    </div>
                </div>
            </div>
            <coupon-companies></coupon-companies>
        </div>
    </section>
@endsection
