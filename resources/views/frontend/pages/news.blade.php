@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')

    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">News</h1>
                        <p class="page-header-text mb-0">Resta aggiornato!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-light py-10">
        <div class="container">
            @foreach($news as $n)
                @if($loop->first)
                    <a class="card post-preview post-preview-featured lift mb-5"
                       href="{{ route('news.detail', ['news' => $n->id]) }}">
                        <div class="row no-gutters">
                            <div class="col-lg-5">
                                <div class="post-preview-featured-img">
                                    <img src="{{ asset($n->header_image)}}" width="100%" height="100%">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="card-body">
                                    <div class="py-5">
                                        <h5 class="card-title">{{ $n->title }}</h5>
                                        <p class="card-text">{{ $n->subtitle }}</p>
                                    </div>
                                    <hr/>
                                    <div class="post-preview-meta">
                                        @if(file_exists(asset($n->user->avatar)))
                                            <img class="post-preview-meta-img"
                                                 src="{{ asset($n->user->avatar) }}"/>
                                        @endif
                                        <div class="post-preview-meta-details">
                                            <div class="post-preview-meta-details-name">{{ $n->user->name() }}</div>
                                            <div class="post-preview-meta-details-date">{{ $n->created_at }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                @endif
            @endforeach
            <div class="row">
                @foreach($news as $n)
                    @if(!$loop->first)
                        <div class="col-md-6 col-xl-4 mb-5">
                            <a class="card post-preview lift h-100"
                               href="{{ route('news.detail', ['news' => $n->id]) }}">
                                <img class="card-img-top" src="{{ asset($n->header_image)}}" alt="..."/>
                                <div class="card-body">
                                    <h5 class="card-title">{{ $n->title }}</h5>
                                    <p class="card-text">{{ $n->subtitle }}</p>
                                </div>
                                <div class="card-footer">
                                    <div class="post-preview-meta">
                                        <img class="post-preview-meta-img"
                                             src="{{ asset($n->user->avatar) }}"/>
                                        <div class="post-preview-meta-details">
                                            <div class="post-preview-meta-details-name">{{ $n->user->name() }}</div>
                                            <div class="post-preview-meta-details-date">{{ $n->created_at }}</div>
                                        </div>
                                    </div>
                                </div>
                            </a
                            >
                        </div>
                    @endif
                @endforeach
            </div>
            <nav class="text-center">
                {{ $news->links() }}
            </nav>
        </div>

    </section>
    </div>
@endsection
@section('scripts')

@endsection
