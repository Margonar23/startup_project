@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content pt-10">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Pacchetti</h1>
                        <p class="page-header-text">Scegli accuratamente tra le nostre offerte per aziende</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-light pt-10">
        <div class="container">
            <div class="text-center mb-5">
                <h2>Prezzi semplici</h2>
                <p class="lead">Prezzi semplici significa nessuna sopresa.</p>
            </div>
            <div class="row z-1">
                @foreach($packages as $package)
                    <div class="col-lg-6 mb-5 aos-init aos-animate">
                        <div class="card pricing h-100">
                            <div class="card-header text-white  bg-{{ $package->color }}">{{ $package->name }}</div>
                            <div class="card-body p-5">
                                <div class="text-center">
                                    <div class="pricing-price">
                                        <sup>CHF</sup>
                                        {{ $package->package_prices->filter(function ($price, $index){
                                                return $price->payment_type_id == 1;
                                        })->first()->price }}
                                        <span class="pricing-price-period">/mese</span>
                                    </div>
                                    <div class="pricing-price">
                                        <sup>CHF</sup>
                                        {{ $package->package_prices->filter(function ($price, $index){
                                                return $price->payment_type_id == 2;
                                        })->first()->price }}
                                        <span class="pricing-price-period">/anno</span>
                                    </div>
                                </div>
                                <ul class="fa-ul pricing-list">
                                    @foreach($package->features as $feature)
                                        <li class="pricing-list-item">
                                            <span class="fa-li"><i
                                                    class="far fa-check-circle text-teal"></i></span><span
                                                class="text-dark">{{ $feature->name }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="svg-border-rounded text-dark">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </section>
    <section class="bg-dark py-10">
        <div class="container">
            <div class="row my-10">
                @foreach($features as $feature)
                    <div class="col-lg-6 mb-5">
                        <div class="d-flex h-100">
                            <div class="icon-stack flex-shrink-0 bg-teal text-white">
                                <i class="fas fa-question"></i></div>
                            <div class="ml-4">
                                <h5 class="text-white">{{ $feature->name }}</h5>
                                <p class="text-white-50">{{ $feature->description }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </section>
    <section class="bg-white py-10">
        <div class="container">
            <div class="row align-items-center mb-2">
                <div class="col-lg-6">
                    <div class="h4 font-weight-normal">Sei pronto a partire?</div>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <a class="btn btn-white btn-marketing rounded-pill my-2 lift lift-sm"
                       href="{{ route('users.registration', ['step' => 3]) }}">
                        Crea il tuo account
                    </a>
                </div>
            </div>
            <div class="row align-items-center mb-10">
                <div class="col-lg-6">
                    <div class="h4 font-weight-normal">I nostri pacchetti non ti soddisfano? <bR>Creane uno AD HOC con un nostro venditore</div>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <a class="btn btn-primary btn-marketing rounded-pill mr-3 my-2 lift lift-sm" href="{{ route('contacts') }}">
                        Contatta un venditore</a>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection
