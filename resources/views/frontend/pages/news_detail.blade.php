@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">News</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-light py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-xl-8">
                    <div class="single-post">
                        <h1>{{ $news->title }}</h1>
                        <p class="lead">{{ $news->subtitle }}</p>
                        <div class="d-flex align-items-center justify-content-between mb-5">
                            <div class="single-post-meta mr-4">
                                @if(file_exists(asset($news->user->avatar)))
                                <img class="single-post-meta-img"
                                     src="{{ asset($news->user->avatar) }}"/>
                                @endif
                                <div class="single-post-meta-details">
                                    <div class="single-post-meta-details-name">{{$news->user->name()}}</div>
                                    <div class="single-post-meta-details-date">{{ $news->created_at }}
                                    </div>
                                </div>
                            </div>


                        </div>
                        <img class="img-fluid mb-2" src="{{ asset($news->header_image) }}"/>
                        <div class="single-post-text my-5">
                            {!! $news->description !!}
                            <hr class="my-5"/>
                            <div class="text-center">
                                <a class="btn btn-transparent-dark btn-marketing rounded-pill"
                                   href="{{ route('news') }}">
                                    Torna alle news
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection
