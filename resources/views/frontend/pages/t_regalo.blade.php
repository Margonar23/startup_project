@extends('layouts.frontend.frontend')

@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content pt-10">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12" data-aos="fade-up">
                        <h1 class="page-header-title text-xl-center font-weight-bolder">
                            T-REGALO <small>La prima iniziativa firmata Tigit!</small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>

    <section class="bg-light py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center">
                        <div class="text-xs text-uppercase-expanded text-primary mb-2">T-REGALO</div>
                        <h2 class="mb-5">Buoni di promozione del Ticino</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-5 aos-init aos-animate" data-aos="fade-up">
                    <a class="card text-center text-decoration-none h-100 lift font-weight-200  bg-green-soft"
                       href="#!">
                        <div class="card-body p-0">
                            <img src="{{ asset('images/coupon/20fr.jpg') }}" width="100%">
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 mb-5 aos-init aos-animate" data-aos="fade-up">
                    <a class="card text-center text-decoration-none h-100 lift font-weight-200  bg-blue-soft" href="#!">
                        <div class="card-body p-0">
                            <img src="{{ asset('images/coupon/50fr.jpg') }}" width="100%">
                        </div>
                    </a>
                </div>

                <div class="col-lg-4 col-md-6 mb-5 aos-init aos-animate" data-aos="fade-up">
                    <a class="card text-center text-decoration-none h-100 lift font-weight-200  bg-yellow-soft"
                       href="#!">
                        <div class="card-body p-0">
                            <img src="{{ asset('images/coupon/100fr.jpg') }}" width="100%">
                        </div>
                    </a>
                </div>
                <div class="col-md-12 text-center">
                    <a href="{{ route('buy_t_regalo') }}" class="btn btn-success btn-lg">Acquista i buoni!</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center" style="font-size: 3em">Privati?</h1>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <img src="{{ asset('images/t-regalo/T-Regalo privati-1.png') }}"  class="img-thumbnail">
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset('images/t-regalo/T-Regalo privati-2.png') }}" class="img-thumbnail">
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset('images/t-regalo/T-Regalo privati-3.png') }}" class="img-thumbnail">
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset('images/t-regalo/T-Regalo privati-4.png') }}" class="img-thumbnail">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <h1 class="text-center" style="font-size: 3em">Aziende?</h1>
                    <div class="row">
                        <div class="col-md-6">
                            <img src="{{ asset('images/t-regalo/T-Regalo spiegazione aziende-1.png') }}" class="img-thumbnail">
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset('images/t-regalo/T-Regalo spiegazione aziende-2.png') }}" class="img-thumbnail">
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset('images/t-regalo/T-Regalo spiegazione aziende-3.png') }}" class="img-thumbnail">
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset('images/t-regalo/T-Regalo spiegazione aziende-4.png') }}" class="img-thumbnail">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-white py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center">
                        <div class="text-xs text-uppercase-expanded text-primary mb-2">AZIENDE</div>
                        <h2 class="mb-5">Ecco dove è possibile utilizzare il buono T-Regalo</h2>
                    </div>
                </div>
            </div>
            <coupon-companies></coupon-companies>
        </div>
    </section>
@endsection
