@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')

    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">I nostri partner</h1>
                        <p class="page-header-text mb-0">Hai bisogno di un’azienda, ma non sai di chi fidarti? Troppe possibilità diverse senza mezzi di confronto?
                            T-VEDO è un nostro motore di ricerca che permette di trovare le migliori aziende in qualsiasi settore.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-light py-10">
        <companies></companies>


    </section>
@endsection
@section('scripts')

@endsection
<script>
    import Companies from "../../../js/components/pages/companies/companies";
    export default {
        components: {Companies}
    }
</script>
