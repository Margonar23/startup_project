@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">{{ $company->name }}</h1>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="svg-border-rounded text-light">--}}
        {{--            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"--}}
        {{--                 fill="currentColor">--}}
        {{--                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>--}}
        {{--            </svg>--}}
        {{--        </div>--}}
    </header>
    <section class="bg-light py-10">
        <div class="container">
            <company-detail v-bind:companyId="{{ $company->id }}"
                            v-bind:user="{{ Auth::check() ? Auth::id() : 0 }}"></company-detail>
            <div class="row">
                <div class="col-md-12">
                    @foreach($company->presentation_pages as $page)
                        @if($page->active)
                            <section class="p-5 border-top {{ $loop->index % 2 == 0 ? 'bg-white' : 'bg-light' }}">
                                @include('frontend.pages.template.'.$page->template->template)
                            </section>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>


        <div class="svg-border-rounded text-dark">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34"
                 preserveAspectRatio="none"
                 fill="currentColor">
                <path
                    d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </section>
@endsection
@section('scripts')

@endsection
