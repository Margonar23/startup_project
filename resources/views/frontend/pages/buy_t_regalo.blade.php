@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content pt-10">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12" data-aos="fade-up">
                        <h1 class="page-header-title text-xl-center font-weight-bolder">
                            T-REGALO <small>La prima iniziativa firmata Tigit!</small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>

    <section class="bg-white py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="text-center">
                        <div class="text-xs text-uppercase-expanded text-primary mb-2">T-REGALO</div>
                        <h2 class="mb-5">Acquista il tuo buono</h2>
                    </div>
                </div>

                @if(Auth::check())
                    <div class="col-md-12">
                        <form action="{{ route('store_buy_t_regalo') }}" method="post" id="payment-form">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label class="text-dark font-weight-bold" for="inputName">Nome: <span
                                            class="text-danger">*</span>
                                    </label>
                                    <input class="form-control" name="name" type="text" placeholder="Nome completo"
                                           value="{{ Auth::user()->first_name }} {{Auth::user()->last_name}}">
                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="text-dark font-weight-bold" for="inputEmail">E-Mail: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" name="email" type="email" placeholder="name@example.com"
                                           value="{{ Auth::user()->email }}">
                                    @error('email')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="text-dark font-weight-bold" for="inputName">Telefono:</label>
                                    <input class="form-control" name="phone" type="text" placeholder="Nr. telefono"
                                           value="{{ isset(Auth::user()->phone) ? Auth::user()->phone : null }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="text-dark font-weight-bold" for="inputName">Indirizzo di spedizione
                                        completo: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" name="address" type="text"
                                           placeholder="Via esempio 4, 6900 Lugano">
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="text-dark font-weight-bold" for="inputName">Seleziona il buono: <span
                                            class="text-danger">*</span></label>
                                    <table class="table table-borderless">
                                        <tr>
                                            <th>Prodotto</th>
                                            <th>Quantità</th>
                                            <th>Prezzo unitario</th>
                                            <th class="text-right">Totale</th>
                                        </tr>
                                        @foreach($tregalo as $item)
                                            <tr class="border-bottom">
                                                <td>{{ $item->name }}</td>
                                                <td>
                                                    <input type="number" min="0" value="0" data-item="{{ $item->id }}"
                                                           data-price="{{ (intval($item->price) / 100) }}"
                                                           class="col-md-3 form-control form-control-sm qty"
                                                            name="item_{{ $item->id }}">
                                                </td>
                                                <td>{{ intval($item->price) / 100 }} CHF</td>
                                                <td class="text-right"><span class="final_price" data-subtotal="0"
                                                                             id="total_{{$item->id}}">0 CHF</span></td>
                                            </tr>
                                        @endforeach
                                        <tr class="border-bottom">
                                            <th colspan="3">Prezzo totale</th>
                                            <th class="text-right"><span id="total_price">0 CHF</span></th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6  form-control-feedback">
                                    <label class="text-dark">Carta di pagamento:</label>
                                    <div id="card-element"></div>
                                    <div class="row" style="display:none;">
                                        <div class="col-xs-12">
                                            <p class="payment-errors"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="text-center">
                                    <input class="btn btn-primary btn-marketing mt-4" type="submit"
                                           value="Acquista"/>
                                </div>
                            </div>
                        </form>
                    </div>
                @else
                    <div class="col-md-12 text-center">
                        <div class="alert alert-warning">
                            Devi registrarti per acquistare i buoni... <a href="{{ route('users.login') }}">clicca
                                qui </a> per fare il login o registrarti
                        </div>
                    </div>
                @endif
            </div>


        </div>
    </section>
@endsection
@section('scripts')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        $(document).ready(function () {
            var total_price = 0;

            function set_total() {
                var total = 0;
                $("span.final_price").each(function () {
                    total = total + parseInt($(this).data('subtotal'));
                });
                $("#total_price").text(total+' CHF');
            }

            $(".qty").on('change', function () {
                let qty = $(this).val();
                let price = $(this).data('price');
                let item_id = $(this).data('item');

                let subtotal = qty * price;
                $("#total_" + item_id).text(subtotal + ' CHF');
                $("#total_" + item_id).data('subtotal', subtotal);

                console.log(price);
                console.log(item_id);
                console.log(qty);
                set_total();
            });


            var style = {
                base: {
                    color: '#32325d',
                    lineHeight: '18px',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#8d98a5'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            const stripe = Stripe('{{ env("STRIPE_KEY") }}', {locale: 'ch'}); // Create a Stripe client.
            const elements = stripe.elements(); // Create an instance of Elements.
            const card = elements.create('card', {hidePostalCode: true, style: style}); // Create an instance of the card Element.

            card.mount('#card-element'); // Add an instance of the card Element into the `card-element` <div>.

            card.on('change', function (event) {
                var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });

            // Handle form submission.
            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function (event) {
                event.preventDefault();

                stripe.createToken(card).then(function (result) {
                    if (result.error) {
                        // Inform the user if there was an error.
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        // Send the token to your server.
                        stripeTokenHandler(result.token);
                    }
                });
            });

            // Submit the form with the token ID.
            function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                form.submit();

            }

        });
    </script>
@endsection()
