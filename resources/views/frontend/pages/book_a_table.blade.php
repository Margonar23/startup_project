@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')

    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Riserva il tuo tavolo</h1>
                        <p class="page-header-text mb-0">Dettagli riservazione</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-white pb-10">

        <div class="container mt-5">
            @auth()
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-borderless table-striped">
                            <tr>
                                <th>{{ $location->name }}</th>
                                <td>{{ $location->address }}</td>
                                <td>max {{ $location->max_capacity }} <i class="fa fa-users"></i></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <form action="{{ route('reservations.booking.store', ['location' => $location->id]) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="font-weight-bold" for="role_name">Data della tua prenotazione *</label>
                                <input class="form-control @error('booking_date') is-invalid @enderror"
                                       name="booking_date"
                                       type="text" id="booking_date" readonly
                                       value="{{ isset($booking_date) ? \Carbon\Carbon::createFromFormat('Y-m-d', $booking_date)->format('d.m.Y') : '' }}"/>
                                @error('booking_date')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-bold" for="role_name"> Orario *</label>
                                <select name="booking_time"
                                        class="form-control @error('booking_time') is_invalid @enderror">
                                    @foreach($booking_time as $time)
                                        <option
                                            value="{{ $time }}">{{ \Carbon\Carbon::createFromFormat('H:i:s', $time)->format('H:i') }}</option>
                                    @endforeach
                                </select>
                                @error('booking_time')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-bold" for="role_name"> Numero di persone *</label>
                                <input class="form-control @error('people_nr') is-invalid @enderror" name="people_nr"
                                       type="number"
                                       min="1"
                                       value="{{ old('people_nr') }}"/>
                                @error('people_nr')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="font-weight-bold" for="role_name">Nome *</label>
                                <input class="form-control @error('firstname') is-invalid @enderror" name="firstname"
                                       type="text"
                                       value="{{ Auth::user()->first_name }}"/>
                                @error('firstname')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-bold" for="role_name">Cognome *</label>
                                <input class="form-control @error('lastname') is-invalid @enderror" name="lastname"
                                       type="text"
                                       value="{{ Auth::user()->last_name }}"/>
                                @error('lastname')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="font-weight-bold" for="role_name">Telefono *</label>
                                <input class="form-control @error('phone') is-invalid @enderror" name="phone"
                                       type="text"
                                       value="{{ Auth::user()->phone }}"/>
                                @error('phone')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-bold" for="role_name">E-Mail *</label>
                                <input class="form-control @error('email') is-invalid @enderror" name="email"
                                       type="email"
                                       value="{{ Auth::user()->email }}"/>
                                @error('email')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="font-weight-bold" for="role_name">Indirizzo</label>
                                <input class="form-control @error('address') is-invalid @enderror" name="address"
                                       type="text"
                                       value="{{ old('address') }}"/>
                                @error('address')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-2">
                                <label class="font-weight-bold" for="role_name">Cap</label>
                                <input class="form-control @error('cap') is-invalid @enderror" name="cap"
                                       type="number"
                                       value="{{ old('cap') }}"/>
                                @error('cap')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-bold" for="role_name">Citta</label>
                                <input class="form-control @error('city') is-invalid @enderror" name="city"
                                       type="text"
                                       value="{{ old('city') }}"/>
                                @error('city')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            @if(count($location->rooms))
                                <div class="col-md-6">
                                    <label class="font-weight-bold" for="role_name">Hai un locale preferito?</label>
                                    <select class="form-control" name="room_id">
                                        <option value="0">---NON HO UNA PREFERENZA</option>
                                        @foreach($location->rooms as $room)
                                            <option
                                                value="{{ $room->id }}" {{ old('room_id') == $room->id ? "selected" : "" }}>{{ $room->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="font-weight-bold" for="role_name">Lasciaci un messaggio</label>
                                <textarea class="form-control " name="message">
                            {{ old('message') }}
                        </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="checkbox" value="1" name="accept_policy">
                                <label class="text-sm-left ml-2  @error('accept_policy') text-danger @enderror"
                                       for="role_name">
                                    Acconsento all'utilizzo dei miei dati per il solo trattamento ai fini di una
                                    risposta
                                    alla mia richiesta.
                                    Maggiori informazioni <a href="#">Privacy Policy</a>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="Salva riservazione" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </form>
            @elseauth()
                <div class="bg-light alert">
                    Devi fare il login per riservare il tuo tavolo!
                </div>

            @endauth
        </div>

    </section>
@endsection
@section('scripts')
    @parent

@endsection
