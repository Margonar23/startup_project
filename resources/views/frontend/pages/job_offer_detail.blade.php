@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content pt-10">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">{{ $job_offer->title }}</h1>
                        <p class="page-header-text">{{ $job_offer->company->name }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>

    <section class="bg-white py-10">
        <div class="container mt-1">
            <div class="row">
                <div class="col-lg-3">
                    <ul class="list-group list-group-flush list-group-careers">
                        <li class="list-group-item pt-0">
                            <a class="small" href="{{ route('job_offers') }}">
                                <i class="fas fa-arrow-left mr-1"></i> Torna alle posizioni aperte</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('companies.detail', ['company' => $job_offer->company->id]) }}">
                                {{ $job_offer->company->name }}
                            </a>
                            <i class="fas fa-globe fa-fw text-gray-400"></i>
                        </li>
                        <li class="list-group-item">
                            {{ $job_offer->profession->name }}
                            <i class="fas fa-user-tie fa-fw text-gray-400"></i>
                        </li>
                        <li class="list-group-item">
                            {{ $job_offer->employment->name }}
                            <i class="fas fa-clock fa-fw text-gray-400"></i>
                        </li>
                        <li class="list-group-item">
                            {{ $job_offer->min_experience }} anni di esperienza
                            <i class="fas fa-code fa-fw text-gray-400"></i>
                        </li>
                        <li class="list-group-item">
                            Scade il {{ \Carbon\Carbon::parse($job_offer->offer_expiry_at)->format('d M Y') }}
                            <i class="fas fa-user-clock fa-fw text-gray-400"></i>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-9">
                    <h1 class="mb-4">{{ $job_offer->title }}</h1>
                    <p class="lead mb-4">{{ $job_offer->profession->name }}</p>
                    <p class="mb-4">{!! $job_offer->description !!}</p>
                    <h5 class="mb-4">Requisiti</h5>
                    <ul class="fa-ul mb-5">
                        @foreach($job_offer->skills_list() as $skill)
                            <li class="mb-3">
                                <span class="fa-li"><i class="far fa-dot-circle text-primary-soft"></i></span>
                                {{ $skill->name }}
                            </li>
                        @endforeach
                    </ul>

                    @if(!$job_offer->applier->contains('user_id', Auth::id()))
                        <div class="card bg-light shadow-none">
                            <div class="card-body d-flex align-items-center justify-content-between p-4">
                                <p class="lead mb-0">Stiamo cercando proprio te!</p>
                                @if(\Illuminate\Support\Facades\Auth::check())
                                    <a class="btn btn-primary btn-marketing rounded-pill ml-2"
                                       href="{{ route('job_offer.apply', ['job_offer' => $job_offer->id]) }}">Candidati
                                        ora</a>
                                @else
                                    <a class="btn btn-primary btn-marketing rounded-pill ml-2"
                                       href="{{ route('users.login') }}">Effettua l'accesso per candidarti al
                                        concorso</a>
                                @endif
                            </div>
                        </div>
                    @else
                        <div class="card bg-success text-white shadow-none">
                            <div class="card-body d-flex align-items-center justify-content-between p-4">
                                <p class="lead mb-0"> Ti sei gia candidato per questa posizione!</p>
                                <i class="fas fa-check-circle fa-2x float-right"></i>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-dark">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </section>

@endsection
@section('scripts')

@endsection
