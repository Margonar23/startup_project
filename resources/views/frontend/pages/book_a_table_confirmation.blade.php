@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')

    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Registrazione effettuata con successo</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-white pb-10">
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-success text-lg-center">Riservazione avvenuta con successo</h1>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <i class="fa fa-thumbs-up fa-5x text-success"></i>
                        </div>
                        <div class="col-md-12 text-lg-center mt-4">
                            <h3>Riceverai una mail di conferma dal ristorante.</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="svg-border-rounded text-dark">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34"
                 preserveAspectRatio="none"
                 fill="currentColor">
                <path
                    d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </section>
@endsection
@section('scripts')

@endsection
<script>

</script>
