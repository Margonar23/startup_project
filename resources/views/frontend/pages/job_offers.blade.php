@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content pt-10">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Offerte di lavoro</h1>
                        <p class="page-header-text">Iscriviti alla nostra piattaforma e compila il tuo profilo
                            personale.
                            In questa sezione puoi tenere sott’occhio le nuove offerte di lavoro e candidarti in quella
                            che più ti ispira!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>

    <section class="bg-white py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="col-md-12">
                        <div class="accordion accordion-faq mb-5" id="authAccordion">
                            <div class="card accordion-faq-item">
                                <a class="card-header collapsed" id="authHeadingTwo" data-toggle="collapse"
                                   data-target="#authCollapseTwo" aria-expanded="false" aria-controls="authCollapseTwo"
                                   href="javascript:void(0);">
                                    <div class="accordion-faq-item-heading">Vuoi saperne di più?
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none"
                                             stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round"
                                             class="feather feather-chevron-down accordion-faq-item-heading-arrow">
                                            <polyline points="6 9 12 15 18 9"></polyline>
                                        </svg>
                                    </div>
                                </a>
                                <div class="collapse" id="authCollapseTwo" aria-labelledby="authHeadingTwo"
                                     data-parent="#authAccordion"
                                     style="">
                                    <div class="card-body border-bottom">
                                        Alcune offerte di lavoro sono in modalità “privata”. Tramite un nostro sistema
                                        di filtraggio equo e preciso segnaleremo alle aziende i candidati più idonei per
                                        il posto in questione!
                                        Cerchiamo di dare al tuo profilo la maggior visibilità possibile!
                                        Unisciti alla nostra comunità!
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-10">
                    @forelse($offers as $company_id => $offer)
                        <div class="d-flex align-items-center justify-content-between">
                            <h2 class="mb-0">{{ \App\Company::find($company_id)->name }}</h2>
                            <div
                                class="badge badge-primary-soft text-primary badge-marketing">{{ count($offers[$company_id]) }}
                                Posizioni aperte
                            </div>
                        </div>
                        <hr class="mb-0">
                        <ul class="list-group list-group-flush list-group-careers">
                            @foreach($offers[$company_id] as $offer)
                                <li class="list-group-item">
                                    <a href="{{ route('job_offer_detail', ['job_offer' => $offer->id]) }}">{{ $offer->title }}
                                        , {{ $offer->profession->name }}</a>
                                    <div class="small">{{ $offer->employment->name }}
{{--                                        - {{\App\Company::find($company_id)->address}}--}}
                                        @if($offer->applier->contains('user_id', Auth::id()))
                                            <i class="fa fa-check-circle text-success" title="ti sei gia candidato"></i>
                                        @endif
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    @empty
                        <div class="alert alert-info-soft"><i class="fa fa-exclamation-triangle"></i> Non ci sono ancora
                            offerte caricate
                        </div>
                    @endforelse
                </div>

            </div>
        </div>

    </section>
    <section class="bg-light py-10">
        <div class="container">
            <div class="row align-items-center mb-10">
                <div class="col-lg-6">
                    <div class="h4 font-weight-normal">Vuoi mettere in evidenza il tuo annuncio?</div>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <a class="btn btn-primary btn-marketing rounded-pill mr-3 my-2 lift lift-sm"
                       href="{{ route('contacts') }}">
                        Contattaci</a>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection
