@extends('layouts.frontend.coupon')
@section('styles')
@endsection
@section('content')

    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Chi siamo</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-light py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <p class="lead mb-5">
                        Tigit Sagl nasce dall’idea di 3 giovani ticinesi con lo scopo di promuovere ed aiutare le impese
                        del Cantone.
                        Con l’innesto di un 4° elemento l’azienda sta sviluppando una piattaforma digitale in grado di
                        rafforzare la visibilità e il successo delle imprese ticinesi.
                    </p>
                    <h1 class="font-italic mb-5">"Tigit per un Ticino digitale"</h1>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-lg-2 mb-5">
                </div>
                <div class="col-lg-4 mb-5">
                    <div
                        class="card card-link border-top border-top-lg border-primary lift text-center o-visible h-100">
                        <div class="card-body">
                            <img src="{{ asset('images/staff/pagna.jpg') }}" class="img-thumbnail mb-3"/>
                            <h5>Roberto Pagnamenta</h5>
                            <p class="card-text"><a href="mailto:pagnamenta@tigit.ch">pagnamenta@tigit.ch</a></p>
                        </div>
                        <div class="card-footer">
                            <div class="text-teal font-weight-bold d-inline-flex align-items-center">
                                Responsabile finanziario & presidente
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-5">
                    <div
                        class="card card-link border-top border-top-lg border-primary lift text-center o-visible h-100">
                        <div class="card-body">
                            <img src="{{ asset('images/staff/willy.jpg') }}" class="img-thumbnail mb-3"/>
                            <h5>Daniel William Raguso </h5>
                            <p class="card-text"><a href="mailto:raguso@tigit.ch">raguso@tigit.ch</a></p>
                        </div>
                        <div class="card-footer">
                            <div class="text-teal font-weight-bold d-inline-flex align-items-center">
                                Responsabile amministrativo
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-lg-2 mb-5"></div>
                <div class="col-lg-4 mb-5">
                    <div
                        class="card card-link border-top border-top-lg border-primary lift text-center o-visible h-100">
                        <div class="card-body">
                            <img src="{{ asset('images/staff/hurni.png') }}" class="img-thumbnail mb-3"/>
                            <h5>Ronny Hurni </h5>
                            <p class="card-text"><a href="mailto:hurni@tigit.ch">hurni@tigit.ch</a></p>
                        </div>
                        <div class="card-footer">
                            <div class="text-teal font-weight-bold d-inline-flex align-items-center">
                                Responsabile marketing
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-5">
                    <div
                        class="card card-link border-top border-top-lg border-primary lift text-center o-visible h-100">
                        <div class="card-body">
                            <img src="{{ asset('images/staff/margonar.jpg') }}" class="img-thumbnail mb-3"/>
                            <h5>Luca Margonar</h5>
                            <p class="card-text"><a href="mailto:margonar@tigit.ch">margonar@tigit.ch</a></p>
                        </div>
                        <div class="card-footer">
                            <div class="text-teal font-weight-bold d-inline-flex align-items-center">
                                Responsabile IT
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('scripts')

@endsection
