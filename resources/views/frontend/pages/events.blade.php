@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')

    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Eventi</h1>
                        <p class="page-header-text mb-0">Non sai cosa fare venerdì sera? E sabato?
                            Vuoi scoprire nuovi concerti o serate in discoteca particolari?
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-light py-10">
        <div class="container">
            <div class="row mt-5">
                @forelse($events as $event)
                    <div class="col-md-3 mb-3">
                        <div class="card" style="width: 100%;">
                            @if(!is_null($event->cover))
                                <img src="{{$event->cover}}" class="card-img-top" alt="{{ $event->name }}">
                            @endif
                            <div class="card-body">
                                <h5 class="card-title text-center border-bottom">{{$event->name}}</h5>
                                <p class="card-text">
                                    <i class="fa fa-calendar"></i> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i', $event->getStartTime())->format('d.m.y') }}
                                    <i class="fa fa-clock"></i> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i', $event->getStartTime())->format('H:i') }}
                                    <br>
                                @if($event->location_id != 0)
                                    <p class="font-weight-bold"> {{ $event->location['name'] }} </p>
                                    <i class="fa fa-map-pin"></i>  {{ $event->location['address'] }}
                                    @endif
                                    </p>
                                    <a href="{{ route('events.detail', ['event' => $event->id]) }}"
                                       class="btn btn-primary-soft btn-sm">Dettagli</a>
                            </div>
                        </div>

                    </div>
                @empty
                    <div class="col-md-12 mb-3">
                        <div class="alert alert-info">Non ci sono eventi in programma.. torna più tardi!</div>
                    </div>
                @endforelse
            </div>
            {{ $events->links() }}
        </div>
                <events></events>
    </section>
    <section class="bg-white py-10">
        <div class="container">
            <div class="row align-items-center mb-10">
                <div class="col-lg-6">
                    <div class="h4 font-weight-normal">Vuoi mettere in anteprima il tuo evento?</div>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <a class="btn btn-primary btn-marketing rounded-pill mr-3 my-2 lift lift-sm"
                       href="{{ route('contacts') }}">
                        Contattaci</a>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection
<script>
    import Companies from "../../../js/components/pages/companies/companies";

    export default {
        components: {Companies}
    }
</script>
