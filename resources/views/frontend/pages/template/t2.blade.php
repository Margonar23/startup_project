<h1 class="text-center">{{ $page->page_content->title  }}</h1>
<i class="text-center">{{ $page->page_content->subtitle  }}</i>
<div class="row">
    <div class="col-md-4 mt-3">
        @if(!is_null($page->page_content->img))
            <img src="{{ asset($page->page_content->img) }}" width="100%">
        @endif
    </div>
    <div class="col-md-8 mt-3">
        {!! $page->page_content->content !!}
    </div>
</div>

