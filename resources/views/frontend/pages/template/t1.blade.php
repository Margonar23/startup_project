<h1>{{ $page->page_content->title  }}</h1>
<i>{{ $page->page_content->subtitle  }}</i>
<div class="mt-3">
    {!! $page->page_content->content !!}
</div>
