@extends('layouts.frontend.frontend')
@section('styles')
@endsection
@section('content')

    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h1 class="page-header-title mb-3">Chi siamo</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-light py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <p class="lead mb-5">
                        Tigit Sagl nasce dall’idea di 3 giovani ticinesi con lo scopo di promuovere ed aiutare le
                        aziende del Cantone e favorire al contempo lo sviluppo di quest’ultimo.
                        <br>Con l’innesto di un 4° elemento l’azienda ha sviluppato la piattaforma digitale su cui vi
                        trovate.
                    </p>
                    <h1 class="font-italic mb-5">"Tigit per un Ticino digitale"</h1>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-lg-2 mb-5">
                </div>
                <div class="col-lg-4 mb-5">
                    <div
                        class="card card-link border-top border-top-lg border-primary lift text-center o-visible h-100">
                        <div class="card-body">
                            <img src="{{ asset('images/staff/pagna.jpg') }}" class="img-thumbnail mb-3"/>
                            <h5>Roberto Pagnamenta</h5>
                            <p class="card-text"><a href="mailto:pagnamenta@tigit.ch">pagnamenta@tigit.ch</a></p>
                        </div>
                        <div class="card-footer">
                            <div class="text-teal font-weight-bold d-inline-flex align-items-center">
                                Responsabile finanziario & presidente
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-5">
                    <div
                        class="card card-link border-top border-top-lg border-primary lift text-center o-visible h-100">
                        <div class="card-body">
                            <img src="{{ asset('images/staff/willy.jpg') }}" class="img-thumbnail mb-3"/>
                            <h5>Daniel William Raguso </h5>
                            <p class="card-text"><a href="mailto:raguso@tigit.ch">raguso@tigit.ch</a></p>
                        </div>
                        <div class="card-footer">
                            <div class="text-teal font-weight-bold d-inline-flex align-items-center">
                                Responsabile amministrativo
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-lg-2 mb-5"></div>
                <div class="col-lg-4 mb-5">
                    <div
                        class="card card-link border-top border-top-lg border-primary lift text-center o-visible h-100">
                        <div class="card-body">
                            <img src="{{ asset('images/staff/hurni.png') }}" class="img-thumbnail mb-3"/>
                            <h5>Ronny Hurni </h5>
                            <p class="card-text"><a href="mailto:hurni@tigit.ch">hurni@tigit.ch</a></p>
                        </div>
                        <div class="card-footer">
                            <div class="text-teal font-weight-bold d-inline-flex align-items-center">
                                Responsabile marketing
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-5">
                    <div
                        class="card card-link border-top border-top-lg border-primary lift text-center o-visible h-100">
                        <div class="card-body">
                            <img src="{{ asset('images/staff/margonar.jpg') }}" class="img-thumbnail mb-3"/>
                            <h5>Luca Margonar</h5>
                            <p class="card-text"><a href="mailto:margonar@tigit.ch">margonar@tigit.ch</a></p>
                        </div>
                        <div class="card-footer">
                            <div class="text-teal font-weight-bold d-inline-flex align-items-center">
                                Responsabile IT
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-white py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <p class="lead mb-5">
                        Gli obiettivi a lungo termine sono i seguenti:
                    </p>
                    <ul class="list-group list-group-flush list-group-careers ">
                        <li class="list-group-item">Favorire la crescita delle aziende ticinesi</li>
                        <li class="list-group-item">Promuovere l’acquisto di prodotti e servizi locali di qualità</li>
                        <li class="list-group-item">Contrastare la disoccupazione ticinese</li>
                        <li class="list-group-item">Ridurre la differenza salariale rispetto agli altri Cantoni svizzeri</li>
                        <li class="list-group-item">Accrescere il peso economico del Ticino</li>
                        <li class="list-group-item">Promuovere e sostenere la digitalizzazione a livello ticinese</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-white py-10">
        <div class="container">
            <div class="row mb-5">
                {{--                <div class="col-md-12">--}}
                {{--                    <div class="accordion accordion-faq mb-5" id="authAccordion">--}}
                {{--                        <div class="card border-bottom">--}}
                {{--                            <div class="card-body">--}}
                {{--                                <div class="accordion-faq-title">--}}
                {{--                                    <div class="mr-2">--}}
                {{--                                        <h4 class="mb-0">Authentication issues</h4>--}}
                {{--                                        <p class="card-text text-gray-500">Issues related to logging in, registering a new--}}
                {{--                                            account,--}}
                {{--                                            and setting your account password</p>--}}
                {{--                                    </div>--}}
                {{--                                </div>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                        <div class="card accordion-faq-item">--}}
                {{--                            <a class="card-header collapsed" id="authHeadingOne" data-toggle="collapse"--}}
                {{--                               data-target="#authCollapseOne" aria-expanded="false" aria-controls="authCollapseOne"--}}
                {{--                               href="javascript:void(0);">--}}
                {{--                                <div class="accordion-faq-item-heading">I can't remember my account email address.--}}
                {{--                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"--}}
                {{--                                         fill="none"--}}
                {{--                                         stroke="currentColor" stroke-width="2" stroke-linecap="round"--}}
                {{--                                         stroke-linejoin="round"--}}
                {{--                                         class="feather feather-chevron-down accordion-faq-item-heading-arrow">--}}
                {{--                                        <polyline points="6 9 12 15 18 9"></polyline>--}}
                {{--                                    </svg>--}}
                {{--                                </div>--}}
                {{--                            </a>--}}
                {{--                            <div class="collapse" id="authCollapseOne" aria-labelledby="authHeadingOne"--}}
                {{--                                 data-parent="#authAccordion"--}}
                {{--                                 style="">--}}
                {{--                                <div class="card-body border-bottom">Anim pariatur cliche reprehenderit, enim eiusmod high--}}
                {{--                                    life--}}
                {{--                                    accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard--}}
                {{--                                    dolor--}}
                {{--                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt--}}
                {{--                                    aliqua put a--}}
                {{--                                    bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh--}}
                {{--                                    helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan--}}
                {{--                                    excepteur--}}
                {{--                                    butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth--}}
                {{--                                    nesciunt you probably haven't heard of them accusamus labore sustainable VHS.--}}
                {{--                                </div>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                        <div class="card accordion-faq-item">--}}
                {{--                            <a class="card-header collapsed" id="authHeadingTwo" data-toggle="collapse"--}}
                {{--                               data-target="#authCollapseTwo" aria-expanded="false" aria-controls="authCollapseTwo"--}}
                {{--                               href="javascript:void(0);">--}}
                {{--                                <div class="accordion-faq-item-heading">Why doesn't my password work?--}}
                {{--                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"--}}
                {{--                                         fill="none"--}}
                {{--                                         stroke="currentColor" stroke-width="2" stroke-linecap="round"--}}
                {{--                                         stroke-linejoin="round"--}}
                {{--                                         class="feather feather-chevron-down accordion-faq-item-heading-arrow">--}}
                {{--                                        <polyline points="6 9 12 15 18 9"></polyline>--}}
                {{--                                    </svg>--}}
                {{--                                </div>--}}
                {{--                            </a>--}}
                {{--                            <div class="collapse" id="authCollapseTwo" aria-labelledby="authHeadingTwo"--}}
                {{--                                 data-parent="#authAccordion"--}}
                {{--                                 style="">--}}
                {{--                                <div class="card-body border-bottom">Anim pariatur cliche reprehenderit, enim eiusmod high--}}
                {{--                                    life--}}
                {{--                                    accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard--}}
                {{--                                    dolor--}}
                {{--                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt--}}
                {{--                                    aliqua put a--}}
                {{--                                    bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh--}}
                {{--                                    helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan--}}
                {{--                                    excepteur--}}
                {{--                                    butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth--}}
                {{--                                    nesciunt you probably haven't heard of them accusamus labore sustainable VHS.--}}
                {{--                                </div>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                        <div class="card accordion-faq-item">--}}
                {{--                            <a class="card-header collapsed" id="authHeadingThree" data-toggle="collapse"--}}
                {{--                               data-target="#authCollapseThree" aria-expanded="true" aria-controls="authCollapseThree"--}}
                {{--                               href="javascript:void(0);">--}}
                {{--                                <div class="accordion-faq-item-heading">Why do I keep getting logged out of my account?--}}
                {{--                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"--}}
                {{--                                         fill="none"--}}
                {{--                                         stroke="currentColor" stroke-width="2" stroke-linecap="round"--}}
                {{--                                         stroke-linejoin="round"--}}
                {{--                                         class="feather feather-chevron-down accordion-faq-item-heading-arrow">--}}
                {{--                                        <polyline points="6 9 12 15 18 9"></polyline>--}}
                {{--                                    </svg>--}}
                {{--                                </div>--}}
                {{--                            </a>--}}
                {{--                            <div class="collapse" id="authCollapseThree" aria-labelledby="authHeadingThree"--}}
                {{--                                 data-parent="#authAccordion">--}}
                {{--                                <div class="card-body border-bottom">Anim pariatur cliche reprehenderit, enim eiusmod high--}}
                {{--                                    life--}}
                {{--                                    accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard--}}
                {{--                                    dolor--}}
                {{--                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt--}}
                {{--                                    aliqua put a--}}
                {{--                                    bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh--}}
                {{--                                    helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan--}}
                {{--                                    excepteur--}}
                {{--                                    butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth--}}
                {{--                                    nesciunt you probably haven't heard of them accusamus labore sustainable VHS.--}}
                {{--                                </div>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                <div class="col-lg-4 mb-5">
                    <div
                        class="card card-link border-top border-top-lg border-primary lift text-center o-visible h-100">
                        <div class="card-body">
                            <div class="icon-stack icon-stack-xl bg-primary-soft text-primary mb-4 mt-n5 z-1 shadow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-user">
                                    <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="12" cy="7" r="4"></circle>
                                </svg>
                            </div>
                            <h5>Prodotti</h5>
                            <p class="card-text">Sei pronto ad entrare nel mondo Tigit? Contatta un venditore per avare
                                tutti i dettagli!</p>
                        </div>
                        <div class="card-footer">
                            <div class="text-primary font-weight-bold d-inline-flex align-items-center">
                                <a href="mailto:info@tigit.ch"> Contatta un venditore</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-5">
                    <div
                        class="card card-link border-top border-top-lg border-secondary lift text-center o-visible h-100">
                        <div class="card-body">
                            <div
                                class="icon-stack icon-stack-xl bg-secondary-soft text-secondary mb-4 mt-n5 z-1 shadow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-life-buoy">
                                    <circle cx="12" cy="12" r="10"></circle>
                                    <circle cx="12" cy="12" r="4"></circle>
                                    <line x1="4.93" y1="4.93" x2="9.17" y2="9.17"></line>
                                    <line x1="14.83" y1="14.83" x2="19.07" y2="19.07"></line>
                                    <line x1="14.83" y1="9.17" x2="19.07" y2="4.93"></line>
                                    <line x1="14.83" y1="9.17" x2="18.36" y2="5.64"></line>
                                    <line x1="4.93" y1="19.07" x2="9.17" y2="14.83"></line>
                                </svg>
                            </div>
                            <h5>Supporto tecnico</h5>
                            <p class="card-text">Ti serve aiuto con qualche cosa? hai problemi con il tuo account?</p>
                        </div>
                        <div class="card-footer">
                            <div class="text-secondary font-weight-bold d-inline-flex align-items-center">
                                <a href="mailto:margonar@tigit.ch">Contact Support</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-5">
                    <div class="card card-link border-top border-top-lg border-teal lift text-center o-visible h-100">
                        <div class="card-body">
                            <div class="icon-stack icon-stack-xl bg-teal-soft text-teal mb-4 mt-n5 z-1 shadow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-tv">
                                    <rect x="2" y="7" width="20" height="15" rx="2" ry="2"></rect>
                                    <polyline points="17 2 12 7 7 2"></polyline>
                                </svg>
                            </div>
                            <h5>Altro</h5>
                            <p class="card-text">Hai altre domande? Non esitare a contattarci</p>
                        </div>
                        <div class="card-footer">
                            <div class="text-teal font-weight-bold d-inline-flex align-items-center">
                                <a href="mailto:info@tigit.ch">Contattaci</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="my-10">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <h2>Non trovi la risposta al tuo problema?</h2>
                    <p class="lead mb-5">
                        Contattaci e ti risponderemo il prima possibile con la soluzione al tuo problema
                    </p>
                </div>
            </div>
            <div class="row  justify-content-center mb-10">
                <div class="col-lg-6 text-center">
                    <a href="mailto:info@tigit.ch">info@tigit.ch</a>
                </div>
            </div>
            <form action="{{ route('contacts.store') }}" method="post">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="text-dark" for="inputName">Nome: <span class="text-danger">*</span> </label>
                        <input class="form-control py-4" name="name" type="text" placeholder="Nome completo">
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label class="text-dark" for="inputEmail">E-Mail: <span class="text-danger">*</span></label>
                        <input class="form-control py-4" name="email" type="email" placeholder="name@example.com">
                        @error('email')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="text-dark" for="inputName">Telefono:</label>
                        <input class="form-control py-4" name="phone" type="text" placeholder="Nr. telefono">
                    </div>
                </div>
                <div class="form-group">
                    <label class="text-dark" for="inputMessage">Messaggio <span class="text-danger">*</span></label>
                    <textarea class="form-control py-3" name="message" type="text"
                              placeholder="Lascia il tuo messaggio..." rows="4"></textarea>
                    @error('message')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="text-center">
                    <input class="btn btn-primary btn-marketing mt-4" type="submit" value="Invia messaggio"/>
                </div>
            </form>
        </div>
    </section>

    </div>
@endsection
@section('scripts')

@endsection
