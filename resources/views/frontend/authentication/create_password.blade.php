@extends('layouts.authentication')

@section('content')
    <div id="layoutAuthentication_content">
        <main>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11">
                        <div class="card-body p-5 text-center">
                            <div
                                class="h3 font-weight-light mb-3">{{  trans('frontend/authentication/create_password.title') }}</div>
                        </div>
                        <hr class="my-0"/>
                        <div class="card my-5">
                            {{ $errors->first('user_id') }}
                            {{ $errors->first('password') }}
                            {{ $errors->first('password_confirmation') }}
                            <div class="card-body p-5">
                                <form action="{{ route('users.store_password') }}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <input name="user_id" type="hidden" value="{{ $user->id }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-gray-600 small" for="passwordExample">
                                            Password
                                        </label>
                                        <input class="form-control form-control-solid py-4" name="password" type="password"
                                               placeholder="" aria-label="Password" aria-describedby="passwordExample"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-gray-600 small" for="passwordExample">
                                            Confirm Password
                                        </label>
                                        <input class="form-control form-control-solid py-4" name="password_confirmation" type="password"
                                               placeholder="" aria-label="Password" aria-describedby="passwordExample"/>
                                    </div>
                                    <div class="form-group d-flex align-items-center justify-content-between mb-0">
                                        <input type="submit" value="{{ trans('general.save') }}" class="btn btn-primary" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
