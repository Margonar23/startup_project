@extends('layouts.authentication')

@section('content')
    <div id="layoutAuthentication_content">
        <main>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-7">
                        <div class="card shadow-lg border-0 rounded-lg mt-5">
                            <div class="card-header justify-content-center">
                                <h3 class="font-weight-light my-4">{{ trans('frontend/authentication/registration.create_company_step_1') }}</h3>
                            </div>
                            <div class="card-body">
                                <p class="lead text-primary">
                                    Tranquillo... ti guideremo in tutti li step necessari alla creazione della tua azienda.
                                </p>
                                <form action="{{ route('users.register') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="role_id"
                                           value="{{ App\Role::where('label', 'company_user')->first()->id }}"/>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="small mb-1"
                                                       for="inputFirstName">{{ trans('frontend/authentication/registration.first_name') }}</label>
                                                <input class="form-control py-4" id="inputFirstName" name="first_name"
                                                       type="text"/>
                                                @error('first_name')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="small mb-1"
                                                       for="inputLastName">{{ trans('frontend/authentication/registration.last_name') }}</label>
                                                <input class="form-control py-4" id="inputLastName" name="last_name"
                                                       type="text"/>
                                                @error('last_name')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="small mb-1"
                                               for="inputEmailAddress">{{ trans('frontend/authentication/registration.email') }}</label>
                                        <input class="form-control py-4" id="inputEmailAddress" type="text" name="email"
                                               aria-describedby="emailHelp"/>
                                        @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="small mb-1"
                                                       for="inputPassword">{{ trans('frontend/authentication/registration.password') }}</label>
                                                <input class="form-control py-4" id="inputPassword" type="password"
                                                       name="password"/>
                                            </div>
                                            @error('password')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="small mb-1"
                                                       for="inputConfirmPassword">{{ trans('frontend/authentication/registration.password_confirmation') }}</label>
                                                <input class="form-control py-4" id="inputConfirmPassword"
                                                       type="password" name="password_confirmation"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-4 mb-0">
                                        <input type="submit" class="btn btn-primary btn-block"
                                               value="{{ trans('frontend/authentication/registration.create_account_btn') }}"/>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer text-center">
                                <div class="small">
                                    <a href="{{ route('users.login') }}">{{ trans('frontend/authentication/registration.already_have_login') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
