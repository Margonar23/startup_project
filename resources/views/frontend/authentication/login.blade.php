@extends('layouts.authentication')

@section('content')
    <div id="layoutAuthentication_content">
        <main>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11">
                        <div class="card my-5">
                            <div class="card-body p-5 text-center">
                                <div
                                    class="h3 font-weight-light mb-1">{{  trans('frontend/authentication/login.login_title') }}</div>
                            </div>
                            <hr class="my-0"/>
                            <div class="card-body p-5">

                                <form action="{{ route('login') }}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label class="text-gray-600 small" for="emailExample">
                                            Email address
                                        </label>
                                        <input class="form-control form-control-solid py-4" name="email" type="text" placeholder=""
                                               aria-label="Email Address" aria-describedby="emailExample"/>
                                        <p class="text-danger">{{ $errors->first('email') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-gray-600 small" for="passwordExample">
                                            Password
                                        </label>
                                        <input class="form-control form-control-solid py-4" name="password" type="password"
                                               placeholder="" aria-label="Password" aria-describedby="passwordExample"/>
                                        <p class="text-danger">{{ $errors->first('password') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <a class="small" href="{{ route('users.forgot_password') }}">
                                            Hai dimenticato la password?
                                        </a>
                                    </div>
                                    <div class="form-group d-flex align-items-center justify-content-between mb-0">
{{--                                        <div class="custom-control custom-control-solid custom-checkbox">--}}
{{--                                            <input class="custom-control-input small" id="customCheck1"--}}
{{--                                                   type="checkbox"/>--}}
{{--                                            <label class="custom-control-label" for="customCheck1">--}}
{{--                                                Remember password--}}
{{--                                            </label>--}}
{{--                                        </div>--}}
                                        <input type="submit" value="Login" class="btn btn-primary" />
                                    </div>
                                </form>
                            </div>
                            <hr class="my-0"/>
                            <div class="card-body px-5 py-4">
                                <div class="small text-center">
                                    {{  trans('frontend/authentication/login.are_you_new') }}
                                    <a href="{{ route('users.registration', ['step' => 1]) }}">
                                        {{  trans('frontend/authentication/login.create_an_account') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
