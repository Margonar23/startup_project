@extends('layouts.frontend.frontend')
@section('content')
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
        <div class="page-header-content pt-10">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6" data-aos="fade-up">
                        <h1 class="page-header-title text-xl-center font-weight-bolder">{{ env('COMPANY_SLOGAN') }} <br>
                        </h1>
                    </div>
                    <div class="col-lg-6 col-xs-12 d-lg-block" data-aos="fade-up" data-aos-delay="50">
                        <img class="img-fluid" src="{{ asset('images/logo-tigit-no-bg.png') }}"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </header>
    <section class="bg-light py-10">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Create Organization-->
                <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11 mt-4">
                    <div class="card text-center h-100">
                        <div class="card-body px-5 pt-5 d-flex flex-column">
                            <div>
                                <div class="h3 text-primary font-weight-300">
                                    {{ trans('frontend/authentication/registration.create_new_user_title')}}
                                </div>
                                <p class="text-muted mb-4">
                                    {{ trans('frontend/authentication/registration.create_new_user_desc')}}
                                </p>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent px-5 py-4">
                            <div class="small text-center">
                                <a class="btn btn-block btn-primary"
                                   href="{{ route('users.registration', ['step' => 2]) }}">
                                    {{ trans('frontend/authentication/registration.create_new_user_btn') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Join Organization-->
                <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11 mt-4">
                    <div class="card text-center h-100">
                        <div class="card-body px-5 pt-5 d-flex flex-column align-items-between">
                            <div>
                                <div class="h3 text-secondary font-weight-300">
                                    {{ trans('frontend/authentication/registration.create_new_company_title') }}
                                </div>
                                <p class="text-muted mb-4">
                                    {{ trans('frontend/authentication/registration.create_new_company_desc') }}
                                </p>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent px-5 py-4">
                            <div class="small text-center">
                                <a class="btn btn-block btn-secondary"
                                   href="{{ route('users.registration', ['step' => 3]) }}">
                                    {{ trans('frontend/authentication/registration.create_new_company_btn') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-white py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center">
                        <div class="text-xs text-uppercase-expanded text-primary mb-2">Perchè Tigit?</div>
                        <h2 class="mb-5">Soluzioni innovative per situazioni tradizionali</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 mb-5 aos-init aos-animate" data-aos="fade-up">
                    <a class="card text-center text-decoration-none h-100 lift" href="{{ route('job_offers') }}">
                        <div class="card-body py-5">
                            <div class="icon-stack icon-stack-lg bg-green-soft text-green mb-4">
                                <i class="fa fa-user-tie fa-fw"></i>
                            </div>
                            <h5>Trovare lavoro</h5>
                            <p class="card-text small">
                                Tramite il nostro sistema è più facile trovare e farsi trovare
                                dalle aziende.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 mb-5 aos-init aos-animate" data-aos="fade-up">
                    <a class="card text-center text-decoration-none h-100 lift" href="{{ route('reservations') }}">
                        <div class="card-body py-5">
                            <div class="icon-stack icon-stack-lg bg-primary-soft text-primary mb-4">
                                <i class="fa fa-cocktail fa-fw"></i>
                            </div>
                            <h5>Riservare un tavolo</h5>
                            <p class="card-text small">Riserva un tavolo nei nostri bar & ristoranti partner.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 mb-5 aos-init aos-animate" data-aos="fade-up">
                    <a class="card text-center text-decoration-none h-100 lift" href="{{ route('events') }}">
                        <div class="card-body py-5">
                            <div class="icon-stack icon-stack-lg bg-cyan-soft text-cyan mb-4">
                                <i class="fa fa-birthday-cake fa-fw"></i>
                            </div>
                            <h5>Partecipare ad eventi</h5>
                            <p class="card-text small">Nel nostro calendario è possibile vedere gli eventi in ticino e
                                iscriversi a partecipare.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 mb-5 aos-init aos-animate" data-aos="fade-up">
                    <a class="card text-center text-decoration-none h-100 lift" href="{{ route('companies') }}">
                        <div class="card-body py-5">
                            <div class="icon-stack icon-stack-lg bg-indigo-soft text-indigo mb-4">
                                <i class="fa fa-building fa-fw"></i>
                            </div>
                            <h5>Visualizzare le aziende</h5>
                            <p class="card-text small">Potrai vedere i dettagli di tutte le aziende che hanno
                                aderito</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-light py-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="text-center mb-10">
                        <h2>I nostri pacchetti</h2>
                        <a href="{{ route('packages') }}" class="lead">Scopri le nostre offerte per le aziende!</a>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                @foreach($packages as $package)
                    <div class="col-lg-6 mb-5  aos-init aos-animate mt-0">
                        <div class="card pricing h-100">
                            <div class="card-header text-white  bg-{{ $package->color }}">{{ $package->name }}</div>
                            <div class="card-body p-5">
                                <div class="text-center">
                                    <div class="pricing-price">
                                        <sup>CHF</sup>
                                        {{ $package->package_prices->filter(function ($price, $index){
                                                return $price->payment_type_id == 1;
                                        })->first()->price }}
                                        <span class="pricing-price-period">/mese</span>
                                    </div>
                                    <div class="pricing-price">
                                        <sup>CHF</sup>
                                        {{ $package->package_prices->filter(function ($price, $index){
                                                return $price->payment_type_id == 2;
                                        })->first()->price }}
                                        <span class="pricing-price-period">/anno</span>
                                    </div>
                                </div>
                                <ul class="fa-ul pricing-list">
                                    @foreach($package->features as $feature)
                                        <li class="pricing-list-item">
                                            <span class="fa-li"><i
                                                    class="far fa-check-circle text-teal"></i></span><span
                                                class="text-dark">{{ $feature->name }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </section>
<h1 class="text-center pt-10">Aziende sponsor</h1>
    <section class="bg-white pb-10 pt-10">
        <div class="container">
            <div class="row brands text-gray-500 align-items-center">
                <div class="col-6 col-sm-6 col-lg-3 d-flex justify-content-center mb-5 mb-lg-0">
                    <a href="https://gehri.swiss/" target="_blank">
                        <img src="{{ asset('/images/sponsor/gheri.png') }}" width="150px">
                    </a>
                </div>
                <div class="col-6 col-sm-6 col-lg-3 d-flex justify-content-center mb-5 mb-lg-0">
                    <a href="http://www.fontana.ch/" target="_blank">
                        <img src="{{ asset('/images/sponsor/logo-fontana-print.jpg') }}" width="250px">
                    </a>
                </div>
                <div class="col-6 col-sm-6 col-lg-3 d-flex justify-content-center mb-5 mb-lg-0">
                    <a href="https://chiccodoro.com/" target="_blank">
                        <img src="{{ asset('/images/sponsor/chiccodoro.png') }}" width="200px">
                    </a>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-white">
            <!-- Rounded SVG Border-->
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
               <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </section>

    <section class="bg-light py-10">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 text-center mb-5">
                    <div class="display-1 font-weight-bold text-gray-400">{{ $users_count }}</div>
                    <div class="h5">Utenti</div>
                </div>
                <div class="col-lg-4 text-center mb-5">
                    <div class="display-1 font-weight-bold text-gray-400">{{ $companies_count }}</div>
                    <div class="h5">Aziende</div>
                </div>
                <div class="col-lg-4 text-center mb-5">
                    <div class="display-1 font-weight-bold text-gray-400">{{ $job_count }}</div>
                    <div class="h5">Posti di lavoro</div>
                </div>
            </div>
        </div>
        <div class="svg-border-rounded text-light">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
                 fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
            </svg>
        </div>
    </section>
{{--    <section class="bg-white py-10">--}}
{{--        <div class="container">--}}
{{--            <div class="row justify-content-center">--}}
{{--                <div class="col-lg-8">--}}
{{--                    <div class="text-center mb-10">--}}
{{--                        <h1>Top 3</h1>--}}
{{--                        <p class="lead">Scopri le migliori aziende in base ai voti dei nostri utenti!</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                    @forelse($top_companies as $company)--}}
{{--                        <div class="col-md-4 text-center mb-5">--}}
{{--                            <div class="card" style="width: 18rem;">--}}
{{--                                <img src="{{ $company->logo }}" class="card-img-top" alt="...">--}}
{{--                                <div class="card-body">--}}
{{--                                    <h5 class="card-title">{{ $company->name }}</h5>--}}
{{--                                    <p class="card-text">--}}
{{--                                        @for($i = 0; $i < intval($company->avg_rating); $i++)--}}
{{--                                            <i class="text-warning fa fa-star"></i>--}}
{{--                                        @endfor--}}
{{--                                        @if(floatval($company->avg_rating) / intval($company->avg_rating) > 1)--}}
{{--                                            <i class="text-warning fa fa-star-half-alt"></i>--}}
{{--                                        @endif--}}
{{--                                        <br>--}}
{{--                                        <br>--}}
{{--                                        <a href="{{ route('companies.detail', ['company' => $company->id]) }}"--}}
{{--                                           class="btn btn-primary btn-sm">Visita la pagina</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @empty--}}
{{--                        <div class="alert alert-info">Non ci sono aziende che corrispondono ai criteri di valutazione--}}
{{--                            dei top 3, corri a votare--}}
{{--                        </div>--}}
{{--                    @endforelse--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="svg-border-rounded text-dark">--}}
{{--            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"--}}
{{--                 fill="currentColor">--}}
{{--                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>--}}
{{--            </svg>--}}
{{--        </div>--}}
{{--    </section>--}}
@endsection
