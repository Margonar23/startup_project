<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * {
        name: 'Sana Canalizzazioni SA',
        logo: 'images/coupon/sana.jpg',
        category: 'Canalizzazioni',
        link: 'http://www.canalizzazioni.ch/',
        address: 'Strada Ponte di Valle 26, 6964 Davesco',
        district: 'Luganese'
        },
         */
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('logo')->nullable();
            $table->integer('category_id');
            $table->string('link')->nullable();
            $table->string('address');
            $table->integer('district_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
