<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeaturePermission extends Model
{
    protected $fillable = ['feature_id', 'permission_id'];

    public function permission()
    {
        return $this->belongsTo('App\Permission', 'permission_id');
    }
}
