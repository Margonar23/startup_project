<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfession extends Model
{
    protected $fillable = ['user_id', 'profession_id', 'experience'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function profession()
    {
        return $this->belongsTo('App\Profession', 'profession_id');
    }
}
