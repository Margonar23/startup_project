<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresentationTemplate extends Model
{
    protected $fillable = ['name', 'template', 'img'];
}
