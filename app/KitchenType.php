<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KitchenType extends Model
{
    protected $fillable = ['name', 'label'];
}
