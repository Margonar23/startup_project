<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['name','cover', 'max_participants', 'with_reservation', 'start_time', 'end_time','location_id', 'company_id', 'recurrent', 'minimum_age'];

    protected $dates = ['reservation_end_at', 'start_time', 'end_time'];

    public function getReservationEndAtAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }

    public function getStartTimeAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }

    public function getStartTime()
    {
        return Carbon::parse($this->start_time)->format('Y-m-d H:i');
    }

    public function getEndTimeAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function participants()
    {
        return $this->hasMany('App\EventParticipant', 'event_id');
    }


}
