<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserJobExperience extends Model
{
    protected $fillable = ['user_id', 'job', 'company', 'link', 'begin_at', 'end_at'];
}
