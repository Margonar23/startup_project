<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $fillable = ['location_id', 'day_id', 'opening_at', 'closing_at'];

    public function location()
    {
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function day()
    {
        return $this->belongsTo('App\Day', 'day_id');
    }

}
