<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'label',
        'logo',
        'uid',
        'status_id',
        'category_id',
        'classification_id',
        'email',
        'fax',
        'phone',
        'address',
        'district_id',
        'website',
        'employee_count_id',
        'created_by',
        'avg_rating',
        'created_by_staff',
    ];

    public function status()
    {
        return $this->belongsTo('App\CompanyStatus', 'status_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function classification()
    {
        return $this->belongsTo('App\CompanyClassification', 'classification_id');
    }

    public function category()
    {
        return $this->belongsTo('App\CompanyCategory', 'category_id');
    }

    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }

    public function company_package()
    {
        return $this->hasOne('App\CompanyPackage', 'company_id')->where('company_packages.active', '=', 1);
    }

    public function subscription()
    {
        return $this->hasOne('App\CompanyPackage', 'company_id');
    }

    public function users()
    {
        return $this->hasMany('App\UserCompany', 'company_id');
    }

    public function roles()
    {
        return $this->hasMany('App\CompanySubRole', 'company_id');
    }

    public function presentation_pages()
    {
        return $this->hasMany('App\CompanyPresentationPage', 'company_id');
    }

    public function events()
    {
        return $this->hasMany('App\Event', 'company_id')
                ->orderBy('start_time');
    }

    public function locations()
    {
        return $this->hasMany('App\Location', 'company_id');
    }

    public function restaurants()
    {
        return $this->hasMany('App\Location', 'company_id')->where('has_restaurant', 1);
    }

    public function job_offers()
    {
        return $this->hasMany('App\JobOffer', 'company_id');
    }

    public function ratings()
    {
        return $this->hasMany('App\UserCompanyRating', 'company_id');
    }

    public function followers()
    {
        return $this->hasMany('App\UserCompanyFollower', 'company_id');
    }



    public function reservationRequest()
    {
        /**
         *  select *
            from bookings b
            inner join reservation_statuses rs on rs.id = b.reservation_status_id AND rs.label = 'request'
            inner join locations l on l.id = b.location_id
            inner join companies c on c.id = l.company_id AND c.id = 6
         */

        return DB::table('bookings')
            ->join('reservation_statuses', 'reservation_statuses.id', '=', 'bookings.reservation_status_id')
            ->join('locations', 'locations.id' , '=', 'bookings.location_id')
            ->join('companies', 'companies.id' , '=', 'locations.company_id')
            ->where([
                'reservation_statuses.label' => 'request',
                'companies.id' => $this->id
            ])->get();


    }
}
