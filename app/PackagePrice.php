<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackagePrice extends Model
{
    protected $fillable = ['package_id', 'package_type_id', 'price'];

    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id');
    }

    public function payment_type()
    {
        return $this->belongsTo('App\PaymentType', 'payment_type_id');
    }
}
