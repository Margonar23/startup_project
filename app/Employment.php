<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
    protected $fillable = ['name', 'label'];

    public function users()
    {
        return $this->hasMany('App\UserEmployment', 'employment_id');
    }
}
