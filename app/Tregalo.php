<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tregalo extends Model
{
    protected $fillable = ['name', 'price', 'stripe_id'];
}
