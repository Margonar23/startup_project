<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresentationPageTemplateContent extends Model
{
    protected $fillable = ['title', 'subtitle', 'img', 'content', 'page_id'];

    public function page()
    {
        return $this->belongsTo('App\CompanyPresentationPage', 'page_id');
    }
}
