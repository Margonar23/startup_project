<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCompanyRating extends Model
{
    protected $fillable = ['user_id', 'company_id', 'rating_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function rating()
    {
        return $this->belongsTo('App\Rating', 'rating_id');
    }
}
