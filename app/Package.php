<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Package extends Model
{
    protected $fillable = ['name', 'description', 'stripe_package_id', 'price', 'price_year','color', 'active'];

    public function features()
    {
        return $this->belongsToMany('App\Feature');
    }

    public function discount_year_package()
    {
        return $this->price_year > 0 ? ' ('.intval($this->price / $this->price_year * 100). '%)' : '';
    }

    public function package_prices()
    {
        return $this->hasMany('App\PackagePrice', 'package_id');
    }

    public function get_stripe_id($payment_type)
    {
        return DB::table('package_prices')
            ->where(['payment_type_id' => $payment_type, 'package_id' => $this->id])
            ->first()->stripe_package_id;
    }
}
