<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPermit extends Model
{
    protected $fillable = ['user_id', 'permit_id'];

    public function permit()
    {
        return $this->belongsTo('App\Permit', 'permit_id');
    }
}
