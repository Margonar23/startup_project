<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOfferApply extends Model
{
    protected $fillable = ['user_id', 'job_offer_id', 'message'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function job_offer()
    {
        return $this->belongsTo('App\JobOffer', 'job_offer_id');
    }
}
