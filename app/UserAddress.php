<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = ['user_id', 'route', 'nr', 'cap', 'town', 'country'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
