<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    protected $fillable = ['user_id', 'company_id', 'subrole_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function subrole()
    {
        return $this->belongsTo('App\CompanySubRole', 'subrole_id');
    }
}
