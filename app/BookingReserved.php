<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingReserved extends Model
{
    protected $fillable = ['booking_id', 'user_id', 'table_id', 'reserved_start', 'reserved_stop'];

    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    public function user()
    {
        return !is_null($this->user_id) ? $this->belongsTo('App\User', 'user_id') : null;
    }

    public function table()
    {
        return !is_null($this->table_id) ? $this->belongsTo('App\Table', 'table_id') : null;
    }


}
