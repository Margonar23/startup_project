<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyStatus extends Model
{
    protected $fillable = ['name', 'label', 'active', 'icon', 'icon_color', 'btn_color'];
}
