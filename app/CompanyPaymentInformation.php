<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPaymentInformation extends Model
{
    protected $fillable = ['company_package_id', 'name', 'email', 'address', 'coupon'];
}
