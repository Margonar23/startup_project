<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEmployment extends Model
{
    protected $fillable = ['user_id' , 'employment_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function employment()
    {
        return $this->belongsTo('App\Employment', 'employment_id');
    }
}
