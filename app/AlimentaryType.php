<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlimentaryType extends Model
{
    protected $fillable = ['name', 'label', 'icon', 'color'];
}
