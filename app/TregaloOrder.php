<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TregaloOrder extends Model
{
    protected $fillable = ['user_id', 'address', 'name', 'email', 'phone','shipped'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function order_row()
    {
        return $this->hasMany('App\TregaloUser', 'order_id');
    }

    public function total()
    {
        $total = 0;
        foreach ($this->order_row as $row){
            $total = $total + ($row->qty * ($row->tregalo->price / 100));
        }
        return $total;
    }
}
