<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayHour extends Model
{
    protected $fillable = ['hour'];
}
