<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyEmployeeCount extends Model
{
    protected $fillable = ['name', 'label'];
}
