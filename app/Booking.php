<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = ['location_id', 'booking_datetime','reservation_status_id','people_nr', 'firstname', 'lastname', 'phone', 'email', 'address', 'cap', 'city', 'room_id', 'message', 'auth_user_id'];

    protected $dates = ['booking_datetime'];

    public function location()
    {
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function room()
    {
        return $this->belongsTo('App\LocationRoom', 'room_id');
    }

    public function reservation_status()
    {
        return $this->belongsTo('App\ReservationStatus', 'reservation_status_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'auth_user_id');
    }

    public function reserved()
    {
        return $this->hasOne('App\BookingReserved', 'booking_id');
    }
}
