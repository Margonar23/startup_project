<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySubRole extends Model
{
    protected $fillable = ['name', 'label', 'company_id'];

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function permissions()
    {
        return $this->hasMany('App\CompanySubRolePermission', 'company_sub_role_id');
    }
}
