<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPackage extends Model
{
    protected $fillable = ['company_id', 'package_id', 'payment_method_id', 'payment_type_id', 'last_payment_at', 'subscription_expiry_at', 'active'];

    protected $dates = ['last_payment_at', 'subscription_expiry_at'];


    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id');
    }

    public function payment_method()
    {
        return $this->belongsTo('App\PaymentMethod', 'payment_method_id');
    }

    public function payment_type()
    {
        return $this->belongsTo('App\PaymentType', 'payment_type_id');
    }

    public function information()
    {
        return $this->hasOne('App\CompanyPaymentInformation', 'company_package_id');
    }
}
