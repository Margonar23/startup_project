<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOffer extends Model
{
    protected $fillable = ['company_id', 'is_public', 'title', 'description', 'employment_id', 'profession_id', 'min_experience', 'skills', 'offer_expiry_at', 'active'];

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function employment()
    {
        return $this->belongsTo('App\Employment', 'employment_id');
    }
    public function profession()
    {
        return $this->belongsTo('App\Profession', 'profession_id');
    }
    public function skills_list()
    {
        $tmp = [];
        foreach (json_decode($this->skills, true) as $skill){
            array_push($tmp, Skill::find($skill));
        }
        return collect($tmp);
    }

    public function applier()
    {
        return $this->hasMany('App\JobOfferApply', 'job_offer_id');
    }
}
