<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPercentage extends Model
{
    protected $fillable = ['user_id', 'percentage_id'];

    public function percentage()
    {
        return $this->belongsTo('App\Percentage', 'percentage_id');
    }
}
