<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Laravel\Cashier\Billable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasApiTokens, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'title_id', 'password', 'role_id', 'avatar', 'looking_for_new_job', 'country_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function name()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }

    public function subrole()
    {
        return $this->hasOne('App\UserCompany', 'user_id');
    }

    public function title()
    {
        return $this->belongsTo('App\UserTitle', 'title_id');
    }


    public function address()
    {
        return $this->hasMany('App\UserAddress', 'user_id');
    }

    public function hasCompanies()
    {
        return $this->hasMany(
            'App\Company',
            'created_by'
        );
    }

    public function event_participants()
    {
        return $this->hasMany('App\EventParticipant', 'user_id');
    }

    public function profession()
    {
        return $this->hasOne('App\UserProfession', 'user_id');
    }

    public function permit()
    {
        return $this->hasOne('App\UserPermit', 'user_id');
    }

    public function percentage()
    {
        return $this->hasOne('App\UserPercentage', 'user_id');
    }

    public function employment()
    {
        return $this->hasOne('App\UserEmployment', 'user_id');
    }

    public function skills()
    {
        return $this->hasMany('App\UserSkill', 'user_id');
    }

    public function licenses()
    {
        return $this->hasMany('App\UserLicense', 'user_id');
    }

    public function languages()
    {
        return $this->hasMany('App\UserLanguage', 'user_id');
    }

    public function hobbies()
    {
        return $this->hasMany('App\UserHobby', 'user_id');
    }

    public function certificates()
    {
        return $this->hasMany('App\UserCertificate', 'user_id')->orderByDesc('acquired_at');
    }

    public function diplomas()
    {
        return $this->hasMany('App\UserDiploma', 'user_id')->orderByDesc('end_at');
    }

    public function job_experiences()
    {
        return $this->hasMany('App\UserJobExperience', 'user_id')->orderByDesc('end_at');
    }

    public function cv()
    {
        return $this->hasOne('App\UserCv', 'user_id');
    }

    public function order_tregalo()
    {
        return $this->hasMany('App\TregaloOrder', 'user_id');
    }

    public function sub()
    {
        return DB::table('subscriptions')->where('user_id', $this->id)->first();
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }



}
