<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantInfo extends Model
{
    protected $fillable = ['location_id', 'description', 'restaurant_type_id', 'kitchen_type_id', 'alimentary_type'];

    public function location()
    {
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function restaurant_type()
    {
        return $this->belongsTo('App\RestaurantType', 'restaurant_type_id');
    }

    public function kitchen_type()
    {
        return $this->belongsTo('App\KitchenType', 'kitchen_type_id');
    }
}
