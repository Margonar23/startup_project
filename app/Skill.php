<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = ['name', 'label'];

    public function users()
    {
        return $this->hasMany('App\UserSkill', 'skill_id');
    }
}
