<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ['name', 'address', 'district_id', 'max_capacity', 'has_tables', 'company_id', 'has_restaurant'];

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function rooms()
    {
        return $this->hasMany('App\LocationRoom', 'location_id');
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking', 'location_id');
    }
    public function restaurant()
    {
        return $this->has_restaurant ? $this->hasMany('App\Restaurant', 'location_id') : null;
    }

    public function restaurant_info()
    {
        return $this->has_restaurant ? $this->hasOne('App\RestaurantInfo', 'location_id') : null;
    }

    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }
}
