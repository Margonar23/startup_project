<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLicense extends Model
{
    protected $fillable = ['user_id', 'license_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function license()
    {
        return $this->belongsTo('App\License', 'license_id');
    }
}
