<?php

namespace App\Notifications;

use App\Company;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class newCompanyNotification extends Notification
{
    use Queueable;

    private $company;
    private $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Company $company, User $user)
    {
        $this->company = $company;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Ciao ' . $notifiable->name() . '!')
            ->line($this->user->name().' ha creato l\'azienda ' . $this->company->name . '.')
            ->action('Conferma l\'iscrizione a Tigit', route('company_management.index'))
            ->salutation('A presto! ');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'action' => 'Nuova azienda',
            'data' => $this->company,
            'message' => $this->user->name().' ha creato l\'azienda ' . $this->company->name . '.',
            'label' => 'fa fa-building'
        ];
    }
}
