<?php

namespace App\Notifications;

use App\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TableBooking extends Notification
{
    use Queueable;

    private $booking;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->greeting('Ciao ' . $notifiable->name() . '!')
                ->line('Un utente ha riservato un tavolo a ' . $this->booking->location->name . '.')
                ->action('Assegna il tavolo', route('company.reservations.index', ['location' => $this->booking->location->company->id]))
                ->salutation('A presto! ')
                ->greeting('Tigit Staff');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'action' => 'Un tavolo è stato riservato',
            'data' => $this->booking,
            'message' => 'Tavolo riservato a ' . $this->booking->location->name . '.',
            'label' => 'fa fa-cocktail'
        ];
    }
}
