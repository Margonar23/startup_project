<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserBuyTRegaloEmployee extends Notification
{
    use Queueable;

    private $information;
    private $total;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($information, $total)
    {
        $this->information = $information;
        $this->total = $total;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Nuovo ordine T-REGALO')
            ->greeting('Buongiorno ' . $notifiable->name() . '!')
            ->line('Nuovo acquisto di buoni T-REGALO per un valore di ' . $this->total . '.')
            ->action('Vai al sito', route('home'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
