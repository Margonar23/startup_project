<?php

namespace App\Notifications;

use App\Company;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class StaffCompanyCreation extends Notification
{
    use Queueable;

    private $company;
    private $str;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Company $company, $str)
    {
        $this->company = $company;
        $this->str = $str;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        error_log($this->str);
        return (new MailMessage)
            ->subject('Hai creato l\'azienda ' . $this->company->name . '!')
            ->greeting('Ciao!')
            ->line('Dati di accesso:')
            ->line('email: '.$this->company->user->email)
            ->line('password: '.$this->str)
            ->salutation('Lo staff di Tigit');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
