<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ContactFormNotification extends Notification
{
    use Queueable;

    private $request;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
        error_log(print_r($this->request,true));
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Contact Form Notification')
            ->greeting('Ciao!')
            ->line('Nuova notifica dal form dei contatti.')
            ->line('Nome: ' . $this->request['name'])
            ->line('E-Mail: ' . $this->request['email'])
            ->line('Telefono: ' . $this->request['phone'])
            ->line('Message:')
            ->line($this->request['message'])
            ->salutation('Tigit');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'action' => 'Nuovo messaggio dal form dei contatti!',
            'data' => $this->request,
            'message' => $this->request['name'] . ' (' . $this->request['email'] . ') scrive:  ' . $this->request['message'] . '.',
            'label' => 'fa fa-envelope-open'
        ];
    }
}
