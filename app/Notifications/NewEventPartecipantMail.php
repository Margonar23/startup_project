<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewEventPartecipantMail extends Notification
{
    use Queueable;

    private $event;
    private $participant;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($event, $participant)
    {
        $this->event = $event;
        $this->participant = $participant;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Ciao ' . $notifiable->name() . '!')
            ->line($this->participant->name().' si è iscritto al tuo evento ' . $this->event->name . '.')
            ->salutation('A presto! ')
            ->greeting('Tigit Staff');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [];
    }

    public function toDatabase($notifiable)
    {
        return [
            'action' => 'Nuovo inscritto ad un evento',
            'data' => $this->event,
            'message' => $this->participant->name().' si è iscritto al tuo evento ' . $this->event->name . '.',
            'label' => 'fa fa-user-plus'
        ];
    }
}
