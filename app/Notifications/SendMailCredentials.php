<?php

namespace App\Notifications;

use App\Company;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendMailCredentials extends Notification
{
    use Queueable;
    private $company;
    private $pwd;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Company $company, $pwd)
    {
        $this->company = $company;
        $this->pwd = $pwd;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Lo staff di Tigit ha creato la tua azienda')
            ->greeting('Buongiorno!')
            ->line('Siamo lieti di informarla che abbiamo creato la sua azienda '. $this->company->name)
            ->line('Puoi accedere al sito cliccando il bottone qui sotto con le credenziali')
            ->line('E-Mail: '.$this->company->user->email)
            ->line('Password: '.$this->pwd)
            ->action('Vai al sito', route('users.login'))
            ->line('ricordati di cambiare la password al primo accesso')
            ->line('Benvenuto su Tigit')
            ->salutation('Lo staff di Tigit');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
