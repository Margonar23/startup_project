<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewApplyJobMail extends Notification
{
    use Queueable;

    private $job_offer;
    private $user;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($job_offer, $user)
    {
        $this->job_offer = $job_offer;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Ciao ' . $notifiable->name() . '!')
            ->line($this->user->name().' si è candidato al tuo annuncio ' . $this->job_offer->title . '.')
            ->salutation('A presto! ')
            ->greeting('Tigit Staff');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [];
    }

    public function toDatabase($notifiable)
    {
        return [
            'action' => 'Nuovo Candidato',
            'data' => $this->job_offer,
            'message' => $this->user->name().' si è candidato al tuo annuncio ' . $this->job_offer->title . '.',
            'label' => 'fa fa-user-plus'
        ];
    }
}
