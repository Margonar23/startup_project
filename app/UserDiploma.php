<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDiploma extends Model
{
    protected $fillable = ['user_id', 'title', 'school', 'link', 'begin_at', 'end_at'];
}
