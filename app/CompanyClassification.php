<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyClassification extends Model
{
    protected $fillable = ['name', 'description'];
}
