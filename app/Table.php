<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Table extends Model
{
    protected $fillable = ['name', 'max_capacity', 'table_shape_id', 'location_room_id'];

    public function shape()
    {
        return $this->belongsTo('App\TableShape','table_shape_id');
    }

    public function room()
    {
        return $this->belongsTo('App\LocationRoom', 'location_room_id');
    }

    public function bookings()
    {
        return $this->hasMany('App\BookingReserved', 'table_id');
    }

    public function is_reserved($date)
    {
        return DB::table('booking_reserveds')
            ->leftJoin('bookings', 'bookings.id', '=', 'booking_id')
            ->where('table_id', $this->id)->whereRaw('"'.$date.'" between reserved_start and reserved_stop')->first();
    }
}
