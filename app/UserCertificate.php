<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCertificate extends Model
{
    protected $fillable = ['user_id', 'certificate', 'link','released_by', 'expiry_at', 'acquired_at'];
}
