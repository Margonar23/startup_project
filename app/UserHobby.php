<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHobby extends Model
{
    protected $fillable = ['user_id', 'hobby'];
}
