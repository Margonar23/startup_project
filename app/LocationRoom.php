<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationRoom extends Model
{
    protected $fillable = ['name', 'max_capacity', 'location_id'];

    public function location()
    {
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function tables()
    {
        return $this->hasMany('App\Table', 'location_room_id');
    }
}
