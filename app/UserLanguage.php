<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLanguage extends Model
{
    protected $fillable = ['user_id', 'language_id', 'level_id'];

    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id');
    }

    public function level()
    {
        return $this->belongsTo('App\LanguageLevel', 'level_id');
    }
}
