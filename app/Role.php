<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'label', 'internal', 'parent_role_id'];

    protected $guarded = ['id'];

    public function parent_role()
    {
        return $this->belongsTo('App\Role', 'parent_role_id');
    }

    public function users()
    {
        return $this->hasMany('App\User', 'role_id');
    }
}
