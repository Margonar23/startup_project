<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $fillable = ['name', 'description', 'label', 'active'];

    public function permissions()
    {
        return $this->hasMany('App\FeaturePermission', 'feature_id');
    }
}
