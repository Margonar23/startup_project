<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'logo' => asset($this->logo),
            'category' => isset($this->category) ? $this->category->name : null,
            'classification' => $this->classification->name,
            'email' => $this->email,
            'fax' => $this->fax,
            'phone' => $this->phone,
            'address' => $this->address,
            'district' => isset($this->district) ? $this->district->name : null,
            'website' => $this->website,
            'pages' => PageResource::collection($this->presentation_pages->where('active', 1)),
            'events' => $this->events,
            'locations' => $this->locations,
            'restaurants' => $this->restaurants,
            'ratings_avg' => $this->rating_avg($this->ratings),
            'ratings_nr' => $this->ratings->count(),
            'followers_nr' => $this->followers->count(),
            'followers' => $this->followers,
            'avg_rating' => $this->avg_rating,
            'job_offers' => $this->job_offers,
            'public_job_offers' => $this->job_offers->where('is_public', 1),
        ];
    }

    public function rating_avg($ratings)
    {
        $sum = 0;
        foreach ($ratings as $rating) {
            $sum += $rating->rating->rate;
        }
        return $ratings->count() ? round($sum / $ratings->count(), 1) : '-';
    }
}
