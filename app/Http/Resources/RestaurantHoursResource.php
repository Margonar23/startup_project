<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantHoursResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'day' => $this->day->name,
            'label' => $this->day->label,
            'week_number' => $this->day->week_number,
            'opening_at' => $this->opening_at,
            'closing_at' => $this->closing_at
        ];
    }
}
