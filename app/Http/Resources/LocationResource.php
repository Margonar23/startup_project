<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class LocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'rooms' => $this->rooms,
            'max_capacity' => $this->max_capacity,
            'has_tables' => $this->has_tables,
            'company' => $this->company,
            'restaurant' => $this->restaurant,
            'restaurant_info' => $this->restaurant_info,
        ];
    }
}
