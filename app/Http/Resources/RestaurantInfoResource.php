<?php

namespace App\Http\Resources;

use App\AlimentaryType;
use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'restaurant_type' => $this->restaurant_type->name,
            'kitchen' => new KitchenTypeResource($this->kitchen_type),
            'alimentary' => AlimentaryTypeResource::collection(AlimentaryType::whereIn('id',json_decode($this->alimentary_type))->get()),
        ];
    }
}
