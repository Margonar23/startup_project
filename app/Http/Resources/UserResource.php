<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name(),
            'profession' => isset($this->profession) ? $this->profession->profession->name : null,
            'permit' => isset($this->permit) ? $this->permit->permit->name : null,
            'percentage' => isset($this->percentage) ? $this->percentage->percentage->percentage : null,
            'experience' => isset($this->profession) ? $this->profession->experience : null,
            'employment' => isset($this->employment) ? $this->employment->employment->name : null,
            'skills' => isset($this->skills) ? SkillResource::collection($this->skills) : null,
            'licenses' => isset($this->licenses) ? LicenseResource::collection($this->licenses) : null,
            'languages' => isset($this->languages) ? LanguageResource::collection($this->languages) : null,
            'hobbies' => isset($this->hobbies) ? HobbyResource::collection($this->hobbies) : null,
            'certificates' => isset($this->certificates) ? CertificateResource::collection($this->certificates) : null,
            'diplomas' => isset($this->diplomas) ? DiplomaResource::collection($this->diplomas) : null,
            'job_experiences' => isset($this->job_experiences) ? JobExperienceResource::collection($this->job_experiences) : null,
            'looking_for_new_job' => $this->looking_for_new_job,
            'cv' => isset($this->cv) ? asset(\Storage::url($this->cv->cv_path)) : null
        ];
    }
}
