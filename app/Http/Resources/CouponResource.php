<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'logo' => asset($this->logo),
            'category' => isset($this->category) ? $this->category->name : null,
            'link' => $this->link,
            'address' => $this->address,
            'district' => isset($this->district) ? $this->district->name : null,
        ];
    }

}
