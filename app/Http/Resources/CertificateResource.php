<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CertificateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'certificate' => $this->certificate,
            'link' => $this->link,
            'released_by' => $this->released_by,
            'expiry_at' => !is_null($this->expiry_at) ? Carbon::createFromFormat('Y-m-d', $this->expiry_at)->format('d.m.Y') : null,
            'acquired_at' => !is_null($this->acquired_at) ? Carbon::createFromFormat('Y-m-d', $this->acquired_at)->format('d.m.Y') : null,
        ];
    }
}
