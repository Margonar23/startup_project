<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingReservedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'booking' => $this->booking,
            'user' => $this->user,
            'table' => $this->table,
            'reserved_date_start' => $this->reserved_start,
            'reserved_date_stop' => $this->reserved_stop,
            'reserved_string' => Carbon::createFromFormat('Y-m-d H:i:s', $this->reserved_start)->format('d.m.Y H:i'). ' - '.Carbon::createFromFormat('Y-m-d H:i:s', $this->reserved_stop)->format('H:i'),
        ];
    }
}
