<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class JobExperienceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'job' => $this->job,
            'company' => $this->company,
            'link' => $this->link,
            'begin_at' => !is_null($this->begin_at) ? Carbon::createFromFormat('Y-m-d', $this->begin_at)->format('d.m.Y') : null,
            'end_at' => !is_null($this->end_at) ? Carbon::createFromFormat('Y-m-d', $this->end_at)->format('d.m.Y') : null,
        ];
    }
}
