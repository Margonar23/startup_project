<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'location' => $this->location,
            'room' => $this->room,
            'reservation_status' => $this->reservation_status,
            'booking_datetime' => $this->booking_datetime,
            'booking_date' => Carbon::createFromFormat('Y-m-d H:i:s', $this->booking_datetime)->format('d.m.Y'),
            'booking_time' => Carbon::createFromFormat('Y-m-d H:i:s', $this->booking_datetime)->format('H:i'),
            'people_nr' => $this->people_nr,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $this->address.', '.$this->cap.' '.$this->city,
            'message' => $this->message,
            'user' => $this->user,
            'reservation' => new BookingReservedResource($this->reserved),
        ];
    }
}
