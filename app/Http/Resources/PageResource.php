<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'page_name' => $this->page_name,
            'label' => $this->label,
            'active' => $this->active,
            'content' => PageContentResource::make($this->page_content),
            'template' => $this->template,
        ];
    }
}
