<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->name,
            'cover' => !is_null($this->cover) ? asset($this->cover) : null,
            'max_participants' => $this->max_participants,
            'minimum_age' => $this->minimum_age,
            'with_reservation' => $this->with_reservation,
            'start' => $this->start_time,
            'end' => $this->end_time,
            'location' => $this->location,
            'company' => $this->company,
            'participants' => $this->participants
        ];
    }
}
