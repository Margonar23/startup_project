<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BackendCreateCompany extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'category_id' => 'required',
            'address' => 'required',
            'district_id' => 'required',
            'email' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'user_mail' => 'required',
            'file' => 'required',
            'package_id' => 'required',
            'invoice_name' => 'required',
            'invoice_email' => 'required',
            'invoice_address' => 'required',
            'payment_type_id' => 'required',
        ];
    }
}
