<?php

namespace App\Http\Controllers\Frontend;

use App\Booking;
use App\Company;
use App\DayHour;
use App\Event;
use App\Feature;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookingRequest;
use App\Http\Requests\ContactFormRequest;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\DayHourResource;
use App\Http\Resources\EventResource;
use App\JobOffer;
use App\JobOfferApply;
use App\Location;
use App\News;
use App\Notifications\ContactFormNotification;
use App\Notifications\NewApplyJob;
use App\Notifications\NewApplyJobMail;
use App\Notifications\TableBooking;
use App\Notifications\UserBuyTRegalo;
use App\Notifications\UserBuyTRegaloEmployee;
use App\Package;
use App\Role;
use App\Tregalo;
use App\TregaloOrder;
use App\TregaloUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Laravel\Cashier\Exceptions\IncompletePayment;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class RoutingController extends Controller
{
    public function index()
    {
        if (env('APP_ENV') == 'middle_dev') {
            return view('frontend.pages.coupon');
        } else {
            return view('frontend.homepage', [
                'packages' => Package::where('active', 1)->get(),
                'features' => Feature::where('active', 1)->get(),
                'users_count' => User::all()->count(),
                'companies_count' => Company::all()->count(),
                'job_count' => JobOffer::all()->count(),
                'top_companies' => CompanyResource::collection(
                    Company::where('avg_rating', '>', 3.8)->orderByDesc('avg_rating')->take(3)->get()
                )
            ]);
        }

    }

    public function coupon()
    {
        return view('frontend.pages.coupon');
    }

    public function packages()
    {
        return view('frontend.pages.packages', ['packages' => Package::where('active', 1)->get(), 'features' => Feature::where('active', 1)->get()]);
    }

    public function news()
    {
        return view('frontend.pages.news', ['news' => News::orderBy('created_at', 'desc')->paginate(10)]);
    }

    public function news_detail(News $news)
    {
        return view('frontend.pages.news_detail', ['news' => $news]);
    }

    public function companies()
    {
        return view('frontend.pages.companies');
    }

    public function company_detail(Company $company)
    {
        return view('frontend.pages.companies_detail', ['company' => $company]);
    }

    public function events()
    {
        return view('frontend.pages.events', ['events' => EventResource::collection(
            Event::whereDate(
                'start_time',
                '>=',
                Carbon::createFromFormat('Y-m-d H:i:s', now())->format('Y-m-d H:i:s'))
                ->orderBy('start_time')
                ->paginate(10)
        )]);
    }

    public function event_detail(Event $event)
    {
        return view('frontend.pages.events_detail', ['event' => $event, 'authUser' => Auth::check() ? Auth::id() : 0]);
    }

    public function job_offers()
    {
        return view('frontend.pages.job_offers', ['offers' => collect(JobOffer::where('is_public', 1)->orderBy('company_id', 'asc')->get())->groupBy('company_id')]);
    }

    public function job_offer_detail(JobOffer $job_offer)
    {
        return view('frontend.pages.job_offer_detail', ['job_offer' => $job_offer]);
    }

    public function job_offer_apply(JobOffer $job_offer)
    {
        return view('frontend.pages.job_offer_apply', ['job_offer' => $job_offer]);
    }

    public function job_offer_user_apply(JobOffer $job_offer, Request $request)
    {
        JobOfferApply::create(['job_offer_id' => $job_offer->id, 'user_id' => Auth::id(), 'message' => $request->message]);
        $user = $job_offer->company->user;
        $user->notify(new NewApplyJob($job_offer, Auth::user()));
        try {
            $user->notify(new NewApplyJobMail($job_offer, Auth::user()));
        } catch (\Exception $exception) {
            error_log($exception->getMessage());
        }

        return redirect()->route('job_offers');
    }

    public function reservations()
    {
        return view('frontend.pages.reservations');
    }

    public function booking(Location $location, $date = null, $time = null)
    {
        return view('frontend.pages.book_a_table', [
            'location' => $location,
            'booking_date' => $date,
            'booking_time' => DayHour::all()->pluck('hour')
        ]);
    }

    public function booking_store(BookingRequest $request, Location $location)
    {

        $request->merge([
            'location_id' => $location->id,
            'booking_datetime' => Carbon::parse($request->booking_date . ' ' . $request->booking_time)->toDateTime()->format('Y-m-d H:i:s'),
            'auth_user_id' => Auth::check() ? Auth::id() : null
        ]);
        $booking = Booking::create($request->all());
        try {
            $user = $booking->location->company->user;
            $user->notify(new TableBooking($booking));
        } catch (\Exception $exception) {
            error_log($exception->getMessage());
        }

        return redirect()->route('reservations.booking.success', ['location' => $location->id, 'booking' => $booking->id]);
    }

    public function booking_success(Location $location, Booking $booking)
    {
        return view('frontend.pages.book_a_table_confirmation', ['location' => $location, 'booking' => $booking]);
    }

    public function terms_and_conditions()
    {
        return view('frontend.pages.terms_and_conditions');
    }

    public function privacy_policy()
    {
        return view('frontend.pages.privacy_policy');
    }

    public function contacts()
    {
        return view('frontend.pages.contacts');
    }

    public function contacts_store(ContactFormRequest $request)
    {
        try {
            $users = Role::where('internal', 1)->first()->users;
            foreach ($users as $user) {
                $user->notify(new ContactFormNotification($request->all()));
            }
            return redirect()->back()->withErrors(['message' => 'Messaggio inviato correttamente', 'alert' => 'success', 'icon' => 'check-circle']);
        } catch (\Exception $exception) {
            error_log($exception->getMessage());
            return redirect()->back()->withErrors(['message' => 'C\'è stato un errore, si prega di riprovare', 'alert' => 'danger', 'icon' => 'exclamation-triangle']);
        }
    }

    public function about()
    {
        return view('frontend.pages.about');
    }

    public function t_regalo()
    {
        return view('frontend.pages.t_regalo');
    }

    public function buy_t_regalo()
    {
        return view('frontend.pages.buy_t_regalo', ['tregalo' => Tregalo::all()]);
    }

    public function store_buy_t_regalo(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string',
                'email' => 'required|email',
                'address' => 'required',
            ]);

            Stripe::setApiKey(env('STRIPE_SECRET'));
            $user = $request->user();
            if (is_null($user->stripe_id))
                $stripeCustomer = $user->createAsStripeCustomer();
            else
                $stripeCustomer = $user->stripe_id;

            Customer::createSource(
                $user->stripe_id,
                ['source' => $request->stripeToken]
            );



            $tregalo = Tregalo::all();
            $total_price = 0;
            foreach ($tregalo as $item) {
                if (intval($request->{'item_' . $item->id}) > 0) {
                    $total_price = $total_price + (intval($request->{'item_' . $item->id}) * ($item->price / 100));
                }
            }

            Charge::create([
                'amount' => ($total_price * 100),
                'currency' => 'chf',
                'customer' => $stripeCustomer
            ]);

//            $user->charge(($total_price * 100), $request->paymentMethod);

            // create order
            $order = TregaloOrder::create([
                'user_id' => $user->id,
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'address' => $request->address,
            ]);

            foreach ($tregalo as $item) {
                if (intval($request->{'item_' . $item->id}) > 0) {
                    TregaloUser::create([
                        'tregalo_id' => $item->id,
                        'order_id' => $order->id,
                        'qty' => intval($request->{'item_' . $item->id})
                    ]);
                }
            }
            $user->notify(new UserBuyTRegalo($order));

            $tigit = User::where('role_id', 2)->get();
            foreach($tigit as $user){
                $user->notify(new UserBuyTRegaloEmployee($order, $total_price));
            }

        } catch (IncompletePayment $exception) {
            return redirect()->route(
                'cashier.payment',
                [$exception->payment->id, 'redirect' => route('home')]
            );
        } catch (Exception $e) {
            return redirect()->back()->withErrors(['alert-danger' => $e->getMessage()]);
        }


        return redirect()->route('users.tregalo')->withErrors(['message' => 'ok', 'result' => 0]);
    }

    public function impressum()
    {
        return view('frontend.pages.impressum');
    }

}
