<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

class HomePageController extends Controller
{

    public function index()
    {
        if (config('app.env') == 'development') {
            return view('frontend.homepage');
        } elseif (config('app.env') == 'construction'){
            return view('frontend.site_under_construction');
        } else{
            return view('frontend.homepage');
        }
    }
}
