<?php

namespace App\Http\Controllers\Frontend\User;


use App\CvCertificate;
use App\CvDescription;
use App\CvEducation;
use App\CvExperience;
use App\CvHobby;
use App\CvLanguage;
use App\CvRole;
use App\CvSkill;
use App\CvSocial;
use App\CvUser;
use App\CvStatus;
use App\CvWidget;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\TregaloOrder;
use App\User;
use App\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class ProfileController extends Controller
{

    public function profile($edit = null)
    {

        return view('frontend.users.profile', ['edit' => $edit]);
    }

    public function update_address(Request $request)
    {
        $userAddress = new UserAddress();
        $userAddress->user_id = Auth::id();
        $userAddress->route = $request->route;
        $userAddress->nr = $request->nr;
        $userAddress->cap = $request->cap;
        $userAddress->town = $request->town;
        $userAddress->country = $request->country;
        $userAddress->save();
        return $userAddress;
    }

    public function update_password(UpdatePasswordRequest $request)
    {
        $user = Auth::user();
        $user->password = bcrypt($request->password);
        $user->save();
        return redirect()->back();
    }

    public function cv()
    {
        return view('frontend.users.cv_vue');
    }


    public function update_profile(UpdateProfileRequest $request)
    {
        $user = Auth::user();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->title_id = $request->title_id;
        $user->country_id = $request->country_id;
        $user->update();
        return redirect()->route('users.profile');
    }

    public function edit_avatar(Request $request)
    {
        if (Storage::disk('avatars')->exists(Auth::id())) {
            Storage::disk('avatars')->deleteDirectory(Auth::id());
        }
        $file = $request->file('file')->store(Auth::id(), 'avatars');
        $user = Auth::user();
        $user->update(['avatar' => 'storage/avatars/' . $file]);
        return redirect()->back();
    }

    public function tregalo()
    {
        return view('frontend.users.tregalo');
    }

    public function delete(User $user)
    {
        error_log($user->email);
        $user->delete();
        Auth::logout();
        return response()->json();
    }

}
