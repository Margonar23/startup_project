<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePasswordRequest;
use App\Http\Requests\Ui\UiCreateUserRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class AuthenticationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function registration($step)
    {
        return view('frontend.authentication.registration.step'.$step);
    }

    public function register(UiCreateUserRequest $request)
    {
        $data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role_id' => $request->role_id,
            'title_id' => 0,
        ];
        $user = User::create($data);

        event(new Registered($user));
        return redirect()->route('users.login');
    }

    public function forgot_password()
    {
        return view('frontend.authentication.forgot');
    }

    public function forgot_password_send(Request $request)
    {
        $email = $request->email;
        $user = User::where('email', $email)->first();
        if(is_null($user)){
            return redirect()->back()->withErrors(['email' => 'User non trovato.']);
        }
        //Create Password Reset Token
        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        $user->sendPasswordResetNotification($token);
        return view('frontend.authentication.forgot');
    }

    public function reset_password(Request $request)
    {
        $reset = DB::table('password_resets')->where(['email' => $request->email, 'token' => $request->token])->first();
        if (is_null($reset)){
            return redirect()->back()->withErrors(['errors' => 'Token scaduto']);
        }
        if ($request->password === $request->password_confirmation){
            $user = User::where('email', $request->email)->first();
            if (is_null($user))
                return redirect()->back()->withErrors(['errors' => 'Utente non trovato']);

            $user->password = bcrypt($request->password);
            $user->update();
            DB::table('password_resets')->where(['email' => $request->email, 'token' => $request->token])->delete();
            return redirect()->route('users.login');
        }else{
            return redirect()->back()->withErrors(['errors' => 'Le password non sono uguali']);
        }

    }



    public function login()
    {
        if (Auth::guest())
            return view('frontend.authentication.login');
        else
            return redirect()->route('backend.dashboard');
    }

//    public function create_password($token)
//    {
//        $user = User::where('password', $token)->first();
//        if (is_null($user)){
//            abort(403, 'Token Expired');
//        }
//        return view('frontend.authentication.create_password', ['user' => $user]);
//    }

    public function store_password(CreatePasswordRequest $request)
    {
        $user = User::find($request->user_id);
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('users.login');
    }
}
