<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Notifications\NewEmployeeNotification;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use const http\Client\Curl\AUTH_ANY;

class UserAccountController extends Controller
{

    public function account($subnav = null)
    {
        return view('backend.users.account', ['subnav' => $subnav]);
    }

    public function update_account(UpdateAccountRequest $request)
    {
        $request->merge(['password' => bcrypt($request->password)]);
        Auth::user()->update($request->all());
        return redirect()->back()->withErrors(['alert-success' => trans('backend/users.account_update')]);
    }

    public function update_account_avatar(Request $request)
    {
        if (Storage::disk('avatars')->exists(Auth::id())){
            Storage::disk('avatars')->deleteDirectory(Auth::id());
        }
        $file = $request->file('file')->store(Auth::id(), 'avatars');
        Auth::user()->update(['avatar' => 'storage/avatars/'.$file]);
        return redirect()->route('backend.account');
    }


}
