<?php

namespace App\Http\Controllers\Backend;

use App\Company;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InboxController extends Controller
{
    public function index(Company $company)
    {
        return view('backend.users.inbox');
    }

    public function mark_all_as_read()
    {
        Auth::user()->unreadNotifications->markAsRead();
        return response()->json(Auth::user()->notifications);
    }

    public function mark_as_read(Request $request)
    {
        $notifications = Auth::user()->notifications->whereIn('id', $request->notifications);

        foreach ($notifications as $notification){
            is_null($notification->read_at) ? $notification->markAsRead() : $notification->markAsUnread();
        }

        return response()->json(Auth::user()->notifications);
    }
    public function delete(Request $request)
    {
        Auth::user()->notifications->where('id', $request->notification)->first()->delete();

        return response()->json(Auth::user()->notifications);
    }

}
