<?php

namespace App\Http\Controllers\Backend;

use App\Company;
use App\CompanyCategory;
use App\CompanyClassification;
use App\CompanyPackage;
use App\CompanyPaymentInformation;
use App\CompanyStatus;
use App\District;
use App\Feature;
use App\Http\Controllers\Controller;
use App\Http\Requests\BackendCreateCompany;
use App\Http\Requests\CreateCompanyCategoryRequest;
use App\Http\Requests\CreateFeatureRequest;
use App\Http\Requests\CreatePackageRequest;
use App\Http\Requests\UpdateCompanyCategoryRequest;
use App\Http\Requests\UpdateFeatureRequest;
use App\Http\Requests\UpdatePackageRequest;
use App\Notifications\CompanyStatusNotification;
use App\Notifications\SendMailCredentials;
use App\Notifications\StaffCompanyCreation;
use App\Package;
use App\PaymentMethod;
use App\PaymentType;
use App\Role;
use App\User;
use App\UserCompany;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use phpseclib\Crypt\Random;
use PHPUnit\TestFixture\MockObject\ParentClass;

class CompanyManagementController extends Controller
{

    public function index()
    {
        return view('backend.company_management.index', [
            'companies' => Company::all()->sortBy('status_id')
        ]);
    }

    public function create_company()
    {
        return view('backend.company_management.create', [
            'classifications' => CompanyClassification::all(),
            'categories' => CompanyCategory::all(),
            'districts' => District::all(),
            'packages' => Package::where('active',1)->get(),
            'payment_types' => PaymentType::where('active',1)->get(),
        ]);
    }

    public function store(BackendCreateCompany $request)
    {
        $str = 'Password dell account: '. Auth::user()->email;
        $file = $request->file('file')->store(0 . '/logo', 'company');
        $user_val = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->user_mail,
            'title_id' => 0,
            'role_id' => Role::where('label', 'company_user')->first()->id,
            'password' => Auth::user()->password,
        ];
        $user = new User($user_val);
        $user->save();

        $company_val = [
            'name' => $request->name,
            'label' => Str::slug($request->name),
            'uid' => $request->uid,
            'logo' => 'storage/company/' . $file,
            'status_id' => CompanyStatus::where('label', 'sub')->first()->id,
            'category_id' => $request->category_id,
            'classification_id' => $request->classification_id,
            'email' => $request->email,
            'avg_rating' => 0,
            'fax' => $request->fax,
            'phone' => $request->phone,
            'address' => $request->address,
            'website' => $request->website,
            'created_by' => $user->id,
            'district_id' => $request->district_id,
            'created_by_staff' => true
        ];

        $company = new Company($company_val);
        $company->save();

        $company_package_val = [
            'company_id' => $company->id,
            'package_id' => $request->package_id,
            'payment_method_id' => PaymentMethod::where('label', 'invoice')->first()->id,
            'payment_type_id' => $request->payment_type_id,
            'last_payment_at' => Carbon::now(),
            'subscription_expiry_at' => Carbon::now()->addMonths(PaymentType::find($request->payment_type_id)->month),
            'active' => true
        ];

        $company_package = new CompanyPackage($company_package_val);
        $company_package->save();

        $company_payment_information_val = [
            'company_package_id' => $company_package->id,
            'name' => $request->invoice_name,
            'email' => $request->invoice_email,
            'address' => $request->invoice_address,
        ];

        $company_payment_information = new CompanyPaymentInformation($company_payment_information_val);
        $company_payment_information->save();

        $userCompany = UserCompany::create(['user_id' => $user->id, 'company_id' => $company->id]);

        try {
            $user = Auth::user();
            $user->notify(new StaffCompanyCreation($company, $str));
        }catch (\Exception $exception){

        }

        return redirect()->route('company_management.show', ['company' => $company->id]);
    }

    public function show(Company $company)
    {
        return view('backend.company_management.show', [
            'company' => $company,
            'statuses' => CompanyStatus::all()
        ]);
    }

    public function send_mail(Company $company)
    {

        try {
            $pwd = Str::random(8);
            $user = $company->user;
            $user->update(['password' => bcrypt($pwd)]);
            $user->notify(new SendMailCredentials($company, $pwd));
            $company->update(['created_by_staff' => 0]);
        } catch (\Exception $exception){
            return redirect()->back()->withErrors(['alert-success' => 'Attenzione! c\'è stato un problema']);
        }

        return redirect()->back()->withErrors(['alert-success' => 'Hai inviato con successo le credenziali']);
    }

    public function update_status(Company $company, $status)
    {
        $company->update(['status_id' => $status]);

        if ($company->status->active)
            $company->user->notify(new CompanyStatusNotification($company));

        return redirect()->back();
    }


}
