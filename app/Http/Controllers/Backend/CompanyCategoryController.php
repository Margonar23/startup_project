<?php

namespace App\Http\Controllers\Backend;

use App\CompanyCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyCategoryRequest;
use App\Http\Requests\UpdateCompanyCategoryRequest;

class CompanyCategoryController extends Controller
{

    public function index()
    {
        return view('backend.company_categories.index', [
            'company_categories' => CompanyCategory::all()
        ]);
    }

    public function create()
    {
        return view('backend.company_categories.create_edit');
    }

    public function store(CreateCompanyCategoryRequest $request)
    {
        CompanyCategory::create($request->all());
        return redirect()->route('company_categories.index');
    }

    public function edit(CompanyCategory $company_category)
    {
        return view('backend.company_categories.create_edit', ['company_category' => $company_category]);
    }

    public function update(CompanyCategory $company_category, UpdateCompanyCategoryRequest $request)
    {
        $company_category->update($request->all());
        return redirect()->route('company_categories.index');
    }


    public function destroy(CompanyCategory $company_category)
    {
        $company_category->delete();
        return redirect()->back()->withErrors(['alert-success' => trans('backend/company_categories.delete', [
            'company_category' => $company_category->name
        ])]);
    }
}
