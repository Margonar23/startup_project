<?php

namespace App\Http\Controllers\Backend;

use App\CompanyCategory;
use App\Feature;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyCategoryRequest;
use App\Http\Requests\CreateFeatureRequest;
use App\Http\Requests\CreatePackageRequest;
use App\Http\Requests\UpdateCompanyCategoryRequest;
use App\Http\Requests\UpdateFeatureRequest;
use App\Http\Requests\UpdatePackageRequest;
use App\Package;
use PHPUnit\TestFixture\MockObject\ParentClass;

class PackageController extends Controller
{

    public function index()
    {
        return view('backend.packages.index', [
            'packages' => Package::all()
        ]);
    }

    public function create()
    {
        return view('backend.packages.create_edit', [
            'features' => Feature::where('active',1)->get()
        ]);
    }

    public function store(CreatePackageRequest $request)
    {

        $request->merge(['active' => $request->has('active') ? 1 : 0]);
        $package = Package::create($request->all());
        $package->features()->sync($request->features);
        return redirect()->route('packages.index');
    }

    public function edit(Package $package)
    {
        return view('backend.packages.create_edit', ['package' => $package, 'features' => Feature::where('active', 1)->get()]);
    }

    public function update(Package $package, UpdatePackageRequest $request)
    {
        $request->merge(['active' => $request->has('active') ? 1 : 0]);
        $package->update($request->all());
        $package->features()->sync($request->features);
        return redirect()->route('packages.index');
    }


    public function destroy(Package $package)
    {
        $package->features()->sync([]);
        $package->delete();
        return redirect()->back()->withErrors(['alert-success' => trans('backend/packages.delete', [
            'package' => $package->name
        ])]);
    }
}
