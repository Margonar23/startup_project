<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\News;
use DOMDocument;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{

    public function index()
    {
        return view('backend.news.index', ['news' => News::all()]);
    }

    public function create()
    {
        return view('backend.news.create_edit');
    }

    public function store(CreateNewsRequest $request)
    {
        $desc = $request->description;
        libxml_use_internal_errors(true);
        $dom = new domdocument();
        $dom->loadHtml($desc, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        foreach ($images as $count => $image) {
            $src = $image->getAttribute('src');
            if (preg_match('/data:image/', $src)) {
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimeType = $groups['mime'];
                $path = uniqid('', true) . '.' . $mimeType;
                Storage::disk('news')->put($path, file_get_contents($src));
                $image->removeAttribute('src');
                $image->setAttribute('src', Storage::disk('news')->url('news/'.$path));
            }
        }
        $desc = $dom->savehtml();
        $file = $request->file('header_img')->store('', 'news');

        News::create([
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'header_image' => 'storage/news/' . $file,
            'description' => $desc,
            'user_id' => Auth::id()
        ]);

        return redirect()->route('news.index');
    }

    public function edit(News $news)
    {
        return view('backend.news.create_edit', ['news' => $news]);
    }

    public function update(News $news, UpdateNewsRequest $request)
    {
        $desc = $request->description;
        libxml_use_internal_errors(true);
        $dom = new domdocument();
        $dom->loadHtml($desc, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        foreach ($images as $count => $image) {
            $src = $image->getAttribute('src');
            if (preg_match('/data:image/', $src)) {
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimeType = $groups['mime'];
                $path = uniqid('', true) . '.' . $mimeType;
                Storage::disk('news')->put($path, file_get_contents($src));
                $image->removeAttribute('src');
                $image->setAttribute('src', Storage::disk('news')->url('news/'.$path));
            }
        }
        $desc = $dom->savehtml();

        $news->update([
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'description' => $desc,
            'user_id' => Auth::id()
        ]);

        return redirect()->route('news.index');
    }

    public function destroy(News $news)
    {
        $news->delete();
        return redirect()->back()->withErrors(['alert-success' => trans('backend/news.delete', ['news' => $news->title])]);
    }


}
