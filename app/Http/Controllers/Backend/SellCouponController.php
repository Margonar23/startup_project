<?php

namespace App\Http\Controllers\Backend;

use App\CompanyCategory;
use App\Coupon;
use App\District;
use App\Feature;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyCategoryRequest;
use App\Http\Requests\CreateCouponRequest;
use App\Http\Requests\CreateFeatureRequest;
use App\Http\Requests\UpdateCompanyCategoryRequest;
use App\Http\Requests\UpdateCouponRequest;
use App\Http\Requests\UpdateFeatureRequest;
use App\TregaloOrder;
use App\TregaloUser;

class SellCouponController extends Controller
{

    public function index()
    {
        return view('backend.sell_coupons.index', [
            'sell_coupons' => TregaloOrder::orderBy('shipped')->get(),
//            'categories' => CompanyCategory::all(),
//            'district' => District::all()
        ]);
    }

    public function show(TregaloOrder $sell_coupon)
    {
        return view('backend.sell_coupons.show', [
            'tregalo' => $sell_coupon,
        ]);
    }

    public function edit(TregaloOrder $sell_coupon)
    {
        $sell_coupon->shipped = !$sell_coupon->shipped;
        $sell_coupon->update();
        return redirect()->route('sell_coupons.index');
    }

}
