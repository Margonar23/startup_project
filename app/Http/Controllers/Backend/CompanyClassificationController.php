<?php

namespace App\Http\Controllers\Backend;

use App\CompanyClassification;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyClassificationRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Http\Requests\UpdateCompanyClassificationRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Notifications\NewEmployeeNotification;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use const http\Client\Curl\AUTH_ANY;

class CompanyClassificationController extends Controller
{

    public function index()
    {
        return view('backend.company_classifications.index', [
            'company_classifications' => CompanyClassification::all()
        ]);
    }

    public function create()
    {
        return view('backend.company_classifications.create_edit');
    }

    public function store(CreateCompanyClassificationRequest $request)
    {
        CompanyClassification::create($request->all());
        return redirect()->route('company_classifications.index');
    }

    public function edit(CompanyClassification $company_classification)
    {
        return view('backend.company_classifications.create_edit', ['company_classification' => $company_classification]);
    }

    public function update(CompanyClassification $company_classification, UpdateCompanyClassificationRequest $request)
    {
        $company_classification->update($request->all());
        return redirect()->route('company_classifications.index');
    }


    public function destroy(CompanyClassification $company_classification)
    {
        $company_classification->delete();
        return redirect()->back()->withErrors(['alert-success' => trans('backend/company_classifications.delete', [
            'company_classification' => $company_classification->name
        ])]);
    }
}
