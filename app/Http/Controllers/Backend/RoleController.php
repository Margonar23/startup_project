<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    public function index()
    {
        return view('backend.roles.index', ['roles' => Role::all()]);
    }

    public function create()
    {
        return view('backend.roles.create_edit');
    }

    public function store(CreateRoleRequest $request)
    {
        $request->request->add(['internal' => true]);
        Role::create($request->all());
        return redirect()->route('roles.index');
    }

    public function edit(Role $role)
    {
        return view('backend.roles.create_edit', ['role' => $role]);
    }

    public function update(Role $role, UpdateRoleRequest $request)
    {
        $role->update($request->all());
        return redirect()->route('roles.index');
    }

    public function destroy(Role $role)
    {
        $role->delete();
        return redirect()->back()->withErrors(['alert-success' => trans('backend/roles.delete', ['role' => $role->name])]);
    }


}
