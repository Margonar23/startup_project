<?php

namespace App\Http\Controllers\Backend;

use App\CompanyCategory;
use App\Feature;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyCategoryRequest;
use App\Http\Requests\CreateFeatureRequest;
use App\Http\Requests\CreateUserTitleRequest;
use App\Http\Requests\UpdateCompanyCategoryRequest;
use App\Http\Requests\UpdateFeatureRequest;
use App\Http\Requests\UpdateUserTitleRequest;
use App\UserTitle;

class UserTitleController extends Controller
{

    public function index()
    {
        return view('backend.user_titles.index', ['user_titles' => UserTitle::all()]);
    }

    public function create()
    {
        return view('backend.user_titles.create_edit');
    }

    public function store(CreateUserTitleRequest $request)
    {
        UserTitle::create($request->all());
        return redirect()->route('user_titles.index');
    }

    public function edit(UserTitle $user_title)
    {
        return view('backend.user_titles.create_edit', ['user_title' => $user_title]);
    }

    public function update(UserTitle $user_title, UpdateUserTitleRequest $request)
    {
        $user_title->update($request->all());
        return redirect()->route('user_titles.index');
    }


    public function destroy(UserTitle $user_title)
    {
        $user_title->delete();
        return redirect()->back()->withErrors(['alert-success' => trans('backend/user_titles.delete', [
            'user_title' => $user_title->name
        ])]);
    }
}
