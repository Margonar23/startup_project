<?php

namespace App\Http\Controllers\Backend;

use App\CompanyCategory;
use App\Coupon;
use App\District;
use App\Feature;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyCategoryRequest;
use App\Http\Requests\CreateCouponRequest;
use App\Http\Requests\CreateFeatureRequest;
use App\Http\Requests\UpdateCompanyCategoryRequest;
use App\Http\Requests\UpdateCouponRequest;
use App\Http\Requests\UpdateFeatureRequest;

class CouponController extends Controller
{

    public function index()
    {
        return view('backend.coupons.index', [
            'coupons' => Coupon::all(),
//            'categories' => CompanyCategory::all(),
//            'district' => District::all()
        ]);
    }

    public function create()
    {
        return view('backend.coupons.create_edit', [
            'categories' => CompanyCategory::all(),
            'districts' => District::all()
        ]);
    }

    public function store(CreateCouponRequest $request)
    {
        if ($request->has('file')){
            $file = $request->file('file')->store(null, 'coupons');
            $request->merge(['logo' => 'storage/coupons/' . $file]);
        }

        Coupon::create($request->all());
        return redirect()->route('coupons.index');
    }

    public function edit(Coupon $coupon)
    {
        return view('backend.coupons.create_edit', [
            'coupon' => $coupon,
            'categories' => CompanyCategory::all(),
            'districts' => District::all()
        ]);
    }

    public function update(Coupon $coupon, UpdateCouponRequest $request)
    {
        if ($request->has('file')){
            $file = $request->file('file')->store(null, 'coupons');
            $request->merge(['logo' => 'storage/coupons/' . $file]);
        }
        $coupon->update($request->all());
        return redirect()->route('coupons.index');
    }


    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
        return redirect()->back()->withErrors(['alert-success' => 'Eliminato con successo']);
    }
}
