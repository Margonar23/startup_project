<?php

namespace App\Http\Controllers\Backend;

use App\CompanyCategory;
use App\CompanyStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyStatusRequest;
use App\Http\Requests\UpdateCompanyCategoryRequest;
use App\Http\Requests\UpdateCompanyStatusRequest;
use Illuminate\Support\Str;

class CompanyStatusController extends Controller
{

    public function index()
    {
        return view('backend.company_statuses.index', [
            'company_statuses' => CompanyStatus::all()
        ]);
    }

    public function create()
    {
        return view('backend.company_statuses.create_edit');
    }

    public function store(CreateCompanyStatusRequest $request)
    {
        $request->merge([
            'active' => $request->has('active') ? 1 : 0,
            'label' => Str::slug($request->name)
        ]);
        CompanyStatus::create($request->all());
        return redirect()->route('company_statuses.index');
    }

    public function edit(CompanyStatus $companyStatus)
    {
        return view('backend.company_statuses.create_edit', ['company_status' => $companyStatus]);
    }

    public function update(CompanyStatus $companyStatus, UpdateCompanyStatusRequest $request)
    {
        $request->merge([
            'active' => $request->has('active') ? 1 : 0,
            'label' => Str::slug($request->name)
        ]);
        $companyStatus->update($request->all());
        return redirect()->route('company_statuses.index');
    }


    public function destroy(CompanyStatus $companyStatus)
    {
        $companyStatus->delete();
        return redirect()->back()->withErrors(['alert-success' => trans('backend/company_statuses.delete', [
            'company_status' => $companyStatus->name
        ])]);
    }
}
