<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Notifications\NewEmployeeNotification;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use const http\Client\Curl\AUTH_ANY;

class UserController extends Controller
{

    public function index()
    {
        $role_ids = collect(Role::where('internal', 1)->get())->pluck('id', 'id');
        return view('backend.users.index', ['users' => User::whereIn('role_id', $role_ids)->get()]);
    }

    public function create()
    {
        return view('backend.users.create_edit', ['roles' => Role::all()]);
    }

    public function store(CreateUserRequest $request)
    {
        $token = Str::random(60);
        $request->request->add(['password' => $token]);
        $request->request->add(['title_id' => 0]);
        $user = User::create($request->all());
        try {
            $user->notify(new NewEmployeeNotification($token));
        }catch (\Exception $exception){
            error_log($exception->getMessage());
        }
        return redirect()->route('users.index');
    }

    public function edit(User $user)
    {
        return view('backend.users.create_edit', ['user' => $user, 'roles' => Role::all()]);
    }

    public function update(User $user, UpdateUserRequest $request)
    {
        $user->update($request->all());
        return redirect()->route('users.index');
    }


    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->withErrors(['alert-success' => trans('backend/users.delete', ['user' => $user->name()])]);
    }

    public function account($subnav = null)
    {
        return view('backend.users.account', ['subnav' => $subnav]);
    }

    public function update_account(UpdateAccountRequest $request)
    {
        $request->merge(['password' => bcrypt($request->password)]);
        Auth::user()->update($request->all());
        return redirect()->back()->withErrors(['alert-success' => trans('backend/users.account_update')]);
    }

    public function update_account_avatar(Request $request)
    {
        if (Storage::disk('avatars')->exists(Auth::id())) {
            Storage::disk('avatars')->deleteDirectory(Auth::id());
        }
        $file = $request->file('file')->store(Auth::id(), 'avatars');
        Auth::user()->update(['avatar' => 'storage/avatars/' . $file]);
        return redirect()->route('backend.account');
    }

    public function registered()
    {
        return view('backend.users.registered', ['users' => User::all()]);
    }
}
