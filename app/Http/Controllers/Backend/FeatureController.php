<?php

namespace App\Http\Controllers\Backend;

use App\CompanyCategory;
use App\Feature;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyCategoryRequest;
use App\Http\Requests\CreateFeatureRequest;
use App\Http\Requests\UpdateCompanyCategoryRequest;
use App\Http\Requests\UpdateFeatureRequest;

class FeatureController extends Controller
{

    public function index()
    {
        return view('backend.features.index', [
            'features' => Feature::all()
        ]);
    }

    public function create()
    {
        return view('backend.features.create_edit');
    }

    public function store(CreateFeatureRequest $request)
    {
        $request->merge(['active' => $request->has('active') ? 1 : 0]);
        Feature::create($request->all());
        return redirect()->route('features.index');
    }

    public function edit(Feature $feature)
    {
        return view('backend.features.create_edit', ['feature' => $feature]);
    }

    public function update(Feature $feature, UpdateFeatureRequest $request)
    {
        $request->merge(['active' => $request->has('active') ? 1 : 0]);
        $feature->update($request->all());
        return redirect()->route('features.index');
    }


    public function destroy(Feature $feature)
    {
        $feature->delete();
        return redirect()->back()->withErrors(['alert-success' => trans('backend/features.delete', [
            'feature' => $feature->name
        ])]);
    }
}
