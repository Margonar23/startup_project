<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyCategory;
use App\CompanyClassification;
use App\CompanyStatus;
use App\CompanySubRole;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Requests\Ui\UiCreateCompanyRequest;
use App\Http\Requests\Ui\UiUpdateCompanyRequest;
use App\Notifications\newCompanyNotification;
use App\Package;
use App\Role;
use App\User;
use App\UserCompany;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CompanyController extends Controller
{

    public function index(Company $company)
    {
        return view('company.company.status.' . $company->status->label, [
            'company' => $company,
            'packages' => Package::where('active', 1)->get()->sortBy('price'),
            'invoices' => Auth::user()->invoices()
        ]);
    }

    public function create()
    {
        return view('company.company.create');
    }

    public function edit(Company $company, $step)
    {
        return view('company.company.step.edit_' . $step, [
            'company' => $company,
            'categories' => CompanyCategory::all()->sortBy('name'),
            'classifications' => CompanyClassification::all()->sortBy('name'),
            'districts' => District::all()->sortBy('name'),
        ]);
    }

    public function edit_data(Company $company)
    {
        return view('company.company.edit', [
            'company' => $company,
            'categories' => CompanyCategory::all(),
            'classifications' => CompanyClassification::all(),
            'districts' => District::all()
        ]);
    }

    public function edit_logo(Company $company)
    {
        return view('company.company.edit_logo', ['company' => $company]);
    }

    public function update(Company $company, UiUpdateCompanyRequest $request)
    {
        $company->update($request->all());
        return redirect()->route('company.index', ['company' => $company->id]);
    }

    public function store(UiCreateCompanyRequest $request)
    {
        $request->merge([
            'label' => Str::slug($request->name),
            'status_id' => CompanyStatus::where('label', 'request')->first()->id,
            'created_by' => Auth::id(),
            'avg_rating' => 0,
        ]);
        $company = Company::create($request->all());

        $userCompany = UserCompany::create(['user_id' => Auth::id(), 'company_id' => $company->id]);
        try {
            $users = Role::where('internal', 1)->first()->users;
            foreach ($users as $user) {
                $user->notify(new newCompanyNotification($company, Auth::user()));
            }
        } catch (\Exception $exception) {
            error_log($exception->getMessage());
        }

        return redirect()->route('company.index', ['company' => $company->id]);
    }

    public function insert_company_logo(Request $request, Company $company)
    {

        $file = $request->file('file')->store($company->id . '/logo', 'company');
        $company->update(['logo' => 'storage/company/' . $file]);
        return redirect()->back();
    }

    public function delete_page(Company $company)
    {
        return view('company.company.delete', ['company' => $company]);
    }

    public function delete(Company $company)
    {
        $company->subscription->update(['active' => 0]);
        $company->delete();

        return view('company.dashboard');
    }


}
