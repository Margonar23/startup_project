<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyPresentationPage;
use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyCreateEvent;
use App\Http\Requests\CompanyCreateLocation;
use App\Http\Requests\CompanyCreatePresentation;
use App\Http\Requests\CompanyLocationRoomCreate;
use App\Http\Requests\CompanyLocationRoomUpdate;
use App\Http\Requests\CompanyUpdateLocation;
use App\Http\Requests\CompanyUpdatePresentation;
use App\Location;
use App\LocationRoom;
use App\TableShape;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DOMDocument;

class CompanyLocationRoomController extends Controller
{

    public function index(Company $company, Location $location)
    {
        return view('company.locations.rooms.index', [
            'company' => $company,
            'location' => $location,
        ]);
    }

    public function create(Company $company, Location $location)
    {
        return view('company.locations.rooms.create_edit', [
            'company' => $company,
            'location' => $location,
            'shapes' => TableShape::all()
        ]);
    }

    public function edit(Company $company, Location $location, LocationRoom $room)
    {
        return view('company.locations.rooms.create_edit', [
            'company' => $company,
            'location' => $location,
            'room' => $room,
        ]);
    }

    public function update(Company $company, Location $location, LocationRoom $room, CompanyLocationRoomUpdate $request)
    {
        $room->update($request->all());
        return redirect()->route('company.locations.rooms.index', ['company' => $company->id, 'location' => $location->id]);
    }

    public function store(CompanyLocationRoomCreate $request, Company $company, Location $location)
    {
        $request->merge(['location_id' => $location->id]);
        LocationRoom::create($request->all());

        return redirect()->route('company.locations.rooms.index', ['company' => $company->id, 'location' => $location->id]);
    }

    public function delete(Company $company, Location $location, LocationRoom $room)
    {
        foreach ($room->tables as $table){
            $table->delete();
        }
        $room->delete();
        return redirect()->back();
    }

}
