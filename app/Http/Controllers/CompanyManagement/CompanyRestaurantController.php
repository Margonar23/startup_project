<?php

namespace App\Http\Controllers\CompanyManagement;

use App\AlimentaryType;
use App\Company;
use App\Day;
use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyCreateEvent;
use App\Http\Requests\CompanyUpdateEvent;
use App\KitchenType;
use App\Location;
use App\Restaurant;
use App\RestaurantInfo;
use App\RestaurantType;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CompanyRestaurantController extends Controller
{

    public function index(Company $company)
    {
        return view('company.restaurants.index', [
            'company' => $company,
        ]);
    }

    public function schedule(Company $company, Location $location)
    {
        return view('company.restaurants.schedule', [
            'company' => $company,
            'location' => $location,
            'days' => Day::all()
        ]);
    }

    public function schedule_store(Request $request, Company $company, Location $location)
    {
        $location->restaurant()->delete();
        foreach (Day::all() as $day) {
            $data = ['location_id' => $location->id, 'day_id' => $day->id, 'opening_at' => $request->{$day->id . '_opening_at'}, 'closing_at' => $request->{$day->id . '_closing_at'}];
            Restaurant::create($data);
        }

        return redirect()->route('company.restaurants.index', ['company' => $company->id]);
    }

    public function info(Company $company, Location $location)
    {
        return view('company.restaurants.info', [
            'company' => $company,
            'location' => $location,
            'types' => RestaurantType::all(),
            'kitchen_types' => KitchenType::all(),
            'alimentary_types' => AlimentaryType::all(),
        ]);
    }

    public function info_store(Request $request, Company $company, Location $location)
    {
        $location->restaurant_info()->delete();
        $request->merge(['location_id' => $location->id, 'alimentary_type' =>  json_encode($request->alimentary_type)]);
        RestaurantInfo::create($request->all());
        return redirect()->route('company.restaurants.index', ['company' => $company->id]);
    }


}
