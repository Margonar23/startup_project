<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyCategory;
use App\CompanyClassification;
use App\CompanyStatus;
use App\CompanySubRole;
use App\CompanySubRolePermission;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyCreateSubRole;
use App\Http\Requests\CompanyUpdateSubRole;
use App\Http\Requests\Ui\UiCreateCompanyRequest;
use App\Http\Requests\Ui\UiUpdateCompanyRequest;
use App\Package;
use App\UserCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CompanyRoleController extends Controller
{

    public function index(Company $company)
    {
        return view('company.roles.index', [
            'company' => $company
        ]);
    }

    public function create(Company $company)
    {
        return view('company.roles.create_edit', [
            'company' => $company
        ]);
    }

    public function edit(Company $company, CompanySubRole $company_sub_role)
    {
        return view('company.roles.create_edit', [
            'company' => $company,
            'company_sub_role' => $company_sub_role,
        ]);
    }

    public function update(Company $company, CompanySubRole $company_sub_role, CompanyUpdateSubRole $request)
    {
        $request->merge(['label' => Str::slug($request->name)]);
        $company_sub_role->update($request->except(['_token', 'permissions']));
        $company_sub_role->permissions()->delete();
        foreach ($request->permissions as $permission){
            CompanySubRolePermission::create(['company_sub_role_id' => $company_sub_role->id, 'permission_id' => $permission]);
        }
        return redirect()->route('company.roles.index', ['company' => $company->id]);
    }

    public function store(CompanyCreateSubRole $request, Company $company)
    {
        $request->merge([
            'label' => Str::slug($request->name),
            'company_id' => $company->id,
        ]);
        $company_sub_role = CompanySubRole::create($request->except(['_token', 'permissions']));
        foreach ($request->permissions as $permission){
            CompanySubRolePermission::create(['company_sub_role_id' => $company_sub_role->id, 'permission_id' => $permission]);
        }

        return redirect()->route('company.roles.index', ['company' => $company->id]);
    }

    public function delete(Company $company, CompanySubRole $company_sub_role)
    {
        $company_sub_role->permissions()->delete();
        $company_sub_role->delete();
        return redirect()->back();
    }

}
