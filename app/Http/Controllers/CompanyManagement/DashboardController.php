<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function index()
    {
        return view('company.dashboard');
    }

}
