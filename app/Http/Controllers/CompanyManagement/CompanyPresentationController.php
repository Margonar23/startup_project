<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyPresentationPage;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyCreatePresentation;
use App\Http\Requests\CompanyUpdatePresentation;
use App\PresentationPageTemplateContent;
use App\PresentationTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DOMDocument;

class CompanyPresentationController extends Controller
{

    public function index(Company $company)
    {
        return view('company.presentation.index', [
            'company' => $company,
        ]);
    }

    public function create(Company $company)
    {
        return view('company.presentation.create_edit', ['company' => $company, 'templates' => PresentationTemplate::all()]);
    }

    public function edit(Company $company, CompanyPresentationPage $page)
    {
        return view('company.presentation.create_edit', [
            'company' => $company,
            'page' => $page,
            'templates' => PresentationTemplate::all()
        ]);
    }

    public function update(Company $company, CompanyPresentationPage $page, CompanyUpdatePresentation $request)
    {
        $request->merge([
            'label' => Str::slug($request->page_name),
        ]);
        $page->update($request->all());
        return redirect()->route('company.presentation', ['company' => $company->id]);
    }

    public function store(CompanyCreatePresentation $request, Company $company)
    {
        $request->merge([
            'label' => Str::slug($request->page_name),
            'company_id' => $company->id,
            'active' => false,
        ]);

        CompanyPresentationPage::create($request->all());

        return redirect()->route('company.presentation', ['company' => $company->id]);
    }

    public function delete(Company $company, CompanyPresentationPage $page)
    {
        $page->delete();
        return redirect()->back();
    }

    public function change_status(Company $company, CompanyPresentationPage $page)
    {
        $page->update(['active' => !$page->active]);

        return redirect()->back();
    }

    public function content(Company $company, CompanyPresentationPage $page)
    {
        return view('company.presentation.content', [
            'company' => $company,
            'page' => $page
        ]);
    }

    public function save_content(Company $company, CompanyPresentationPage $page, Request $request)
    {
        $desc = $request->pc;
        if (!empty($desc)) {
            libxml_use_internal_errors(true);
            $dom = new domdocument();
            $dom->encoding = 'utf-8';
            $dom->loadHtml(utf8_decode($desc), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $dom->getElementsByTagName('img');
            foreach ($images as $count => $image) {
                $src = $image->getAttribute('src');
                if (preg_match('/data:image/', $src)) {
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimeType = $groups['mime'];
                    $path = uniqid('', true) . '.' . $mimeType;
                    Storage::disk('page_content')->put($path, file_get_contents($src));
                    $image->removeAttribute('src');
                    $image->setAttribute('src', Storage::disk('page_content')->url('page_content/' . $path));
                }
            }
            $desc = $dom->savehtml();
        }
        $file = $request->has('image') ? 'storage/company/' .$request->file('image')->store($company->id . '/logo', 'company') : null;
        $value = [
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'content' => $desc,
            'img' => $file,
            'page_id' => $page->id
        ];
        PresentationPageTemplateContent::create($value);

        return redirect()->route('company.presentation', ['company' => $company->id]);
    }

    public function update_content(Company $company, CompanyPresentationPage $page, Request $request)
    {
        $desc = $request->pc;
        if (!empty($desc)) {
            libxml_use_internal_errors(true);
            $dom = new domdocument();
            $dom->encoding = 'utf-8';
            $dom->loadHtml(utf8_decode($desc), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $dom->getElementsByTagName('img');
            foreach ($images as $count => $image) {
                $src = $image->getAttribute('src');
                if (preg_match('/data:image/', $src)) {
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimeType = $groups['mime'];
                    $path = uniqid('', true) . '.' . $mimeType;
                    Storage::disk('page_content')->put($path, file_get_contents($src));
                    $image->removeAttribute('src');
                    $image->setAttribute('src', Storage::disk('page_content')->url('page_content/' . $path));
                }
            }
            $desc = $dom->savehtml();
        }
        $file = $request->has('image') ? 'storage/company/' .$request->file('image')->store($company->id . '/logo', 'company') : $page->page_content->img;
        $value = [
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'content' => $desc,
            'img' => $file,
            'page_id' => $page->id
        ];
        $page->page_content->update($value);

        return redirect()->route('company.presentation', ['company' => $company->id]);
    }


}
