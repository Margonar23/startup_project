<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Booking;
use App\BookingReserved;
use App\Company;
use App\Http\Controllers\Controller;
use App\Http\Resources\BookingResource;
use App\Location;
use App\Notifications\AcceptBooking;
use App\Notifications\RejectBooking;
use App\ReservationStatus;
use App\Table;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CompanyReservationController extends Controller
{

    public function index(Company $company)
    {
        return view('company.reservations.index', [
            'company' => $company,
            'reservation_statuses' => ReservationStatus::all()
        ]);
    }


    public function bookings(Company $company, Location $location)
    {
        $today_bookings = Booking::where(['location_id' => $location->id, 'reservation_status_id' => 2])->whereDate('booking_datetime', '=', Carbon::today()->toDateString())->get();

        return view('company.reservations.bookings', [
            'company' => $company,
            'location' => $location,
            'today_bookings' => $today_bookings
        ]);
    }

    public function booking(Company $company, Location $location, Booking $booking)
    {
        return view('company.reservations.booking', [
            'company' => $company,
            'location' => $location,
            'booking' => $booking
        ]);
    }

    public function assign_table(Company $company, Location $location, Booking $booking, Table $table)
    {
        if ($booking->reservation_status->label == 'request' || $booking->reservation_status->label == 'rejected') {
            $booking->reservation_status_id = ReservationStatus::where('label', 'confirmed')->first()->id;

            BookingReserved::create([
                'booking_id' => $booking->id,
                'user_id' => $booking->auth_user_id,
                'table_id' => $table->id,
                'reserved_start' => $booking->booking_datetime,
                'reserved_stop' => Carbon::parse($booking->booking_datetime)->addHours(2)
            ]);

            $booking->update();

            try{
                $booking->user->notify(new AcceptBooking($booking));
            }catch (\Exception $exception){

            }
        } else {
            //  booking reservation
            $reservation = BookingReserved::where('booking_id', $booking->id)->first();
            $reservation->table_id = $table->id;
            $reservation->update();
        }
        return redirect()->route('company.reservations.bookings', ['company' => $company->id, 'location' => $location->id]);
    }

    public function reject_booking(Company $company, Location $location, Booking $booking)
    {
        $booking->reservation_status_id = ReservationStatus::where('label', 'rejected')->first()->id;
        $booking->update();
        $booking->reserved()->delete();
        try{
            $booking->user->notify(new RejectBooking($booking));
        }catch (\Exception $exception){

        }
        return redirect()->route('company.reservations.bookings', ['company' => $company->id, 'location' => $location->id]);
    }

    public function date_bookings(Company $company, Location $location, Request $request)
    {
        $bookings = Booking::where(['location_id' => $location->id, 'reservation_status_id' => 2])->whereDate('booking_datetime', '=', $request->date)->get();
        return response()->json(BookingResource::collection($bookings));
    }

}
