<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyPresentationPage;
use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyCreateEvent;
use App\Http\Requests\CompanyCreateLocation;
use App\Http\Requests\CompanyCreatePresentation;
use App\Http\Requests\CompanyLocationTableCreate;
use App\Http\Requests\CompanyLocationTableUpdate;
use App\Http\Requests\CompanyUpdateLocation;
use App\Http\Requests\CompanyUpdatePresentation;
use App\Location;
use App\LocationRoom;
use App\Table;
use App\TableShape;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DOMDocument;

class CompanyLocationTableController extends Controller
{

    public function index(Company $company, Location $location, LocationRoom $room)
    {
        return view('company.locations.tables.index', [
            'company' => $company,
            'location' => $location,
            'room' => $room,
        ]);
    }

    public function create(Company $company, Location $location, LocationRoom $room)
    {
        return view('company.locations.tables.create_edit', [
            'company' => $company,
            'location' => $location,
            'room' => $room,
            'shapes' => TableShape::all()
        ]);
    }

    public function edit(Company $company, Location $location, LocationRoom $room, Table $table)
    {
        return view('company.locations.tables.create_edit', [
            'company' => $company,
            'location' => $location,
            'room' => $room,
            'table' => $table,
            'shapes' => TableShape::all()
        ]);
    }

    public function update(Company $company, Location $location, LocationRoom $room, Table $table, CompanyLocationTableUpdate $request)
    {

        $table->update($request->all());
        return redirect()->route('company.locations.tables.index', ['company' => $company->id, 'location' => $location->id, 'room' => $room->id]);
    }

    public function store(CompanyLocationTableCreate $request, Company $company, Location $location, LocationRoom $room)
    {
        $request->merge(['location_room_id' => $room->id]);
        Table::create($request->all());

        return redirect()->route('company.locations.tables.index', ['company' => $company->id, 'location' => $location->id, 'room' => $room->id]);
    }

    public function delete(Company $company, Location $location)
    {
        $events = Event::where('location_id', $location->id)->get();
        foreach ($events as $event) {
            $event->update(['location_id' => null]);
        }
        $location->delete();
        return redirect()->back();
    }

}
