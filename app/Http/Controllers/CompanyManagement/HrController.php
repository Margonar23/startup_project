<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyCategory;
use App\CompanyClassification;
use App\CompanyStatus;
use App\CompanySubRole;
use App\Employment;
use App\Http\Controllers\Controller;
use App\Http\Requests\JobOfferCreateRequest;
use App\Http\Requests\JobOfferUpdateRequest;
use App\Http\Requests\Ui\UiCreateCompanyRequest;
use App\Http\Requests\Ui\UiUpdateCompanyRequest;
use App\Http\Resources\UserResource;
use App\JobOffer;
use App\JobOfferApply;
use App\Package;
use App\Profession;
use App\Skill;
use App\User;
use App\UserCompany;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use function foo\func;
use DOMDocument;

class HrController extends Controller
{

    public function index(Company $company)
    {
        return view('company.hr.index', [
            'company' => $company,
        ]);
    }

    public function create(Company $company)
    {
        return view('company.hr.create_edit', [
            'company' => $company,
            'professions' => Profession::orderBy('name')->get(),
            'employments' => Employment::orderBy('name')->get(),
            'skills' => Skill::orderBy('name')->get(),
        ]);
    }

    public function edit(Company $company, JobOffer $job_offer)
    {
        return view('company.hr.create_edit', [
            'company' => $company,
            'professions' => Profession::orderBy('name')->get(),
            'employments' => Employment::orderBy('name')->get(),
            'skills' => Skill::orderBy('name')->get(),
            'job_offer' => $job_offer
        ]);
    }

    public function update(Company $company, JobOfferUpdateRequest $request, JobOffer $job_offer)
    {
        $request->merge([
            'is_public' => $request->has('is_public') ? 1 : 0,
            'skills' => json_encode($request->skills)
        ]);
        $job_offer->update($request->all());
        return redirect()->route('company.hr.index', ['company' => $company->id]);
    }

    public function store(JobOfferCreateRequest $request, Company $company)
    {
        $desc = $request->description;
        libxml_use_internal_errors(true);
        $dom = new domdocument();
        $dom->loadHtml($desc, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        foreach ($images as $count => $image) {
            $src = $image->getAttribute('src');
            if (preg_match('/data:image/', $src)) {
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimeType = $groups['mime'];
                $path = uniqid('', true) . '.' . $mimeType;
                Storage::disk('news')->put($path, file_get_contents($src));
                $image->removeAttribute('src');
                $image->setAttribute('src', Storage::disk('news')->url('news/'.$path));
            }
        }
        $desc = $dom->savehtml();

        $request->merge([
            'description' => $desc,
            'company_id' => $company->id,
            'active' => 1,
            'is_public' => $request->has('is_public') ? 1 : 0,
            'skills' => json_encode($request->skills)
        ]);

        JobOffer::create($request->all());

        return redirect()->route('company.hr.index', ['company' => $company->id]);
    }

    public function delete(Company $company, JobOffer $job_offer)
    {
        $job_offer->delete();
        return redirect()->back();
    }

    public function people(Company $company, JobOffer $job_offer)
    {
        return view('company.hr.people', [
            'company' => $company,
            'job_offer' => $job_offer,
            'users' => $this->checkMatching($job_offer, UserResource::collection(User::where('looking_for_new_job', 1)->get())),
            'applier' => $this->checkMatching($job_offer, UserResource::collection(User::find(JobOfferApply::where('job_offer_id', $job_offer->id)->get()->pluck('user_id'))))
        ]);
    }

    public function people_detail(Company $company, JobOffer $job_offer, User $user)
    {
        return view('company.hr.people_detail', [
            'company' => $company,
            'job_offer' => $job_offer,
            'user' => $user
        ]);
    }

    private function checkMatching(JobOffer $job_offer, $users)
    {
        $retArray = [];
        foreach ($users as &$user) {
            $matching = 100;
            if (isset($user->profession)) {
                $matching += $job_offer->profession->name == $user->profession->profession->name ? 0 : -10;
            } else {
                $matching += -10;
            }
            if (isset($user->employment)) {
                $matching += $job_offer->employment->name == $user->employment->name ? 0 : -10;
            } else {
                $matching += -10;
            }
            if (isset($user->profession)) {
                $matching += $job_offer->min_experience <= $user->profession->experience ? 0 : -10;
            } else {
                $matching += -10;
            }

            $user_skills = [];
            foreach ($job_offer->skills_list() as $skill) {
                if (collect($user->skills->pluck('skill_id'))->contains($skill->id)) {
                    $user_skills[] = $skill->name;
                } else {
                    $matching = $matching - 5;
                }
            }

            $matching_props = 'success';
            if ($matching >= 80)
                $matching_props = 'success';
            elseif ($matching < 80 && $matching >= 60)
                $matching_props = 'warning';
            else
                continue;


            $retArray[] = [
                'user_id' => [
                    'value' => $user->id,
                    'props' => 'default'
                ],
                'user_name' => [
                    'value' => $user->name(),
                    'props' => 'default'
                ],
                'profession' => isset($user->profession) ? [
                    'value' => $user->profession->profession->name,
                    'props' => $job_offer->profession->name == $user->profession->profession->name ? 'success' : 'danger'
                ] : null,
                'employment' => isset($user->employment) ? [
                    'value' => $user->employment->employment->name,
                    'props' => $job_offer->employment->name == $user->employment->employment->name ? 'success' : 'danger'
                ] : null,
                'experience' => isset($user->profession) ? [
                    'value' => $user->profession->experience,
                    'props' => $job_offer->min_experience <= $user->profession->experience ? 'success' : 'danger'
                ] : null,
                'skills' => [
                    'value' => $user_skills,
                    'props' => 'warning'
                ],
                'matching' => [
                    'value' => $matching . '%',
                    'props' => $matching_props
                ]
            ];
        }

        return collect($retArray)->sortByDesc(function ($value, $key) {
            return $value['matching']['value'];
        });
    }
}
