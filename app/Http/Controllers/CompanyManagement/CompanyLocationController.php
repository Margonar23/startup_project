<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyPresentationPage;
use App\District;
use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyCreateEvent;
use App\Http\Requests\CompanyCreateLocation;
use App\Http\Requests\CompanyCreatePresentation;
use App\Http\Requests\CompanyUpdateLocation;
use App\Http\Requests\CompanyUpdatePresentation;
use App\Location;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DOMDocument;

class CompanyLocationController extends Controller
{

    public function index(Company $company)
    {
        return view('company.locations.index', [
            'company' => $company,
        ]);
    }

    public function create(Company $company)
    {
        return view('company.locations.create_edit', [
            'company' => $company,
            'state' => State::select('name')->where('name', 'Ticino')->first(),
            'districts' => District::orderBy('name')->get()
        ]);
    }

    public function edit(Company $company, Location $location)
    {
        return view('company.locations.create_edit', [
            'company' => $company,
            'location' => $location,
            'state' => State::select('name')->where('name', 'Ticino')->first(),
            'districts' => District::orderBy('name')->get()
        ]);
    }

    public function update(Company $company, Location $location, CompanyUpdateLocation $request)
    {
        $request->merge([
            'has_tables' => $request->has('has_tables') ? 1 : 0,
            'has_restaurant' => $request->has('has_restaurant') ? 1 : 0
        ]);
        $location->update($request->all());
        return redirect()->route('company.locations.index', ['company' => $company->id]);
    }

    public function store(CompanyCreateLocation $request, Company $company)
    {
        $request->merge([
            'company_id' => $company->id,
            'has_tables' => $request->has('has_tables') ? 1 : 0,
            'has_restaurant' => $request->has('has_restaurant') ? 1 : 0
        ]);
        Location::create($request->all());

        return redirect()->route('company.locations.index', ['company' => $company->id]);
    }

    public function delete(Company $company, Location $location)
    {
        $events = Event::where('location_id', $location->id)->get();
        foreach ($events as $event) {
            $event->update(['location_id' => null]);
        }
        $location->delete();
        return redirect()->back();
    }

}
