<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyCategory;
use App\CompanyClassification;
use App\CompanyStatus;
use App\CompanySubRole;
use App\CompanySubRolePermission;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyCreateSubRole;
use App\Http\Requests\CompanyCreateUser;
use App\Http\Requests\CompanyUpdateSubRole;
use App\Http\Requests\CompanyUpdateUser;
use App\Http\Requests\Ui\UiCreateCompanyRequest;
use App\Http\Requests\Ui\UiUpdateCompanyRequest;
use App\Notifications\NewEmployeeNotification;
use App\Package;
use App\Role;
use App\User;
use App\UserCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CompanyUserController extends Controller
{

    public function index(Company $company)
    {
        return view('company.users.index', [
            'company' => $company
        ]);
    }

    public function create(Company $company)
    {
        return view('company.users.create_edit', [
            'company' => $company
        ]);
    }

    public function edit(Company $company, User $user)
    {
        return view('company.users.create_edit', [
            'company' => $company,
            'user' => $user,
        ]);
    }

    public function update(Company $company, User $user, CompanyUpdateUser $request)
    {
        $user->update($request->all());
        $userCompany = UserCompany::where(['user_id' => $user->id, 'company_id' => $company->id])->first();
        $userCompany->update(['subrole_id' => $request->role]);
        return redirect()->route('company.users.index', ['company' => $company->id]);
    }

    public function store(CompanyCreateUser $request, Company $company)
    {
        $token = Str::random(60);
        $request->request->add(['password' => $token]);
        $request->request->add(['title_id' => 0]);
        $request->request->add(['role_id' => Role::where('label', 'company_user')->first()->id]);

        $user = User::create($request->all());
        UserCompany::create(['user_id' => $user->id, 'company_id' => $company->id, 'subrole_id' => $request->role]);

        try {
            $user->notify(new NewEmployeeNotification($token));
        }catch (\Exception $e){
            error_log($e->getMessage());
        }


        return redirect()->route('company.users.index', ['company' => $company->id]);
    }

    public function delete(Company $company, User $user)
    {
        $user->subrole->delete();
        $user->delete();

        return redirect()->back();
    }

}
