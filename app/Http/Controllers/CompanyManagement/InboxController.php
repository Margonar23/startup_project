<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyCategory;
use App\CompanyClassification;
use App\CompanyStatus;
use App\CompanySubRole;
use App\Employment;
use App\Http\Controllers\Controller;
use App\Http\Requests\JobOfferCreateRequest;
use App\Http\Requests\JobOfferUpdateRequest;
use App\Http\Requests\Ui\UiCreateCompanyRequest;
use App\Http\Requests\Ui\UiUpdateCompanyRequest;
use App\Http\Resources\UserResource;
use App\JobOffer;
use App\JobOfferApply;
use App\Package;
use App\Profession;
use App\Skill;
use App\User;
use App\UserCompany;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use function foo\func;

class InboxController extends Controller
{
    public function index(Company $company)
    {
        return view('company.company.inbox');
    }

    public function mark_all_as_read()
    {
        Auth::user()->unreadNotifications->markAsRead();
        return response()->json(Auth::user()->notifications);
    }

    public function mark_as_read(Request $request)
    {
        $notifications = Auth::user()->notifications->whereIn('id', $request->notifications);

        foreach ($notifications as $notification){
            is_null($notification->read_at) ? $notification->markAsRead() : $notification->markAsUnread();
        }

        return response()->json(Auth::user()->notifications);
    }
    public function delete(Request $request)
    {
        Auth::user()->notifications->where('id', $request->notification)->first()->delete();

        return response()->json(Auth::user()->notifications);
    }

}
