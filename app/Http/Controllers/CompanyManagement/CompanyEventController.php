<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyPresentationPage;
use App\Event;
use App\EventRecurrentDay;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyCreateEvent;
use App\Http\Requests\CompanyCreatePresentation;
use App\Http\Requests\CompanyUpdateEvent;
use App\Http\Requests\CompanyUpdatePresentation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DOMDocument;

class CompanyEventController extends Controller
{

    public function index(Company $company)
    {
        return view('company.events.index', [
            'company' => $company,
        ]);
    }

    public function create(Company $company)
    {
        return view('company.events.create_edit', ['company' => $company]);
    }

    public function edit(Company $company, Event $event)
    {
        return view('company.events.create_edit', [
            'company' => $company,
            'event' => $event
        ]);
    }

    public function update(Company $company, Event $event, CompanyUpdateEvent $request)
    {
        if ($request->has('file')){
            $file = $request->file('file')->store(null, 'events');
            $request->merge(['cover' =>  'storage/events/' . $file]);
        }

        $request->merge([
            'with_reservation' => $request->has('with_reservation') ? 1 : 0,
            'reservation_end_at' => $request->has('with_reservation') ? Carbon::parse($request->reservation_end_at)->format('Y-m-d H:i:s') : null,
            'start_time' => Carbon::parse($request->start_time)->format('Y-m-d H:i:s'),
            'end_time' => Carbon::parse($request->end_time)->format('Y-m-d H:i:s'),
            'company_id' => $company->id
        ]);

        $event->update($request->all());

        return redirect()->route('company.events.index', ['company' => $company->id]);
    }

    public function store(CompanyCreateEvent $request, Company $company)
    {
        if ($request->has('file')){
            $file = $request->file('file')->store(null, 'events');
            $request->merge(['cover' =>  'storage/events/' . $file]);
        }
        $request->merge([
            'with_reservation' => $request->has('with_reservation') ? 1 : 0,
            'reservation_end_at' => $request->has('with_reservation') && $request->has('reservation_end_at') ? Carbon::parse($request->reservation_end_at)->format('Y-m-d H:i:s') : null,
            'start_time' => Carbon::parse($request->start_time)->format('Y-m-d H:i:s'),
            'end_time' => Carbon::parse($request->end_time)->format('Y-m-d H:i:s'),
            'company_id' => $company->id,
        ]);

        $event = Event::create($request->all());

        return redirect()->route('company.events.index', ['company' => $company->id]);
    }

    public function delete(Company $company, Event $event)
    {
        $event->delete();
        return redirect()->back();
    }

    public function participants(Company $company, Event $event)
    {
        return view('company.events.participants', ['company' => $company, 'event' => $event]);
    }

}
