<?php

namespace App\Http\Controllers\CompanyManagement;

use App\Company;
use App\CompanyPackage;
use App\CompanyPaymentInformation;
use App\CompanyStatus;
use App\CompanySubRole;
use App\CompanySubRolePermission;
use App\Http\Controllers\Controller;
use App\Http\Requests\Subscription\CreateSubscriptionRequest;
use App\Package;
use App\PaymentMethod;
use App\PaymentType;
use App\UserCompany;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Exceptions\IncompletePayment;
use Stripe\Customer;
use Stripe\Stripe;

class CompanySubscriptionController extends Controller
{

    public function subscription(Company $company, Package $package)
    {
        return view('company.company.packages.manage_package_subscriptions', [
            'package' => $package,
            'company' => $company,
            'payment_methods' => PaymentMethod::where('active', 1)->get(),
        ]);
    }

    public function store_subscription_invoice(CreateSubscriptionRequest $request, Company $company, Package $package)
    {
        try {
            $values = [
                'company_id' => $company->id,
                'package_id' => $package->id,
                'payment_method_id' => $request->payment_method_id,
                'payment_type_id' => $request->payment_type_id,
                'last_payment_at' => Carbon::now(),
                'subscription_expiry_at' => Carbon::now()->addMonths(PaymentType::find($request->payment_type_id)->month),
                'active' => true
            ];
            $company_package = CompanyPackage::create($values);
            CompanyPaymentInformation::create([
                'company_package_id' => $company_package->id,
                'name' => $request->username,
                'email' => $request->email,
                'address' => $request->route . ', ' . $request->cap . ', ' . $request->city . ', ' . $request->country,
                'coupon' => $request->couponCode
            ]);
            $company->update(['status_id' => CompanyStatus::where('label', 'sub')->first()->id]);

            $subrole = CompanySubRole::create([
                'name' => 'Adminstrator',
                'label' => 'administrator',
                'company_id' => $company->id
            ]);
            foreach ($package->features as $feature) {
                foreach ($feature->permissions as $permission) {
                    CompanySubRolePermission::create([
                        'company_sub_role_id' => $subrole->id,
                        'permission_id' => $permission->id,
                    ]);
                }
            }

            $user_company = UserCompany::where(['user_id' => Auth::id(), 'company_id' => $company->id])->first();
            $user_company->subrole_id = $subrole->id;
            $user_company->update();

            Stripe::setApiKey(env('STRIPE_SECRET'));
            $user = $request->user();
            if (is_null($user->stripe_id))
                $stripeCustomer = $user->createAsStripeCustomer();

//            Customer::createSource(
//                $user->stripe_id,
//                ['source' => $request->stripeToken]
//            );

            $user->newSubscription($package->name, $package->get_stripe_id($request->payment_type_id))
                ->trialDays(60)
                ->create($request->paymentMethod, ['email' => $user->email]);

            return redirect()->route('company.dashboard');
        } catch (Exception $exception) {
            return back()->withErrors(['alert-danger' => $exception->getMessage()]);
        }

    }

    public function store_subscription_card(Request $request, Company $company, Package $package)
    {
        try {
            $values = [
                'company_id' => $company->id,
                'package_id' => $package->id,
                'payment_method_id' => $request->payment_method_id,
                'payment_type_id' => $request->payment_type_id,
                'last_payment_at' => Carbon::now(),
                'subscription_expiry_at' => Carbon::now()->addMonths(PaymentType::find($request->payment_type_id)->month),
                'active' => true
            ];
            $company_package = CompanyPackage::create($values);
            CompanyPaymentInformation::create([
                'company_package_id' => $company_package->id,
                'name' => $request->username,
                'email' => $request->email,
                'address' => $request->route . ', ' . $request->cap . ', ' . $request->city . ', ' . $request->country,
                'coupon' => $request->couponCode
            ]);
            $company->update(['status_id' => CompanyStatus::where('label', 'sub')->first()->id]);

            $subrole = CompanySubRole::create([
                'name' => 'Adminstrator',
                'label' => 'administrator',
                'company_id' => $company->id
            ]);
            foreach ($package->features as $feature) {
                foreach ($feature->permissions as $permission) {
                    CompanySubRolePermission::create([
                        'company_sub_role_id' => $subrole->id,
                        'permission_id' => $permission->id,
                    ]);
                }
            }

            $user_company = UserCompany::where(['user_id' => Auth::id(), 'company_id' => $company->id])->first();
            $user_company->subrole_id = $subrole->id;
            $user_company->update();

            Stripe::setApiKey(env('STRIPE_SECRET'));
            $user = $request->user();

            if (is_null($user->stripe_id))
                $stripeCustomer = $user->createAsStripeCustomer();

            Customer::createSource(
                $user->stripe_id,
                ['source' => $request->stripeToken]
            );

            $user->newSubscription($package->name, $package->get_stripe_id($request->payment_type_id))
                    ->trialDays(60)
                ->create($request->paymentMethod, ['email' => $user->email]);

            return redirect()->route('company.dashboard');
        } catch (IncompletePayment $exception) {
            return redirect()->route(
                'cashier.payment',
                [$exception->payment->id, 'redirect' => route('company.dashboard')]
            );
        } catch (Exception $e) {
            return back()->withErrors(['alert-danger' => $e->getMessage()]);
        }
    }

    public function download_invoice(Request $request, $invoice)
    {
        return $request->user()->downloadInvoice($invoice, [
            'vendor' => env('COMPANY_NAME'),
            'product' => 'Subscription',
        ]);
    }

}
