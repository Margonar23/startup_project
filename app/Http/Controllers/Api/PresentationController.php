<?php

namespace App\Http\Controllers\Api;

use App\AlimentaryType;
use App\Booking;
use App\Company;
use App\CompanyCategory;
use App\CompanyPresentationPage;
use App\CompanyStatus;
use App\DayHour;
use App\Http\Controllers\Controller;
use App\Http\Resources\AlimentaryTypeResource;
use App\Http\Resources\BookingResource;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\DayHourResource;
use App\Http\Resources\KitchenTypeResource;
use App\Http\Resources\LocationResource;
use App\Http\Resources\PageContentResource;
use App\Http\Resources\ReservationStatusResource;
use App\Http\Resources\RestaurantResource;
use App\Http\Resources\RestaurantTypeResource;
use App\Http\Resources\RoomResource;
use App\KitchenType;
use App\Location;
use App\LocationRoom;
use App\PresentationPageTemplateContent;
use App\ReservationStatus;
use App\RestaurantType;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;

class PresentationController extends Controller
{
    public function get_content(CompanyPresentationPage $page)
    {
        $content = isset($page->page_content) ? PageContentResource::make($page->page_content) : null;
        return response()->json($content);
    }

    public function save_content(Request $request, CompanyPresentationPage $page)
    {
        if ($request->has('file')) {
            $file = $request->file('file')->store($page->company->id . '/presentation', 'company');
            $request->merge(['img' => 'storage/company/' . $file]);

        }
        $request->merge(['page_id' => $page->id]);

        if (isset($page->page_content)) {
            $content = $page->page_content()->update($request->except('file'));
        } else {
            $content = PresentationPageTemplateContent::create($request->except('file'));
        }

        return response()->json(PageContentResource::make($content));
    }
}
