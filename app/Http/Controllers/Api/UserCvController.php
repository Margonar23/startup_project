<?php

namespace App\Http\Controllers\Api;


use App\Employment;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Language;
use App\LanguageLevel;
use App\License;
use App\Percentage;
use App\Permit;
use App\Profession;
use App\Skill;
use App\User;
use App\UserCertificate;
use App\UserCv;
use App\UserDiploma;
use App\UserEmployment;
use App\UserHobby;
use App\UserJobExperience;
use App\UserLanguage;
use App\UserLicense;
use App\UserPercentage;
use App\UserPermit;
use App\UserProfession;
use App\UserSkill;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UserCvController extends Controller
{
    public function user(User $user)
    {
        return response()->json(new UserResource($user));
    }

    public function get_professions()
    {
        return response()->json(Profession::orderBy('name')->pluck('name'));
    }

    public function get_permits()
    {
        return response()->json(Permit::orderBy('name')->pluck('name'));
    }

    public function get_percentages()
    {
        return response()->json(Percentage::orderBy('percentage')->pluck('percentage'));
    }
    public function get_languages()
    {
        return response()->json(Language::orderBy('name')->pluck('name'));
    }
    public function get_levels()
    {
        return response()->json(LanguageLevel::pluck('name'));
    }
    public function get_employments()
    {
        return response()->json(Employment::orderBy('name')->pluck('name'));
    }

    public function get_skills()
    {
        return response()->json(Skill::orderBy('name')->pluck('name'));
    }

    public function get_licenses()
    {
        return response()->json(License::orderBy('name')->pluck('name'));
    }

    public function store_user_profession(Request $request)
    {
        $profession = Profession::where('name', $request->profession)->first();
        $user_profession = UserProfession::where('user_id', $request->user)->first();

        if ($request->has('experience')) {
            $user_profession = $user_profession->update(['experience' => $request->experience]);
        } else {
            if (!is_null($user_profession)) {
                $user_profession = $user_profession->update(['profession_id' => $profession->id, 'experience' => 0]);
            } else {
                $user_profession = UserProfession::create([
                    'user_id' => $request->user,
                    'profession_id' => $profession->id,
                    'experience' => 0
                ]);
            }
        }
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_employment(Request $request)
    {
        $employment = Employment::where('name', $request->employment)->first();
        $user_employment = UserEmployment::where('user_id', $request->user)->first();

        if (!is_null($user_employment)) {
            $user_employment = $user_employment->update(['employment_id' => $employment->id]);
        } else {
            $user_employment = UserEmployment::create([
                'user_id' => $request->user,
                'employment_id' => $employment->id
            ]);
        }
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_skills(Request $request)
    {
        UserSkill::where('user_id', $request->user)->delete();
        foreach ($request->skills as $skill) {
            if (is_null(Skill::where('name', $skill)->first()))
                Skill::create(['name' => $skill, 'label' => Str::slug($skill)]);
            UserSkill::create(['user_id' => $request->user, 'skill_id' => Skill::where('name', $skill)->first()->id]);
        }
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_licenses(Request $request)
    {
        UserLicense::where('user_id', $request->user)->delete();
        foreach ($request->licenses as $license) {
            UserLicense::create(['user_id' => $request->user, 'license_id' => License::where('name', $license)->first()->id]);
        }
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_permit(Request $request)
    {
        UserPermit::where('user_id', $request->user)->delete();
        if ($request->permit != '' || !is_null($request->permit) || isset($request->permit))
            UserPermit::create(['user_id' => $request->user, 'permit_id' => Permit::where('name', $request->permit)->first()->id]);
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_percentage(Request $request)
    {
        UserPercentage::where('user_id', $request->user)->delete();
        if ($request->percentage != '' || !is_null($request->percentage) || isset($request->percentage))
            UserPercentage::create(['user_id' => $request->user, 'percentage_id' => Percentage::where('percentage', $request->percentage)->first()->id]);
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_languages(Request $request)
    {
        UserLanguage::where('user_id', $request->user)->delete();
        foreach ($request->lang as $language) {
            UserLanguage::create([
                'user_id' => $request->user,
                'language_id' => Language::where('name', $language[0])->first()->id,
                'level_id' => LanguageLevel::where('name', $language[1])->first()->id,
            ]);
        }
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_hobbies(Request $request)
    {
        UserHobby::where('user_id', $request->user)->delete();
        foreach ($request->hobbies as $hobby) {
            UserHobby::create(['user_id' => $request->user, 'hobby' => $hobby]);
        }
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_certificate(Request $request)
    {
        $certificate = $request->certificate;
        $certificate['user_id'] = $request->user;
        $certificate['acquired_at'] = Carbon::createFromFormat('d.m.Y', $certificate['acquired_at'])->format('d.m.Y');
        $certificate['expiry_at'] = Carbon::createFromFormat('d.m.Y', $certificate['expiry_at'])->format('d.m.Y');
        UserCertificate::create($certificate);
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function delete_user_certificate(Request $request)
    {
        UserCertificate::find($request->certificate)->delete();
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_diploma(Request $request)
    {
        $diploma = $request->diploma;
        $diploma['user_id'] = $request->user;
        $diploma['begin_at'] = Carbon::createFromFormat('d.m.Y', $diploma['begin_at'])->format('d.m.Y');
        $diploma['end_at'] = Carbon::createFromFormat('d.m.Y', $diploma['end_at'])->format('d.m.Y');
        UserDiploma::create($diploma);
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function delete_user_diploma(Request $request)
    {
        UserDiploma::find($request->diploma)->delete();
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_job_experience(Request $request)
    {
        $job_experience = $request->job_experience;
        $job_experience['user_id'] = $request->user;
        $job_experience['begin_at'] = Carbon::createFromFormat('d.m.Y', $job_experience['begin_at'])->format('d.m.Y');
        $job_experience['end_at'] = Carbon::createFromFormat('d.m.Y', $job_experience['end_at'])->format('d.m.Y');
        UserJobExperience::create($job_experience);
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function delete_user_job_experience(Request $request)
    {
        UserJobExperience::find($request->job_experience)->delete();
        return response()->json(new UserResource(User::find($request->user)));
    }

    public function store_user_looking_for_new_job(Request $request)
    {
        $user = User::find($request->user);
        $user->looking_for_new_job = !$user->looking_for_new_job;
        $user->update();
        return response()->json(new UserResource($user));
    }

    public function user_cv_upload(Request $request, User $user)
    {
        UserCv::where('user_id', $user->id)->delete();
        $file = $request->file('file')->store(null, 'user_cv');
        UserCv::create(['user_id' => $user->id, 'cv_path' => 'user_cv/' . $file]);

        return response()->json(new UserResource($user));
    }

}
