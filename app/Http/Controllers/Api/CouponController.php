<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\CompanyCategory;
use App\CompanyClassification;
use App\CompanyStatus;
use App\Coupon;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\CouponResource;
use App\Rating;
use App\UserCompanyFollower;
use App\UserCompanyRating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CouponController extends Controller
{
    public function get_coupon_companies()
    {
        return response()->json(
            CouponResource::collection(
                Coupon::orderBy('name')->get()
            )
        );
    }

    public function get_coupon_categories()
    {
        return response()->json(CompanyCategory::all()->pluck('name'));
    }

    public function get_coupon_districts()
    {
        return response()->json(District::all()->pluck('name'));
    }
}
