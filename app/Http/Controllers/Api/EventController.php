<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\CompanyCategory;
use App\CompanyStatus;
use App\Event;
use App\EventParticipant;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\EventResource;
use App\Notifications\NewEventPartecipant;
use App\Notifications\NewEventPartecipantMail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function events()
    {
        return response()->json(
            EventResource::collection(
                Event::orderBy('start_time', 'desc')->get()
            )
        );
    }

    public function event(Event $event)
    {
        return response()->json(new EventResource($event));
    }

    public function participation(Event $event, User $user)
    {
        EventParticipant::create(['event_id' => $event->id, 'user_id' => $user->id]);
        try {
            $user = $event->company->user;
            $user->notify(new NewEventPartecipant($event, $user));
            $user->notify(new NewEventPartecipantMail($event, $user));
        } catch (\Exception $exception) {
            error_log($exception->getMessage());
        }
        return response()->json(new EventResource($event));
    }

    public function delete_participation(Event $event, User $user)
    {
        $ep = EventParticipant::where(['event_id' => $event->id, 'user_id' => $user->id])->first();
        $ep->delete();

        return response()->json(new EventResource($event));
    }

}
