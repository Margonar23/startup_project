<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function test_auth()
    {
        return response()->json([
            'success' => true,
            'message' => 'Access to auth zone',
        ], 200);
    }
}
