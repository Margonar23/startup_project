<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PackageResource;
use App\Package;

class PackageController extends Controller
{
    public function get_packages()
    {
        return PackageResource::collection(Package::where('active', 1)->get());
    }
}
