<?php

namespace App\Http\Controllers\Api;

use App\Feature;
use App\Http\Controllers\Controller;
use App\Http\Resources\FeatureResource;

class FeatureController extends Controller
{
    public function get_features()
    {
        return FeatureResource::collection(Feature::where('active', 1)->get());
    }
}
