<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\CompanyCategory;
use App\CompanyClassification;
use App\CompanyStatus;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Rating;
use App\UserCompanyFollower;
use App\UserCompanyRating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function foo\func;

class CompanyController extends Controller
{
    public function companies()
    {
        return response()->json(
            CompanyResource::collection(
                Company::where('status_id', CompanyStatus::where('active', 1)->first()->id)->orderBy('name')->get()
            )
        );
    }

    public function company(Company $company)
    {
        return response()->json(CompanyResource::make($company));
    }

    public function companies_filter(Request $request)
    {
        $companies = Company::query();
        $companies->where('status_id', CompanyStatus::where('active', 1)->first()->id);
        $companies->when($request->catFilter != null, function ($q) use ($request) {
            return $q->where('category_id', CompanyCategory::where('name', $request->catFilter)->first()->id);
        });
        $companies->when($request->filter != null, function ($q) use ($request) {
            return $q->where('name', 'LIKE', '%' . $request->filter . '%');
        });
        $companies->when($request->district != null, function ($q) use ($request) {
            return $q->where('district_id', District::where('name', $request->district)->first()->id);
        });

        return response()->json(CompanyResource::collection($companies->orderBy('name')->get()));
    }

    public function company_categories()
    {
        return response()->json(CompanyCategory::all()->pluck('name'));
    }

    public function company_districts()
    {
        return response()->json(District::all()->pluck('name'));
    }

    public function company_classifications()
    {
        return response()->json(CompanyClassification::all()->pluck('name'));
    }

    public function edit(Request $request, Company $company)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required',
        ]);

        $request->merge([
            'category_id' => CompanyCategory::where('name', $request->category)->first()->id,
            'classification_id' => CompanyClassification::where('name', $request->classification)->first()->id,
            'district_id' => District::where('name', $request->district)->first()->id,
        ]);

        $company->update($request->all());

        return;
    }

    public function edit_logo(Request $request, Company $company)
    {
        $file = $request->file('file')->store($company->id . '/logo', 'company');

        $company->update(['logo' => 'storage/company/' . $file]);

        return;
    }

    public function ratings(Request $request)
    {
        $rating = UserCompanyRating::where([
            'user_id' => $request->user,
            'company_id' => $request->company
        ])->first();

        if (empty($rating)) {
            return response()->json(['rating' => Rating::orderBy('rate')->get(), 'rate' => 0]);
        } else {
            return response()->json(['rating' => Rating::orderBy('rate')->get(), 'rate' => $rating->rating->rate]);
        }

    }

    public function rate_company(Request $request)
    {
        UserCompanyRating::where(['user_id' => $request->user, 'company_id' => $request->company])->delete();
        $rating = UserCompanyRating::create(['user_id' => $request->user, 'company_id' => $request->company, 'rating_id' => $request->rating]);
        $this->updateCompanyAverage($rating->company);
        return response()->json($rating);
    }

    public function follow_company(Request $request)
    {
        UserCompanyFollower::create(['user_id' => $request->user, 'company_id' => $request->company]);
        return response()->json();
    }

    public function unfollow_company(Request $request)
    {
        UserCompanyFollower::where(['user_id' => $request->user, 'company_id' => $request->company])->delete();
        return response()->json();
    }

    public function updateCompanyAverage(Company $company)
    {
        /**
         *** imdb rating ***
         *  (WR) = (v ÷ (v+m)) × R + (m ÷ (v+m)) × C
         *  R = average for the movie (mean) = (Rating)
         *  v = number of votes for the movie = (votes)
         *  m = minimum votes required to be listed in the Top 250 (currently 25000)
         *  C = the mean vote across the whole report (currently 7.1)
         **/
        $R = $company->ratings()->avg('rating_id');
        $v = $company->ratings()->count();
        $m = 10;
        $C = 3.8;

        $wr = ($v / ($v+$m)) * $R + ($m / ($v+$m)) * $C;
        $company->update(['avg_rating' => $wr]);
    }
}
