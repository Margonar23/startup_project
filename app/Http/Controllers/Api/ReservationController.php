<?php

namespace App\Http\Controllers\Api;

use App\AlimentaryType;
use App\Booking;
use App\BookingReserved;
use App\Company;
use App\CompanyCategory;
use App\CompanyStatus;
use App\DayHour;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Resources\AlimentaryTypeResource;
use App\Http\Resources\BookingResource;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\DayHourResource;
use App\Http\Resources\DistrictResource;
use App\Http\Resources\KitchenTypeResource;
use App\Http\Resources\LocationResource;
use App\Http\Resources\ReservationStatusResource;
use App\Http\Resources\RestaurantResource;
use App\Http\Resources\RestaurantTypeResource;
use App\Http\Resources\RoomResource;
use App\KitchenType;
use App\Location;
use App\LocationRoom;
use App\ReservationStatus;
use App\RestaurantType;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;

class ReservationController extends Controller
{
    public function alimentaries()
    {
        return response()->json(AlimentaryTypeResource::collection(AlimentaryType::orderBy('name')->get()));
    }

    public function restaurant_types()
    {
        return response()->json(RestaurantTypeResource::collection(RestaurantType::orderBy('name')->get()));
    }

    public function kitchens()
    {
        return response()->json(KitchenTypeResource::collection(KitchenType::orderBy('name')->get()));
    }

    public function districts()
    {
        return response()->json(DistrictResource::collection(District::orderBy('name')->get()));
    }

    public function get_restaurants(Request $request)
    {
        $locations = RestaurantResource::collection(Location::where('has_restaurant', 1)->get());
        /** get filters */
        $searchBar = $request->searchbar;
        $dateFilter = $request->datefilter;
        $timeFilter = $request->timefilter;
        $alimentaries = $request->alimentaries;
        $restaurantTypes = $request->restauranttypes;
        $kitchens = $request->kitchens;
        $district = $request->district;

        /** apply filters */
        // restaurant type
        if ($district != 0) {
            $locations = $locations->flatten()->filter(function ($location, $key) use ($district) {
                return $location->district_id == $district;
            });
        }

        // searchbar
        if ($searchBar) {
            $locations = $locations->flatten()->filter(function ($value, $key) use ($searchBar) {
                return Str::contains(strtolower($value->name), strtolower($searchBar));
            });
        }

        // kitchen
        if ($kitchens) {
            $locations = $locations->flatten()->filter(function ($location, $key) use ($kitchens) {
                return in_array($location->restaurant_info->kitchen_type_id, $kitchens);
            });
        }

        // restaurant type
        if ($restaurantTypes) {
            $locations = $locations->flatten()->filter(function ($location, $key) use ($restaurantTypes) {
                return in_array($location->restaurant_info->restaurant_type_id, $restaurantTypes);
            });
        }

        // alimentaries type
        if ($alimentaries) {
            $locations = $locations->flatten()->filter(function ($location, $key) use ($alimentaries) {
                return count(array_intersect(json_decode($location->restaurant_info->alimentary_type, true), $alimentaries));
            });
        }

        // check if open
        $filterDay = empty($dateFilter) ? null : Carbon::createFromFormat('Y-m-d', $dateFilter)->dayOfWeek;
        $filterTime = empty($timeFilter) ? null : Carbon::createFromFormat('H:i', $timeFilter);

        if ($filterDay) {
            $locations = $locations->flatten()->filter(function ($location, $key) use ($filterDay) {
                return $location->restaurant->contains('day_id', $filterDay)
                    && !is_null($location->restaurant->where('day_id', $filterDay)->first()->opening_at);
            });
        }

        if ($filterTime && $filterDay) {
            $locations = $locations->flatten()->filter(function ($location, $key) use ($filterDay, $filterTime) {
                return $filterTime->between(
                    Carbon::createFromFormat('H:i:s', $location->restaurant->where('day_id', $filterDay)->first()->opening_at),
                    Carbon::createFromFormat('H:i:s', $location->restaurant->where('day_id', $filterDay)->first()->closing_at)
                );
            });
        }


        return response()->json(RestaurantResource::collection($locations));
    }

    public function get_restaurant(Location $location)
    {
        return response()->json(LocationResource::make($location));
    }

    public function get_room(LocationRoom $room)
    {
        return response()->json(RoomResource::make($room));
    }

    public function get_reservation_status()
    {
        return response()->json(ReservationStatusResource::collection(ReservationStatus::all()));
    }

    public function get_bookings(ReservationStatus $status, Location $location)
    {
        return response()->json(
            BookingResource::collection(
                Booking::where([
                    'reservation_status_id' => $status->id,
                    'location_id' => $location->id,
                ])->get()
            )
        );
    }

    public function get_hours(Location $restaurant, $date)
    {
        $day = Carbon::createFromFormat('d-m-Y', $date)->dayOfWeek;
        $hours = collect(DayHour::all()->pluck('hour'));
        $openingAt = null;
        $closingAt = null;
        foreach ($restaurant->restaurant as $openingHour) {
            if ($openingHour->day_id == $day && !is_null($openingHour->opening_at)) {
                $openingAt = $openingHour->opening_at;
                $closingAt = $openingHour->closing_at;
            }
        }

        if (!is_null($openingAt) && !is_null($closingAt)) {
            $hours = $hours->flatten()->filter(function ($hour, $key) use ($openingAt, $closingAt) {
                return Carbon::createFromFormat('H:i:s', $hour)->betweenIncluded(
                    Carbon::createFromFormat('H:i:s', $openingAt),
                    Carbon::createFromFormat('H:i:s', $closingAt)
                );
            });

            $hours = $hours->map(function ($item, $key) {
                return Carbon::createFromFormat('H:i:s', $item)->format('H:i');
            });
            return response()->json($hours);
        } else {
            return response()->json(null);
        }


    }

    public function assign_table(Request $request)
    {
        $booking = Booking::find($request->booking);
        if ($booking->reservation_status->label == 'request' || $booking->reservation_status->label == 'rejected') {
            $booking->reservation_status_id = ReservationStatus::where('label', 'confirmed')->first()->id;

            BookingReserved::create([
                'booking_id' => $booking->id,
                'user_id' => $booking->auth_user_id,
                'table_id' => $request->table,
                'reserved_start' => $booking->booking_datetime,
                'reserved_stop' => Carbon::parse($booking->booking_datetime)->addHours(2)
            ]);

            $booking->update();
        } else {
            //  booking reservation
            $reservation = BookingReserved::where('booking_id', $booking->id)->first();
            $reservation->table_id = $request->table;
            $reservation->update();
        }


        return \response()->json();
    }

    public function reject_reservation(Request $request)
    {
        $booking = Booking::find($request->booking);
        $booking->reservation_status_id = ReservationStatus::where('label', 'rejected')->first()->id;
        $booking->update();

        if (isset($booking->reserved))
            BookingReserved::where('booking_id', $booking->id)->delete();


        return \response()->json();
    }

    public function get_tables(Request $request)
    {
        $date = $request->date;
        $hour = $request->hour;
        $room = $request->room;

        $date = Carbon::createFromFormat('d-m-Y H:i', $date . ' ' . $hour)->format('Y-m-d H:i:s');

        $response = [];

        error_log($date);

        foreach (LocationRoom::find($room)->tables as $table) {
            $response[] = [
                'table' => $table,
                'reserved' => $table->is_reserved($date)
            ];
        }


        return \response()->json($response);
    }

}
