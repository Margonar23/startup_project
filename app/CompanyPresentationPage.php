<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPresentationPage extends Model
{
    protected $fillable = ['page_name', 'label', 'active', 'company_id', 'template_id'];

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function template()
    {
        return $this->belongsTo('App\PresentationTemplate', 'template_id');
    }

    public function page_content()
    {
        return $this->hasOne('App\PresentationPageTemplateContent', 'page_id');
    }
}
