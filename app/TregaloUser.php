<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TregaloUser extends Model
{
    protected $fillable = ['tregalo_id', 'qty', 'order_id'];

    public function tregalo()
    {
        return $this->belongsTo('App\Tregalo' , 'tregalo_id');
    }
    public function order()
    {
        return $this->belongsTo('App\TregaloOrder' , 'order_id');
    }
}
