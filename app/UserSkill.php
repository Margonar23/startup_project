<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSkill extends Model
{
    protected $fillable = ['user_id', 'skill_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function skill()
    {
        return $this->belongsTo('App\Skill', 'skill_id');
    }
}
