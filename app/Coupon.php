<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{

    protected $fillable = ['name', 'logo', 'category_id', 'link', 'address', 'district_id'];

    public function category()
    {
        return $this->belongsTo('App\CompanyCategory', 'category_id');
    }

    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }
}
