<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySubRolePermission extends Model
{
    protected $fillable = ['company_sub_role_id', 'permission_id'];

    public function permission()
    {
        return $this->belongsTo('App\Permission', 'permission_id');
    }
}
