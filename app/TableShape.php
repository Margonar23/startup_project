<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableShape extends Model
{
    protected $fillable = ['name'];
}
