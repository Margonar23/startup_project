<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);

Route::group(['namespace' => 'Frontend'], function () {
    // home page
    Route::get('/', 'RoutingController@index')->name('home');


    // user management
    Route::get('/users/registration/{step}', 'User\AuthenticationController@registration')->name('users.registration');
    Route::post('/users/registration', 'User\AuthenticationController@register')->name('users.register');
    Route::get('/users/login', 'User\AuthenticationController@login')->name('users.login');
    Route::get('/users/forgot_password', 'User\AuthenticationController@forgot_password')->name('users.forgot_password');
    Route::post('/users/forgot_password/send', 'User\AuthenticationController@forgot_password_send')->name('users.forgot_password_send');
    Route::post('/users/reset_password', 'User\AuthenticationController@reset_password')->name('users.reset_password');
    Route::get('/users/create_password/{token}', 'User\AuthenticationController@create_password')->name('users.create_password');
    Route::post('/users/create_password', 'User\AuthenticationController@store_password')->name('users.store_password');

    // pages
    Route::get('/packages', 'RoutingController@packages')->name('packages');
    Route::get('/news', 'RoutingController@news')->name('news');
    Route::get('/news/{news}', 'RoutingController@news_detail')->name('news.detail');
    Route::get('/companies', 'RoutingController@companies')->name('companies');
    Route::get('/companies/{company}', 'RoutingController@company_detail')->name('companies.detail');
    Route::get('/events', 'RoutingController@events')->name('events');
    Route::get('/events/{event}', 'RoutingController@event_detail')->name('events.detail');
    Route::get('/job_offers', 'RoutingController@job_offers')->name('job_offers');
    Route::get('/job_offer/{job_offer}', 'RoutingController@job_offer_detail')->name('job_offer_detail');
    Route::get('/job_offer/{job_offer}/apply', 'RoutingController@job_offer_apply')->name('job_offer.apply');
    Route::post('/job_offer/{job_offer}/user_apply', 'RoutingController@job_offer_user_apply')->name('job_offer.user_apply');
    Route::get('/reservations', 'RoutingController@reservations')->name('reservations');
    Route::get('/booking/{location}/{date?}/{time?}', 'RoutingController@booking')->name('reservations.booking');
    Route::post('/booking/{location}', 'RoutingController@booking_store')->name('reservations.booking.store');
    Route::get('/booking_success/{location}/{booking}', 'RoutingController@booking_success')->name('reservations.booking.success');
    Route::get('/contacts', 'RoutingController@contacts')->name('contacts');
    Route::post('/contacts/store', 'RoutingController@contacts_store')->name('contacts.store');
    Route::get('/about', 'RoutingController@about')->name('about');
    Route::get('/coupon', 'RoutingController@coupon')->name('coupon');
    Route::get('/t_regalo', 'RoutingController@t_regalo')->name('tRegalo');
    Route::get('/t_regalo/buy', 'RoutingController@buy_t_regalo')->name('buy_t_regalo');
    Route::post('/t_regalo/buy', 'RoutingController@store_buy_t_regalo')->name('store_buy_t_regalo');

    // legal
    Route::get('/privacy_policy', 'RoutingController@privacy_policy')->name('privacy_policy');
    Route::get('/terms_and_conditions', 'RoutingController@terms_and_conditions')->name('terms_and_conditions');
    Route::get('/impressum', 'RoutingController@impressum')->name('impressum');



    Route::group(['middleware' => 'auth', 'prefix' => 'fx'], function () {
        // user profile
        Route::get('/users/profile/{edit?}', 'User\ProfileController@profile')->name('users.profile');
        Route::post('/users/delete/{user}', 'User\ProfileController@delete')->name('users.delete');
        Route::get('/users/tregalo', 'User\ProfileController@tregalo')->name('users.tregalo');
        Route::post('/users/profile/address/update', 'User\ProfileController@update_address')->name('users.update_address');
        Route::post('/users/profile/password/update', 'User\ProfileController@update_password')->name('users.update_password');
        Route::get('/users/cv', 'User\ProfileController@cv')->name('users.cv');
        Route::get('/users/cv/edit', 'User\ProfileController@cv_edit')->name('users.cv_edit');
        Route::post('/users/profile/update_profile', 'User\ProfileController@update_profile')->name('users.update_profile');
        Route::post('/users/profile/edit_avatar', 'User\ProfileController@edit_avatar')->name('users.edit_avatar');
        Route::get('/cv/add_widget', 'User\ProfileController@add_widget')->name('cv.add_widget');
        Route::post('/cv/edit_widget', 'User\ProfileController@edit_widget');
        Route::post('/cv/delete_widget', 'User\ProfileController@delete_widget');
        Route::get('/cv/add_social_widget', 'User\ProfileController@add_social_widget')->name('cv.add_social_widget');
        Route::post('/cv/edit_social_widget', 'User\ProfileController@edit_social_widget');
        Route::post('/cv/delete_social_widget', 'User\ProfileController@delete_social_widget');
        Route::get('/cv/add_experience_widget', 'User\ProfileController@add_experience_widget')->name('cv.add_experience_widget');
        Route::post('/cv/edit_experience_widget', 'User\ProfileController@edit_experience_widget');
        Route::post('/cv/delete_experience_widget', 'User\ProfileController@delete_experience_widget');
        Route::get('/cv/add_education_widget', 'User\ProfileController@add_education_widget')->name('cv.add_education_widget');
        Route::post('/cv/edit_education_widget', 'User\ProfileController@edit_education_widget');
        Route::post('/cv/delete_education_widget', 'User\ProfileController@delete_education_widget');
        Route::get('/cv/add_certificate_widget', 'User\ProfileController@add_certificate_widget')->name('cv.add_certificate_widget');
        Route::post('/cv/edit_certificate_widget', 'User\ProfileController@edit_certificate_widget');
        Route::post('/cv/delete_certificate_widget', 'User\ProfileController@delete_certificate_widget');
        Route::get('/cv/add_language_widget', 'User\ProfileController@add_language_widget')->name('cv.add_language_widget');
        Route::post('/cv/edit_language_widget', 'User\ProfileController@edit_language_widget');
        Route::post('/cv/delete_language_widget', 'User\ProfileController@delete_language_widget');
        Route::get('/cv/add_skill_widget', 'User\ProfileController@add_skill_widget')->name('cv.add_skill_widget');
        Route::post('/cv/edit_skill_widget', 'User\ProfileController@edit_skill_widget');
        Route::post('/cv/delete_skill_widget', 'User\ProfileController@delete_skill_widget');
        Route::get('/cv/add_hobby_widget', 'User\ProfileController@add_hobby_widget')->name('cv.add_hobby_widget');
        Route::post('/cv/edit_hobby_widget', 'User\ProfileController@edit_hobby_widget');
        Route::post('/cv/delete_hobby_widget', 'User\ProfileController@delete_hobby_widget');
        Route::post('/cv/edit_role_description', 'User\ProfileController@edit_role_description');
        Route::post('/cv/edit_description', 'User\ProfileController@edit_description');
        Route::get('/cv/change_cv_status/{status}', 'User\ProfileController@change_cv_status')->name('cv.change_status');
    });

});

Auth::routes();


Route::group(['middleware' => ['auth', 'internal_user'], 'prefix' => 'backend', 'namespace' => 'Backend'], function () {

    /* INTERNAL USERS ROUTES */

    Route::get('dashboard', 'DashboardController@index')->name('backend.dashboard');
    Route::get('/users/account/{subnav?}', 'UserAccountController@account')->name('backend.account');
    Route::post('/users/account', 'UserAccountController@update_account')->name('backend.update_account');
    Route::post('/users/account/avatar', 'UserAccountController@update_account_avatar')->name('backend.update_account_avatar');

    Route::get('/company/list', 'CompanyManagementController@index')->name('company_management.index');
    Route::get('/company/create', 'CompanyManagementController@create_company')->name('company_management.create');
    Route::post('/company/store', 'CompanyManagementController@store')->name('company_management.store');
    Route::get('/company/show/{company}', 'CompanyManagementController@show')->name('company_management.show');
    Route::get('/company/send_mail/{company}', 'CompanyManagementController@send_mail')->name('company_management.send_mail');
    Route::get('/company/update_status/{company}/{status}', 'CompanyManagementController@update_status')->name('company_management.update_status');

    Route::get('/inbox', 'InboxController@index')->name('backend.inbox');
    Route::post('/inbox/mark_all_as_read', 'InboxController@mark_all_as_read')->name('backend.inbox.mark_all_as_read');
    Route::post('/inbox/mark_as_read', 'InboxController@mark_as_read')->name('backend.inbox.mark_as_read');
    Route::post('/inbox/delete', 'InboxController@delete')->name('backend.inbox.delete');

    Route::get('/registered', 'UserController@registered')->name('backend.registered');

    Route::resources([
        'roles' => 'RoleController',
        'users' => 'UserController',
        'company_classifications' => 'CompanyClassificationController',
        'company_categories' => 'CompanyCategoryController',
        'company_statuses' => 'CompanyStatusController',
        'company_employees_count' => 'CompanyEmployeeCountController',
        'features' => 'FeatureController',
        'packages' => 'PackageController',
        'news' => 'NewsController',
        'user_titles' => 'UserTitleController',
        'coupons' => 'CouponController',
        'sell_coupons' => 'SellCouponController',
    ]);
});

Route::group(['middleware' => ['auth', 'company'], 'prefix' => 'company', 'namespace' => 'CompanyManagement'], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('company.dashboard');

    Route::get('/inbox', 'InboxController@index')->name('company.inbox');
    Route::post('/inbox/mark_all_as_read', 'InboxController@mark_all_as_read')->name('company.inbox.mark_all_as_read');
    Route::post('/inbox/mark_as_read', 'InboxController@mark_as_read')->name('company.inbox.mark_as_read');
    Route::post('/inbox/delete', 'InboxController@delete')->name('company.inbox.delete');

    Route::get('/create', 'CompanyController@create')->name('company.create');
    Route::get('/edit/{company}/{step}', 'CompanyController@edit')->name('company.edit');
    Route::get('/edit_data/{company}', 'CompanyController@edit_data')->name('company.edit_data');
    Route::get('/delete_page/{company}', 'CompanyController@delete_page')->name('company.delete_page');
    Route::get('/delete/{company}', 'CompanyController@delete')->name('company.delete');
    Route::get('/edit_logo/{company}', 'CompanyController@edit_logo')->name('company.edit_logo');
    Route::post('/store', 'CompanyController@store')->name('company.store');
    Route::post('/update/{company}', 'CompanyController@update')->name('company.update');
    Route::get('/index/{company}', 'CompanyController@index')->name('company.index');
    Route::post('/insert_company_logo/{company}', 'CompanyController@insert_company_logo')->name('company.insert_company_logo');
    Route::get('/subscription/{company}/{package}', 'CompanySubscriptionController@subscription')->name('company.subscription');
    Route::post('/subscription/store/invoice/{company}/{package}', 'CompanySubscriptionController@store_subscription_invoice')->name('company.subscription.store.invoice');
    Route::post('/subscription/store/card/{company}/{package}', 'CompanySubscriptionController@store_subscription_card')->name('company.subscription.store.card');
    Route::get('/download/invoice/{invoice}', 'CompanySubscriptionController@download_invoice')->name('company.invoice.download');


    Route::get('/roles/{company}', 'CompanyRoleController@index')->name('company.roles.index');
    Route::get('/roles/{company}/create', 'CompanyRoleController@create')->name('company.roles.create');
    Route::get('/roles/{company}/edit/{company_sub_role}', 'CompanyRoleController@edit')->name('company.roles.edit');
    Route::post('/roles/{company}/store', 'CompanyRoleController@store')->name('company.roles.store');
    Route::post('/roles/{company}/update/{company_sub_role}', 'CompanyRoleController@update')->name('company.roles.update');
    Route::delete('/roles/{company}/delete/{company_sub_role}', 'CompanyRoleController@delete')->name('company.roles.delete');

    Route::get('/users/{company}', 'CompanyUserController@index')->name('company.users.index');
    Route::get('/users/{company}/create', 'CompanyUserController@create')->name('company.users.create');
    Route::get('/users/{company}/edit/{user}', 'CompanyUserController@edit')->name('company.users.edit');
    Route::post('/users/{company}/store', 'CompanyUserController@store')->name('company.users.store');
    Route::post('/users/{company}/update/{user}', 'CompanyUserController@update')->name('company.users.update');
    Route::delete('/users/{company}/delete/{user}', 'CompanyUserController@delete')->name('company.users.delete');

    Route::get('/presentation/{company}', 'CompanyPresentationController@index')->name('company.presentation');
    Route::get('/presentation/{company}/create', 'CompanyPresentationController@create')->name('company.presentation.create');
    Route::get('/presentation/{company}/edit/{page}', 'CompanyPresentationController@edit')->name('company.presentation.edit');
    Route::post('/presentation/{company}/update/{page}', 'CompanyPresentationController@update')->name('company.presentation.update');
    Route::post('/presentation/{company}/store', 'CompanyPresentationController@store')->name('company.presentation.store');
    Route::delete('/presentation/{company}/delete/{page}', 'CompanyPresentationController@delete')->name('company.presentation.delete');
    Route::get('/presentation/{company}/change_status/{page}', 'CompanyPresentationController@change_status')->name('company.presentation.change_status');
    Route::get('/presentation/{company}/content/{page}', 'CompanyPresentationController@content')->name('company.presentation.content');
    Route::post('/presentation/{company}/save_content/{page}', 'CompanyPresentationController@save_content')->name('company.presentation.save_content');
    Route::post('/presentation/{company}/update_content/{page}', 'CompanyPresentationController@update_content')->name('company.presentation.update_content');

    Route::get('/reservations/{company}', 'CompanyReservationController@index')->name('company.reservations.index');
    Route::get('/reservations/{company}/{location}', 'CompanyReservationController@bookings')->name('company.reservations.bookings');
    Route::get('/reservations/{company}/{location}/{booking}', 'CompanyReservationController@booking')->name('company.reservations.booking');
    Route::get('/reservations/{company}/{location}/{booking}/{table}', 'CompanyReservationController@assign_table')->name('company.reservations.assign_table');
    Route::get('/reject_booking/{company}/{location}/{booking}', 'CompanyReservationController@reject_booking')->name('company.reservations.reject_booking');
    Route::post('/date_bookings/{company}/{location}', 'CompanyReservationController@date_bookings')->name('company.reservations.date_bookings');


    Route::get('/hr/{company}', 'HrController@index')->name('company.hr.index');
    Route::get('/hr/{company}/create', 'HrController@create')->name('company.hr.create');
    Route::get('/hr/{company}/edit/{job_offer}', 'HrController@edit')->name('company.hr.edit');
    Route::post('/hr/{company}/store', 'HrController@store')->name('company.hr.store');
    Route::post('/hr/{company}/update/{job_offer}', 'HrController@update')->name('company.hr.update');
    Route::delete('/hr/{company}/delete/{job_offer}', 'HrController@delete')->name('company.hr.delete');
    Route::get('/hr/{company}/people/{job_offer}', 'HrController@people')->name('company.hr.people');
    Route::get('/hr/{company}/people/{job_offer}/detail/{user}', 'HrController@people_detail')->name('company.hr.people_detail');

    Route::get('/events/{company}', 'CompanyEventController@index')->name('company.events.index');
    Route::get('/events/{company}/create', 'CompanyEventController@create')->name('company.events.create');
    Route::get('/events/{company}/edit/{event}', 'CompanyEventController@edit')->name('company.events.edit');
    Route::get('/events/participants/{company}/edit/{event}', 'CompanyEventController@participants')->name('company.events.participants');
    Route::post('/events/{company}/store', 'CompanyEventController@store')->name('company.events.store');
    Route::post('/events/{company}/update/{event}', 'CompanyEventController@update')->name('company.events.update');
    Route::delete('/events/{company}/delete/{event}', 'CompanyEventController@delete')->name('company.events.delete');

    Route::get('/locations/{company}', 'CompanyLocationController@index')->name('company.locations.index');
    Route::get('/locations/{company}/create', 'CompanyLocationController@create')->name('company.locations.create');
    Route::get('/locations/{company}/edit/{location}', 'CompanyLocationController@edit')->name('company.locations.edit');
    Route::post('/locations/{company}/store', 'CompanyLocationController@store')->name('company.locations.store');
    Route::post('/locations/{company}/update/{location}', 'CompanyLocationController@update')->name('company.locations.update');
    Route::delete('/locations/{company}/delete/{location}', 'CompanyLocationController@delete')->name('company.locations.delete');

    Route::get('/locations/{company}/{location}/rooms', 'CompanyLocationRoomController@index')->name('company.locations.rooms.index');
    Route::get('/locations/{company}/{location}/rooms/create', 'CompanyLocationRoomController@create')->name('company.locations.rooms.create');
    Route::get('/locations/{company}/{location}/rooms/edit/{room}', 'CompanyLocationRoomController@edit')->name('company.locations.rooms.edit');
    Route::post('/locations/{company}/{location}/rooms/store', 'CompanyLocationRoomController@store')->name('company.locations.rooms.store');
    Route::post('/locations/{company}/{location}/rooms/update/{room}', 'CompanyLocationRoomController@update')->name('company.locations.rooms.update');
    Route::delete('/locations/{company}/{location}/rooms/delete/{room}', 'CompanyLocationRoomController@delete')->name('company.locations.rooms.delete');

    Route::get('/locations/{company}/{location}/{room}/tables', 'CompanyLocationTableController@index')->name('company.locations.tables.index');
    Route::get('/locations/{company}/{location}/{room}/tables/create', 'CompanyLocationTableController@create')->name('company.locations.tables.create');
    Route::get('/locations/{company}/{location}/{room}/tables/edit/{table}', 'CompanyLocationTableController@edit')->name('company.locations.tables.edit');
    Route::post('/locations/{company}/{location}/{room}/tables/store', 'CompanyLocationTableController@store')->name('company.locations.tables.store');
    Route::post('/locations/{company}/{location}/{room}/tables/update/{table}', 'CompanyLocationTableController@update')->name('company.locations.tables.update');
    Route::delete('/locations/{company}/{location}/{room}/tables/delete/{table}', 'CompanyLocationTableController@delete')->name('company.locations.tables.delete');

    Route::get('/restaurants/{company}', 'CompanyRestaurantController@index')->name('company.restaurants.index');
    Route::get('/restaurants/{company}/info/{location}', 'CompanyRestaurantController@info')->name('company.restaurants.info');
    Route::post('/restaurants/{company}/info/{location}/store', 'CompanyRestaurantController@info_store')->name('company.restaurants.info.store');
    Route::get('/restaurants/{company}/schedule/{location}', 'CompanyRestaurantController@schedule')->name('company.restaurants.schedule');
    Route::post('/restaurants/{company}/schedule/store/{location}', 'CompanyRestaurantController@schedule_store')->name('company.restaurants.schedule.store');
});
