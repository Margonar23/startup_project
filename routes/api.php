<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::post('/login', 'Auth\AuthController@login');
    Route::post('/register', 'Auth\AuthController@register_user');

    Route::post('/packages', 'PackageController@get_packages');
    Route::post('/features', 'FeatureController@get_features');
    Route::post('/companies', 'CompanyController@companies');
    Route::post('/ratings', 'CompanyController@ratings');
    Route::post('/rate_company', 'CompanyController@rate_company');
    Route::post('/follow_company', 'CompanyController@follow_company');
    Route::post('/unfollow_company', 'CompanyController@unfollow_company');
    Route::post('/company/{company}', 'CompanyController@company');
    Route::post('/company/{company}/edit', 'CompanyController@edit');
    Route::post('/company/{company}/edit_logo', 'CompanyController@edit_logo');
    Route::post('/companies/categories', 'CompanyController@company_categories');
    Route::post('/companies/districts', 'CompanyController@company_districts');
    Route::post('/companies/classifications', 'CompanyController@company_classifications');
    Route::post('/companies/filter', 'CompanyController@companies_filter');
    Route::post('/events', 'EventController@events');
    Route::post('/events/{event}', 'EventController@event');
    Route::post('/events/participation/{event}/{user}', 'EventController@participation');
    Route::post('/events/delete_participation/{event}/{user}', 'EventController@delete_participation');
    Route::post('/users/{user}', 'UserCvController@user');
    Route::post('/professions', 'UserCvController@get_professions');
    Route::post('/permits', 'UserCvController@get_permits');
    Route::post('/percentages', 'UserCvController@get_percentages');
    Route::post('/employments', 'UserCvController@get_employments');
    Route::post('/skills', 'UserCvController@get_skills');
    Route::post('/licenses', 'UserCvController@get_licenses');
    Route::post('/languages', 'UserCvController@get_languages');
    Route::post('/levels', 'UserCvController@get_levels');
    Route::post('/user_profession', 'UserCvController@store_user_profession');
    Route::post('/user_permit', 'UserCvController@store_user_permit');
    Route::post('/user_percentage', 'UserCvController@store_user_percentage');
    Route::post('/user_employment', 'UserCvController@store_user_employment');
    Route::post('/user_skills', 'UserCvController@store_user_skills');
    Route::post('/user_licenses', 'UserCvController@store_user_licenses');
    Route::post('/user_languages', 'UserCvController@store_user_languages');
    Route::post('/user_hobbies', 'UserCvController@store_user_hobbies');
    Route::post('/user_certificates', 'UserCvController@store_user_certificate');
    Route::post('/user_certificate_delete', 'UserCvController@delete_user_certificate');
    Route::post('/user_diplomas', 'UserCvController@store_user_diploma');
    Route::post('/user_diploma_delete', 'UserCvController@delete_user_diploma');
    Route::post('/user_job_experiences', 'UserCvController@store_user_job_experience');
    Route::post('/user_job_experience_delete', 'UserCvController@delete_user_job_experience');
    Route::post('/looking_for_new_job', 'UserCvController@store_user_looking_for_new_job');
    Route::post('/user_cv_upload/{user}', 'UserCvController@user_cv_upload');
    Route::post('/tmp', 'UserCvController@tmp');
    Route::post('/alimentaries', 'ReservationController@alimentaries');
    Route::post('/restaurant_types', 'ReservationController@restaurant_types');
    Route::post('/kitchens', 'ReservationController@kitchens');
    Route::post('/districts', 'ReservationController@districts');
    Route::post('/get_restaurants', 'ReservationController@get_restaurants');
    Route::post('/get_room/{room}', 'ReservationController@get_room');
    Route::post('/get_restaurant/{location}', 'ReservationController@get_restaurant');
    Route::post('/get_reservation_status', 'ReservationController@get_reservation_status');
    Route::post('/get_bookings/{status}/{location}', 'ReservationController@get_bookings');
    Route::post('/get_hours/{restaurant}/{date}', 'ReservationController@get_hours');
    Route::post('/get_page_content/{page}', 'PresentationController@get_content');
    Route::post('/save_page_content/{page}', 'PresentationController@save_content');
    Route::post('/get_coupon_companies', 'CouponController@get_coupon_companies');
    Route::post('/get_coupon_categories', 'CouponController@get_coupon_categories');
    Route::post('/get_coupon_districts', 'CouponController@get_coupon_districts');
    Route::post('/assign_table', 'ReservationController@assign_table');
    Route::post('/get_tables', 'ReservationController@get_tables');
    Route::post('/reject_reservation', 'ReservationController@reject_reservation');

    Route::middleware('auth:api')->group(function () {
        Route::get('/logout', 'Auth\AuthController@logout');
        Route::get('/test', 'TestController@test_auth');
    });
});
